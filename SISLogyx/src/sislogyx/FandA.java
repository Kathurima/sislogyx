/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import com.teknikindustries.bulksms.SMS;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public class FandA extends javax.swing.JFrame {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    
    byte[] pupil_image = null;
    
    int day, month, year, second, minute, hour;
    
    String classlevel = "";
    
    
    DefaultTableModel feebal = new DefaultTableModel();
    
    DefaultTableModel lvlfeebal = new DefaultTableModel();
    
    DefaultTableModel msglist = new DefaultTableModel();

    /**
     * Creates new form Finance
     * @throws java.sql.SQLException
     */
    public FandA() throws SQLException {
        super("Finance and Accounts");
        
        initComponents();
        
        conn = (Connection) DBConnect.connect();
        
        falblloggedinasusercode.setText(String.valueOf(Emp.empId));
        
        Current_Date();
        
        tblmsglist.setModel(messageList());
        
        //Admin's
        String admin = falblloggedinasusercode.getText();
        
        if (admin.contains("ADM")) {
            jbtnBack.setVisible(true);
        } else {
            jbtnBack.setVisible(false);
        }
        
    }

    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    day = cal.get(Calendar.DAY_OF_MONTH);
                    month = cal.get(Calendar.MONTH);
                    year = cal.get(Calendar.YEAR);

                    txtfapaydate.setText(year+"/"+(month+1)+"/"+day);
                    txtfasalarymonth.setText(String.valueOf(month+1)+"/"+year);
                    
                    second = cal.get(Calendar.SECOND);
                    minute = cal.get(Calendar.MINUTE);
                    hour = cal.get(Calendar.HOUR_OF_DAY);

                    jlblloggedintime.setText(hour+":"+minute+":"+second);
                    jlblloggedintime.setForeground(Color.red);
                }
            }
        };
        clock.start();
   }
    
    private void receiptNo(){
        int num;
        String frcpt = "FP/";
        num = 5895 + (int) (Math.random()*150);
        frcpt += num + 5;
        txtfareceiptno.setText(frcpt+"/"+year);
    }
    
    private DefaultTableModel FeesBalance(){
        feebal.addColumn("Admission No.");
        feebal.addColumn("Pupil's Name");
        feebal.addColumn("Class");
        feebal.addColumn("Level");
        feebal.addColumn("Paid");
        feebal.addColumn("Balance");
        
        try{
            String sql = "SELECT * FROM `fees_payment` WHERE `balance` > 0 OR `class_level`=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, cbofaclvlreports.getSelectedItem().toString());
            rs = pst.executeQuery();

            while (rs.next()) {
                String admno = rs.getString("adm_no");
                String pupilsname = rs.getString("name");
                String pclass = rs.getString("class");
                String pclasslvl = rs.getString("class_level");
                String amntpaid = rs.getString("total_paid");
                String bala = rs.getString("balance");
                
                String[] rowdata = {admno, pupilsname, pclass, pclasslvl, amntpaid, bala};
                feebal.addRow(rowdata);
            }
            
            return feebal;
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        return null;
    }
    
    private DefaultTableModel SearchClassLevelFeesBalance(){
        lvlfeebal.addColumn("Admission No.");
        lvlfeebal.addColumn("Pupil's Name");
        lvlfeebal.addColumn("Class");
        lvlfeebal.addColumn("Level");
        lvlfeebal.addColumn("Paid");
        lvlfeebal.addColumn("Balance");
        
        try{
            String sql = "SELECT * FROM `fees_payment` WHERE `balance` > 0 AND `class_level`=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, cbofaclvlreports.getSelectedItem().toString());
            rs = pst.executeQuery();

            while (rs.next()) {
                String admno = rs.getString("adm_no");
                String pupilsname = rs.getString("name");
                String pclass = rs.getString("class");
                String pclasslvl = rs.getString("class_level");
                String amntpaid = rs.getString("total_paid");
                String bala = rs.getString("balance");
                
                String[] rowdata = {admno, pupilsname, pclass, pclasslvl, amntpaid, bala};
                lvlfeebal.addRow(rowdata);
            }
            
            return lvlfeebal;
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        return null;
    }
    
    private DefaultTableModel messageList(){
        //msglist.addColumn("Message Type");
        msglist.addColumn("Adm no.");
        //msglist.addColumn("Email Recipient");
        msglist.addColumn("From");
        msglist.addColumn("To");
        msglist.addColumn("Message");
        
        try {
            String sql = "SELECT * FROM `messages` WHERE `m_from`=\"Finance and Accounts\" ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                //String msgtype = rs.getString("m_type");
                String padmno = rs.getString("padmno");
                String mfrom = rs.getString("m_from");
                String mto = rs.getString("m_to");
                //String msbjct = rs.getString("m_sbjct");
                String m_msg = rs.getString("m_msg");
                
                String[] rowdata = {padmno, mfrom, mto, m_msg};
                msglist.addRow(rowdata);
            }
            
            return msglist;
            
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel19 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtfapadmnosearch = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtfapname = new javax.swing.JTextField();
        txtfapadmno = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtfapclass = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtfapcurrterm = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        pupilprofileimg = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        cbopymntmode = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtfaamntpaid = new javax.swing.JTextField();
        txtfareceiptno = new javax.swing.JTextField();
        txtfabranch = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        cbofaterm = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        txtfapaydate = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtfatransactionno = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtfabalance = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtfatermfees = new javax.swing.JTextField();
        jPanel21 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtfafeereceipt = new javax.swing.JTextArea();
        btnFAClear = new javax.swing.JButton();
        btnFAFeesSave = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblfaballist = new javax.swing.JTable();
        btnFASearch = new javax.swing.JButton();
        cbofaclvlreports = new javax.swing.JComboBox<>();
        btnFAShowList = new javax.swing.JButton();
        btnFAGenFeeStmnt = new javax.swing.JButton();
        btnFAGenFeeStruct = new javax.swing.JButton();
        btnFAGenFeeBalXLSSheet = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        txtfapaytype = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtfapayslipno = new javax.swing.JTextField();
        txtfabasicsalary = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        txtfatax = new javax.swing.JTextField();
        txtfanhif = new javax.swing.JTextField();
        txtfanssf = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtfamiscdeductions = new javax.swing.JTextField();
        txtfadeductionstotal = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtfapayslip = new javax.swing.JTextArea();
        jPanel14 = new javax.swing.JPanel();
        btnNetPay = new javax.swing.JButton();
        txtfanetpay = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        btnfaSave = new javax.swing.JButton();
        btnfaPrint = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        txtfaadditionalpymntstotal = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtfaovertime = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtfatrainingamnt = new javax.swing.JTextField();
        txtfabonus = new javax.swing.JTextField();
        txtfamiscpay = new javax.swing.JTextField();
        btnClearPayments = new javax.swing.JButton();
        txtfasalarymonth = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        txtfaempname = new javax.swing.JTextField();
        txtfaempidsearch = new javax.swing.JTextField();
        txtfadept = new javax.swing.JTextField();
        txtfaworkid = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        cbofaempworkcat = new javax.swing.JComboBox<>();
        jPanel9 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtsm6 = new javax.swing.JTextArea();
        btnsmCancel = new javax.swing.JButton();
        txtsmSend = new javax.swing.JButton();
        txtsm4 = new javax.swing.JTextField();
        lblmsg = new javax.swing.JLabel();
        txtsm3 = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblmsglist = new javax.swing.JTable();
        rbparent = new javax.swing.JRadioButton();
        rbts = new javax.swing.JRadioButton();
        rbnt = new javax.swing.JRadioButton();
        txtsm5 = new javax.swing.JTextField();
        lblmsg1 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        btnLibChangePssd = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        btnFnALogout = new javax.swing.JButton();
        jbtnBack = new javax.swing.JButton();
        falblloggedinasusercode = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jlblloggedintime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));

        jPanel5.setBackground(new java.awt.Color(102, 0, 0));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("FINANCE AND ACCOUNTS");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jTabbedPane2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pupil's Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Search Admission No.");

        txtfapadmnosearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfapadmnosearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfapadmnosearchKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Name");

        txtfapname.setEditable(false);

        txtfapadmno.setEditable(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Admission No");

        txtfapclass.setEditable(false);
        txtfapclass.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfapclassMouseClicked(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Class");

        txtfapcurrterm.setEditable(false);

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Term");

        jDesktopPane1.setLayer(pupilprofileimg, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pupilprofileimg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pupilprofileimg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtfapadmnosearch, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                                .addComponent(txtfapadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel41)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfapcurrterm, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                                .addComponent(txtfapname, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtfapclass, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jDesktopPane1)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtfapadmnosearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtfapclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(txtfapname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtfapcurrterm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel41))
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtfapadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jDesktopPane1)
                .addContainerGap())
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payments", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        cbopymntmode.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbopymntmode.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mode of Payment", "Bank Deposit", "Mpesa", "Cheque" }));
        cbopymntmode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbopymntmodeActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Receipt No.");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Bank Branch / Agent ");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Amount Paid");

        txtfaamntpaid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfaamntpaid.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfaamntpaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfaamntpaidKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfaamntpaidKeyReleased(evt);
            }
        });

        txtfareceiptno.setEditable(false);
        txtfareceiptno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtfabranch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfabranch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfabranchKeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Term");

        cbofaterm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbofaterm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Term", "Term 1", "Term 2", "Term 3" }));
        cbofaterm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbofatermActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Date");

        txtfapaydate.setEditable(false);
        txtfapaydate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Cheque Code / Transaction No. ");

        txtfatransactionno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfatransactionno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfatransactionnoKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Balance");

        txtfabalance.setEditable(false);
        txtfabalance.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfabalance.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfabalance.setText("0.00");
        txtfabalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfabalanceActionPerformed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setText("Term Fees ");

        txtfatermfees.setEditable(false);
        txtfatermfees.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfatermfees.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfatermfees.setText("0.00");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtfareceiptno)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(txtfabranch, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(91, 91, 91)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(txtfapaydate, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(txtfatransactionno, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbofaterm, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfatermfees))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfaamntpaid, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txtfabalance, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(77, 77, 77))
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(212, 212, 212)
                .addComponent(cbopymntmode, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(cbopymntmode, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel12))
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfareceiptno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel9))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(txtfapaydate, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel13)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(txtfabranch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cbofaterm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel31)
                                    .addComponent(txtfatermfees, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtfatransactionno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtfaamntpaid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel10)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfabalance, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 23, Short.MAX_VALUE))
        );

        jPanel21.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Receipt", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtfafeereceipt.setEditable(false);
        txtfafeereceipt.setColumns(20);
        txtfafeereceipt.setRows(5);
        jScrollPane1.setViewportView(txtfafeereceipt);

        btnFAClear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFAClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnFAClear.setText("CLEAR");
        btnFAClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFAClearActionPerformed(evt);
            }
        });

        btnFAFeesSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFAFeesSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btnFAFeesSave.setText("SAVE");
        btnFAFeesSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFAFeesSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addComponent(btnFAFeesSave, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFAClear, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFAFeesSave, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFAClear, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31)
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(77, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 582, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Payments", jPanel19);

        jPanel22.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        tblfaballist.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        tblfaballist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblfaballist);

        btnFASearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFASearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search.png"))); // NOI18N
        btnFASearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFASearchActionPerformed(evt);
            }
        });

        cbofaclvlreports.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbofaclvlreports.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select class level", "Pre-primary", "Lower Primary", "Upper Primary" }));
        cbofaclvlreports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbofaclvlreportsActionPerformed(evt);
            }
        });

        btnFAShowList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFAShowList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnFAShowList.setText("SHOW LIST");
        btnFAShowList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFAShowListActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1018, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addGap(224, 224, 224)
                .addComponent(btnFAShowList, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbofaclvlreports, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFASearch, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnFASearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbofaclvlreports, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnFAShowList, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnFAGenFeeStmnt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFAGenFeeStmnt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnFAGenFeeStmnt.setText("GENERATE FEES STATEMENT");
        btnFAGenFeeStmnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFAGenFeeStmntActionPerformed(evt);
            }
        });

        btnFAGenFeeStruct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFAGenFeeStruct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnFAGenFeeStruct.setText("GENERATE FEES STRUCTURE");
        btnFAGenFeeStruct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFAGenFeeStructActionPerformed(evt);
            }
        });

        btnFAGenFeeBalXLSSheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFAGenFeeBalXLSSheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnFAGenFeeBalXLSSheet.setText("GENERATE FEES BALANCE LIST");
        btnFAGenFeeBalXLSSheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFAGenFeeBalXLSSheetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnFAGenFeeStmnt)
                    .addComponent(btnFAGenFeeStruct)
                    .addComponent(btnFAGenFeeBalXLSSheet))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(btnFAGenFeeStmnt, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(btnFAGenFeeStruct, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(btnFAGenFeeBalXLSSheet, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Reports", jPanel20);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1306, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 616, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 3, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("FEES", jPanel3);

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payroll Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N
        jPanel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtfapaytype.setEditable(false);
        txtfapaytype.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfapaytype.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfapaytypeActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("P F No.");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Payment Type");

        txtfapayslipno.setEditable(false);
        txtfapayslipno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtfabasicsalary.setEditable(false);
        txtfabasicsalary.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfabasicsalary.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfabasicsalary.setText("0.00");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Salary Month");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Basic Salary(PM)");

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Deductions", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel26.setText("Tax");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setText("NSSF");

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel28.setText("NHIF");

        txtfatax.setEditable(false);
        txtfatax.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfatax.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfatax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfataxKeyReleased(evt);
            }
        });

        txtfanhif.setEditable(false);
        txtfanhif.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfanhif.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfanhif.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfanhifKeyReleased(evt);
            }
        });

        txtfanssf.setEditable(false);
        txtfanssf.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfanssf.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfanssf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfanssfKeyReleased(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel29.setText("TOTAL");

        txtfamiscdeductions.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfamiscdeductions.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfamiscdeductions.setText("0");
        txtfamiscdeductions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfamiscdeductionsMouseClicked(evt);
            }
        });
        txtfamiscdeductions.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfamiscdeductionsKeyReleased(evt);
            }
        });

        txtfadeductionstotal.setEditable(false);
        txtfadeductionstotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtfadeductionstotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfadeductionstotal.setText("0.00");

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setText("Misc. Deductions");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28)
                    .addComponent(jLabel35)
                    .addComponent(jLabel27)
                    .addComponent(jLabel26)
                    .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtfanhif, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfamiscdeductions, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfanssf, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfadeductionstotal, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(39, Short.MAX_VALUE))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(txtfatax, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfatax, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfanssf, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfanhif, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfamiscdeductions, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfadeductionstotal, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)))
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payslip", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtfapayslip.setEditable(false);
        txtfapayslip.setColumns(20);
        txtfapayslip.setRows(5);
        jScrollPane2.setViewportView(txtfapayslip);

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Net Pay", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        btnNetPay.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnNetPay.setText("NET PAY");
        btnNetPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNetPayActionPerformed(evt);
            }
        });

        txtfanetpay.setEditable(false);
        txtfanetpay.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtfanetpay.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfanetpay.setText("0.00");

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel30.setText("KSh");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addGap(18, 18, 18)
                        .addComponent(txtfanetpay, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(btnNetPay)
                        .addGap(89, 89, 89))))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfanetpay, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNetPay))
        );

        btnfaSave.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnfaSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnfaSave.setText("SAVE");
        btnfaSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfaSaveActionPerformed(evt);
            }
        });

        btnfaPrint.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnfaPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/print_sml.png"))); // NOI18N
        btnfaPrint.setText("Print");
        btnfaPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfaPrintActionPerformed(evt);
            }
        });

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Additional Payments", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtfaadditionalpymntstotal.setEditable(false);
        txtfaadditionalpymntstotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtfaadditionalpymntstotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfaadditionalpymntstotal.setText("0.00");

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setText("Bonus/Appreciation");

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Training Amount");

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("Overtime Benefits");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("Misc. Payment");

        txtfaovertime.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfaovertime.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfaovertime.setText("0");
        txtfaovertime.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfaovertimeMouseClicked(evt);
            }
        });
        txtfaovertime.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfaovertimeKeyReleased(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel25.setText("TOTAL");

        txtfatrainingamnt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfatrainingamnt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfatrainingamnt.setText("0");
        txtfatrainingamnt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfatrainingamntMouseClicked(evt);
            }
        });
        txtfatrainingamnt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfatrainingamntKeyReleased(evt);
            }
        });

        txtfabonus.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfabonus.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfabonus.setText("0");
        txtfabonus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfabonusMouseClicked(evt);
            }
        });
        txtfabonus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfabonusKeyReleased(evt);
            }
        });

        txtfamiscpay.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfamiscpay.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfamiscpay.setText("0");
        txtfamiscpay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfamiscpayMouseClicked(evt);
            }
        });
        txtfamiscpay.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfamiscpayKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(jLabel24))
                        .addGap(40, 40, 40)
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtfamiscpay, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                            .addComponent(txtfaadditionalpymntstotal)))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addComponent(jLabel22)
                            .addComponent(jLabel21))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtfabonus, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfaovertime, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfatrainingamnt, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(126, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txtfaovertime, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txtfatrainingamnt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtfabonus, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txtfamiscpay, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfaadditionalpymntstotal, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addGap(56, 56, 56))
        );

        btnClearPayments.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnClearPayments.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnClearPayments.setText("Clear");
        btnClearPayments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearPaymentsActionPerformed(evt);
            }
        });

        txtfasalarymonth.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtfapayslipno, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtfapaytype)))
                        .addGap(69, 69, 69)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtfasalarymonth)
                            .addComponent(txtfabasicsalary, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(btnfaSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnfaPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClearPayments, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26))))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel15)
                                    .addComponent(txtfapayslipno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtfasalarymonth, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel17))
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel16)
                                        .addComponent(txtfapaytype, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel18)
                                    .addComponent(txtfabasicsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(38, 38, 38)))
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnfaPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnfaSave, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnClearPayments, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(63, Short.MAX_VALUE))
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Employee Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel32.setText("Search Work ID ");

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setText("Employee Name ");

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("Department ");

        txtfaempname.setEditable(false);
        txtfaempname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtfaempidsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtfaempidsearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfaempidsearchMouseClicked(evt);
            }
        });
        txtfaempidsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfaempidsearchKeyReleased(evt);
            }
        });

        txtfadept.setEditable(false);
        txtfadept.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtfaworkid.setEditable(false);
        txtfaworkid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel42.setText("Employee Work ID");

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel39.setText("Work Category ");

        cbofaempworkcat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbofaempworkcat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Work Category", "Teaching", "Non-Teaching" }));
        cbofaempworkcat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbofaempworkcatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel39)
                        .addGap(18, 18, 18)
                        .addComponent(cbofaempworkcat, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfaworkid, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(29, 29, 29)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel32)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfaempidsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtfadept, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel33)
                        .addGap(18, 18, 18)
                        .addComponent(txtfaempname)))
                .addGap(41, 41, 41))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtfaempidsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39)
                    .addComponent(cbofaempworkcat, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel33)
                        .addComponent(txtfaempname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel42)
                        .addComponent(txtfaworkid, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel34)
                        .addComponent(txtfadept, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(48, 48, 48))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("PAYMENTS", jPanel4);

        jPanel34.setBackground(new java.awt.Color(153, 153, 255));
        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Send Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel40.setText("Phone Number");

        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel43.setText("Message");

        txtsm6.setColumns(20);
        txtsm6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm6.setRows(5);
        jScrollPane7.setViewportView(txtsm6);

        btnsmCancel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnsmCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/cancel.png"))); // NOI18N
        btnsmCancel.setText("Cancel");
        btnsmCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsmCancelActionPerformed(evt);
            }
        });

        txtsmSend.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsmSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/send.png"))); // NOI18N
        txtsmSend.setText("SEND");
        txtsmSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsmSendActionPerformed(evt);
            }
        });

        txtsm4.setEditable(false);
        txtsm4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm4KeyReleased(evt);
            }
        });

        lblmsg.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg.setText("Pupils Adm No.");

        txtsm3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm3KeyReleased(evt);
            }
        });

        tblmsglist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane8.setViewportView(tblmsglist);

        rbparent.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbparent.setText("Parent");
        rbparent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbparentActionPerformed(evt);
            }
        });

        rbts.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbts.setText("Teaching Staff");
        rbts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtsActionPerformed(evt);
            }
        });

        rbnt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbnt.setText("Non-Teaching Staff");
        rbnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbntActionPerformed(evt);
            }
        });

        txtsm5.setEditable(false);
        txtsm5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm5KeyReleased(evt);
            }
        });

        lblmsg1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg1.setText("Name");

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnsmCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(144, 144, 144))
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8)
                .addContainerGap())
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblmsg)
                            .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblmsg1)
                            .addComponent(txtsm5, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(rbparent, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(68, 68, 68)
                        .addComponent(rbts, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(rbnt))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtsm4, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(267, 267, 267)
                        .addComponent(jLabel43))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbparent)
                            .addComponent(rbts)
                            .addComponent(rbnt))
                        .addGap(15, 15, 15)
                        .addComponent(lblmsg, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(lblmsg1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm5, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsm4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel43)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsmCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(324, 324, 324)
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(388, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("SEND MESSAGE", jPanel9);

        jPanel17.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnLibChangePssd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLibChangePssd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/changepssd.png"))); // NOI18N
        btnLibChangePssd.setText("CHANGE PASSWORD");
        btnLibChangePssd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLibChangePssdActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(204, 0, 0));
        jLabel20.setText("Click the button below to change your account credentials");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(btnLibChangePssd, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addGap(84, 84, 84))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLibChangePssd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(435, 435, 435)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(427, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(482, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("ACCOUNT MANAGEMENT", jPanel16);

        btnFnALogout.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnFnALogout.setText("LOGOUT");
        btnFnALogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFnALogoutActionPerformed(evt);
            }
        });

        jbtnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jbtnBack.setText("BACK");
        jbtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 888, Short.MAX_VALUE)
                        .addComponent(jbtnBack)
                        .addGap(33, 33, 33)
                        .addComponent(btnFnALogout))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnFnALogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jbtnBack))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 658, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        falblloggedinasusercode.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        falblloggedinasusercode.setText("currently logged in usercode");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Logged in as:");

        jlblloggedintime.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jlblloggedintime.setText("time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(falblloggedinasusercode)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlblloggedintime, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(falblloggedinasusercode)
                    .addComponent(jlblloggedintime))
                .addGap(35, 35, 35))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 754, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtfapaytypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfapaytypeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfapaytypeActionPerformed

    private void btnLibChangePssdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLibChangePssdActionPerformed
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            Change_Password cp = new Change_Password();
            cp.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnLibChangePssdActionPerformed

    private void btnFnALogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFnALogoutActionPerformed
        // TODO add your handling code here:
        try {
            try {
                String asql = "INSERT INTO `auditlog`(`empid`, `date`, `time`, `status`) VALUES (?,?,?,?)";
                pst = conn.prepareStatement(asql);
                Date currentDate = GregorianCalendar.getInstance().getTime();
                DateFormat df = DateFormat.getInstance();
                String dateString = df.format(currentDate);
                
                Date d = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String timeString = sdf.format(d);
                
                String time = timeString;
                String date = dateString;
                pst.setString(1, Emp.empId);
                pst.setString(2, date);
                pst.setString(3, time);
                pst.setString(4, "Logged out");
                pst.execute();
                
            } catch (SQLException e) {
            }
            
            this.dispose();
            Login l = new Login();
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnFnALogoutActionPerformed

    private void txtfaempidsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfaempidsearchKeyReleased
        // TODO add your handling code here:
        int empcat = cbofaempworkcat.getSelectedIndex();
        
        switch (empcat) {
            case 1:
                if (txtfaempidsearch.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Please select work categeory.");
                    clearPaymentFields();
                }   
                try {
                    String sql = "SELECT * FROM `teachers` WHERE `t_trid` = ? AND `status` = \"1\" ";
                    
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, txtfaempidsearch.getText());
                    rs = pst.executeQuery();
                    
                    while (rs.next()) {
                        txtfaworkid.setText(rs.getString("t_trid"));
                        txtfadept.setText(rs.getString("t_dept"));
                        txtfaempname.setText(rs.getString("t_name"));
                        txtfapayslipno.setText(rs.getString("t_payslip_no"));
                        txtfabasicsalary.setText(rs.getString("t_salary"));
                    }
                    
                    txtfapaytype.setText("Monthly");
                    
                } catch (SQLException ex) {
                    Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
                }   
                break;
                
            case 2:
                if (txtfaempidsearch.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Please select work categeory.");
                    clearPaymentFields();
                }   
                try {
                    String sql = "SELECT * FROM `otherstaff` WHERE `e_work_id` = ? AND `status` = \"1\" ";
                    
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, txtfaempidsearch.getText());
                    rs = pst.executeQuery();
                    
                    while (rs.next()) {
                        txtfaworkid.setText(rs.getString("e_work_id"));
                        txtfadept.setText(rs.getString("e_dept"));
                        txtfaempname.setText(rs.getString("ename"));
                        txtfapayslipno.setText(rs.getString("e_payslip_no"));
                        txtfabasicsalary.setText(rs.getString("e_salary"));
                    }
                    
                    txtfapaytype.setText("Monthly");
                    
                } catch (SQLException ex) {
                    Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
                }   
                break;
                
            default:
                JOptionPane.showMessageDialog(null, "Please select work categeory.");
                clearPaymentFields();
                break;
        }
        
    }//GEN-LAST:event_txtfaempidsearchKeyReleased

    private void txtfaempidsearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfaempidsearchMouseClicked
        // TODO add your handling code here:
        int empcat = cbofaempworkcat.getSelectedIndex();
        
        if (empcat == 0) {
            JOptionPane.showMessageDialog(null, "Please select work categeory.");
            txtfaempidsearch.setEditable(false);
        }else{
            txtfaempidsearch.setEditable(true);
        }
    }//GEN-LAST:event_txtfaempidsearchMouseClicked

    private void btnClearPaymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearPaymentsActionPerformed
        // TODO add your handling code here:
        clearPaymentFields();
    }//GEN-LAST:event_btnClearPaymentsActionPerformed

    private void btnNetPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNetPayActionPerformed
        // TODO add your handling code here:
        if (txtfaovertime.getText().matches("[0-9]+(.){0,1}[0-9]*") && txtfatrainingamnt.getText().matches("[0-9]+(.){0,1}[0-9]*") && txtfabonus.getText().matches("[0-9]+(.){0,1}[0-9]*")
                && txtfamiscpay.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            
            try {
                double ot = Double.parseDouble(txtfaovertime.getText());
                double ta = Double.parseDouble(txtfatrainingamnt.getText());
                double b_a = Double.parseDouble(txtfabonus.getText());
                double miscb = Double.parseDouble(txtfamiscpay.getText());

                double totalbenefits = ot + ta + b_a + miscb;
                
                txtfaadditionalpymntstotal.setText(String.valueOf(totalbenefits));
                
                if (ot <= 12298  ) {
                    //TAXATION WITHOUT OVERTIME
                    double bs = Double.parseDouble(txtfabasicsalary.getText());

                    if (bs <= 12298) {
                       double tax =  (0.1 * bs);
                       txtfatax.setText(String.valueOf(tax));
                    } else if (bs <= 23885) {
                        double tax =  (0.15 * bs);
                        txtfatax.setText(String.valueOf(tax));
                    }else if (bs <= 35472) {
                        double tax =  (0.2 * bs);
                        txtfatax.setText(String.valueOf(tax));
                    }else if (bs <= 47059) {
                        double tax =  (0.25 * bs);
                        txtfatax.setText(String.valueOf(tax));
                    }else{
                        double tax =  (0.3 * bs);
                        txtfatax.setText(String.valueOf(tax));
                    }
                    
                    //NSSF
                    if (bs >= 6000 && bs <=17999) {
                        double nlel = (0.06 * bs);
                        txtfanssf.setText(String.valueOf(nlel));
                    } else if(bs >= 18000 ){
                        double nlel = (0.06 * bs);
                        txtfanssf.setText(String.valueOf(nlel));
                    }

                    //NHIF
                    if (bs <= 5999) {
                       txtfanhif.setText("150");
                    } else if (bs <= 7999) {
                        txtfanhif.setText("300");
                    }else if (bs <= 11999) {
                        txtfanhif.setText("400");
                    }else if (bs <= 14999) {
                        txtfanhif.setText("500");
                    }else if (bs <= 19999) {
                        txtfanhif.setText("600");
                    }else if (bs <= 24999) {
                        txtfanhif.setText("750");
                    }else if (bs <= 29999) {
                        txtfanhif.setText("850");
                    }else if (bs <= 34999) {
                        txtfanhif.setText("900");
                    }else if (bs <= 39999) {
                        txtfanhif.setText("950");
                    }else if (bs <= 44999) {
                        txtfanhif.setText("1000");
                    }else if (bs <= 49999) {
                        txtfanhif.setText("1100");
                    }else if (bs <= 59999) {
                        txtfanhif.setText("1200");
                    }else if (bs <= 69999) {
                        txtfanhif.setText("1300");
                    }else if (bs <= 79999) {
                        txtfanhif.setText("1400");
                    }else if (bs <= 89999) {
                        txtfanhif.setText("1500");
                    }else if (bs <= 99999) {
                        txtfanhif.setText("1600");
                    }
                    else{
                        txtfanhif.setText("1700");
                    }
                    
                } else {
                    //TAXATION ON OVERTIME ABOVE MINIMUM TAXABLE WAGE
                    double bs = Double.parseDouble(txtfabasicsalary.getText());
                    double taxableincome = ot + bs;

                    if (taxableincome <= 12298) {
                       double tax =  (0.1 * taxableincome);
                       txtfatax.setText(String.valueOf(tax));
                    } else if (taxableincome <= 23885) {
                        double tax =  (0.15 * taxableincome);
                        txtfatax.setText(String.valueOf(tax));
                    }else if (taxableincome <= 35472) {
                        double tax =  (0.2 * taxableincome);
                        txtfatax.setText(String.valueOf(tax));
                    }else if (taxableincome <= 47059) {
                        double tax =  (0.25 * taxableincome);
                        txtfatax.setText(String.valueOf(tax));
                    }else{
                        double tax =  (0.3 * taxableincome);
                        txtfatax.setText(String.valueOf(tax));
                    }
                    
                    //NSSF
                    if (taxableincome >= 6000 && taxableincome <=17999) {
                        double nlel = (0.06 * taxableincome);
                        txtfanssf.setText(String.valueOf(nlel));
                    } else if(taxableincome >= 18000 ){
                        double nlel = (0.06 * taxableincome);
                        txtfanssf.setText(String.valueOf(nlel));
                    }

                    //NHIF
                    if (taxableincome <= 5999) {
                       txtfanhif.setText("150");
                    } else if (taxableincome <= 7999) {
                        txtfanhif.setText("300");
                    }else if (taxableincome <= 11999) {
                        txtfanhif.setText("400");
                    }else if (taxableincome <= 14999) {
                        txtfanhif.setText("500");
                    }else if (taxableincome <= 19999) {
                        txtfanhif.setText("600");
                    }else if (taxableincome <= 24999) {
                        txtfanhif.setText("750");
                    }else if (taxableincome <= 29999) {
                        txtfanhif.setText("850");
                    }else if (taxableincome <= 34999) {
                        txtfanhif.setText("900");
                    }else if (taxableincome <= 39999) {
                        txtfanhif.setText("950");
                    }else if (taxableincome <= 44999) {
                        txtfanhif.setText("1000");
                    }else if (taxableincome <= 49999) {
                        txtfanhif.setText("1100");
                    }else if (taxableincome <= 59999) {
                        txtfanhif.setText("1200");
                    }else if (taxableincome <= 69999) {
                        txtfanhif.setText("1300");
                    }else if (taxableincome <= 79999) {
                        txtfanhif.setText("1400");
                    }else if (taxableincome <= 89999) {
                        txtfanhif.setText("1500");
                    }else if (taxableincome <= 99999) {
                        txtfanhif.setText("1600");
                    }
                    else{
                        txtfanhif.setText("1700");
                    }
                    
                }
                
                                
                double tax = Double.parseDouble(txtfatax.getText());
                double nssf = Double.parseDouble(txtfanssf.getText());
                double insu = Double.parseDouble(txtfanhif.getText());
                double miscd = Double.parseDouble(txtfamiscdeductions.getText());

                double totaldeductions = tax + nssf + insu + miscd;
                
                txtfadeductionstotal.setText(String.valueOf(totaldeductions));
                
                double basicsal = Double.parseDouble(txtfabasicsalary.getText());
                
                double grosspay = (basicsal + totalbenefits );
                
                double netpay = (grosspay - totaldeductions );
                
                txtfanetpay.setText(String.valueOf(netpay));
                
            } catch (NumberFormatException e) {
                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "Invalid field values. Please check whether the field are empty or contains invalid characters.");
        }
        
    }//GEN-LAST:event_btnNetPayActionPerformed
   
    private void checkingRowCountinSalaryPayment(){
        int nor = -1;
        try {
            String sql = "SELECT COUNT(`id`) AS id FROM `salaries_payment` WHERE `emp_work_id`=? ORDER BY `id` DESC LIMIT 1 ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txtfaworkid.getText());
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                nor = Integer.parseInt(rs.getString("id"));
            }
            
            //JOptionPane.showMessageDialog(null, nor+" rows...");
            
            if (nor <= 0) {
                makingPayment();
            } else {
                checkIfPaid();
            }
            
        } catch (SQLException e) {
            Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
        }       
    }
    String salamonth;
    private void checkIfPaid(){
        String empid = txtfaworkid.getText();      
        
        try {
            String sql = "SELECT `salary_month` FROM `salaries_payment` WHERE `emp_work_id`=? ORDER BY `id` DESC LIMIT 1 " ;
            pst = conn.prepareStatement(sql);
            pst.setString(1, empid);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                salamonth = rs.getString("salary_month");
            }
            
            //JOptionPane.showMessageDialog(null, salamonth+" Checking when paid..."); 
            
            if (salamonth == null ? txtfasalarymonth.getText() != null : !salamonth.equals(txtfasalarymonth.getText())) {
                //JOptionPane.showMessageDialog(null, salamonth+" Month NOT paid!");
                makingPayment();
            } else {
                JOptionPane.showMessageDialog(null, salamonth+" Month paid!");
                clearPaymentFields();
            }
            
        } catch (SQLException e) {
            Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
        }       
        
    }
    
    private void makingPayment(){
        String empid = txtfaworkid.getText();      
        
        try {
            String sql = "SELECT `salary_month` FROM `salaries_payment` WHERE `emp_work_id`=? ORDER BY `id` DESC LIMIT 1 " ;
            pst = conn.prepareStatement(sql);
            pst.setString(1, empid);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                salamonth = rs.getString("salary_month");
            }
            
            if (salamonth == null ? txtfasalarymonth.getText() != null : !salamonth.equals(txtfasalarymonth.getText())) {
                JOptionPane.showMessageDialog(null, salamonth+" Month NOT paid!");
                txtfapayslip.setText("");
        
                String workid = txtfaworkid.getText();
                String dept = txtfadept.getText();
                String name = txtfaempname.getText();
                String payslipno = txtfapayslipno.getText();
                String bsal = txtfabasicsalary.getText();
                String paytype = txtfapaytype.getText();
                String salmonth = txtfasalarymonth.getText();
                String overt = txtfaovertime.getText();
                String training = txtfatrainingamnt.getText();
                String bna = txtfabonus.getText();
                String miscpay = txtfamiscpay.getText();
                String bentotal = txtfaadditionalpymntstotal.getText();
                String tax = txtfatax.getText();
                String nssf = txtfanssf.getText();
                String insur = txtfanhif.getText();
                String miscded = txtfamiscdeductions.getText();
                String dedtotal = txtfadeductionstotal.getText();
                String netpay = txtfanetpay.getText();

                txtfapayslip.append("\tNDEMU PRIMARY SCHOOL\n"
                    + "         P.O Box 766 - 10400, Nanyuki, Kenya\n"
                    + "         Hardwork and determination brings succes\n\n" +
                    "\t     PAYSLIP \n" + 
                    "\n==================================================\t" + 
                    "\nName: \t\t\t"+name+
                    "\nWork ID: \t\t\t"+workid+
                    "\nDepartment: \t\t\t"+dept+
                    "\nPayslip No.: \t\t\t"+payslipno+
                    "\nBasic Salary: \t\t\t"+bsal+
                    "\nPayment Type: \t\t\t"+paytype+
                    "\nSalary Period: \t\t\t"+salmonth+
                    "\n\n--------------------------------------------------------------------------------------\t" +
                    "\nOvertime: \t\t\t"+overt+
                    "\nTraining: \t\t\t"+training+
                    "\nBonus and Allowance: \t\t"+bna+
                    "\nMisc. Payments: \t\t"+miscpay+
                    "\n\nTotal Allowances: \t\t"+bentotal+
                    "\n\n--------------------------------------------------------------------------------------\t" +
                    "\n\nTax: \t\t\t"+tax+
                    "\nNSSF: \t\t\t"+nssf+
                    "\nInsurance: \t\t\t"+insur+
                    "\nMisc. Deductions: \t\t"+miscded+
                    "\n\nTotal Deductions: \t\t"+dedtotal+
                    "\n\nNet Pay: \t\t\t"+netpay+
                    "\n===================================================\t" + 
                    "\nDate:\t\t"+day+"/"+(month+1)+"/"+year +
                    "\nTime:\t\t"+hour+":"+minute+":"+second +
                    "\n====================================================\t"+
                    "\n\n\tThankyou for being part of us.");

                    String emppayslip = txtfapayslip.getText();

                    if (!workid.isEmpty() && !dept.isEmpty() && !name.isEmpty() && !payslipno.isEmpty() && cbofaempworkcat.getSelectedIndex() != 0 && !bsal.isEmpty()
                            && !paytype.isEmpty() && !salmonth.isEmpty() && !overt.isEmpty() && !training.isEmpty() && !bna.isEmpty() && 
                            !miscpay.isEmpty() && !bentotal.isEmpty() && !tax.isEmpty() && !nssf.isEmpty() && !insur.isEmpty() && !miscded.isEmpty() &&
                            !dedtotal.isEmpty() && !netpay.isEmpty() && !emppayslip.isEmpty() ) {

                        try {
                            String paysql = "INSERT INTO `salaries_payment`(`emp_work_id`, `name`, `dpt`, `payslip_no`, `pay_type`, `basic_sal`, `salary_month`,"
                                    + " `overtime_benefits`, `training_amt`, `bonus_appr`, `misc_addtnl_pymts`, `total_addtnl_pymts`, `tax`, `nssf`,"
                                    + " `insurance`, `misc_deductions`, `total_deductions`, `netpay`, `receipt`)"
                                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                            pst = conn.prepareStatement(paysql);
                            pst.setString(1, workid);
                            pst.setString(2, name);
                            pst.setString(3, dept);
                            pst.setString(4, payslipno);
                            pst.setString(5, paytype);
                            pst.setString(6, bsal);
                            pst.setString(7, salmonth);
                            pst.setString(8, overt);
                            pst.setString(9, training);
                            pst.setString(10, bna);
                            pst.setString(11, miscpay);
                            pst.setString(12, bentotal);
                            pst.setString(13, tax);
                            pst.setString(14, nssf);
                            pst.setString(15, insur);
                            pst.setString(16, miscded);
                            pst.setString(17, dedtotal);
                            pst.setString(18, netpay);  
                            pst.setString(19, emppayslip);

                            pst.execute();

                            JOptionPane.showMessageDialog(null, "Saved.");

                            try {
                                Boolean payslip = txtfapayslip.print();

                                if(payslip){
                                    JOptionPane.showMessageDialog(null, "Done Printing.");
                                }else{
                                    JOptionPane.showMessageDialog(null, "Printing...");
                                }
                            } catch (HeadlessException | PrinterException e) {
                               Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }

                            clearPaymentFields();

                        } catch (SQLException e) {
                            JOptionPane.showMessageDialog(null, "Month already paid!");
                            clearPaymentFields();
                            Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                        }finally{
                            try {
                                rs.close();
                                pst.close();
                            } catch (SQLException e) {
                            }
                        }


                    } else {
                        JOptionPane.showMessageDialog(null, "Please fill all the fields.");
                    }
                    
            } else {
                JOptionPane.showMessageDialog(null, salamonth+" Month paid!");
                clearPaymentFields();
            }
            
        } catch (SQLException e) {
            Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
        }       
        
        
    }
    
    private void btnfaSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfaSaveActionPerformed
        // TODO add your handling code here:
        //checkIfPaid();
        checkingRowCountinSalaryPayment();
        
    }//GEN-LAST:event_btnfaSaveActionPerformed

    private void btnfaPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfaPrintActionPerformed
        // TODO add your handling code here:
        try {
            Boolean emppayslip = txtfapayslip.print();

            if(emppayslip){
                JOptionPane.showMessageDialog(null, "Done Printing.");
            }else{
                JOptionPane.showMessageDialog(null, "Printing...");
            }
        } catch (HeadlessException | PrinterException e) {

        }
    }//GEN-LAST:event_btnfaPrintActionPerformed

    private void txtfapadmnosearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfapadmnosearchKeyReleased
        // TODO add your handling code here:
        try {
            String sql = "SELECT * FROM `admissions` WHERE `admno`=? AND `status`=\"1\" ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txtfapadmnosearch.getText());
            rs = pst.executeQuery();

            while (rs.next()) {
                txtfapclass.setText(rs.getString("curr_class"));
                txtfapcurrterm.setText(rs.getString("curr_term"));
                txtfapadmno.setText(rs.getString("admno"));
                txtfapname.setText(rs.getString("name"));
                byte[] pupil_profileimage = rs.getBytes("profile_image");
                ImageIcon imageIcon = new ImageIcon(new ImageIcon(pupil_profileimage).getImage().getScaledInstance(pupilprofileimg.getWidth(), pupilprofileimg.getHeight(), Image.SCALE_SMOOTH));
                pupilprofileimg.setIcon(imageIcon);
                this.pupil_image = pupil_profileimage;
            }

        } catch (SQLException e) {
            Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_txtfapadmnosearchKeyReleased

    private void txtfapclassMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfapclassMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfapclassMouseClicked

    private void cbopymntmodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbopymntmodeActionPerformed
        // TODO add your handling code here:
        int pymntmode = cbopymntmode.getSelectedIndex();

        switch (pymntmode) {
            case 1:
            receiptNo();
            break;
            case 2:
            receiptNo();
            break;
            case 3:
            receiptNo();
            break;
            default:
            txtfareceiptno.setText("");
            break;
        }
    }//GEN-LAST:event_cbopymntmodeActionPerformed

    private void txtfaamntpaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfaamntpaidKeyReleased
        // TODO add your handling code here:
        if (!txtfaamntpaid.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtfaamntpaid.setText("0.00");
        }
        
        termCorrespodence();
        
        double bal = 0;
        
        try {
                String sql = "SELECT * FROM `fees_payment` WHERE `adm_no`='"+txtfapadmno.getText()+"' ORDER BY `id` DESC LIMIT 1 ";
                pst = conn.prepareStatement(sql);
                rs = pst.executeQuery();

                while (rs.next()) {
                    bal = Double.parseDouble(rs.getString("balance"));
                }
                
                //JOptionPane.showMessageDialog(null, bal);
                
                double termfees = Double.parseDouble(txtfatermfees.getText());
                double amntp = Double.parseDouble(txtfaamntpaid.getText());
                
                if (txtfaamntpaid.getText().isEmpty()) {
                    txtfabalance.setText(String.valueOf(bal));
                }

                if (bal < 0) {
                    double currb = termfees + (bal);
                    double balance = currb - amntp ;

                    System.out.println(balance);

                    txtfabalance.setText(String.valueOf(balance));

                } else if(bal == 0) {
                    double currb = termfees + (bal);
                    System.out.println(currb);

                    double blnce = currb - amntp;
                    System.out.println(blnce);

                    txtfabalance.setText(String.valueOf(blnce));

                }else{
                    double newbal = bal - amntp;
                    System.out.println(newbal);

                    txtfabalance.setText(String.valueOf(newbal));
                }

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Invalid value!");
                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
            }

        
    }//GEN-LAST:event_txtfaamntpaidKeyReleased

    private void termCorrespodence(){
        String cboterm = (String) cbofaterm.getSelectedItem();
        String pterm = txtfapcurrterm.getText();
            if (!cboterm.equals(pterm)) {
                JOptionPane.showMessageDialog(null, "Selected term and pupil's current term do not correspond!");
                cbofaterm.setSelectedIndex(0);
                txtfatermfees.setText("0.00");
            }else{
                System.out.println("Correspodence okay!");
            }
    }
    
    private void cbofatermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbofatermActionPerformed
        // TODO add your handling code here:
        int term = cbofaterm.getSelectedIndex();
        String pclass = txtfapclass.getText();

        if (!pclass.isEmpty()) {
            //termCorrespodence();
                  
            switch (term) {
                
                case 1:

                    switch (pclass) {
                        case "Pre-primary 1":
                        case "Pre-primary 2":
                            classlevel = "Pre-primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `prepfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("ptost"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                        case "Grade 1":
                        case "Grade 2":
                        case "Grade 3":
                            classlevel = "Lower Primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `lpfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("ltost"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                            
                        case "Grade 4":
                        case "Grade 5":
                        case "Grade 6":
                            classlevel = "Upper Primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `upfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("utost"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                            
                        default:
                            JOptionPane.showMessageDialog(null, "Ops! Nothing found.");
                            break;
                    }
                    
                break;

                case 2:
                    switch (pclass) {
                        case "Pre-primary 1":
                        case "Pre-primary 2":
                            classlevel = "Pre-primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `prepfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("pttst"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                        case "Grade 1":
                        case "Grade 2":
                        case "Grade 3":
                            classlevel = "Lower Primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `lpfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("lttst"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                            
                        case "Grade 4":
                        case "Grade 5":
                        case "Grade 6":
                            classlevel = "Upper Primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `upfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("uttst"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                            
                        default:
                            JOptionPane.showMessageDialog(null, "Ops! Nothing found.");
                            break;
                    }
                break;

                case 3:
                    switch (pclass) {
                        case "Pre-primary 1":
                        case "Pre-primary 2":
                            classlevel = "Pre-primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `prepfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("ptthst"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                        case "Grade 1":
                        case "Grade 2":
                        case "Grade 3":
                            classlevel = "Lower Primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `lpfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("ltthst"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                            
                        case "Grade 4":
                        case "Grade 5":
                        case "Grade 6":
                            classlevel = "Upper Primary";
                            //JOptionPane.showMessageDialog(null, classlevel);
                            try {
                                String sql = "SELECT * FROM `upfeesstructure` WHERE `class_lvl`=? ";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, classlevel);
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    txtfatermfees.setText(rs.getString("utthst"));
                                }else{
                                    txtfatermfees.setText("0.00");
                                }
                            } catch (SQLException e) {
                                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
                            }
                            break;
                            
                        default:
                            JOptionPane.showMessageDialog(null, "Ops! Nothing found.");
                            //Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null,"" );
                            break;
                    }
                break;

                default:
                    txtfatermfees.setText("0.00");
                    txtfaamntpaid.setText("0.00");
                break;
            }
        
        } else {
            JOptionPane.showMessageDialog(null, "Please fill the fields as required.");
            cbofaterm.setSelectedIndex(0);
            
        }
    }//GEN-LAST:event_cbofatermActionPerformed

    private void btnFAClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFAClearActionPerformed
        // TODO add your handling code here:
        clearFeesFields();
    }//GEN-LAST:event_btnFAClearActionPerformed

    private void btnFAFeesSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFAFeesSaveActionPerformed
        // TODO add your handling code here:
        String pname = txtfapname.getText();
        String pclass = txtfapclass.getText();
        String padmno = txtfapadmno.getText();
        String pcurrterm = txtfapcurrterm.getText();
        String paymode = cbopymntmode.getSelectedItem().toString();
        String prcptno = txtfareceiptno.getText();
        String ppdate = txtfapaydate.getText();
        String pagbr = txtfabranch.getText();
        String ptransno = txtfatransactionno.getText();
        String pterm = cbofaterm.getSelectedItem().toString();
        String pttpaid = txtfaamntpaid.getText();
        String pbal = txtfabalance.getText();
//        String pamtwrds = txtfaamntinwords.getText();

        txtfafeereceipt.setText("");

        txtfafeereceipt.append("\tNDEMU PRIMARY SCHOOL\n"
            + "         P.O Box 766 - 10400, Nanyuki, Kenya.\n"
            + "         Hardwork and determination brings success\n\n" +
            "\t FEES RECEIPT \n" +
            "\n==================================================\t" +
            "\nName: \t\t"+pname+
            "\nAdmission No: \t"+padmno+
            "\nClass: \t\t"+pclass+
            "\nTerm: \t\t"+pcurrterm+
            "\n\nReceipt No.: \t\t"+prcptno+
            "\nPayment Mode: \t"+paymode+
            "\nTransaction No.: \t"+ptransno+
            "\n\nAmount Paid: \t\t"+pttpaid+
            "\nBalance: \t\t"+pbal+
            "\n===================================================\t" +
            "\nDate:\t\t"+day+"/"+month+"/"+year +
            "\nTime:\t\t"+hour+":"+minute+":"+second +
            "\n====================================================\t"+
            "\n\n\tThankyou for being part of us.");

        String prcpt = txtfafeereceipt.getText();
        
        int pymntmode = cbopymntmode.getSelectedIndex();
        int trm = cbofaterm.getSelectedIndex();

        if (!pname.isEmpty() && !pclass.isEmpty() && !padmno.isEmpty() && !pcurrterm.isEmpty() && !prcptno.isEmpty() && pymntmode != 0
            && !ppdate.isEmpty() && trm != 0 && !pagbr.isEmpty() && !ptransno.isEmpty() &&  !pbal.isEmpty() && !prcpt.isEmpty() ) {        
            
            try {
                String bala = txtfabalance.getText();

                String sql = "INSERT INTO `fees_payment`(`name`, `adm_no`, `class`, `class_level`, `curr_term`, `profile_img`,"
                        + " `mode_of_payment`, `receipt_no`, `date`, `bank_branch`, `transaction_code`,"
                        + " `term`, `total_paid`, `balance`, `receipt`)"
                        + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                pst = conn.prepareStatement(sql);
                pst.setString(1, pname);
                pst.setString(2, padmno);
                pst.setString(3, pclass);
                pst.setString(4, classlevel);
                pst.setString(5, pcurrterm);
                pst.setBytes(6, pupil_image);
                pst.setString(7, paymode);
                pst.setString(8, prcptno);
                pst.setString(9, ppdate);
                pst.setString(10, pagbr);
                pst.setString(11, ptransno);
                pst.setString(12, pterm);
                pst.setString(13, pttpaid);
                pst.setString(14, bala);
                pst.setString(15, prcpt);

                try {
                    Boolean feereceipt = txtfafeereceipt.print();

                    if(feereceipt){
                        JOptionPane.showMessageDialog(null, "Done Printing.");
                    }else{
                        JOptionPane.showMessageDialog(null, "Printing...");
                    }
                } catch (HeadlessException | PrinterException e) {

                }

                pst.execute();

                JOptionPane.showMessageDialog(null, "Saved.");

            } catch (SQLException e) {
                Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, e);
            }
                    
        } else {
            JOptionPane.showMessageDialog(null, "Please fill the fields as required.");
        }
        
        clearFeesFields();
        
    }//GEN-LAST:event_btnFAFeesSaveActionPerformed

    private void btnFAShowListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFAShowListActionPerformed
        // TODO add your handling code here:
            DefaultTableModel model = (DefaultTableModel)tblfaballist.getModel();
            model.setColumnCount(0);
            model.setRowCount(0);

            tblfaballist.setModel(FeesBalance());
            //FeesBalance()
        
    }//GEN-LAST:event_btnFAShowListActionPerformed

    private void btnFASearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFASearchActionPerformed
        // TODO add your handling code here:
        String cl = cbofaclvlreports.getSelectedItem().toString();
        if (cbofaclvlreports.getSelectedIndex() != 0) {
            try {
                String sql = "SELECT * FROM `fees_payment` WHERE `class_level`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, cl);
                rs = pst.executeQuery();
                
                if (rs.next()) {
                    String admno = rs.getString("adm_no");
                    String pupilsname = rs.getString("name");
                    String pclass = rs.getString("class");
                    String pclasslvl = rs.getString("class_level");
                    String amntpaid = rs.getString("total_paid");
                    String baln = rs.getString("balance");
                    
                    String[] rowdata = {admno, pupilsname, pclass, pclasslvl, amntpaid, baln};
                    feebal.addRow(rowdata);
                    
                    DefaultTableModel model = (DefaultTableModel)tblfaballist.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    tblfaballist.setModel(SearchClassLevelFeesBalance());
                    
                } else {
                    DefaultTableModel model = (DefaultTableModel)tblfaballist.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    JOptionPane.showMessageDialog(null, "Ops! Nothing found.");
                }
            
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }finally{
                try{
                    rs.close();
                    pst.close();
                }catch(SQLException e){

                }
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "Please select class level.");
        }
    }//GEN-LAST:event_btnFASearchActionPerformed

    private String getCellValue(int x, int y){
        return tblfaballist.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btnFAGenFeeBalXLSSheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFAGenFeeBalXLSSheetActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{tblfaballist.getColumnName(0),tblfaballist.getColumnName(1),tblfaballist.getColumnName(2), tblfaballist.getColumnName(3),
            tblfaballist.getColumnName(4), tblfaballist.getColumnName(5) });
        
        //Looping through the table rows
        for (int i = 0; i < tblfaballist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getCellValue(i, 0),getCellValue(i, 1),getCellValue(i, 2),getCellValue(i, 3),getCellValue(i, 4),
            getCellValue(i, 5) });
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Fees Balance List.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btnFAGenFeeBalXLSSheetActionPerformed

    private void btnFAGenFeeStmntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFAGenFeeStmntActionPerformed
        try {
            // TODO add your handling code here:
            GenFeesStmnt gfs = new GenFeesStmnt();
            gfs.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnFAGenFeeStmntActionPerformed

    private void cbofaclvlreportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbofaclvlreportsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbofaclvlreportsActionPerformed

    private void btnFAGenFeeStructActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFAGenFeeStructActionPerformed
        // TODO add your handling code here:
        try {
            JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\FeesStructureSetup.jrxml");

            JasperReport jreport = JasperCompileManager.compileReport(jdesign);
            JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

            JasperViewer.viewReport(jprint, false);

        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnFAGenFeeStructActionPerformed

    private void txtsmSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsmSendActionPerformed
        // TODO add your handling code here:
//        if (rb1.isSelected()) {
//            sendSMS();
//        }else if(rb2.isSelected()){
//            try {
//                sendEmail();
//            } catch (MessagingException ex) {
//                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }else{
//            JOptionPane.showMessageDialog(null, "Please select message type");
//        }

        sendSMS();

    }//GEN-LAST:event_txtsmSendActionPerformed

    private void txtsm3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm3KeyReleased
        // TODO add your handling code here:
        getSMSContact();
    }//GEN-LAST:event_txtsm3KeyReleased

    private void txtfabranchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfabranchKeyReleased
        // TODO add your handling code here:
        if (txtfabranch.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtfabranch.setText("");
        }else if(txtfabranch.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txtfabranch.setText("");
        }
    }//GEN-LAST:event_txtfabranchKeyReleased

    private void txtsm4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm4KeyReleased
        // TODO add your handling code here:
        if (txtsm4.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtsm4.setText("");
        }
    }//GEN-LAST:event_txtsm4KeyReleased

    private void rbtsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtsActionPerformed
        // TODO add your handling code here:
        if (rbts.isSelected()) {
            lblmsg.setText("Work ID");
            rbparent.setSelected(false);
            rbnt.setSelected(false);
            txtsm3.setText("");
            txtsm4.setText("");
            txtsm5.setText("");
            txtsm6.setText("");
        }
    }//GEN-LAST:event_rbtsActionPerformed

    private void rbparentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbparentActionPerformed
        // TODO add your handling code here:
        if (rbparent.isSelected()) {
            lblmsg.setText("Pupils Adm No.");
            rbts.setSelected(false);
            rbnt.setSelected(false);
            txtsm3.setText("");
            txtsm4.setText("");
            txtsm5.setText("");
            txtsm6.setText("");
        }
    }//GEN-LAST:event_rbparentActionPerformed

    private void rbntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbntActionPerformed
        // TODO add your handling code here:
        if (rbts.isSelected()) {
            lblmsg.setText("Work ID");
            rbparent.setSelected(false);
            rbts.setSelected(false);
            txtsm3.setText("");
            txtsm4.setText("");
            txtsm5.setText("");
            txtsm6.setText("");
        }
    }//GEN-LAST:event_rbntActionPerformed

    private void txtsm5KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm5KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtsm5KeyReleased

    private void btnsmCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsmCancelActionPerformed
        // TODO add your handling code here:
        rbparent.setSelected(false);
        rbts.setSelected(false);
        rbnt.setSelected(false);
        txtsm3.setText("");
        txtsm4.setText("");
        txtsm5.setText("");
        txtsm6.setText("");
    }//GEN-LAST:event_btnsmCancelActionPerformed

    private void txtfabalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfabalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfabalanceActionPerformed

    private void txtfaamntpaidKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfaamntpaidKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfaamntpaidKeyPressed

    private void txtfatransactionnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfatransactionnoKeyReleased
        // TODO add your handling code here:
        String trnlen = txtfatransactionno.getText();
        
        if (trnlen.length() > 15) {
            JOptionPane.showMessageDialog(null, "Invalid transaction number.");
            txtfatransactionno.setText("");
        }else if(txtfatransactionno.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Transaction number too long!");
            txtfatransactionno.setText("");
        } else {
            System.out.println("All good");
        }
    }//GEN-LAST:event_txtfatransactionnoKeyReleased

    private void jbtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnBackActionPerformed
        // TODO add your handling code here:
        String admin = falblloggedinasusercode.getText();
        
        if (admin.contains("ADM")) {
            this.dispose();
            try {
                Admin ad = new Admin();
                ad.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                FandA fa = new FandA();
                fa.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }//GEN-LAST:event_jbtnBackActionPerformed

    private void txtfaovertimeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfaovertimeKeyReleased
        // TODO add your handling code here:
        if (!txtfaovertime.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfaovertime.setText("");
        }
    }//GEN-LAST:event_txtfaovertimeKeyReleased

    private void txtfatrainingamntKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfatrainingamntKeyReleased
        // TODO add your handling code here:
        if (!txtfatrainingamnt.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfatrainingamnt.setText("");
        }
    }//GEN-LAST:event_txtfatrainingamntKeyReleased

    private void txtfabonusKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfabonusKeyReleased
        // TODO add your handling code here:
        if (!txtfabonus.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfabonus.setText("");
        }
    }//GEN-LAST:event_txtfabonusKeyReleased

    private void txtfamiscpayKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfamiscpayKeyReleased
        // TODO add your handling code here:
        if (!txtfamiscpay.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfamiscpay.setText("");
        }
    }//GEN-LAST:event_txtfamiscpayKeyReleased

    private void txtfataxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfataxKeyReleased
        // TODO add your handling code here:
        if (!txtfatax.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfatax.setText("");
        }
    }//GEN-LAST:event_txtfataxKeyReleased

    private void txtfanssfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfanssfKeyReleased
        // TODO add your handling code here:
        if (!txtfanssf.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfanssf.setText("");
        }
    }//GEN-LAST:event_txtfanssfKeyReleased

    private void txtfanhifKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfanhifKeyReleased
        // TODO add your handling code here:
        if (!txtfanhif.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfanhif.setText("");
        }
    }//GEN-LAST:event_txtfanhifKeyReleased

    private void txtfamiscdeductionsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfamiscdeductionsKeyReleased
        // TODO add your handling code here:
        if (!txtfamiscdeductions.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtfamiscdeductions.setText("");
        }
    }//GEN-LAST:event_txtfamiscdeductionsKeyReleased

//    private void salaryPeriod(){
//        checkingSalaryPeriod();
//        int test = sper;
//        
//        JOptionPane.showMessageDialog(null, " for sper");
//        
//        int salper = Integer.parseInt(txtfasalaryperiod.getText());
//        
//        if (sper == salper) {
//            JOptionPane.showMessageDialog(null, "Month already paid!");
//            clearPaymentFields();
//            //System.out.println("Both sper and salper From textfield equal");            
//        } else {
//            JOptionPane.showMessageDialog(null, "Month can be paid...");            
//        }
//    }
    
    private void txtfaovertimeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfaovertimeMouseClicked
        // TODO add your handling code here:
        txtfaovertime.setText("");
    }//GEN-LAST:event_txtfaovertimeMouseClicked

    private void txtfatrainingamntMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfatrainingamntMouseClicked
        // TODO add your handling code here:
        txtfatrainingamnt.setText("");
    }//GEN-LAST:event_txtfatrainingamntMouseClicked

    private void txtfabonusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfabonusMouseClicked
        // TODO add your handling code here:
        txtfabonus.setText("");
    }//GEN-LAST:event_txtfabonusMouseClicked

    private void txtfamiscpayMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfamiscpayMouseClicked
        // TODO add your handling code here:
        txtfamiscpay.setText("");
    }//GEN-LAST:event_txtfamiscpayMouseClicked

    private void txtfamiscdeductionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfamiscdeductionsMouseClicked
        // TODO add your handling code here:
        txtfamiscdeductions.setText("");
    }//GEN-LAST:event_txtfamiscdeductionsMouseClicked

    private void cbofaempworkcatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbofaempworkcatActionPerformed
        // TODO add your handling code here:
        txtfapaytype.setText("");
        txtfasalarymonth.setText("");
        txtfaempidsearch.setText("");
        txtfaworkid.setText("");
        txtfadept.setText("");
        txtfaempname.setText("");
        txtfapayslipno.setText("");
        txtfabasicsalary.setText("");
        salamonth = null;
    }//GEN-LAST:event_cbofaempworkcatActionPerformed
    
    private void sendSMS(){
        String bulksmsusername = System.getenv("BULKSMSUSRNAME");
        String bulksmspssd = System.getenv("BULKSMSPSSD");
        String msg = txtsm6.getText();
        String phoneno = txtsm4.getText();
//        if (rb1.isSelected()) {
//            txtsm3.setEditable(true);
//            
//            String msgtype = "";
//            if (rb1.isSelected()) {
//                msgtype = rb1.getText();
//            }
            String padmno = txtsm3.getText();
            String mrecepient = txtsm4.getText();
            String mssg = txtsm6.getText();

            if (!padmno.isEmpty() && !mrecepient.isEmpty() && !mssg.isEmpty()) {
            
                getSMSContact();
                SMS send = new SMS();
                send.SendSMS(bulksmsusername, bulksmspssd, msg, phoneno, "https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0");

                JOptionPane.showMessageDialog(null, "Message sent");
                
                
                try {
                    String mfrom = "Finance and Accounts";
                
                    String sql = "INSERT INTO `messages`(`padmno`, `m_from`, `m_to`, `m_msg`) VALUES (?,?,?)";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, padmno);
                    pst.setString(2, mfrom);
                    pst.setString(3, mrecepient);
                    pst.setString(4, mssg);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Message saved.");

                    //rb1.setSelected(false);
                    
                    messageList();
                
                } catch (SQLException e) {
                }
            
            }else{
                    JOptionPane.showMessageDialog(null, "Please ensure all the fields are filled.");
            }          
                        
//        } else if(rb2.isSelected() && cbosm2.getSelectedIndex() == 3) {
//                txtsm3.setEditable(false);
//                txtsm4.setEditable(false);
//                txtsm8.setEditable(true);
//            
//            
//        }else{
//            JOptionPane.showMessageDialog(null, "Please select the type of message to send.");
//        }
    }
    
    private void getSMSContact(){
        if (rbparent.isSelected()) {
            String msgadmno = txtsm3.getText();
            
            try {
                String sql = "SELECT * FROM `admissions` WHERE `admno`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, msgadmno);
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String contact = rs.getString("ecpc_phone");
                    txtsm4.setText(contact);
                    txtsm5.setText(rs.getString("name"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
            
        } else if (rbts.isSelected()) {
            try {
                String msgadmno = txtsm3.getText();
                
                String sql = "SELECT * FROM `teachers` WHERE `t_trid`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, msgadmno);
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String contact = rs.getString("t_phone");
                    txtsm4.setText(contact);
                    txtsm5.setText(rs.getString("t_name"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }else if (rbnt.isSelected()) {
            try {
                String msgadmno = txtsm3.getText();
                
                String sql = "SELECT * FROM `otherstaff` WHERE `e_work_id`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, msgadmno);
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String contact = rs.getString("ecpc_phone");
                    txtsm4.setText(contact);
                    txtsm5.setText(rs.getString("ename"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Please select recipient.");
            txtsm3.setText("");
        }
    }
    
//    private void sendEmail() throws MessagingException{
//        System.out.println("Preparing to send mail...");
//        String emailmsg = txtsm6.getText();
//        String recepient = txtsm8.getText();        
//        String sjct = txtsm9.getText();
//        
//        String msgtype = "";
//        if (rb2.isSelected()) {
//            msgtype = rb2.getText();
//        }
//
//        if (!msgtype.isEmpty() && !recepient.isEmpty() && !sjct.isEmpty()) {
//            
//            if (cbosm2.getSelectedIndex() == 2) {
//                if (isValid(recepient)) {
//                    System.out.print("Valid email");
//                } else {
//                    JOptionPane.showMessageDialog(null, "Invalid email");
//                }
//            }
//            
//        
//            Properties props = new Properties();
//            props.put("mail.smtp.auth", "true");
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", "smtp.gmail.com");
//            props.put("mail.smtp.port", "587");
//    //        
//            String myEmailAcc = System.getenv("EMAIL");
//            String myEmailAppPssd = System.getenv("APP_PSSD");
//
//            Session session = Session.getInstance(props, new Authenticator() {
//                @Override
//                protected PasswordAuthentication getPasswordAuthentication(){
//                    return new PasswordAuthentication(myEmailAcc, myEmailAppPssd);
//                }
//            });
//            Message message = prepareMessage(session, myEmailAcc,recepient, sjct, emailmsg);
//
//            Transport.send(message);
//            JOptionPane.showMessageDialog(null, "Mail sent succesfully");
//            
//            messageList();
//        
//        try {  
//            String sql = "INSERT INTO `messages` (`m_type`, `e_recipient`, `m_from`, `m_sbjct`, `m_msg`) VALUES (?,?,?,?,?)";
//            pst = conn.prepareStatement(sql);
//            pst.setString(1, msgtype);
//            pst.setString(2, recepient);
//            pst.setString(3, myEmailAcc);
//            pst.setString(4, sjct);
//            pst.setString(5, emailmsg);
//
//            pst.execute();
//
//            JOptionPane.showMessageDialog(null, "Message saved");
//
//            rb1.setSelected(false);
//                
//            } catch (SQLException e) {
//            }
//        
//        }else{
//            JOptionPane.showMessageDialog(null, "Please fill the fields as required");
//        }
//        
//    }
    
//    private static Message prepareMessage(Session session, String myEmailAcc, String recepient, String sjct, String emailmsg){
//        try {
//            Message msg = new MimeMessage(session);
//            msg.setFrom(new InternetAddress(myEmailAcc));
//            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient.trim()));
//            msg.setSubject(sjct);
//            msg.setText(emailmsg);
//            return msg;
//        } catch (MessagingException ex) {
//            JOptionPane.showMessageDialog(null, "Sorry. We are currently unable to send the email");
//            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//
////        try {
////            Properties props = new Properties();
////            props.put("mail.smtp.host", "smtp.gmail.com");
////            props.put("mail.smtp.port", "587");
////            props.put("mail.smtp.user", "kathurimakimathi415@gmail.com");
////            props.put("mail.smtp.auth", "true");
////            props.put("mail.smtp.starttls.enable", "true");
////            props.put("mail.smtp.debug", "true");
////            props.put("mail.smtp.socketFactory.port", "587");
////            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////            props.put("mail.smtp.socketFactory.fallback", "false");
////
////            Session session = Session.getDefaultInstance(props, null);
////            session.setDebug(true);
////            MimeMessage msg = new MimeMessage(session);
////            msg.setText(emailmsg);
////            msg.setSubject(sjct);
////            msg.setFrom(myEmailAcc.trim());
////            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recepient.trim()));
////            msg.saveChanges();
////
////            try (Transport transport = session.getTransport("smtp")) {
////                transport.connect("smtp.gmail.com", myEmailAcc, myEmailAppPssd);
////                transport.sendMessage(msg, msg.getAllRecipients());
////            }
////            JOptionPane.showMessageDialog(null, "Mail sent succesfully");
////        
////        } catch (HeadlessException | MessagingException e) {
////            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
////        }
//    }
//    
//    private boolean isValid(String email){
//        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
//                
//        Pattern pat = Pattern.compile(emailRegex); 
//
//        if (email == null) 
//
//            return false; 
//
//        return pat.matcher(email).matches();
//    }
    
    private void clearFeesFields(){
        txtfapname.setText("");
        txtfapclass.setText("");
        txtfapadmno.setText("");
        txtfapcurrterm.setText("");
        cbopymntmode.setSelectedIndex(0);
        //cbopayinstallment.setSelectedIndex(0);
        txtfareceiptno.setText("");
        txtfapaydate.setText("");
        txtfabranch.setText("");
        txtfatransactionno.setText("");
        cbofaterm.setSelectedIndex(0);
        txtfaamntpaid.setText("0");
        txtfabalance.setText("");
//        txtfaamntinwords.setText("");
        txtfafeereceipt.setText("");
        ImageIcon img = new ImageIcon();
        pupilprofileimg.setIcon(img);
        txtfatermfees.setText("");
        txtfapadmnosearch.setText("");
    }
    
    private void clearPaymentFields(){
        salamonth = null;
        txtfapaytype.setText("");
        txtfasalarymonth.setText("");
        txtfaempidsearch.setText("");
        txtfaworkid.setText("");
        txtfadept.setText("");
        txtfaempname.setText("");
        txtfapayslipno.setText("");
        txtfabasicsalary.setText("");
        cbofaempworkcat.setSelectedIndex(0);
        txtfaovertime.setText("0");
        txtfatrainingamnt.setText("0");
        txtfabonus.setText("0");
        txtfamiscpay.setText("0");
        txtfatax.setText("0");
        txtfanssf.setText("0");
        txtfanhif.setText("0");
        txtfamiscdeductions.setText("0");
        txtfadeductionstotal.setText("0.00");
        txtfaadditionalpymntstotal.setText("0.00");
        txtfapayslip.setText("");
        txtfanetpay.setText("0.00");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FandA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FandA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FandA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FandA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new FandA().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(FandA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearPayments;
    private javax.swing.JButton btnFAClear;
    private javax.swing.JButton btnFAFeesSave;
    private javax.swing.JButton btnFAGenFeeBalXLSSheet;
    private javax.swing.JButton btnFAGenFeeStmnt;
    private javax.swing.JButton btnFAGenFeeStruct;
    private javax.swing.JButton btnFASearch;
    private javax.swing.JButton btnFAShowList;
    private javax.swing.JButton btnFnALogout;
    private javax.swing.JButton btnLibChangePssd;
    private javax.swing.JButton btnNetPay;
    private javax.swing.JButton btnfaPrint;
    private javax.swing.JButton btnfaSave;
    private javax.swing.JButton btnsmCancel;
    private javax.swing.JComboBox<String> cbofaclvlreports;
    private javax.swing.JComboBox<String> cbofaempworkcat;
    private javax.swing.JComboBox<String> cbofaterm;
    private javax.swing.JComboBox<String> cbopymntmode;
    private javax.swing.JLabel falblloggedinasusercode;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JButton jbtnBack;
    private javax.swing.JLabel jlblloggedintime;
    private javax.swing.JLabel lblmsg;
    private javax.swing.JLabel lblmsg1;
    private javax.swing.JLabel pupilprofileimg;
    private javax.swing.JRadioButton rbnt;
    private javax.swing.JRadioButton rbparent;
    private javax.swing.JRadioButton rbts;
    private javax.swing.JTable tblfaballist;
    private javax.swing.JTable tblmsglist;
    private javax.swing.JTextField txtfaadditionalpymntstotal;
    private javax.swing.JTextField txtfaamntpaid;
    private javax.swing.JTextField txtfabalance;
    private javax.swing.JTextField txtfabasicsalary;
    private javax.swing.JTextField txtfabonus;
    private javax.swing.JTextField txtfabranch;
    private javax.swing.JTextField txtfadeductionstotal;
    private javax.swing.JTextField txtfadept;
    private javax.swing.JTextField txtfaempidsearch;
    private javax.swing.JTextField txtfaempname;
    private javax.swing.JTextArea txtfafeereceipt;
    private javax.swing.JTextField txtfamiscdeductions;
    private javax.swing.JTextField txtfamiscpay;
    private javax.swing.JTextField txtfanetpay;
    private javax.swing.JTextField txtfanhif;
    private javax.swing.JTextField txtfanssf;
    private javax.swing.JTextField txtfaovertime;
    private javax.swing.JTextField txtfapadmno;
    private javax.swing.JTextField txtfapadmnosearch;
    private javax.swing.JTextField txtfapaydate;
    private javax.swing.JTextArea txtfapayslip;
    private javax.swing.JTextField txtfapayslipno;
    private javax.swing.JTextField txtfapaytype;
    private javax.swing.JTextField txtfapclass;
    private javax.swing.JTextField txtfapcurrterm;
    private javax.swing.JTextField txtfapname;
    private javax.swing.JTextField txtfareceiptno;
    private javax.swing.JTextField txtfasalarymonth;
    private javax.swing.JTextField txtfatax;
    private javax.swing.JTextField txtfatermfees;
    private javax.swing.JTextField txtfatrainingamnt;
    private javax.swing.JTextField txtfatransactionno;
    private javax.swing.JTextField txtfaworkid;
    private javax.swing.JTextField txtsm3;
    private javax.swing.JTextField txtsm4;
    private javax.swing.JTextField txtsm5;
    private javax.swing.JTextArea txtsm6;
    private javax.swing.JButton txtsmSend;
    // End of variables declaration//GEN-END:variables
}
