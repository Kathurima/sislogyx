/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public class Academics extends javax.swing.JFrame {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    
    DefaultTableModel prepresultlist = new DefaultTableModel();
    DefaultTableModel lpresultlist = new DefaultTableModel();
    DefaultTableModel upresultlist = new DefaultTableModel();
    
    int yr;

    /**
     * Creates new form Academics
     * @throws java.sql.SQLException
     */
    public Academics() throws SQLException {
        super("Exams Panel");
        
        initComponents();
        
        Current_Date();
        
        conn = (Connection) DBConnect.connect();
        
        jlblacademicsloggedinas.setText(String.valueOf(Emp.empId));
        
    }
    
    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);
                    
                    yr = year;
                    //txtlibborrowdate.setText(day+"/"+month+"/"+year);

                    int second = cal.get(Calendar.SECOND);
                    int minute = cal.get(Calendar.MINUTE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);

                    lblacademicstime.setText(hour+":"+minute+":"+second);
                    lblacademicstime.setForeground(Color.red);
                }
            }
        };
        clock.start();
    }
    
    private DefaultTableModel PrePrimaryResultListTable(){
        prepresultlist.addColumn("Name");
        prepresultlist.addColumn("Admission No.");
        prepresultlist.addColumn("Class");
        prepresultlist.addColumn("Year");
        prepresultlist.addColumn("Term");
        prepresultlist.addColumn("Exam Term");
        prepresultlist.addColumn("Exam Type");
        prepresultlist.addColumn("Language Act.");
        prepresultlist.addColumn("Mathematical Act.");
        prepresultlist.addColumn("Env. Act.");
        prepresultlist.addColumn("Psychomotor Act.");
        prepresultlist.addColumn("Religious Act.");
        prepresultlist.addColumn("Total Marks");        
        prepresultlist.addColumn("Average");
        prepresultlist.addColumn("Grade");
        
        try{
            String sql = "SELECT * FROM `academics_preprimary` WHERE `exam_type`=? "
                    + "AND `exam_term`=? AND `class`=? "
                    + "AND `year`=? ";
            pst = conn.prepareStatement(sql); 
            pst.setString(1, cborsltlistexamtype.getSelectedItem().toString());
            pst.setString(2, cborsltlistpterm.getSelectedItem().toString());
            pst.setString(3, cborsltclass.getSelectedItem().toString());
            pst.setString(4, txtrsltyear.getText());
            rs = pst.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                String admno = rs.getString("admission_no");
                String pclass = rs.getString("class");
                String year = rs.getString("year");
                String term = rs.getString("curr_term");
                String eterm = rs.getString("exam_term");
                String etype = rs.getString("exam_type");
                String langact = rs.getString("lang_actv");
                String mathact = rs.getString("math_actv");
                String envact = rs.getString("env_actv");
                String pcaact = rs.getString("psychomotor_actv");
                String relact = rs.getString("rel_actv");
                String tm = rs.getString("total_mrks");
                String avg = rs.getString("average");
                String grade=  rs.getString("grade");
                
                String[] rowdata = {name,admno,pclass,year,term,eterm,etype,langact,mathact,envact,pcaact,relact,tm,avg,grade};
                prepresultlist.addRow(rowdata);
            }
            
            return prepresultlist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }
        }
        
        return null;
    }
    
    private DefaultTableModel LowerPrimaryResultListTable(){
        lpresultlist.addColumn("Name");
        lpresultlist.addColumn("Admission No.");
        lpresultlist.addColumn("Class");
        lpresultlist.addColumn("Year");
        lpresultlist.addColumn("Term");
        lpresultlist.addColumn("Exam Term");
        lpresultlist.addColumn("Exam Type");
        lpresultlist.addColumn("Literacy");
        lpresultlist.addColumn("Kisw/Kisw_Deaf");
        lpresultlist.addColumn("Eng Lang.");
        lpresultlist.addColumn("Indigenous Act.");
        lpresultlist.addColumn("Mathematical Act.");
        lpresultlist.addColumn("Environmental Act.");
        lpresultlist.addColumn("Religious Act.");
        lpresultlist.addColumn("Mvt and Crtv Act.");        
        lpresultlist.addColumn("Total Marks");
        lpresultlist.addColumn("Average");
        lpresultlist.addColumn("Grade");
        
        try{
            String sql = "SELECT * FROM `academics_lowerprimary` WHERE `exam_type`=? "
                    + "AND `exam_term`=? AND `class`=? "
                    + "AND `year`=? ";
            pst = conn.prepareStatement(sql); 
            pst.setString(1, cborsltlistexamtype.getSelectedItem().toString());
            pst.setString(2, cborsltlistpterm.getSelectedItem().toString());
            pst.setString(3, cborsltclass.getSelectedItem().toString());
            pst.setString(4, txtrsltyear.getText());
            rs = pst.executeQuery();
            
            while (rs.next()) {
                String name = rs.getString("name");
                String admno = rs.getString("admission_no");
                String pclass = rs.getString("class");
                String year = rs.getString("year");
                String term = rs.getString("curr_term");
                String eterm = rs.getString("exam_term");
                String etype = rs.getString("exam_type");
                String lit = rs.getString("literacy");
                String ksl = rs.getString("ksl_ksl_deaf");
                String englang = rs.getString("eng_lang");
                String indig = rs.getString("indigenous_actv");
                String math = rs.getString("math_actv");
                String env = rs.getString("env_actv");
                String rel = rs.getString("rel_actv");
                String mvtnca=  rs.getString("mvmnt_creative_actv");
                String tm=  rs.getString("total_mrks");
                String avg = rs.getString("average");
                String grade=  rs.getString("grade");
                
                String[] rowdata = {name,admno,pclass,year,term,eterm,etype,lit,ksl,englang,indig,math,env,rel,mvtnca,tm,avg,grade};
                lpresultlist.addRow(rowdata);
            }
            
            return lpresultlist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }
        }
        
        return null;
    }
    
    private DefaultTableModel UpperPrimaryResultListTable(){
        upresultlist.addColumn("Name");
        upresultlist.addColumn("Admission No.");
        upresultlist.addColumn("Class");
        upresultlist.addColumn("Year");
        upresultlist.addColumn("Term");
        upresultlist.addColumn("Exam Term");
        upresultlist.addColumn("Exam Type");
        upresultlist.addColumn("Kisw/Kisw_Deaf");
        upresultlist.addColumn("Eng Lang.");
        upresultlist.addColumn("Scie & Tech");
        upresultlist.addColumn("Social Studies");
        upresultlist.addColumn("Mathematics");
        upresultlist.addColumn("Home Scie.");
        upresultlist.addColumn("Agriculture");
        upresultlist.addColumn("Religious Act.");        
        upresultlist.addColumn("Creative Arts");
        upresultlist.addColumn("Physical H.E");
        upresultlist.addColumn("Pastoral P.I");
        upresultlist.addColumn("Total Marks");
        upresultlist.addColumn("Average");
        upresultlist.addColumn("Grade");
        
        try{
            String sql = "SELECT * FROM `academics_upperprimary` WHERE `exam_type`=? "
                    + "AND `exam_term`=? AND `class`=? "
                    + "AND `year`=? ";
            pst = conn.prepareStatement(sql); 
            pst.setString(1, cborsltlistexamtype.getSelectedItem().toString());
            pst.setString(2, cborsltlistpterm.getSelectedItem().toString());
            pst.setString(3, cborsltclass.getSelectedItem().toString());
            pst.setString(4, txtrsltyear.getText());
            rs = pst.executeQuery();
            
            while (rs.next()) {
                String name = rs.getString("pupils_name");
                String admno = rs.getString("admission_no");
                String pclass = rs.getString("class");
                String year = rs.getString("year");
                String term = rs.getString("curr_term");
                String eterm = rs.getString("exam_term");
                String etype = rs.getString("exam_type");
                String ksl = rs.getString("ksl_ksl_deaf");
                String eng = rs.getString("eng_lang");
                String scintech = rs.getString("sci_tech");
                String sst = rs.getString("sst");
                String math = rs.getString("math");
                String hs = rs.getString("home_science");
                String agri = rs.getString("agric");
                String rel =  rs.getString("rel_activities");
                String ca =  rs.getString("creative_arts");
                String phe = rs.getString("physical_health_edu");
                String ppi =  rs.getString("ppi");
                String tm =  rs.getString("total_marks");
                String avg = rs.getString("average");
                String grade=  rs.getString("grade");
                
                String[] rowdata = {name,admno,pclass,year,term,eterm,etype,ksl,eng,scintech,sst,math,hs,agri,rel,ca,phe,ppi,tm,avg,grade};
                upresultlist.addRow(rowdata);
            }
            
            return upresultlist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }
        }
        
        return null;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        txtprepsearch = new javax.swing.JTextField();
        jPanel18 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        txtpreppname = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        txtprepadmno = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        txtprepclass = new javax.swing.JTextField();
        txtprepcurrterm = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        cboppclass = new javax.swing.JComboBox<>();
        jPanel19 = new javax.swing.JPanel();
        txtprprel = new javax.swing.JTextField();
        txtppmath = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtppenv = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtpplang = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtprepcomment = new javax.swing.JTextArea();
        txtpppmotor = new javax.swing.JTextField();
        jPanel21 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        txtppavg = new javax.swing.JTextField();
        txtppgrade = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        txtpptm = new javax.swing.JTextField();
        btnPrepClear = new javax.swing.JButton();
        btnPrepCalc = new javax.swing.JButton();
        jLabel62 = new javax.swing.JLabel();
        cboppexamtype = new javax.swing.JComboBox<>();
        cboppterm = new javax.swing.JComboBox<>();
        jLabel63 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        txtlpsearch = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        txtlppname = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txtlpadmno = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txtlpclass = new javax.swing.JTextField();
        txtlpterm = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        cbolpclass = new javax.swing.JComboBox<>();
        jLabel55 = new javax.swing.JLabel();
        cbolpexamtype = new javax.swing.JComboBox<>();
        jLabel56 = new javax.swing.JLabel();
        cbolpterm = new javax.swing.JComboBox<>();
        jPanel9 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtlplit = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtlpksl = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtlpenglang = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtlpindiglang = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtlpmath = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtlpenv = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtlprel = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtlpmvmntca = new javax.swing.JTextField();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtlptrcomment = new javax.swing.JTextArea();
        jPanel15 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txtlpavg = new javax.swing.JTextField();
        txtlpgrade = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        txtlptm = new javax.swing.JTextField();
        jPanel22 = new javax.swing.JPanel();
        txtlpclear = new javax.swing.JButton();
        btnLPCalculate = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        txtuppname = new javax.swing.JTextField();
        txtupadmno = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        txtupclass = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        txtupcurrterm = new javax.swing.JTextField();
        cboupclass = new javax.swing.JComboBox<>();
        txtupsearch = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        cboupterm = new javax.swing.JComboBox<>();
        jLabel54 = new javax.swing.JLabel();
        cboupexamtype = new javax.swing.JComboBox<>();
        jPanel11 = new javax.swing.JPanel();
        txtupst = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtupmath = new javax.swing.JTextField();
        txtupppi = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtupss = new javax.swing.JTextField();
        txtupagric = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txtupavg = new javax.swing.JTextField();
        txtupgrade = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtuptm = new javax.swing.JTextField();
        btnUpClear = new javax.swing.JButton();
        btnUpCalculate = new javax.swing.JButton();
        txtupksllang = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtupenglang = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtuprel = new javax.swing.JTextField();
        txtupphe = new javax.swing.JTextField();
        txtupca = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txtuphs = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtTrComment = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        cborsltclass = new javax.swing.JComboBox<>();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblrsltlist = new javax.swing.JTable();
        btnReportSheet = new javax.swing.JButton();
        btnReportForm = new javax.swing.JButton();
        cborsltlistexamtype = new javax.swing.JComboBox<>();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        txtrsltlistsearch = new javax.swing.JTextField();
        jLabel71 = new javax.swing.JLabel();
        cborsltlistpterm = new javax.swing.JComboBox<>();
        txtrsltyear = new javax.swing.JTextField();
        jLabel72 = new javax.swing.JLabel();
        btnAcadSearch = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jLabel75 = new javax.swing.JLabel();
        txtvisyear = new javax.swing.JTextField();
        btnVisualCharts = new javax.swing.JButton();
        btnPrintChart = new javax.swing.JButton();
        btnVisualChartsRefresh = new javax.swing.JButton();
        panelgraphcontainer = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jlblacademicsloggedinas = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblacademicstime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        jPanel3.setBackground(new java.awt.Color(102, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("EXAMINATIONS");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel1)
                .addContainerGap(97, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(255, 0, 0));
        jLabel45.setText("Admission No");

        txtprepsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtprepsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtprepsearchKeyReleased(evt);
            }
        });

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Pupil's Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel46.setText("Pupil's Name");

        txtpreppname.setEditable(false);
        txtpreppname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel47.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel47.setText("Admission No.");

        txtprepadmno.setEditable(false);
        txtprepadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel48.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel48.setText("Class");

        txtprepclass.setEditable(false);
        txtprepclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtprepcurrterm.setEditable(false);
        txtprepcurrterm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel49.setText("Current Term");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46)
                    .addComponent(txtpreppname, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel47)
                    .addComponent(txtprepadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(81, 81, 81)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel48)
                    .addComponent(txtprepclass, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                .addGap(87, 87, 87)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel49)
                    .addComponent(txtprepcurrterm, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(135, 135, 135))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(jLabel46)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtpreppname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(jLabel47)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtprepadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel18Layout.createSequentialGroup()
                            .addComponent(jLabel48)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtprepclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel49)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtprepcurrterm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cboppclass.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboppclass.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Class", "Pre-Primary 1", "Pre-Primary 2" }));
        cboppclass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboppclassActionPerformed(evt);
            }
        });

        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Subjects", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        txtprprel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtprprel.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtprprel.setText("0");
        txtprprel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtprprelKeyReleased(evt);
            }
        });

        txtppmath.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtppmath.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtppmath.setText("0");
        txtppmath.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtppmathKeyReleased(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Psychomotor and Creative Activities");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Language Activities");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Mathematical Activities");

        txtppenv.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtppenv.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtppenv.setText("0");
        txtppenv.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtppenvKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Religious Activities (CRE/IRE/HRE/PPI)");

        txtpplang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtpplang.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtpplang.setText("0");
        txtpplang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpplangKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpplangKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtpplangKeyTyped(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Environmental Activities");

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Teacher's Comment", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        txtprepcomment.setColumns(20);
        txtprepcomment.setRows(5);
        jScrollPane5.setViewportView(txtprepcomment);

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        txtpppmotor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtpppmotor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtpppmotor.setText("0");
        txtpppmotor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpppmotorKeyReleased(evt);
            }
        });

        jPanel21.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel50.setText("Total Marks");

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel51.setText("Average");

        txtppavg.setEditable(false);
        txtppavg.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtppavg.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtppavg.setText("0");

        txtppgrade.setEditable(false);
        txtppgrade.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtppgrade.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel52.setText("GRADE");

        txtpptm.setEditable(false);
        txtpptm.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtpptm.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtpptm.setText("0");

        btnPrepClear.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPrepClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnPrepClear.setText("CLEAR");
        btnPrepClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrepClearActionPerformed(evt);
            }
        });

        btnPrepCalc.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPrepCalc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/calculate.png"))); // NOI18N
        btnPrepCalc.setText("CALCULATE");
        btnPrepCalc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrepCalcActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtpptm, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel50)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtppavg, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel51)))
                .addGap(55, 55, 55)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtppgrade, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel52)
                        .addGap(70, 70, 70)))
                .addGap(46, 46, 46))
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGap(175, 175, 175)
                .addComponent(btnPrepClear)
                .addGap(18, 18, 18)
                .addComponent(btnPrepCalc, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel50)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtpptm, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel52)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtppgrade, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel51)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtppavg, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrepClear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrepCalc, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4)
                            .addComponent(jLabel2)
                            .addComponent(txtpplang)
                            .addComponent(txtppmath, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(55, 55, 55)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6)
                            .addComponent(txtppenv)
                            .addComponent(jLabel5)
                            .addComponent(txtpppmotor, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(59, 59, 59)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(txtprprel, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(63, 63, 63))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtpplang, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtppenv, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtprprel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtppmath, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtpppmotor, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49))
        );

        jLabel62.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel62.setText("Exam Type");

        cboppexamtype.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboppexamtype.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Exam Type", "Opener", "Mid-Term", "End-Term" }));

        cboppterm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboppterm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Term", "Term 1", "Term 2", "Term 3" }));
        cboppterm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbopptermActionPerformed(evt);
            }
        });

        jLabel63.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel63.setText("Term");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(cboppclass, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtprepsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jLabel62)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboppexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                        .addComponent(jLabel63)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboppterm, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14))
                    .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel63)
                        .addComponent(cboppterm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel62)
                        .addComponent(cboppexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel45)
                        .addComponent(txtprepsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cboppclass, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Pre-Primary (1 & 2)", jPanel6);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Environmental Activities");

        jTextField11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Hygiene and Nutrition Activities");

        jTextField12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlpsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpsearchKeyReleased(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 0, 0));
        jLabel32.setText("Pupil's Admission No.");

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Pupil's Detail", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setText("Pupil's Name");

        txtlppname.setEditable(false);
        txtlppname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("Admission No.");

        txtlpadmno.setEditable(false);
        txtlpadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setText("Class");

        txtlpclass.setEditable(false);
        txtlpclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlpterm.setEditable(false);
        txtlpterm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel44.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel44.setText("Current Term");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel33)
                    .addComponent(txtlppname, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34)
                    .addComponent(txtlpadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel35)
                    .addComponent(txtlpclass, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                .addGap(91, 91, 91)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel44)
                    .addComponent(txtlpterm, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(173, 173, 173))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlppname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel44)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpterm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cbolpclass.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbolpclass.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Class", "Grade 1", "Grade 2", "Grade 3" }));
        cbolpclass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbolpclassActionPerformed(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel55.setText("Exam Type");

        cbolpexamtype.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbolpexamtype.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Exam Type", "Opener", "Mid-Term", "End-Term" }));

        jLabel56.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel56.setText("Term");

        cbolpterm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbolpterm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Term", "Term 1", "Term 2", "Term 3" }));
        cbolpterm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbolptermActionPerformed(evt);
            }
        });

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Subjects", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Literacy");

        txtlplit.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlplit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlplit.setText("0");
        txtlplit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlplitKeyReleased(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Kiswahili Language Activities / KSL for the Deaf");

        txtlpksl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpksl.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpksl.setText("0");
        txtlpksl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpkslKeyReleased(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("English Language Activities");

        txtlpenglang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpenglang.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpenglang.setText("0");
        txtlpenglang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpenglangKeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Indigeneous Language Activities");

        txtlpindiglang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpindiglang.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpindiglang.setText("0");
        txtlpindiglang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpindiglangKeyReleased(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Mathematical Activities");

        txtlpmath.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpmath.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpmath.setText("0");
        txtlpmath.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpmathKeyReleased(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel36.setText("Environmental Activities");

        txtlpenv.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpenv.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpenv.setText("0");
        txtlpenv.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpenvKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("Religious Activities (CRE/IRE/HRE/PPI) & Life Skills Activities");

        txtlprel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlprel.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlprel.setText("0");
        txtlprel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlprelKeyReleased(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Movement and Creative Activities( Art, Craft, Music and Physics Ed.)");

        txtlpmvmntca.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlpmvmntca.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpmvmntca.setText("0");
        txtlpmvmntca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlpmvmntcaKeyReleased(evt);
            }
        });

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Teacher's Comment", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtlptrcomment.setColumns(20);
        txtlptrcomment.setRows(5);
        jScrollPane4.setViewportView(txtlptrcomment);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 4, Short.MAX_VALUE))
        );

        jPanel15.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel37.setText("Total Marks");

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel38.setText("Average");

        txtlpavg.setEditable(false);
        txtlpavg.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtlpavg.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlpavg.setText("0");

        txtlpgrade.setEditable(false);
        txtlpgrade.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtlpgrade.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel39.setText("GRADE");

        txtlptm.setEditable(false);
        txtlptm.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtlptm.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtlptm.setText("0");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(txtlptm, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtlpavg, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel38)
                        .addGap(106, 106, 106))))
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(171, 171, 171)
                .addComponent(jLabel39)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(txtlpgrade, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(jLabel38))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlpavg)
                    .addComponent(txtlptm, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtlpgrade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        jPanel22.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        txtlpclear.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtlpclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        txtlpclear.setText("CLEAR");
        txtlpclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlpclearActionPerformed(evt);
            }
        });

        btnLPCalculate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnLPCalculate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/calculate.png"))); // NOI18N
        btnLPCalculate.setText("CALCULATE");
        btnLPCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLPCalculateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel22Layout.createSequentialGroup()
                .addContainerGap(62, Short.MAX_VALUE)
                .addComponent(txtlpclear)
                .addGap(42, 42, 42)
                .addComponent(btnLPCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLPCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtlpclear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10)
                                    .addComponent(txtlplit, javax.swing.GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
                                    .addComponent(txtlpksl)
                                    .addComponent(txtlpenglang)
                                    .addComponent(txtlpindiglang))
                                .addGap(31, 31, 31)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtlpmath)
                                    .addComponent(txtlpenv)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel12)
                                            .addComponent(jLabel36)
                                            .addComponent(txtlpmvmntca, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel16))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtlprel, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel15))
                                        .addGap(10, 10, 10))))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpmath, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel36)
                        .addGap(9, 9, 9)
                        .addComponent(txtlpenv, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlprel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(3, 3, 3)
                        .addComponent(txtlplit, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpksl, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpenglang, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpindiglang, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpmvmntca, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(cbolpclass, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(jLabel32)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtlpsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel55)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbolpexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                        .addComponent(jLabel56)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbolpterm, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1264, Short.MAX_VALUE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtlpsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel56)
                    .addComponent(cbolpterm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel55)
                    .addComponent(cbolpexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbolpclass, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Lower Primary (Grade 1 - 3)", jPanel7);

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 0, 0));
        jLabel31.setText("Pupil's Admission No.");

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Pupil Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel40.setText("Pupil's Name");

        txtuppname.setEditable(false);
        txtuppname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtupadmno.setEditable(false);
        txtupadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Admission No.");

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel42.setText("Class");

        txtupclass.setEditable(false);
        txtupclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel43.setText("Current Term");

        txtupcurrterm.setEditable(false);
        txtupcurrterm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40)
                    .addComponent(txtuppname, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41)
                    .addComponent(txtupadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel42)
                    .addComponent(txtupclass, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel43)
                    .addComponent(txtupcurrterm, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtuppname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel43)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtupcurrterm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtupclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtupadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cboupclass.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboupclass.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Class", "Grade 4", "Grade 5", "Grade 6" }));
        cboupclass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboupclassActionPerformed(evt);
            }
        });

        txtupsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupsearchKeyReleased(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel53.setText("Term");

        cboupterm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboupterm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Term", "Term 1", "Term 2", "Term 3" }));
        cboupterm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbouptermActionPerformed(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel54.setText("Exam Type");

        cboupexamtype.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cboupexamtype.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Exam Type", "Opener", "Mid-Term", "End-Term" }));

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Subjects", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        txtupst.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupst.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupst.setText("0");
        txtupst.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupstKeyReleased(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("Creative Arts (Art, Craft, Music)");

        txtupmath.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupmath.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupmath.setText("0");
        txtupmath.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupmathKeyReleased(evt);
            }
        });

        txtupppi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupppi.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupppi.setText("0");
        txtupppi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupppiKeyReleased(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("Agriculture");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("English Language");

        txtupss.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupss.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupss.setText("0");
        txtupss.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupssKeyReleased(evt);
            }
        });

        txtupagric.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupagric.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupagric.setText("0");
        txtupagric.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupagricKeyReleased(evt);
            }
        });

        jPanel12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel28.setText("Total Marks");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel29.setText("Average");

        txtupavg.setEditable(false);
        txtupavg.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtupavg.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupavg.setText("0");

        txtupgrade.setEditable(false);
        txtupgrade.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtupgrade.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel30.setText("GRADE");

        txtuptm.setEditable(false);
        txtuptm.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtuptm.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtuptm.setText("0");

        btnUpClear.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnUpClear.setText("CLEAR");
        btnUpClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpClearActionPerformed(evt);
            }
        });

        btnUpCalculate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpCalculate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/calculate.png"))); // NOI18N
        btnUpCalculate.setText("CALCULATE");
        btnUpCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpCalculateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtuptm, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabel28)))
                        .addGap(62, 62, 62)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addGap(56, 56, 56))
                            .addComponent(txtupavg, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                                .addComponent(jLabel30)
                                .addGap(151, 151, 151))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                                .addComponent(btnUpCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnUpClear)
                                .addGap(29, 29, 29))))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtupgrade, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(98, 98, 98))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtuptm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtupavg, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtupgrade, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpClear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtupksllang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupksllang.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupksllang.setText("0");
        txtupksllang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupksllangKeyReleased(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel26.setText("Religious Activities (CRE/IRE/HRE/PPI)");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setText("Physical & Health Education");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Kiswahili Language Activities / KSL for the Deaf");

        txtupenglang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupenglang.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupenglang.setText("0");
        txtupenglang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupenglangKeyReleased(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setText("Science & Technology");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setText("Pastoral Programme of Instruction (PPI)");

        txtuprel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtuprel.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtuprel.setText("0");
        txtuprel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtuprelKeyReleased(evt);
            }
        });

        txtupphe.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupphe.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupphe.setText("0");
        txtupphe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtuppheKeyReleased(evt);
            }
        });

        txtupca.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupca.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupca.setText("0");
        txtupca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupcaKeyReleased(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setText("Mathematics");

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Home Science");

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Social Studies (Citizenship, Geography, History)");

        txtuphs.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtuphs.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtuphs.setText("0");
        txtuphs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtuphsKeyReleased(evt);
            }
        });

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Teacher's Comment", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtTrComment.setColumns(20);
        txtTrComment.setRows(5);
        txtTrComment.setText("     ");
        jScrollPane3.setViewportView(txtTrComment);

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addGap(59, 59, 59))
                            .addComponent(jLabel18)
                            .addComponent(jLabel17)
                            .addComponent(txtupst)
                            .addComponent(jLabel19)
                            .addComponent(txtupenglang)
                            .addComponent(jLabel21)
                            .addComponent(txtupmath)
                            .addComponent(txtupksllang)
                            .addComponent(txtupss)
                            .addComponent(jLabel22)
                            .addComponent(txtuphs, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel26)
                                    .addComponent(jLabel24)
                                    .addComponent(jLabel25)
                                    .addComponent(jLabel23))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel27)
                                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtupagric, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtuprel, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtupca, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtupphe, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtupppi, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)))
                .addGap(35, 35, 35))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupksllang, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupenglang, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupst, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel20)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupss, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupmath, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel22))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel23)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupagric, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtuprel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel24)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupca, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel25)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtupphe, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(60, 60, 60))
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel27)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtupppi, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtuphs, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(cboupclass, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtupsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel54)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cboupexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel53)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cboupterm, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(16, 16, 16))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboupclass, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(txtupsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel53)
                    .addComponent(cboupterm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel54)
                    .addComponent(cboupexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 1226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, 586, 586, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jTabbedPane2.addTab("Upper Primary (Grade 4 - 6)", jPanel8);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1289, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 617, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Exam Marks", jPanel4);

        cborsltclass.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cborsltclass.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Class", "Pre-Primary 1", "Pre-Primary 2", "Grade 1", "Grade 2", "Grade 3", "Grade 4", "Grade 5", "Grade 6" }));
        cborsltclass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cborsltclassActionPerformed(evt);
            }
        });

        tblrsltlist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblrsltlist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblrsltlistMouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(tblrsltlist);

        btnReportSheet.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnReportSheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnReportSheet.setText("Generate Marks Score Sheet");
        btnReportSheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportSheetActionPerformed(evt);
            }
        });

        btnReportForm.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnReportForm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnReportForm.setText("Generate End Term Report Form");
        btnReportForm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportFormActionPerformed(evt);
            }
        });

        cborsltlistexamtype.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cborsltlistexamtype.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Exam Type", "Opener", "Mid-Term", "End-Term" }));
        cborsltlistexamtype.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cborsltlistexamtypeActionPerformed(evt);
            }
        });

        jLabel69.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel69.setText("Exam Type");

        jLabel70.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel70.setText("Adm. no");

        txtrsltlistsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtrsltlistsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrsltlistsearchKeyReleased(evt);
            }
        });

        jLabel71.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel71.setText("Term");

        cborsltlistpterm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cborsltlistpterm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Term", "Term 1", "Term 2", "Term 3" }));
        cborsltlistpterm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cborsltlistptermActionPerformed(evt);
            }
        });

        txtrsltyear.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtrsltyear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtrsltyearActionPerformed(evt);
            }
        });
        txtrsltyear.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrsltyearKeyReleased(evt);
            }
        });

        jLabel72.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel72.setText("Year");

        btnAcadSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAcadSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search.png"))); // NOI18N
        btnAcadSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcadSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnReportSheet)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReportForm)
                .addGap(232, 232, 232))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane7)
                        .addContainerGap())
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel72)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrsltyear, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel69)
                        .addGap(18, 18, 18)
                        .addComponent(cborsltlistexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel71)
                        .addGap(18, 18, 18)
                        .addComponent(cborsltlistpterm, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(cborsltclass, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(jLabel70)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtrsltlistsearch, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAcadSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cborsltlistexamtype, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel69)
                                .addComponent(jLabel71))
                            .addComponent(cborsltlistpterm)
                            .addComponent(cborsltclass)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtrsltlistsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel70)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtrsltyear, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel72)))
                    .addComponent(btnAcadSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnReportSheet, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportForm, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        jTabbedPane1.addTab("Result List", jPanel5);

        jPanel23.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel75.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel75.setText("Year");

        txtvisyear.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtvisyear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtvisyearActionPerformed(evt);
            }
        });
        txtvisyear.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtvisyearKeyReleased(evt);
            }
        });

        btnVisualCharts.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnVisualCharts.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/visualization.png"))); // NOI18N
        btnVisualCharts.setText("Show Chart");
        btnVisualCharts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVisualChartsActionPerformed(evt);
            }
        });

        btnPrintChart.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPrintChart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/print_sml.png"))); // NOI18N
        btnPrintChart.setText("Print Chart");
        btnPrintChart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintChartActionPerformed(evt);
            }
        });

        btnVisualChartsRefresh.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnVisualChartsRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnVisualChartsRefresh.setText("Refresh");
        btnVisualChartsRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVisualChartsRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel23Layout.createSequentialGroup()
                .addGap(289, 289, 289)
                .addComponent(jLabel75)
                .addGap(18, 18, 18)
                .addComponent(txtvisyear, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
                .addComponent(btnVisualCharts, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(btnPrintChart, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(btnVisualChartsRefresh)
                .addContainerGap())
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtvisyear, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel75))
                    .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnVisualCharts, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPrintChart, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(btnVisualChartsRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelgraphcontainer.setBackground(new java.awt.Color(255, 255, 255));
        panelgraphcontainer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        panelgraphcontainer.setForeground(new java.awt.Color(255, 255, 255));

        jPanel26.setBackground(new java.awt.Color(255, 255, 255));
        jPanel26.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Pre-Primary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N
        jPanel26.setForeground(new java.awt.Color(255, 255, 255));
        jPanel26.setPreferredSize(new java.awt.Dimension(450, 30));
        jPanel26.setLayout(new java.awt.BorderLayout());

        jPanel24.setBackground(new java.awt.Color(255, 255, 255));
        jPanel24.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Lower Primary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N
        jPanel24.setForeground(new java.awt.Color(255, 255, 255));
        jPanel24.setPreferredSize(new java.awt.Dimension(450, 30));
        jPanel24.setLayout(new java.awt.BorderLayout());

        jPanel27.setBackground(new java.awt.Color(255, 255, 255));
        jPanel27.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Upper Primary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N
        jPanel27.setForeground(new java.awt.Color(255, 255, 255));
        jPanel27.setPreferredSize(new java.awt.Dimension(450, 30));
        jPanel27.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout panelgraphcontainerLayout = new javax.swing.GroupLayout(panelgraphcontainer);
        panelgraphcontainer.setLayout(panelgraphcontainerLayout);
        panelgraphcontainerLayout.setHorizontalGroup(
            panelgraphcontainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelgraphcontainerLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        panelgraphcontainerLayout.setVerticalGroup(
            panelgraphcontainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelgraphcontainerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelgraphcontainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                    .addComponent(jPanel24, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelgraphcontainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelgraphcontainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Annual Performance Visualization", jPanel25);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 650, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );

        jlblacademicsloggedinas.setText("currently logged in user code");

        jLabel3.setText("Logged in as:");

        lblacademicstime.setText("time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jlblacademicsloggedinas)
                        .addGap(18, 18, 18)
                        .addComponent(lblacademicstime, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jlblacademicsloggedinas)
                    .addComponent(lblacademicstime))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cborsltlistptermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cborsltlistptermActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cborsltlistptermActionPerformed

    private void cborsltlistexamtypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cborsltlistexamtypeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cborsltlistexamtypeActionPerformed

    private void btnReportFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportFormActionPerformed
        // TODO add your handling code here:
        String tableid = txtrsltlistsearch.getText();
        String examterm = cborsltlistpterm.getSelectedItem().toString();
        String year = txtrsltyear.getText();
        String ppclass;
        
        if (!year.isEmpty() && !tableid.isEmpty()) {
        
            int pclass = cborsltclass.getSelectedIndex();

            switch (pclass) {
                case 1:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Pre_Primary_report.jrxml");
                        String query = "SELECT * FROM `academics_preprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't generate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 2:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Pre_Primary_report.jrxml");
                        String query = "SELECT * FROM `academics_preprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 3:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Lower_primary_report_form.jrxml");
                        String query = "SELECT * FROM `academics_lowerprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 4:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Lower_primary_report_form.jrxml");
                        String query = "SELECT * FROM `academics_lowerprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 5:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Lower_primary_report_form.jrxml");
                        String query = "SELECT * FROM `academics_lowerprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 6:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Upper_Primary_Report.jrxml");
                        String query = "SELECT * FROM `academics_upperprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 7:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Upper_Primary_Report.jrxml");
                        String query = "SELECT * FROM `academics_upperprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                case 8:
                {
                    ppclass = cborsltclass.getSelectedItem().toString();
                    try {
                        JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Upper_Primary_Report.jrxml");
                        String query = "SELECT * FROM `academics_upperprimary` WHERE `admission_no`='"+tableid+"' AND `class`='"+ppclass+"' AND `exam_type`=\"End-Term\" AND `exam_term`='"+examterm+"' AND `year`='"+year+"' ";

                        JRDesignQuery updateQuery = new JRDesignQuery();
                        updateQuery.setText(query);

                        jdesign.setQuery(updateQuery);

                        JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                        JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                        JasperViewer.viewReport(jprint, false);

                    } catch (JRException ex) {
                        JOptionPane.showMessageDialog(null, "Ops!Couldn't genereate the report form. Please fill or select the required fields appropriately.");
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
                default:
                {
                    JOptionPane.showMessageDialog(null, "Please select class.");
                    break;
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please fill all the required fields.");
        }
        
    }//GEN-LAST:event_btnReportFormActionPerformed

    private String getPrepCellValue(int x, int y){
        return prepresultlist.getValueAt(x, y).toString();
    }
    
    private String getLPCellValue(int x, int y){
        return lpresultlist.getValueAt(x, y).toString();
    }
    
    private String getUpCellValue(int x, int y){
        return upresultlist.getValueAt(x, y).toString();
    }
    
    private void PrepSheet(){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();

        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();

        //Add column headers
        data.put("-1", new Object[]{prepresultlist.getColumnName(0),prepresultlist.getColumnName(1),prepresultlist.getColumnName(2), prepresultlist.getColumnName(3), 
            prepresultlist.getColumnName(4), prepresultlist.getColumnName(5), prepresultlist.getColumnName(6), prepresultlist.getColumnName(7),
            prepresultlist.getColumnName(8), prepresultlist.getColumnName(9), prepresultlist.getColumnName(10), prepresultlist.getColumnName(11),
            prepresultlist.getColumnName(12), prepresultlist.getColumnName(13), prepresultlist.getColumnName(14)});

        //Looping through the table rows
        for (int i = 0; i < prepresultlist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getPrepCellValue(i, 0), getPrepCellValue(i, 1), getPrepCellValue(i, 2), getPrepCellValue(i, 3),
            getPrepCellValue(i, 4), getPrepCellValue(i, 5), getPrepCellValue(i, 6),getPrepCellValue(i, 7), getPrepCellValue(i, 8),getPrepCellValue(i, 9),
            getPrepCellValue(i, 10),getPrepCellValue(i, 11),getPrepCellValue(i, 12), getPrepCellValue(i, 13), getPrepCellValue(i, 14)});
        }

        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;

        for (String key : ids) {
            row = ws.createRow(rowID++);

            //Get Data as per Key
            Object[] values = data.get(key);

            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }

        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Pre-Primary Results.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");               
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void LowerPriSheet(){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();

        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();

        //Add column headers
        data.put("-1", new Object[]{lpresultlist.getColumnName(0),lpresultlist.getColumnName(1),lpresultlist.getColumnName(2), lpresultlist.getColumnName(3), 
            lpresultlist.getColumnName(4), lpresultlist.getColumnName(5), lpresultlist.getColumnName(6), lpresultlist.getColumnName(7),
            lpresultlist.getColumnName(8), lpresultlist.getColumnName(9), lpresultlist.getColumnName(10), lpresultlist.getColumnName(11),
            lpresultlist.getColumnName(12), lpresultlist.getColumnName(13), lpresultlist.getColumnName(14),lpresultlist.getColumnName(15),
            lpresultlist.getColumnName(16),lpresultlist.getColumnName(17)
        });

        //Looping through the table rows
        for (int i = 0; i < lpresultlist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getLPCellValue(i, 0), getLPCellValue(i, 1), getLPCellValue(i, 2), getLPCellValue(i, 3),
            getLPCellValue(i, 4), getLPCellValue(i, 5), getLPCellValue(i, 6),getLPCellValue(i, 7), getLPCellValue(i, 8),getLPCellValue(i, 9),
            getLPCellValue(i, 10),getLPCellValue(i, 11),getLPCellValue(i, 12), getLPCellValue(i, 13), getLPCellValue(i, 14),getLPCellValue(i, 15),
            getLPCellValue(i, 16),getLPCellValue(i, 17)
            });
        }

        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;

        for (String key : ids) {
            row = ws.createRow(rowID++);

            //Get Data as per Key
            Object[] values = data.get(key);

            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }

        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Lower_Primary Results.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");               
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void UppSheet(){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();

        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();

        //Add column headers
        data.put("-1", new Object[]{upresultlist.getColumnName(0),upresultlist.getColumnName(1),upresultlist.getColumnName(2), upresultlist.getColumnName(3), 
            upresultlist.getColumnName(4), upresultlist.getColumnName(5), upresultlist.getColumnName(6), upresultlist.getColumnName(7),
            upresultlist.getColumnName(8), upresultlist.getColumnName(9), upresultlist.getColumnName(10), upresultlist.getColumnName(11),
            upresultlist.getColumnName(12), upresultlist.getColumnName(13), upresultlist.getColumnName(14), upresultlist.getColumnName(15),
            upresultlist.getColumnName(16), upresultlist.getColumnName(17), upresultlist.getColumnName(18), upresultlist.getColumnName(19),
            upresultlist.getColumnName(20)});

        //Looping through the table rows
        for (int i = 0; i < upresultlist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getUpCellValue(i, 0), getUpCellValue(i, 1), getUpCellValue(i, 2), getUpCellValue(i, 3),
            getUpCellValue(i, 4), getUpCellValue(i, 5), getUpCellValue(i, 6),getUpCellValue(i, 7), getUpCellValue(i, 8),getUpCellValue(i, 9),
            getUpCellValue(i, 10),getUpCellValue(i, 11),getUpCellValue(i, 12), getUpCellValue(i, 13), getUpCellValue(i, 14),getUpCellValue(i, 15),
            getUpCellValue(i, 16),getUpCellValue(i, 17),getUpCellValue(i, 18), getUpCellValue(i, 19), getUpCellValue(i, 20)});
        }

        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;

        for (String key : ids) {
            row = ws.createRow(rowID++);

            //Get Data as per Key
            Object[] values = data.get(key);

            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }

        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Upper Primary Results.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void btnReportSheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportSheetActionPerformed
        // TODO add your handling code here:
        int rsltpclass = cborsltclass.getSelectedIndex();
        
        switch (rsltpclass) {
            case 1:
            case 2:
                PrepSheet();
                break;
            case 3:
            case 4:
            case 5:
                LowerPriSheet();
                break; 
            case 6:
            case 7:
            case 8:
                UppSheet();
                break;
            default:
                JOptionPane.showMessageDialog(null, "Please select class level.");
                break;
        }
    }//GEN-LAST:event_btnReportSheetActionPerformed

    private void cborsltclassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cborsltclassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cborsltclassActionPerformed

    private void btnUpCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpCalculateActionPerformed
        // TODO add your handling code here:
        if (cboupexamtype.getSelectedIndex() != 0 && cboupterm.getSelectedIndex() != 0 && !txtuppname.getText().isEmpty()
                && !txtupadmno.getText().isEmpty() && !txtupclass.getText().isEmpty() && !txtupcurrterm.getText().isEmpty() &&
                txtupksllang.getText().matches("[0-9]+") && txtupenglang.getText().matches("[0-9]+") && txtupst.getText().matches("[0-9]+")
                && txtupss.getText().matches("[0-9]+") && txtupmath.getText().matches("[0-9]+") && txtuphs.getText().matches("[0-9]+")
                && txtupagric.getText().matches("[0-9]+") && txtuprel.getText().matches("[0-9]+") && txtupca.getText().matches("[0-9]+")
                && txtupphe.getText().matches("[0-9]+") && txtupppi.getText().matches("[0-9]+")) {
            try {
                int ksl = Integer.parseInt(txtupksllang.getText());
                int eng = Integer.parseInt(txtupenglang.getText());
                int scientech = Integer.parseInt(txtupst.getText());
                int ss = Integer.parseInt(txtupss.getText());
                int math = Integer.parseInt(txtupmath.getText());
                int hs = Integer.parseInt(txtuphs.getText());
                int agric = Integer.parseInt(txtupagric.getText());
                int rel = Integer.parseInt(txtuprel.getText());
                int ca = Integer.parseInt(txtupca.getText());
                int phe = Integer.parseInt(txtupphe.getText());
                int ppi = Integer.parseInt(txtupppi.getText());

                int totalmarks = ksl + eng + scientech + ss + math + hs + agric + rel + ca + phe + ppi;

                txtuptm.setText(String.valueOf(totalmarks));

                double avg = totalmarks / 11;
                txtupavg.setText(String.valueOf(avg));

                if (avg >= 80) {
                    txtupgrade.setText("A");
                } else if(avg >= 70){
                    txtupgrade.setText("B+");
                }else if(avg >= 60){
                    txtupgrade.setText("B");
                }else if(avg >= 50){
                    txtupgrade.setText("C+");
                }else if(avg >= 40){
                    txtupgrade.setText("C");
                }else if(avg >= 30){
                    txtupgrade.setText("C-");
                }else if(avg >= 20){
                    txtupgrade.setText("D+");
                }else if(avg >= 10){
                    txtupgrade.setText("D+");
                }else{
                    txtupgrade.setText("E");
                }            

                int examtype = cboupexamtype.getSelectedIndex();

                switch (examtype) {
                    case 1:
                    try {
                        String sql = "INSERT INTO `academics_upperprimary`(`exam_type`,`pupils_name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `oksl_ksl_deaf`, `oeng_lang`, "
                        + "`osci_tech`, `osst`, `omath`, `ohome_science`, `oagric`, `orel_activities`, `ocreative_arts`, `ophysical_health_edu`, `oppi`, "
                        + "`ototal_marks`, `oaverage`, `ograde`, `otr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cboupexamtype.getSelectedItem().toString());
                        pst.setString(2, txtuppname.getText());
                        pst.setString(3, txtupadmno.getText());
                        pst.setString(4, txtupclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtupcurrterm.getText());
                        pst.setString(7, cboupterm.getSelectedItem().toString());
                        pst.setInt(8, ksl);
                        pst.setInt(9, eng);
                        pst.setInt(10, scientech);
                        pst.setInt(11, ss);
                        pst.setInt(12, math);
                        pst.setInt(13, hs);
                        pst.setInt(14, agric);
                        pst.setInt(15, rel);
                        pst.setInt(16, ca);
                        pst.setInt(17, phe);
                        pst.setInt(18, ppi);
                        pst.setInt(19, Integer.parseInt(txtuptm.getText()));
                        pst.setDouble(20, Double.parseDouble(txtupavg.getText()));
                        pst.setString(21, txtupgrade.getText());
                        pst.setString(22, txtTrComment.getText());

                        if (!txtTrComment.getText().isEmpty()) {
                            pst.execute();
                        } else {
                            JOptionPane.showMessageDialog(null, "Teacher's comment required.");
                        }
                        

                        JOptionPane.showMessageDialog(null, "Opener saved");

                        clearUP();

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;
                    case 2:
                    try {
                        String sql = "INSERT INTO `academics_upperprimary`(`exam_type`,`pupils_name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `mksl_ksl_deaf`, `meng_lang`, "
                        + "`msci_tech`, `msst`, `mmath`, `mhome_science`, `magric`, `mrel_activities`, `mcreative_arts`, `mphysical_health_edu`, `mppi`, "
                        + "`mtotal_marks`, `maverage`, `mgrade`, `mtr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cboupexamtype.getSelectedItem().toString());
                        pst.setString(2, txtuppname.getText());
                        pst.setString(3, txtupadmno.getText());
                        pst.setString(4, txtupclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtupcurrterm.getText());
                        pst.setString(7, cboupterm.getSelectedItem().toString());
                        pst.setInt(8, ksl);
                        pst.setInt(9, eng);
                        pst.setInt(10, scientech);
                        pst.setInt(11, ss);
                        pst.setInt(12, math);
                        pst.setInt(13, hs);
                        pst.setInt(14, agric);
                        pst.setInt(15, rel);
                        pst.setInt(16, ca);
                        pst.setInt(17, phe);
                        pst.setInt(18, ppi);
                        pst.setInt(19, Integer.parseInt(txtuptm.getText()));
                        pst.setDouble(20, Double.parseDouble(txtupavg.getText()));
                        pst.setString(21, txtupgrade.getText());
                        pst.setString(22, txtTrComment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "Mid-Term Saved");

                        clearUP();

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;

                    case 3:
                    try {
                        String sql = "INSERT INTO `academics_upperprimary`(`exam_type`,`pupils_name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `ksl_ksl_deaf`, `eng_lang`, "
                        + "`sci_tech`, `sst`, `math`, `home_science`, `agric`, `rel_activities`, `creative_arts`, `physical_health_edu`, `ppi`, "
                        + "`total_marks`, `average`, `grade`, `tr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cboupexamtype.getSelectedItem().toString());
                        pst.setString(2, txtuppname.getText());
                        pst.setString(3, txtupadmno.getText());
                        pst.setString(4, txtupclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtupcurrterm.getText());
                        pst.setString(7, cboupterm.getSelectedItem().toString());
                        pst.setInt(8, ksl);
                        pst.setInt(9, eng);
                        pst.setInt(10, scientech);
                        pst.setInt(11, ss);
                        pst.setInt(12, math);
                        pst.setInt(13, hs);
                        pst.setInt(14, agric);
                        pst.setInt(15, rel);
                        pst.setInt(16, ca);
                        pst.setInt(17, phe);
                        pst.setInt(18, ppi);
                        pst.setInt(19, Integer.parseInt(txtuptm.getText()));
                        pst.setDouble(20, Double.parseDouble(txtupavg.getText()));
                        pst.setString(21, txtupgrade.getText());
                        pst.setString(22, txtTrComment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "End term saved");

                        clearUP();

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;

                    default:
                        JOptionPane.showMessageDialog(null, "Please select exam type");
                       break;
                }

            } catch (NumberFormatException e) {
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                //JOptionPane.showMessageDialog(null, "Invalid characters.");
            }
        
        } else {
            JOptionPane.showMessageDialog(null, "Some fields are empty or contain Invalid characters. Please fill or select as required.");
        }
        
    }//GEN-LAST:event_btnUpCalculateActionPerformed

    private void btnUpClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpClearActionPerformed
        // TODO add your handling code here:
        clearUP();
    }//GEN-LAST:event_btnUpClearActionPerformed

    private void txtupsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupsearchKeyReleased
        // TODO add your handling code here:
        int upclass = cboupclass.getSelectedIndex();

        String pclass = "";
        switch (upclass) {
            case 1:
            pclass = cboupclass.getSelectedItem().toString();
            break;
            case 2:
            pclass = cboupclass.getSelectedItem().toString();
            break;
            case 3:
            pclass = cboupclass.getSelectedItem().toString();
            break;
            default:
            JOptionPane.showMessageDialog(null, "Please select class.");
            txtupsearch.setText("");
            break;
        }

        if (txtupsearch.getText().isEmpty()) {
            txtuppname.setText("");
            txtupadmno.setText("");
            txtupclass.setText("");
            txtupcurrterm.setText("");
            clearLP();

        } else {
            try {
                String sql = "SELECT * FROM `admissions` WHERE `admno`=? AND `curr_class`='"+pclass+"' AND `status`=\"1\"  ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtupsearch.getText().toUpperCase());
                rs = pst.executeQuery();

                while (rs.next()) {
                    txtuppname.setText(rs.getString("name"));
                    txtupadmno.setText(rs.getString("admno"));
                    txtupclass.setText(rs.getString("curr_class"));
                    txtupcurrterm.setText(rs.getString("curr_term"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
            }finally{
                try {
                    rs.close();
                    pst.close();
                } catch (SQLException e) {
                    Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }//GEN-LAST:event_txtupsearchKeyReleased

    private void btnLPCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLPCalculateActionPerformed
        // TODO add your handling code here:
        if (cbolpexamtype.getSelectedIndex() != 0 && cbolpterm.getSelectedIndex() != 0 && !txtlptrcomment.getText().isEmpty() && !txtlppname.getText().isEmpty()
                && !txtlpadmno.getText().isEmpty() && !txtlpclass.getText().isEmpty() && !txtlpterm.getText().isEmpty()
                && txtlplit.getText().matches("[0-9]+") && txtlpksl.getText().matches("[0-9]+") && txtlpenglang.getText().matches("[0-9]+")
                && txtlpindiglang.getText().matches("[0-9]+") && txtlpmath.getText().matches("[0-9]+") && txtlpenv.getText().matches("[0-9]+")
                && txtlprel.getText().matches("[0-9]+") && txtlpmvmntca.getText().matches("[0-9]+") ) {
            try {
                int literacy = Integer.parseInt(txtlplit.getText());
                int ksllang = Integer.parseInt(txtlpksl.getText());
                int englang = Integer.parseInt(txtlpenglang.getText());
                int indigla = Integer.parseInt(txtlpindiglang.getText());
                int math = Integer.parseInt(txtlpmath.getText());
                int envva = Integer.parseInt(txtlpenv.getText());
                int relact = Integer.parseInt(txtlprel.getText());
                int mvmntca = Integer.parseInt(txtlpmvmntca.getText());

                int totalmarks = literacy + ksllang + englang + indigla + math + envva + relact + mvmntca;

                txtlptm.setText(String.valueOf(totalmarks));

                double avg = totalmarks / 8;
                txtlpavg.setText(String.valueOf(avg));

                if (avg >= 80) {
                    txtlpgrade.setText("A");
                } else if(avg >= 70){
                    txtlpgrade.setText("B+");
                }else if(avg >= 60){
                    txtlpgrade.setText("B");
                }else if(avg >= 50){
                    txtlpgrade.setText("C+");
                }else if(avg >= 40){
                    txtlpgrade.setText("C");
                }else if(avg >= 30){
                    txtlpgrade.setText("C-");
                }else if(avg >= 20){
                    txtlpgrade.setText("D+");
                }else if(avg >= 10){
                    txtlpgrade.setText("D+");
                }else{
                    txtlpgrade.setText("E");
                }

                int examtype = cbolpexamtype.getSelectedIndex();

                switch (examtype) {
                    case 1:
                    try {
                        String sql = "INSERT INTO `academics_lowerprimary`(`exam_type`, `name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `oliteracy`, `oksl_ksl_deaf`, "
                        + "`oeng_lang`, `oindigenous_actv`, `omath_actv`, `oenv_actv`, `orel_actv`, `omvmnt_creative_actv`, `ototal_mrks`, `oaverage`, "
                        + "`ograde`, `otr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cbolpexamtype.getSelectedItem().toString());
                        pst.setString(2, txtlppname.getText());
                        pst.setString(3, txtlpadmno.getText());
                        pst.setString(4, txtlpclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtlpterm.getText());
                        pst.setString(7, cbolpterm.getSelectedItem().toString());
                        pst.setInt(8, literacy);
                        pst.setInt(9, ksllang);
                        pst.setInt(10, englang);
                        pst.setInt(11, indigla);
                        pst.setInt(12, math);
                        pst.setInt(13, envva);
                        pst.setInt(14, relact);
                        pst.setInt(15, mvmntca);
                        pst.setInt(16, Integer.parseInt(txtlptm.getText()));
                        pst.setDouble(17, Double.parseDouble(txtlpavg.getText()));
                        pst.setString(18, txtlpgrade.getText());
                        pst.setString(19, txtlptrcomment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "Opener saved");

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;
                    case 2:
                    try {
                        String sql = "INSERT INTO `academics_lowerprimary`(`exam_type`, `name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `mliteracy`, `mksl_ksl_deaf`, "
                        + "`meng_lang`, `mindigenous_actv`, `mmath_actv`, `menv_actv`, `mrel_actv`, `mmvmnt_creative_actv`, `mtotal_mrks`, `maverage`, "
                        + "`mgrade`, `mtr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cbolpexamtype.getSelectedItem().toString());
                        pst.setString(2, txtlppname.getText());
                        pst.setString(3, txtlpadmno.getText());
                        pst.setString(4, txtlpclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtlpterm.getText());
                        pst.setString(7, cbolpterm.getSelectedItem().toString());
                        pst.setInt(8, literacy);
                        pst.setInt(9, ksllang);
                        pst.setInt(10, englang);
                        pst.setInt(11, indigla);
                        pst.setInt(12, math);
                        pst.setInt(13, envva);
                        pst.setInt(14, relact);
                        pst.setInt(15, mvmntca);
                        pst.setInt(16, Integer.parseInt(txtlptm.getText()));
                        pst.setDouble(17, Double.parseDouble(txtlpavg.getText()));
                        pst.setString(18, txtlpgrade.getText());
                        pst.setString(19, txtlptrcomment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "Mid-Term saved");

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;

                    case 3:
                    try {
                        String sql = "INSERT INTO `academics_lowerprimary`(`exam_type`, `name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `literacy`, `ksl_ksl_deaf`, "
                        + "`eng_lang`, `indigenous_actv`, `math_actv`, `env_actv`, `rel_actv`, `mvmnt_creative_actv`, `total_mrks`, `average`, "
                        + "`grade`, `tr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cbolpexamtype.getSelectedItem().toString());
                        pst.setString(2, txtlppname.getText());
                        pst.setString(3, txtlpadmno.getText());
                        pst.setString(4, txtlpclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtlpterm.getText());
                        pst.setString(7, cbolpterm.getSelectedItem().toString());
                        pst.setInt(8, literacy);
                        pst.setInt(9, ksllang);
                        pst.setInt(10, englang);
                        pst.setInt(11, indigla);
                        pst.setInt(12, math);
                        pst.setInt(13, envva);
                        pst.setInt(14, relact);
                        pst.setInt(15, mvmntca);
                        pst.setInt(16, Integer.parseInt(txtlptm.getText()));
                        pst.setDouble(17, Double.parseDouble(txtlpavg.getText()));
                        pst.setString(18, txtlpgrade.getText());
                        pst.setString(19, txtlptrcomment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "End Term saved");

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;

                    default:
                        JOptionPane.showMessageDialog(null, "Please select exam type");
                       break;
                }

                } catch (NumberFormatException e) {
                    Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                }
        
        } else {
            JOptionPane.showMessageDialog(null, "Some fields are empty or contain Invalid Characters. Please fill or select as required.");
        }
    }//GEN-LAST:event_btnLPCalculateActionPerformed

    private void txtlpclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlpclearActionPerformed
        // TODO add your handling code here:
        clearLP();
    }//GEN-LAST:event_txtlpclearActionPerformed

    private void cbolpclassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbolpclassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbolpclassActionPerformed

    private void txtlpsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpsearchKeyReleased
        // TODO add your handling code here:
        int lpclass = cbolpclass.getSelectedIndex();

        String pclass = "";
        switch (lpclass) {
            case 1:
            pclass = cbolpclass.getSelectedItem().toString();
            break;
            case 2:
            pclass = cbolpclass.getSelectedItem().toString();
            break;
            case 3:
            pclass = cbolpclass.getSelectedItem().toString();
            break;
            default:
            JOptionPane.showMessageDialog(null, "Please select class.");
            txtlpsearch.setText("");
            break;
        }

        if (txtlpsearch.getText().isEmpty()) {
            txtlppname.setText("");
            txtlpadmno.setText("");
            txtlpclass.setText("");
            txtlpterm.setText("");
            clearLP();

        } else {
            try {
                String sql = "SELECT * FROM `admissions` WHERE `admno`=? AND `curr_class`='"+pclass+"' AND `status`=\"1\"  ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtlpsearch.getText().toUpperCase());
                rs = pst.executeQuery();

                while (rs.next()) {
                    txtlppname.setText(rs.getString("name"));
                    txtlpadmno.setText(rs.getString("admno"));
                    txtlpclass.setText(rs.getString("curr_class"));
                    txtlpterm.setText(rs.getString("curr_term"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
            }finally{
                try {
                    rs.close();
                    pst.close();
                } catch (SQLException e) {
                    Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }//GEN-LAST:event_txtlpsearchKeyReleased

    private void btnPrepCalcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrepCalcActionPerformed
        // TODO add your handling code here:
        if (cboppexamtype.getSelectedIndex() != 0 && cboppterm.getSelectedIndex() != 0 && !txtprepcomment.getText().isEmpty() && !txtpreppname.getText().isEmpty()
                && !txtprepadmno.getText().isEmpty() && !txtprepclass.getText().isEmpty() && !txtprepcurrterm.getText().isEmpty()
                && txtpplang.getText().matches("[0-9]+") && txtppmath.getText().matches("[0-9]+") && txtppenv.getText().matches("[0-9]+")
                && txtpppmotor.getText().matches("[0-9]+") && txtprprel.getText().matches("[0-9]+")) {
            try {
                int lang = Integer.parseInt(txtpplang.getText());
                int math = Integer.parseInt(txtppmath.getText());
                int env = Integer.parseInt(txtppenv.getText());
                int pca = Integer.parseInt(txtpppmotor.getText());
                int ra = Integer.parseInt(txtprprel.getText());

                int totalmarks = lang + math + env + pca + ra;

                txtpptm.setText(String.valueOf(totalmarks));

                double avg = totalmarks / 5;
                txtppavg.setText(String.valueOf(avg));

                if (avg >= 80) {
                    txtppgrade.setText("A");
                } else if(avg >= 70){
                    txtppgrade.setText("B+");
                }else if(avg >= 60){
                    txtppgrade.setText("B");
                }else if(avg >= 50){
                    txtppgrade.setText("C+");
                }else if(avg >= 40){
                    txtppgrade.setText("C");
                }else if(avg >= 30){
                    txtppgrade.setText("C-");
                }else if(avg >= 20){
                    txtppgrade.setText("D+");
                }else if(avg >= 10){
                    txtppgrade.setText("D-");
                }else{
                    txtppgrade.setText("E");
                }

                int examtype = cboppexamtype.getSelectedIndex();

                switch (examtype) {
                    case 1:
                    try {
                        String sql = "INSERT INTO `academics_preprimary`(`exam_type`, `name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, "
                        + "`olang_actv`, `omath_actv`, `oenv_actv`, `opsychomotor_actv`, `orel_actv`, `ototal_mrks`, `oaverage`, `ograde`, "
                        + "`otr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cboppexamtype.getSelectedItem().toString());
                        pst.setString(2, txtpreppname.getText());
                        pst.setString(3, txtprepadmno.getText());
                        pst.setString(4, txtprepclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtprepcurrterm.getText());
                        pst.setString(7, cboppterm.getSelectedItem().toString());
                        pst.setInt(8, lang);
                        pst.setInt(9, math);
                        pst.setInt(10, env);
                        pst.setInt(11, pca);
                        pst.setInt(12, ra);
                        pst.setInt(13, Integer.parseInt(txtpptm.getText()));
                        pst.setDouble(14, Double.parseDouble(txtppavg.getText()));
                        pst.setString(15, txtppgrade.getText());
                        pst.setString(16, txtprepcomment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "Opener marks saved");

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;
                    case 2:
                    try {
                        String sql = "INSERT INTO `academics_preprimary`(`exam_type`, `name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `mlang_actv`, `mmath_actv`, `menv_actv`,"
                        + " `mpsychomotor_actv`, `mrel_actv`, `mtotal_mrks`, `maverage`, `mgrade`, `mtr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cboppexamtype.getSelectedItem().toString());
                        pst.setString(2, txtpreppname.getText());
                        pst.setString(3, txtprepadmno.getText());
                        pst.setString(4, txtprepclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtprepcurrterm.getText());
                        pst.setString(7, cboppterm.getSelectedItem().toString());
                        pst.setInt(8, lang);
                        pst.setInt(9, math);
                        pst.setInt(10, env);
                        pst.setInt(11, pca);
                        pst.setInt(12, ra);
                        pst.setInt(13, Integer.parseInt(txtpptm.getText()));
                        pst.setDouble(14, Double.parseDouble(txtppavg.getText()));
                        pst.setString(15, txtppgrade.getText());
                        pst.setString(16, txtprepcomment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "Mid-term marks saved");

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;
                    
                    case 3:
                    try {
                        String sql = "INSERT INTO `academics_preprimary`(`exam_type`, `name`, `admission_no`, `class`, `year`, `curr_term`, `exam_term`, `lang_actv`, `math_actv`, `env_actv`,"
                        + " `psychomotor_actv`, `rel_actv`, `total_mrks`, `average`, `grade`, `tr_comment`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, cboppexamtype.getSelectedItem().toString());
                        pst.setString(2, txtpreppname.getText());
                        pst.setString(3, txtprepadmno.getText());
                        pst.setString(4, txtprepclass.getText());
                        pst.setInt(5, yr);
                        pst.setString(6, txtprepcurrterm.getText());
                        pst.setString(7, cboppterm.getSelectedItem().toString());
                        pst.setInt(8, lang);
                        pst.setInt(9, math);
                        pst.setInt(10, env);
                        pst.setInt(11, pca);
                        pst.setInt(12, ra);
                        pst.setInt(13, Integer.parseInt(txtpptm.getText()));
                        pst.setDouble(14, Double.parseDouble(txtppavg.getText()));
                        pst.setString(15, txtppgrade.getText());
                        pst.setString(16, txtprepcomment.getText());

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "End term marks saved");

                    } catch (SQLException e) {
                        Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                    }   break;
                    
                    default:
                        JOptionPane.showMessageDialog(null, "Please select exam type");
                       break;
                }

            } catch (NumberFormatException e) {
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
            }
        
        } else {
            JOptionPane.showMessageDialog(null, "Some fields are empty or contain Invalid characters. Please fill or select as required.");
        }
    }//GEN-LAST:event_btnPrepCalcActionPerformed

    private void btnPrepClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrepClearActionPerformed
        // TODO add your handling code here:
        clearPrep();
    }//GEN-LAST:event_btnPrepClearActionPerformed

    private void cboppclassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboppclassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboppclassActionPerformed

    private void txtprepsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprepsearchKeyReleased
        // TODO add your handling code here:
        int prepclass = cboppclass.getSelectedIndex();

        String pclass = "";
        switch (prepclass) {
            case 1:
            pclass = cboppclass.getSelectedItem().toString();
            break;
            case 2:
            pclass = cboppclass.getSelectedItem().toString();
            break;
            default:
            JOptionPane.showMessageDialog(null, "Please select class.");
            txtprepsearch.setText("");
            break;
        }

        if (txtprepsearch.getText().isEmpty()) {
            txtpreppname.setText("");
            txtprepadmno.setText("");
            txtprepclass.setText("");
            txtprepcurrterm.setText("");
            clearPrep();

        } else {
            try {
                String sql = "SELECT * FROM `admissions` WHERE `admno`=? AND `curr_class`='"+pclass+"' AND `status`=\"1\" ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtprepsearch.getText().toUpperCase());
                rs = pst.executeQuery();

                while (rs.next()) {
                    txtpreppname.setText(rs.getString("name"));
                    txtprepadmno.setText(rs.getString("admno"));
                    txtprepclass.setText(rs.getString("curr_class"));
                    txtprepcurrterm.setText(rs.getString("curr_term"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
            }finally{
                try {
                    rs.close();
                    pst.close();
                } catch (SQLException e) {
                    Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }//GEN-LAST:event_txtprepsearchKeyReleased

    private void tblrsltlistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblrsltlistMouseClicked
        // TODO add your handling code here:
        JTable table = (JTable) evt.getSource();
        int selectedRow = table.getSelectedRow();
        int selectedColumn = table.getSelectedColumn();
        String selectedCellValue = (String) table.getValueAt(selectedRow, selectedColumn);
        txtrsltlistsearch.setText(selectedCellValue);
    }//GEN-LAST:event_tblrsltlistMouseClicked

    private void cboupclassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboupclassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboupclassActionPerformed

    private void txtvisyearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtvisyearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtvisyearActionPerformed

    private void txtvisyearKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtvisyearKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtvisyearKeyReleased

    int prept1sum, prept2sum,prept3sum,lpt1sum,lpt2sum,lpt3sum,upt1sum,upt2sum,upt3sum;
    private void PrepAnnualTotalMarks(){
        try {
            String t1sql = "SELECT SUM(`total_mrks`) AS totalmrks FROM `academics_preprimary` WHERE `year`='"+txtvisyear.getText()+"' "
                    + "AND `exam_term`=\"Term 1\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t1sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                prept1sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            //JOptionPane.showMessageDialog(null, prept1sum);
            
            String t2sql = "SELECT SUM(`total_mrks`) AS totalmrks FROM `academics_preprimary` WHERE `year`='"+txtvisyear.getText()+"' "
                    + "AND `exam_term`=\"Term 2\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t2sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                prept2sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
            String t3sql = "SELECT SUM(`total_mrks`) AS totalmrks FROM `academics_preprimary` WHERE `year`='"+txtvisyear.getText()+"' "
                    + "AND `exam_term`=\"Term 3\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t3sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                prept3sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ops! No data found about that year.");
            Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DefaultCategoryDataset prepdataset = new DefaultCategoryDataset();
        prepdataset.setValue(prept1sum, "Term 1", "Total Score");
        prepdataset.setValue(prept2sum, "Term 2", "Total Score");
        prepdataset.setValue(prept3sum, "Term 3", "Total Score");

        JFreeChart barChart = ChartFactory.createBarChart("Pre-Primary Annual Score Per Term", "Term", "Annual Total Score", prepdataset, PlotOrientation.VERTICAL, false,true,false);
        CategoryPlot barChrt = barChart.getCategoryPlot();
        barChrt.setRangeGridlinePaint(Color.ORANGE);
        
        ChartPanel barPanel = new ChartPanel(barChart);
        barPanel.setPreferredSize(new Dimension(450,30));
        jPanel26.removeAll();
        jPanel26.add(barPanel, BorderLayout.CENTER);
        jPanel26.validate();
        
    }
    
    private void LPAnnualTotalMarks(){
        try {
            String t1sql = "SELECT SUM(`total_mrks`) AS totalmrks FROM `academics_lowerprimary` WHERE `year`='"+txtvisyear.getText()+"' "
                    + "AND `exam_term`=\"Term 1\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t1sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                lpt1sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
            String t2sql = "SELECT SUM(`total_mrks`) AS totalmrks FROM `academics_lowerprimary` WHERE `year`='"+txtvisyear.getText()+"' "
                    + "AND `exam_term`=\"Term 2\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t2sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                lpt2sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
            String t3sql = "SELECT SUM(`total_mrks`) AS totalmrks FROM `academics_lowerprimary` WHERE `year`='"+txtvisyear.getText()+"' "
                    + "AND `exam_term`=\"Term 3\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t3sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                lpt3sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ops! No data found about that year.");
            Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DefaultCategoryDataset prepdataset = new DefaultCategoryDataset();
        prepdataset.setValue(lpt1sum, "Term 1", "Total Score");
        prepdataset.setValue(lpt2sum, "Term 2", "Total Score");
        prepdataset.setValue(lpt3sum, "Term 3", "Total Score");

        JFreeChart barChart = ChartFactory.createBarChart("Lower Primary Annual Score Per Term", "Term", "Annual Total Score", prepdataset, PlotOrientation.VERTICAL, false,true,false);
        CategoryPlot barChrt = barChart.getCategoryPlot();
        barChrt.setRangeGridlinePaint(Color.ORANGE);
        
        ChartPanel barPanel = new ChartPanel(barChart);
        barPanel.setPreferredSize(new Dimension(450,30));
        jPanel24.removeAll();
        jPanel24.add(barPanel, BorderLayout.CENTER);
        jPanel24.validate();
        
    }
    
    private void UPAnnualTotalMarks(){
        try {
            String t1sql = "SELECT SUM(`total_marks`) AS totalmrks FROM `academics_upperprimary` WHERE `year`='"+txtvisyear.getText()+"'"
                    + " AND `exam_term`=\"Term 1\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t1sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                upt1sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
            String t2sql = "SELECT SUM(`total_marks`) AS totalmrks FROM `academics_upperprimary` WHERE `year`='"+txtvisyear.getText()+"'"
                    + " AND `exam_term`=\"Term 2\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t2sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                upt2sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
            String t3sql = "SELECT SUM(`total_marks`) AS totalmrks FROM `academics_upperprimary` WHERE `year`='"+txtvisyear.getText()+"'"
                    + " AND `exam_term`=\"Term 3\" AND `exam_type`=\"End-Term\" ";
            pst = conn.prepareStatement(t3sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                upt3sum = Integer.parseInt(rs.getString("totalmrks"));
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ops! No data found about that year.");
            Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DefaultCategoryDataset prepdataset = new DefaultCategoryDataset();
        prepdataset.setValue(upt1sum, "Term 1", "Total Score");
        prepdataset.setValue(upt2sum, "Term 2", "Total Score");
        prepdataset.setValue(upt3sum, "Term 3", "Total Score");

        JFreeChart barChart = ChartFactory.createBarChart("Upper Primary Annual Score Per Term", "Term", "Annual Total Score", prepdataset, PlotOrientation.VERTICAL, false,true,false);
        CategoryPlot barChrt = barChart.getCategoryPlot();
        barChrt.setRangeGridlinePaint(Color.ORANGE);
        
        ChartPanel barPanel = new ChartPanel(barChart);
        barPanel.setPreferredSize(new Dimension(450,30));
        jPanel27.removeAll();
        jPanel27.add(barPanel, BorderLayout.CENTER);
        jPanel27.validate();
        
    }
    
    private void btnVisualChartsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVisualChartsActionPerformed
        if (!txtvisyear.getText().isEmpty()) {
            try {
                PrepAnnualTotalMarks();
                LPAnnualTotalMarks();
                UPAnnualTotalMarks();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ops! No data on that year.");
                jPanel27.removeAll();
                jPanel24.removeAll();
                jPanel26.removeAll();
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please enter the year.");
        }       
    }//GEN-LAST:event_btnVisualChartsActionPerformed

    private void btnPrintChartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintChartActionPerformed
        // TODO add your handling code here:
        PrinterJob pjob = PrinterJob.getPrinterJob();
        pjob.setJobName("Annual Performance Chart");
        
        pjob.setPrintable((Graphics pg, PageFormat pf, int pageNum) -> {
            pf.setOrientation(PageFormat.LANDSCAPE);
            
            if (pageNum > 0) {
                return Printable.NO_SUCH_PAGE;                
            }
            
            java.awt.Graphics2D g2 = (java.awt.Graphics2D) pg;
            g2.translate(pf.getImageableX(), pf.getImageableY());
            g2.scale(0.65, 0.97);
            
            panelgraphcontainer.paint(g2);
            
            return Printable.PAGE_EXISTS;
        });
        
        if (pjob.printDialog() == false) {
            return;
        }
        
        try {
            pjob.print();
        } catch (PrinterException e) {
            Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }//GEN-LAST:event_btnPrintChartActionPerformed

    private void txtrsltyearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtrsltyearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtrsltyearActionPerformed

    private void txtrsltyearKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrsltyearKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtrsltyearKeyReleased

    private void txtrsltlistsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrsltlistsearchKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtrsltlistsearchKeyReleased

    private void btnVisualChartsRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVisualChartsRefreshActionPerformed
        // TODO add your handling code here:
        try {
            PrepAnnualTotalMarks();
            LPAnnualTotalMarks();
            UPAnnualTotalMarks();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ops! No data on that year.");
            jPanel27.removeAll();
            jPanel24.removeAll();
            jPanel26.removeAll();
            Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btnVisualChartsRefreshActionPerformed

    private void btnAcadSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcadSearchActionPerformed
        // TODO add your handling code here:
        int et = cborsltlistexamtype.getSelectedIndex();
        int st = cborsltlistpterm.getSelectedIndex();
        int sc = cborsltclass.getSelectedIndex();
        if (et != 0 && st != 0 && sc != 0 ) {
            switch (sc) {
                case 1:
                    {
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(PrePrimaryResultListTable());
                        break;
                    }
                case 2:
                    {
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(PrePrimaryResultListTable());
                        break;
                    }
                case 3:
                    {
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(LowerPrimaryResultListTable());
                        break;
                    }
                
                case 4:
                    {
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(LowerPrimaryResultListTable());
                        break;
                    }
                    
                case 5:
                    {
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(LowerPrimaryResultListTable());
                        break;
                    }
                    
                case 6:
                    {                        
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(UpperPrimaryResultListTable());
                        break;
                    }
                    
                case 7:
                    {                        
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(UpperPrimaryResultListTable());
                        break;
                    }
                    
                case 8:
                    {                        
                        DefaultTableModel model = (DefaultTableModel)tblrsltlist.getModel();
                        model.setColumnCount(0);
                        model.setRowCount(0);
                        tblrsltlist.setModel(UpperPrimaryResultListTable());
                        break;
                    }
                    
                default:
                    JOptionPane.showMessageDialog(null, "Ops! Nothing found.");
                    break;
            }

        } else {
            JOptionPane.showMessageDialog(null, "Please provide all the fields.");
        }
    }//GEN-LAST:event_btnAcadSearchActionPerformed

    private void txtpplangKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpplangKeyReleased
        // TODO add your handling code here:
        if (!txtpplang.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numbers allowed!");
            txtpplang.setText("0");
        }else{
            int idlen = Integer.parseInt(txtpplang.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtpplang.setText("0");
            }else{
                System.out.println("All good!");
            }
        }

    }//GEN-LAST:event_txtpplangKeyReleased

    private void txtppenvKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtppenvKeyReleased
        // TODO add your handling code here:
        //int lang = Integer.parseInt(txtppenv.getText());
        
        if (!txtppenv.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtppenv.setText("0");
        }else{
            int idlen = Integer.parseInt(txtppenv.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtppenv.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtppenvKeyReleased

    private void txtprprelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprprelKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtprprel.getText());
        
        if (!txtprprel.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtprprel.setText("0");
        }else{
            int idlen = Integer.parseInt(txtprprel.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtprprel.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtprprelKeyReleased

    private void txtpppmotorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpppmotorKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtpppmotor.getText());
        
        if (!txtpppmotor.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtpppmotor.setText("0");
        }else{
            int idlen = Integer.parseInt(txtpppmotor.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtpppmotor.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtpppmotorKeyReleased

    private void txtppmathKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtppmathKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtppmath.getText());
        
        if (!txtppmath.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtppmath.setText("0");
        }else{
            int idlen = Integer.parseInt(txtppmath.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtppmath.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtppmathKeyReleased

    private void txtlplitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlplitKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlplit.getText());
        
        if (!txtlplit.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlplit.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlplit.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlplit.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlplitKeyReleased

    private void txtlpkslKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpkslKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlpksl.getText());
        
        if (!txtlpksl.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlpksl.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlpksl.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlpksl.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlpkslKeyReleased

    private void txtlpenglangKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpenglangKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlpenglang.getText());
        
        if (!txtlpenglang.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlpenglang.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlpenglang.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlpenglang.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlpenglangKeyReleased

    private void txtlpindiglangKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpindiglangKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlpindiglang.getText());
        
        if (!txtlpindiglang.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlpindiglang.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlpindiglang.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlpindiglang.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlpindiglangKeyReleased

    private void txtlpmathKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpmathKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlpmath.getText());
        
        if (!txtlpmath.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlpmath.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlpmath.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlpmath.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlpmathKeyReleased

    private void txtlpenvKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpenvKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlpenv.getText());
        
        if (!txtlpenv.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlpenv.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlpenv.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlpenv.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlpenvKeyReleased

    private void txtlprelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlprelKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlprel.getText());
        
        if (!txtlprel.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlprel.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlprel.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlprel.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlprelKeyReleased

    private void txtlpmvmntcaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlpmvmntcaKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtlpmvmntca.getText());
        
        if (!txtlpmvmntca.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtlpmvmntca.setText("0");
        }else{
            int idlen = Integer.parseInt(txtlpmvmntca.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtlpmvmntca.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtlpmvmntcaKeyReleased

    private void txtupksllangKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupksllangKeyReleased
        // TODO add your handling code here:        
        if (!txtupksllang.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupksllang.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupksllang.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupksllang.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupksllangKeyReleased

    private void txtupenglangKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupenglangKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupenglang.getText());
        
        if (!txtupenglang.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupenglang.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupenglang.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupenglang.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupenglangKeyReleased

    private void txtupstKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupstKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupst.getText());
        
        if (!txtupst.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupst.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupst.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupst.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupstKeyReleased

    private void txtupssKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupssKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupss.getText());
        
        if (!txtupss.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupss.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupss.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupss.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupssKeyReleased

    private void txtupmathKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupmathKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupmath.getText());
        
        if (!txtupmath.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupmath.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupmath.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupmath.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupmathKeyReleased

    private void txtuphsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtuphsKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtuphs.getText());
        
        if (!txtuphs.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtuphs.setText("0");
        }else{
            int idlen = Integer.parseInt(txtuphs.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtuphs.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtuphsKeyReleased

    private void txtupagricKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupagricKeyReleased
        // TODO add your handling code here:
        
        if (!txtupagric.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupagric.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupagric.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupagric.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupagricKeyReleased

    private void txtuprelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtuprelKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtuprel.getText());
        
        if (!txtuprel.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtuprel.setText("0");
        }else{
            int idlen = Integer.parseInt(txtuprel.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtuprel.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtuprelKeyReleased

    private void txtupcaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupcaKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupca.getText());
        
        if (!txtupca.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupca.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupca.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupca.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupcaKeyReleased

    private void txtuppheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtuppheKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupphe.getText());
        
        if (!txtupphe.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupphe.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupphe.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupphe.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtuppheKeyReleased

    private void txtupppiKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupppiKeyReleased
        // TODO add your handling code here:
        int lang = Integer.parseInt(txtupppi.getText());
        
        if (!txtupppi.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupppi.setText("0");
        }else{
            int idlen = Integer.parseInt(txtupppi.getText());
            if (idlen > 100) {
                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
                txtupppi.setText("0");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtupppiKeyReleased

    private void txtpplangKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpplangKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpplangKeyTyped

    private void txtpplangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpplangKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpplangKeyPressed

    private void preptermCorrespodence(){
        String examterm = (String) cboppterm.getSelectedItem();
        String pterm = txtprepcurrterm.getText();
        if (!examterm.equals(pterm)) {
            JOptionPane.showMessageDialog(null, "Selected term and pupil's current term do not correspond!");
            cboppterm.setSelectedIndex(0);
        }else{
            System.out.println("Correspodence okay!");
        }
    }
    
    private void cbopptermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbopptermActionPerformed
        // TODO add your handling code here:
        preptermCorrespodence();
    }//GEN-LAST:event_cbopptermActionPerformed

    private void lptermCorrespodence(){
        String examterm = (String) cbolpterm.getSelectedItem();
        String pterm = txtlpterm.getText();
        if (!examterm.equals(pterm)) {
            JOptionPane.showMessageDialog(null, "Selected term and pupil's current term do not correspond!");
            cbolpterm.setSelectedIndex(0);
        }else{
            System.out.println("Correspodence okay!");
        }
    }
    
    private void cbolptermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbolptermActionPerformed
        // TODO add your handling code here:
        lptermCorrespodence();
    }//GEN-LAST:event_cbolptermActionPerformed

    private void uptermCorrespodence(){
        String examterm = (String) cboupterm.getSelectedItem();
        String pterm = txtupcurrterm.getText();
        if (!examterm.equals(pterm)) {
            JOptionPane.showMessageDialog(null, "Selected term and pupil's current term do not correspond!");
            cboupterm.setSelectedIndex(0);
        }else{
            System.out.println("Correspodence okay!");
        }
    }
    
    private void cbouptermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbouptermActionPerformed
        // TODO add your handling code here:
        uptermCorrespodence();
    }//GEN-LAST:event_cbouptermActionPerformed

        
    private void clearPrep(){
        txtpplang.setText("0");
        txtppmath.setText("0");
        txtppenv.setText("0");
        txtpppmotor.setText("0");
        txtprprel.setText("0");
        txtpreppname.setText("");
        txtprepadmno.setText("");
        txtprepclass.setText("");
        txtprepcurrterm.setText("");
        txtpptm.setText("000");
        txtppavg.setText("00.0");
        txtppgrade.setText("");
    }
    
    private void clearLP(){
        txtlpsearch.setText("");
        txtlplit.setText("0");
        txtlpksl.setText("0");
        txtlpenglang.setText("0");
        txtlpindiglang.setText("0");
        txtlpmath.setText("0");
        txtlpenv.setText("0");
        txtlprel.setText("0");
        txtlpmvmntca.setText("0");
        txtlppname.setText("");
        txtlpadmno.setText("");
        txtlpclass.setText("");
        txtlpterm.setText("");
        txtlptm.setText("000");
        txtlpavg.setText("00.0");
        txtlpgrade.setText("");
    }
    
    private void clearUP(){
        txtupsearch.setText("");
        txtuppname.setText("");
        txtupadmno.setText("");
        txtupclass.setText("");
        txtupcurrterm.setText("");
        
        txtupksllang.setText("0");
        txtupenglang.setText("0");
        txtupst.setText("0");
        txtupss.setText("0");
        txtupmath.setText("0");
        txtuphs.setText("0");
        txtupagric.setText("0");
        txtuprel.setText("0");
        txtupca.setText("0");
        txtupphe.setText("0");
        txtupppi.setText("0");
        
        txtuptm.setText("000");
        txtupavg.setText("00.0");
        txtupgrade.setText("");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Academics.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Academics.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Academics.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Academics.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Academics().setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Academics.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAcadSearch;
    private javax.swing.JButton btnLPCalculate;
    private javax.swing.JButton btnPrepCalc;
    private javax.swing.JButton btnPrepClear;
    private javax.swing.JButton btnPrintChart;
    private javax.swing.JButton btnReportForm;
    private javax.swing.JButton btnReportSheet;
    private javax.swing.JButton btnUpCalculate;
    private javax.swing.JButton btnUpClear;
    private javax.swing.JButton btnVisualCharts;
    private javax.swing.JButton btnVisualChartsRefresh;
    private javax.swing.JComboBox<String> cbolpclass;
    private javax.swing.JComboBox<String> cbolpexamtype;
    private javax.swing.JComboBox<String> cbolpterm;
    private javax.swing.JComboBox<String> cboppclass;
    private javax.swing.JComboBox<String> cboppexamtype;
    private javax.swing.JComboBox<String> cboppterm;
    private javax.swing.JComboBox<String> cborsltclass;
    private javax.swing.JComboBox<String> cborsltlistexamtype;
    private javax.swing.JComboBox<String> cborsltlistpterm;
    private javax.swing.JComboBox<String> cboupclass;
    private javax.swing.JComboBox<String> cboupexamtype;
    private javax.swing.JComboBox<String> cboupterm;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JLabel jlblacademicsloggedinas;
    private javax.swing.JLabel lblacademicstime;
    private javax.swing.JPanel panelgraphcontainer;
    private javax.swing.JTable tblrsltlist;
    private javax.swing.JTextArea txtTrComment;
    private javax.swing.JTextField txtlpadmno;
    private javax.swing.JTextField txtlpavg;
    private javax.swing.JTextField txtlpclass;
    private javax.swing.JButton txtlpclear;
    private javax.swing.JTextField txtlpenglang;
    private javax.swing.JTextField txtlpenv;
    private javax.swing.JTextField txtlpgrade;
    private javax.swing.JTextField txtlpindiglang;
    private javax.swing.JTextField txtlpksl;
    private javax.swing.JTextField txtlplit;
    private javax.swing.JTextField txtlpmath;
    private javax.swing.JTextField txtlpmvmntca;
    private javax.swing.JTextField txtlppname;
    private javax.swing.JTextField txtlprel;
    private javax.swing.JTextField txtlpsearch;
    private javax.swing.JTextField txtlpterm;
    private javax.swing.JTextField txtlptm;
    private javax.swing.JTextArea txtlptrcomment;
    private javax.swing.JTextField txtppavg;
    private javax.swing.JTextField txtppenv;
    private javax.swing.JTextField txtppgrade;
    private javax.swing.JTextField txtpplang;
    private javax.swing.JTextField txtppmath;
    private javax.swing.JTextField txtpppmotor;
    private javax.swing.JTextField txtpptm;
    private javax.swing.JTextField txtprepadmno;
    private javax.swing.JTextField txtprepclass;
    private javax.swing.JTextArea txtprepcomment;
    private javax.swing.JTextField txtprepcurrterm;
    private javax.swing.JTextField txtpreppname;
    private javax.swing.JTextField txtprepsearch;
    private javax.swing.JTextField txtprprel;
    private javax.swing.JTextField txtrsltlistsearch;
    private javax.swing.JTextField txtrsltyear;
    private javax.swing.JTextField txtupadmno;
    private javax.swing.JTextField txtupagric;
    private javax.swing.JTextField txtupavg;
    private javax.swing.JTextField txtupca;
    private javax.swing.JTextField txtupclass;
    private javax.swing.JTextField txtupcurrterm;
    private javax.swing.JTextField txtupenglang;
    private javax.swing.JTextField txtupgrade;
    private javax.swing.JTextField txtuphs;
    private javax.swing.JTextField txtupksllang;
    private javax.swing.JTextField txtupmath;
    private javax.swing.JTextField txtupphe;
    private javax.swing.JTextField txtuppname;
    private javax.swing.JTextField txtupppi;
    private javax.swing.JTextField txtuprel;
    private javax.swing.JTextField txtupsearch;
    private javax.swing.JTextField txtupss;
    private javax.swing.JTextField txtupst;
    private javax.swing.JTextField txtuptm;
    private javax.swing.JTextField txtvisyear;
    // End of variables declaration//GEN-END:variables
}
