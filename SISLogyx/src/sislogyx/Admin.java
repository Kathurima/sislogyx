/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import com.mysql.jdbc.Connection;
import com.teknikindustries.bulksms.SMS;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Image;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public final class Admin extends javax.swing.JFrame {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    
    private final ImageIcon format = null;
    String filename = null;
    byte[] other_image = null;
    byte[] teacher_image =null;
    byte[] pupil_image = null;
    
    byte[] sch_album = null;
    
    DefaultTableModel dm = new DefaultTableModel();
    
    DefaultTableModel trdm = new DefaultTableModel();
    
    DefaultTableModel nt = new DefaultTableModel();
    
    DefaultTableModel pupillist = new DefaultTableModel();
    
    DefaultTableModel loglist = new DefaultTableModel();
    
    DefaultTableModel msglist = new DefaultTableModel();
    
    /**
     * Creates new form Admin
     * @throws java.sql.SQLException
     */
    public Admin() throws SQLException{
        super("Admin Panel");
        
        initComponents();
        
        conn = (Connection) DBConnect.connect();
        
        txttrotherallergy.setEditable(false);     
        
                
        tblothlist.setModel(UpdateNT_table());
        tbldisclist.setModel(Discipline_table());
        tbltrlist.setModel(UpdateTr_table());
        tblpupilslist.setModel(Pupils_List());
        tbllogdetail.setModel(Log_List());
        tblmsglist.setModel(messageList());
        
        Current_Date();
        
        lblloggedinusercode.setText(String.valueOf(Emp.empId));

        int pslp;
        String pslpno = "PSLP";
        pslp = 225 + (int) (Math.random()*9585);
        pslpno += pslp + 7895;
        txtotpayslip.setText(pslpno);
        txttrpayslipno.setText(pslpno);
                
    }
    
    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);

                    txtdiscdate.setText(day+"/"+month+"/"+year);
                    txttrdateoe.setText(day+"/"+month+"/"+year);
                    txtotdoe.setText(day+"/"+month+"/"+year);

                    int second = cal.get(Calendar.SECOND);
                    int minute = cal.get(Calendar.MINUTE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);

                    lbltime.setText(hour+":"+minute+":"+second);
                    lbltime.setForeground(Color.red);
                }
            }
        };
        clock.start();
   }
    
    private DefaultTableModel UpdateNT_table(){
        nt.addColumn("Name");
        nt.addColumn("ID No.");
        nt.addColumn("Phone No");
        nt.addColumn("Department");
        nt.addColumn("Employed As");
        nt.addColumn("Work ID.");
        
        try{
            String sql = "SELECT * FROM `otherstaff` WHERE `status`=\"1\" ";
            pst = conn.prepareStatement(sql);
            
            rs = pst.executeQuery();
            while (rs.next()) {
                String empname = rs.getString("ename");
                String empidno = rs.getString("eid_no");
                String empphone = rs.getString("e_phone");
                String empdept = rs.getString("e_dept");
                String empempas = rs.getString("e_employed_as");
                String empwrkid = rs.getString("e_work_id");
                
                String[] rowdata = {empname, empidno, empphone, empdept, empempas, empwrkid};
                nt.addRow(rowdata);
            }
            
            return nt;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        return null;
    }
    
    private DefaultTableModel UpdateTr_table(){
        trdm.addColumn("Name");
        trdm.addColumn("ID No.");
        trdm.addColumn("Phone No");
        trdm.addColumn("Department");
        trdm.addColumn("Employed As");
        trdm.addColumn("TSC No.");
        trdm.addColumn("Working ID");
        
        try{
            //String sql = "SELECT `ename`, `eid_no`, `e_gender`, `e_nationality`, `e_phone`, `e_email`,`e_dept`, `e_employed_as`, `e_work_id`, `e_doe`, `e_nssf_no`, `e_kra_pin`, `e_payslip_no`, `e_salary`, `status` FROM `otherstaff`";
            String sql = "SELECT * FROM `teachers` WHERE `status`=\"1\" ";
            pst = conn.prepareStatement(sql);
            
            rs = pst.executeQuery();
            while (rs.next()) {
                String trname = rs.getString("t_name");
                String tridno = rs.getString("t_idno");
                String trphone = rs.getString("t_phone");
                String trdept = rs.getString("t_dept");
                String trempas = rs.getString("t_emp_as");
                String trtscno = rs.getString("t_tscno");
                String trworkid = rs.getString("t_trid");
                
                String[] rowdata = {trname, tridno, trphone, trdept, trempas, trtscno, trworkid};
                trdm.addRow(rowdata);
            }
            
            return trdm;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        return null;
    }
    
    private DefaultTableModel Discipline_table(){
        dm.addColumn("Date");
        dm.addColumn("Pupil's Name");
        dm.addColumn("Adm. No");
        dm.addColumn("Gender");
        dm.addColumn("Class");
        dm.addColumn("Age");
        dm.addColumn("Offence");
        dm.addColumn("Punishment");
        dm.addColumn("Punished By");
        dm.addColumn("Status");
        
        try{
            String sql = "SELECT `d_pdate`,`d_pname`,`d_padmno`,`d_pgender`,`d_pclass`,`d_p_age`,`d_poffence`,`d_ppunishment`,`d_pwhom`,`status` FROM `discipline` ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();

            while (rs.next()) {
                String date = rs.getString("d_pdate");
                String pupilsname = rs.getString("d_pname");
                String padmno = rs.getString("d_padmno");
                String gender = rs.getString("d_pgender");
                String pclass = rs.getString("d_pclass");
                String age = rs.getString("d_p_age");
                String offense = rs.getString("d_poffence");
                String punishment = rs.getString("d_ppunishment");
                String punisher = rs.getString("d_pwhom");
                String status = rs.getString("status");
                
                String[] rowdata = {date, pupilsname, padmno, gender, pclass, age, offense, punishment, punisher, status};
                dm.addRow(rowdata);
            }
            
            return dm;
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        return null;
    }
    
    private DefaultTableModel Pupils_List(){
        pupillist.addColumn("Adm. No.");
        pupillist.addColumn("Name");
        pupillist.addColumn("Surname");
        pupillist.addColumn("Gender");
        pupillist.addColumn("Class");
        
        try {
            String sql = "SELECT * FROM `admissions` WHERE `status`=\"1\" ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                String pladmno = rs.getString("admno");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String gender = rs.getString("gender");
                String pclass = rs.getString("n_class");
                
                String[] rowdata = {pladmno, name, surname, gender, pclass};
                pupillist.addRow(rowdata);
            }
            
            return pupillist;
            
        } catch (SQLException e) {
        }
        
        return null;        
    }
    
    private DefaultTableModel Log_List(){
        //loglist.addColumn("Audit ID");
        loglist.addColumn("Work ID");
        loglist.addColumn("Date");
        loglist.addColumn("Time");
        loglist.addColumn("Status");
        
        try {
            String sql = "SELECT * FROM `auditlog` ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                //String auditno = rs.getString("audit_id");
                String workid = rs.getString("empid");
                String date = rs.getString("date");
                String time = rs.getString("time");
                String status = rs.getString("status");
                
                String[] rowdata = {workid, date, time, status};
                loglist.addRow(rowdata);
            }
            
            return loglist;
            
        } catch (SQLException e) {
        }
        
        return null;        
    }
    
    private DefaultTableModel messageList(){
        //msglist.addColumn("Message Type");
        msglist.addColumn("Adm no.");
        //msglist.addColumn("Email Recipient");
        msglist.addColumn("From");
        msglist.addColumn("To");
        msglist.addColumn("Message");
        
        try {
            String sql = "SELECT * FROM `messages` ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                //String msgtype = rs.getString("m_type");
                String padmno = rs.getString("padmno");
                String mfrom = rs.getString("m_from");
                String mto = rs.getString("m_to");
                //String msbjct = rs.getString("m_sbjct");
                String m_msg = rs.getString("m_msg");
                
                String[] rowdata = {padmno, mfrom, mto, m_msg};
                msglist.addRow(rowdata);
            }
            
            return msglist;
            
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    private void nt_clearFields(){
        txtsearchtrid_two.setText("");
        txtntsearch.setText("");
        txtotnamee.setText("");
        txtotidno.setText("");
        jotDob.setDate(null);
        cbootgndr.setSelectedIndex(0);
        cbootrel.setSelectedIndex(0);

        txtotnationality.setText("");
        txtotphone.setText("");
        txtotdob.setText("");

        chkotaspirin.setSelected(false);
        chkotpenicillin.setSelected(false);
        chkotpollen.setSelected(false);
        chkotmeatandrelated.setSelected(false);
        chkototherhealthinfo.setSelected(false);
        txtototherhealthinfo.setText("");

        txtotinsurancecarrier.setText("");
        txtotpolicyno.setText("");
        txtotpolicyholdername.setText("");
        txtotgroup_category.setText("");
        lbl_other_profileimage.setText("");
        ImageIcon sch_alb = new ImageIcon();
        lbl_other_profileimage.setIcon(sch_alb);
        cbootdept.setSelectedIndex(0);
        cbootemployedas.setSelectedIndex(0);

        txtotworkid.setText("");
        txtotdoe.setText("");
        txtotnssf.setText("");
        txtotkrapin.setText("");
        txtotpayslip.setText("");
        txtotsalary.setText("");
        ntstatus.setText("");
    }
    
    private void clearTrFields(){
        txttridno.setText("");
        txtsearchtrid_two.setText("");
        txttrname.setText("");
        txttridno.setText("");
        jtrDob.setDate(null);
        cbotrgender.setSelectedIndex(0);
        cbotrrel.setSelectedIndex(0);

        txttrnationality.setText("");
        txttrphone.setText("");
        //txttremail.setText("");

        chktraspirin.setSelected(false);
        chktrpenicillin.setSelected(false);
        chktrpollen.setSelected(false);
        chktrmeatandrelated.setSelected(false);
        chktrotherallergy.setSelected(false);
        txttrotherallergy.setText("");

        txttrinsurancecarrier.setText("");
        txttrpolicyno.setText("");
        txttrpolictholdername.setText("");
        txttrgroupno.setText("");
        ImageIcon sch_alb = new ImageIcon();
        lbl_tr_profileimage.setIcon(sch_alb);
        cbotremployedas.setSelectedIndex(0);
        txttrtscno.setText("");
        cbotrjobgroup.setSelectedIndex(0);

        txttrsubjectspeciality.setText("");
        txttrworkingid.setText("");
        txttrdateoe.setText("");
        cbotrclasstr.setSelectedIndex(0);
        cbowhichclass.setSelectedIndex(0);
        txttrnssfno.setText("");
        txttrkrapin.setText("");
        txttrpayslipno.setText("");
        txttrsalary.setText("");
        lbltrstatus.setText("");
    }
    
    int pos = 0;
    
    public List<AlbumItems> getAlbumItemsList(){
        try {
            String sql = "SELECT * FROM `schoolalbum` ";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            List<AlbumItems> albumList = new ArrayList<>();
            AlbumItems items;
            
            while (rs.next()) {                
                items = new AlbumItems(rs.getString("event_date"), rs.getString("event"), rs.getString("location"), 
                        rs.getString("commemorator"), rs.getString("comments"), rs.getBytes("photo"));
                
                albumList.add(items);
            }
            return albumList;
            
        } catch (SQLException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            
            return null;
        }       
    }
    
    public void showAlbumItems(int index){
        txtsa1.setText(getAlbumItemsList().get(index).getDateofEvent());
        txtsa2.setText(getAlbumItemsList().get(index).getEvent());
        txtsa3.setText(getAlbumItemsList().get(index).getLocation());
        txtsa4.setText(getAlbumItemsList().get(index).getCommemorator());
        txtsa5.setText(getAlbumItemsList().get(index).getComment());
        
        ImageIcon icon = new ImageIcon(getAlbumItemsList().get(index).getAlbumPhoto());
        Image image = icon.getImage().getScaledInstance(sa_photo.getWidth(), sa_photo.getHeight(), Image.SCALE_SMOOTH);
        sa_photo.setIcon(new ImageIcon(image));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jbtnlogout = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btnadmission = new javax.swing.JButton();
        btnfinance = new javax.swing.JButton();
        btnstore = new javax.swing.JButton();
        btnlib = new javax.swing.JButton();
        btnacademics = new javax.swing.JButton();
        btnnewterm = new javax.swing.JButton();
        btnnewterm1 = new javax.swing.JButton();
        btnnewterm2 = new javax.swing.JButton();
        btnPromoteNextToClass = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel15 = new javax.swing.JPanel();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel16 = new javax.swing.JPanel();
        jPanel89 = new javax.swing.JPanel();
        jPanel99 = new javax.swing.JPanel();
        jLabel113 = new javax.swing.JLabel();
        jDesktopPane = new javax.swing.JDesktopPane();
        lbl_other_profileimage = new javax.swing.JLabel();
        btn_other_uploadimg = new javax.swing.JButton();
        jPanel18 = new javax.swing.JPanel();
        jLabel186 = new javax.swing.JLabel();
        txtotnamee = new javax.swing.JTextField();
        jLabel126 = new javax.swing.JLabel();
        txtotidno = new javax.swing.JTextField();
        jLabel141 = new javax.swing.JLabel();
        jLabel142 = new javax.swing.JLabel();
        cbootgndr = new javax.swing.JComboBox<>();
        jLabel187 = new javax.swing.JLabel();
        cbootrel = new javax.swing.JComboBox<>();
        jLabel143 = new javax.swing.JLabel();
        txtotnationality = new javax.swing.JTextField();
        jLabel120 = new javax.swing.JLabel();
        txtotphone = new javax.swing.JTextField();
        jotDob = new com.toedter.calendar.JDateChooser();
        txtotdob = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jLabel164 = new javax.swing.JLabel();
        chkotaspirin = new javax.swing.JCheckBox();
        chkotpenicillin = new javax.swing.JCheckBox();
        chkotpollen = new javax.swing.JCheckBox();
        chkotmeatandrelated = new javax.swing.JCheckBox();
        chkototherhealthinfo = new javax.swing.JCheckBox();
        jScrollPane25 = new javax.swing.JScrollPane();
        txtototherhealthinfo = new javax.swing.JTextArea();
        jPanel104 = new javax.swing.JPanel();
        jLabel179 = new javax.swing.JLabel();
        txtotgroup_category = new javax.swing.JTextField();
        jLabel180 = new javax.swing.JLabel();
        txtotinsurancecarrier = new javax.swing.JTextField();
        jLabel181 = new javax.swing.JLabel();
        txtotpolicyno = new javax.swing.JTextField();
        jLabel182 = new javax.swing.JLabel();
        txtotpolicyholdername = new javax.swing.JTextField();
        jPanel17 = new javax.swing.JPanel();
        jPanel90 = new javax.swing.JPanel();
        jPanel101 = new javax.swing.JPanel();
        jLabel114 = new javax.swing.JLabel();
        jLabel127 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jLabel101 = new javax.swing.JLabel();
        cbootdept = new javax.swing.JComboBox<>();
        jLabel102 = new javax.swing.JLabel();
        jLabel103 = new javax.swing.JLabel();
        txtotkrapin = new javax.swing.JTextField();
        jLabel104 = new javax.swing.JLabel();
        txtotpayslip = new javax.swing.JTextField();
        jLabel105 = new javax.swing.JLabel();
        txtotnssf = new javax.swing.JTextField();
        cbootemployedas = new javax.swing.JComboBox<>();
        jLabel106 = new javax.swing.JLabel();
        txtotsalary = new javax.swing.JTextField();
        jLabel131 = new javax.swing.JLabel();
        jLabel133 = new javax.swing.JLabel();
        txtotdoe = new javax.swing.JTextField();
        txtotworkid = new javax.swing.JTextField();
        jPanel102 = new javax.swing.JPanel();
        jScrollPane26 = new javax.swing.JScrollPane();
        tblothlist = new javax.swing.JTable();
        jPanel93 = new javax.swing.JPanel();
        btnothergenexcelsheet = new javax.swing.JButton();
        btn_non_teaching_save = new javax.swing.JButton();
        btnotherupdate = new javax.swing.JButton();
        btnotherdelete = new javax.swing.JButton();
        btnotherclear = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        ntstatus = new javax.swing.JLabel();
        txtntsearch = new javax.swing.JTextField();
        jPanel56 = new javax.swing.JPanel();
        jTabbedPane5 = new javax.swing.JTabbedPane();
        jPanel21 = new javax.swing.JPanel();
        jPanel91 = new javax.swing.JPanel();
        jPanel103 = new javax.swing.JPanel();
        jLabel115 = new javax.swing.JLabel();
        tr_JDesktopPane = new javax.swing.JDesktopPane();
        lbl_tr_profileimage = new javax.swing.JLabel();
        btntr_uploadimg = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        jLabel189 = new javax.swing.JLabel();
        txttrname = new javax.swing.JTextField();
        jLabel129 = new javax.swing.JLabel();
        jLabel145 = new javax.swing.JLabel();
        jLabel146 = new javax.swing.JLabel();
        cbotrgender = new javax.swing.JComboBox<>();
        jLabel190 = new javax.swing.JLabel();
        cbotrrel = new javax.swing.JComboBox<>();
        jLabel147 = new javax.swing.JLabel();
        txttrnationality = new javax.swing.JTextField();
        jLabel121 = new javax.swing.JLabel();
        txttrphone = new javax.swing.JTextField();
        txttridno = new javax.swing.JTextField();
        jtrDob = new com.toedter.calendar.JDateChooser();
        txttrdob = new javax.swing.JTextField();
        jPanel23 = new javax.swing.JPanel();
        jLabel174 = new javax.swing.JLabel();
        chktraspirin = new javax.swing.JCheckBox();
        chktrpenicillin = new javax.swing.JCheckBox();
        chktrpollen = new javax.swing.JCheckBox();
        chktrmeatandrelated = new javax.swing.JCheckBox();
        chktrotherallergy = new javax.swing.JCheckBox();
        jScrollPane27 = new javax.swing.JScrollPane();
        txttrotherallergy = new javax.swing.JTextArea();
        jPanel105 = new javax.swing.JPanel();
        jLabel183 = new javax.swing.JLabel();
        txttrgroupno = new javax.swing.JTextField();
        jLabel184 = new javax.swing.JLabel();
        txttrinsurancecarrier = new javax.swing.JTextField();
        jLabel185 = new javax.swing.JLabel();
        txttrpolicyno = new javax.swing.JTextField();
        jLabel192 = new javax.swing.JLabel();
        txttrpolictholdername = new javax.swing.JTextField();
        jPanel24 = new javax.swing.JPanel();
        jPanel94 = new javax.swing.JPanel();
        jPanel106 = new javax.swing.JPanel();
        jLabel116 = new javax.swing.JLabel();
        jLabel130 = new javax.swing.JLabel();
        txtsearchtrid_two = new javax.swing.JTextField();
        jPanel25 = new javax.swing.JPanel();
        jLabel107 = new javax.swing.JLabel();
        jLabel108 = new javax.swing.JLabel();
        jLabel109 = new javax.swing.JLabel();
        txttrkrapin = new javax.swing.JTextField();
        jLabel110 = new javax.swing.JLabel();
        txttrpayslipno = new javax.swing.JTextField();
        jLabel111 = new javax.swing.JLabel();
        txttrdept = new javax.swing.JTextField();
        cbotremployedas = new javax.swing.JComboBox<>();
        jLabel117 = new javax.swing.JLabel();
        txttrsalary = new javax.swing.JTextField();
        txttrtscno = new javax.swing.JTextField();
        jLabel112 = new javax.swing.JLabel();
        txttrsubjectspeciality = new javax.swing.JTextField();
        txttrnssfno = new javax.swing.JTextField();
        jLabel119 = new javax.swing.JLabel();
        jLabel122 = new javax.swing.JLabel();
        jLabel123 = new javax.swing.JLabel();
        cbotrjobgroup = new javax.swing.JComboBox<>();
        jLabel124 = new javax.swing.JLabel();
        cbotrclasstr = new javax.swing.JComboBox<>();
        jLabel132 = new javax.swing.JLabel();
        jLabel118 = new javax.swing.JLabel();
        txttrworkingid = new javax.swing.JTextField();
        txttrdateoe = new javax.swing.JTextField();
        cbowhichclass = new javax.swing.JComboBox<>();
        jPanel107 = new javax.swing.JPanel();
        jScrollPane28 = new javax.swing.JScrollPane();
        tbltrlist = new javax.swing.JTable();
        jPanel95 = new javax.swing.JPanel();
        btntrgenerateexcelsheet = new javax.swing.JButton();
        btntrsave = new javax.swing.JButton();
        btntrupdate = new javax.swing.JButton();
        btntrdelete = new javax.swing.JButton();
        btntrclear = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lbltrstatus = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel30 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        txtdisciplinesearch = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        txtdiscdate = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        txtdiscbywhom = new javax.swing.JTextField();
        jScrollPane14 = new javax.swing.JScrollPane();
        tbldisclist = new javax.swing.JTable();
        txtdiscpupilname = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        txtdiscgender = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        txtdiscclass = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        txtdiscage = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        jScrollPane17 = new javax.swing.JScrollPane();
        txtdiscoffence = new javax.swing.JTextArea();
        jLabel50 = new javax.swing.JLabel();
        jScrollPane29 = new javax.swing.JScrollPane();
        txtdiscpunishment = new javax.swing.JTextArea();
        btndiscsave = new javax.swing.JButton();
        btndiscclear = new javax.swing.JButton();
        disc_jDesktopPane = new javax.swing.JDesktopPane();
        disc_pupil_img = new javax.swing.JLabel();
        txtdiscadmno = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        discpstatus = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblpupilslist = new javax.swing.JTable();
        btngeneratepupilsexcelsheet = new javax.swing.JButton();
        jLabel64 = new javax.swing.JLabel();
        txtlistsearch = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        txtclass = new javax.swing.JTextField();
        btnPupilListRefresh = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jLabel74 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        txttrnfname = new javax.swing.JTextField();
        jLabel76 = new javax.swing.JLabel();
        txttrnfparentname = new javax.swing.JTextField();
        jLabel77 = new javax.swing.JLabel();
        txttrnfdob = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        txttrnfadmno = new javax.swing.JTextField();
        jLabel79 = new javax.swing.JLabel();
        txttrnfcurrclass = new javax.swing.JTextField();
        jLabel81 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        btnclear = new javax.swing.JButton();
        btngeneratetranferletter = new javax.swing.JButton();
        jScrollPane24 = new javax.swing.JScrollPane();
        txttrnfhdtrremarks = new javax.swing.JTextArea();
        jLabel83 = new javax.swing.JLabel();
        txttrnfsearch = new javax.swing.JTextField();
        trnsfr_jDesktopPane = new javax.swing.JDesktopPane();
        trnf_profile_img = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txttrnsfconduct = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        txttrnfcauseoftrnf = new javax.swing.JTextArea();
        jPanel9 = new javax.swing.JPanel();
        jPanel39 = new javax.swing.JPanel();
        txtsearchleaving = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnleavingcert = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jPanel37 = new javax.swing.JPanel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        sa_photo = new javax.swing.JLabel();
        jPanel42 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txtsa2 = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        txtsa3 = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtsa4 = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtsa5 = new javax.swing.JTextArea();
        txtsa1 = new javax.swing.JTextField();
        jPanel43 = new javax.swing.JPanel();
        btnsasave = new javax.swing.JButton();
        btsauploadnewImage = new javax.swing.JButton();
        btnsaview = new javax.swing.JButton();
        btnprevious = new javax.swing.JButton();
        btnnext = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jPanel38 = new javax.swing.JPanel();
        cbovr1 = new javax.swing.JComboBox<>();
        jLabel52 = new javax.swing.JLabel();
        txtvr1 = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        txtvr2 = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        txtvr5 = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        cbovr2 = new javax.swing.JComboBox<>();
        jLabel58 = new javax.swing.JLabel();
        jScrollPane19 = new javax.swing.JScrollPane();
        txtvr3 = new javax.swing.JTextArea();
        jLabel61 = new javax.swing.JLabel();
        jScrollPane20 = new javax.swing.JScrollPane();
        txtvr4 = new javax.swing.JTextArea();
        btnVRClear = new javax.swing.JButton();
        btnVRSave = new javax.swing.JButton();
        jPanel14 = new javax.swing.JPanel();
        jLabel66 = new javax.swing.JLabel();
        txtvr7 = new javax.swing.JTextField();
        btnvrsignout = new javax.swing.JButton();
        jLabel59 = new javax.swing.JLabel();
        txtvr9 = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        jScrollPane18 = new javax.swing.JScrollPane();
        txtvr8 = new javax.swing.JTextArea();
        txtvr6 = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtsm6 = new javax.swing.JTextArea();
        btnsmCancel = new javax.swing.JButton();
        txtsmSend = new javax.swing.JButton();
        txtsm4 = new javax.swing.JTextField();
        lblmsg = new javax.swing.JLabel();
        txtsm3 = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblmsglist = new javax.swing.JTable();
        rbparent = new javax.swing.JRadioButton();
        rbts = new javax.swing.JRadioButton();
        rbnt = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        txtsm5 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jPanel33 = new javax.swing.JPanel();
        jPanel35 = new javax.swing.JPanel();
        jScrollPane15 = new javax.swing.JScrollPane();
        tbllogdetail = new javax.swing.JTable();
        jPanel36 = new javax.swing.JPanel();
        btnlogclear = new javax.swing.JButton();
        btnloggenreport = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtlogworkid = new javax.swing.JTextField();
        lblloggedinusercode = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lbltime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        jPanel3.setBackground(new java.awt.Color(102, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("ADMIN DASHBOARD");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel1)
                .addContainerGap(97, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        jbtnlogout.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jbtnlogout.setText("Logout");
        jbtnlogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnlogoutActionPerformed(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(102, 255, 204));

        btnadmission.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnadmission.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/new_hr.png"))); // NOI18N
        btnadmission.setText("ADMISSIONS");
        btnadmission.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnadmissionActionPerformed(evt);
            }
        });

        btnfinance.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnfinance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/finance_sml.png"))); // NOI18N
        btnfinance.setText("FINANCE");
        btnfinance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfinanceActionPerformed(evt);
            }
        });

        btnstore.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnstore.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/store_1.png"))); // NOI18N
        btnstore.setText("STORE KEEPING");
        btnstore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstoreActionPerformed(evt);
            }
        });

        btnlib.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnlib.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/book.png"))); // NOI18N
        btnlib.setText("LIBRARY");
        btnlib.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlibActionPerformed(evt);
            }
        });

        btnacademics.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnacademics.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnacademics.setText("ACADEMICS");
        btnacademics.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnacademicsActionPerformed(evt);
            }
        });

        btnnewterm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnnewterm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/signup.png"))); // NOI18N
        btnnewterm.setText("UPDATE TERM");
        btnnewterm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnewtermActionPerformed(evt);
            }
        });

        btnnewterm1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnnewterm1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/details.png"))); // NOI18N
        btnnewterm1.setText("VIEW PROFILE");
        btnnewterm1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnewterm1ActionPerformed(evt);
            }
        });

        btnnewterm2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnnewterm2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnnewterm2.setText("SET FEE STRUCTURE");
        btnnewterm2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnewterm2ActionPerformed(evt);
            }
        });

        btnPromoteNextToClass.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnPromoteNextToClass.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/pnextclass.png"))); // NOI18N
        btnPromoteNextToClass.setText("PROMOTE TO NEXT CLASS");
        btnPromoteNextToClass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromoteNextToClassActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnadmission, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnfinance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnnewterm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnnewterm1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnacademics, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnlib, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnstore, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnnewterm2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPromoteNextToClass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(btnadmission, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(btnfinance, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(btnstore, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(btnlib, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(btnacademics, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(btnnewterm, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(btnPromoteNextToClass, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(btnnewterm1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(btnnewterm2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jTabbedPane3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTabbedPane3.setForeground(new java.awt.Color(204, 0, 153));
        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jTabbedPane4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jPanel99.setBackground(new java.awt.Color(255, 153, 153));

        jLabel113.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel113.setForeground(new java.awt.Color(255, 255, 255));
        jLabel113.setText("Personal Info");

        javax.swing.GroupLayout jPanel99Layout = new javax.swing.GroupLayout(jPanel99);
        jPanel99.setLayout(jPanel99Layout);
        jPanel99Layout.setHorizontalGroup(
            jPanel99Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel99Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel113)
                .addContainerGap(81, Short.MAX_VALUE))
        );
        jPanel99Layout.setVerticalGroup(
            jPanel99Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel99Layout.createSequentialGroup()
                .addComponent(jLabel113)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        jDesktopPane.setLayer(lbl_other_profileimage, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPaneLayout = new javax.swing.GroupLayout(jDesktopPane);
        jDesktopPane.setLayout(jDesktopPaneLayout);
        jDesktopPaneLayout.setHorizontalGroup(
            jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_other_profileimage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDesktopPaneLayout.setVerticalGroup(
            jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_other_profileimage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        btn_other_uploadimg.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_other_uploadimg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/attach.png"))); // NOI18N
        btn_other_uploadimg.setText("Choose Image");
        btn_other_uploadimg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_other_uploadimgActionPerformed(evt);
            }
        });

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Personal Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel186.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel186.setText("Employee Name");

        txtotnamee.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotnamee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtotnameeKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotnameeKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtotnameeKeyTyped(evt);
            }
        });

        jLabel126.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel126.setText("ID No.");

        txtotidno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotidno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtotidnoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotidnoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtotidnoKeyTyped(evt);
            }
        });

        jLabel141.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel141.setText("D. O. B");

        jLabel142.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel142.setText("Gender");

        cbootgndr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbootgndr.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Gender", "Male", "Female" }));

        jLabel187.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel187.setText("Religion");

        cbootrel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbootrel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Religion", "Catholic", "Protestant", "Islamic", "Budhist", "Rastafari" }));

        jLabel143.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel143.setText("Nationality");

        txtotnationality.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotnationality.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotnationalityKeyReleased(evt);
            }
        });

        jLabel120.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel120.setText("Phone No.");

        txtotphone.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotphone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotphoneKeyReleased(evt);
            }
        });

        jotDob.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jotDobMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jotDobMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jotDobMouseReleased(evt);
            }
        });
        jotDob.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jotDobKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jotDobKeyReleased(evt);
            }
        });

        txtotdob.setEditable(false);
        txtotdob.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotdob.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtotdobMouseClicked(evt);
            }
        });
        txtotdob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtotdobActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtotnationality, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addComponent(txtotnamee, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addComponent(txtotidno)
                    .addComponent(txtotphone)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addComponent(txtotdob)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jotDob, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel142)
                            .addComponent(cbootgndr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbootrel, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel187)
                            .addComponent(jLabel143)
                            .addComponent(jLabel141)
                            .addComponent(jLabel120)
                            .addComponent(jLabel186)
                            .addComponent(jLabel126))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(63, 63, 63))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel186)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotnamee, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel126)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotidno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel141)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jotDob, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtotdob, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel142)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbootgndr, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel187)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbootrel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel143)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotnationality, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel120)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotphone, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64))
        );

        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Health Info", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel164.setForeground(new java.awt.Color(255, 0, 51));
        jLabel164.setText("Is the employee allergic to any of the following?");

        chkotaspirin.setText("Aspirin");
        chkotaspirin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkotaspirinActionPerformed(evt);
            }
        });

        chkotpenicillin.setText("Penicillin");
        chkotpenicillin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkotpenicillinActionPerformed(evt);
            }
        });

        chkotpollen.setText("Pollen");
        chkotpollen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkotpollenActionPerformed(evt);
            }
        });

        chkotmeatandrelated.setText("Meat and Related");
        chkotmeatandrelated.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkotmeatandrelatedActionPerformed(evt);
            }
        });

        chkototherhealthinfo.setText("Other, Please specify ");
        chkototherhealthinfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkototherhealthinfoActionPerformed(evt);
            }
        });

        txtototherhealthinfo.setEditable(false);
        txtototherhealthinfo.setColumns(20);
        txtototherhealthinfo.setRows(5);
        txtototherhealthinfo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtototherhealthinfoKeyReleased(evt);
            }
        });
        jScrollPane25.setViewportView(txtototherhealthinfo);

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel164)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane25)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(chkotaspirin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkotpenicillin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkotpollen)
                        .addGap(10, 10, 10)
                        .addComponent(chkotmeatandrelated))
                    .addComponent(chkototherhealthinfo, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel164)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkotpenicillin)
                    .addComponent(chkotpollen)
                    .addComponent(chkotaspirin)
                    .addComponent(chkotmeatandrelated))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkototherhealthinfo)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane25, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(332, 332, 332))
        );

        jPanel104.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Insurance", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel179.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel179.setText("Insurance Carrier");

        txtotgroup_category.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotgroup_category.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotgroup_categoryKeyReleased(evt);
            }
        });

        jLabel180.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel180.setText("Policy Number");

        txtotinsurancecarrier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotinsurancecarrier.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotinsurancecarrierKeyReleased(evt);
            }
        });

        jLabel181.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel181.setText("Policy Holders Name");

        txtotpolicyno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotpolicyno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotpolicynoKeyReleased(evt);
            }
        });

        jLabel182.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel182.setText("Group / Category");

        txtotpolicyholdername.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotpolicyholdername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotpolicyholdernameKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel104Layout = new javax.swing.GroupLayout(jPanel104);
        jPanel104.setLayout(jPanel104Layout);
        jPanel104Layout.setHorizontalGroup(
            jPanel104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel104Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtotpolicyno, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtotpolicyholdername, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtotgroup_category, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel179)
                    .addComponent(jLabel180)
                    .addComponent(jLabel181)
                    .addComponent(jLabel182)
                    .addComponent(txtotinsurancecarrier, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel104Layout.setVerticalGroup(
            jPanel104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel104Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel179)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotinsurancecarrier, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel180)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotpolicyno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel181)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotpolicyholdername, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel182)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotgroup_category, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel89Layout = new javax.swing.GroupLayout(jPanel89);
        jPanel89.setLayout(jPanel89Layout);
        jPanel89Layout.setHorizontalGroup(
            jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel89Layout.createSequentialGroup()
                .addGroup(jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel89Layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDesktopPane)
                    .addComponent(btn_other_uploadimg, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel89Layout.setVerticalGroup(
            jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel89Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_other_uploadimg)
                .addGap(333, 333, 333))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel89Layout.createSequentialGroup()
                .addComponent(jPanel99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel89Layout.createSequentialGroup()
                        .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel18, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jPanel89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 17, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jPanel89, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane4.addTab("Personal Details", jPanel16);

        jPanel101.setBackground(new java.awt.Color(255, 153, 153));

        jLabel114.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel114.setForeground(new java.awt.Color(255, 255, 255));
        jLabel114.setText("Work & Related");

        javax.swing.GroupLayout jPanel101Layout = new javax.swing.GroupLayout(jPanel101);
        jPanel101.setLayout(jPanel101Layout);
        jPanel101Layout.setHorizontalGroup(
            jPanel101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel101Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel114)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel101Layout.setVerticalGroup(
            jPanel101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel101Layout.createSequentialGroup()
                .addComponent(jLabel114)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        jLabel127.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel127.setText("Employee ID");

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Work & Related", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel101.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel101.setText("Employed As");

        cbootdept.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbootdept.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select the Department", "Finance and Accounts", "Stock-keeping", "Library" }));

        jLabel102.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel102.setText("NSSF No.");

        jLabel103.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel103.setText("KRA Pin");

        txtotkrapin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotkrapin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotkrapinKeyReleased(evt);
            }
        });

        jLabel104.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel104.setText("P F No.");

        txtotpayslip.setEditable(false);
        txtotpayslip.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel105.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel105.setText("Department");

        txtotnssf.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotnssf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotnssfKeyReleased(evt);
            }
        });

        cbootemployedas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbootemployedas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Employed As", "Accountant", "Librarian", "Store-keeper", "Guard", "Cook" }));
        cbootemployedas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbootemployedasActionPerformed(evt);
            }
        });

        jLabel106.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel106.setText("Salary");

        txtotsalary.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtotsalary.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotsalaryKeyReleased(evt);
            }
        });

        jLabel131.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel131.setText("Date of Employment");

        jLabel133.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel133.setText("Work ID");

        txtotdoe.setEditable(false);
        txtotdoe.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtotworkid.setEditable(false);
        txtotworkid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel102)
                    .addComponent(jLabel103)
                    .addComponent(jLabel101, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel105)
                    .addComponent(txtotpayslip)
                    .addComponent(txtotkrapin, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbootdept, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbootemployedas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel104)
                    .addComponent(jLabel106)
                    .addComponent(txtotsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel131, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel133)
                    .addComponent(txtotworkid, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtotdoe)
                    .addComponent(txtotnssf, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(61, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jLabel105)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbootdept, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel101)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbootemployedas, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel133)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotworkid, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel131)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotdoe, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel102)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotnssf, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel103)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotkrapin, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel104)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotpayslip, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel106)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtotsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel102.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Employee List", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        tblothlist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Name", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane26.setViewportView(tblothlist);

        javax.swing.GroupLayout jPanel102Layout = new javax.swing.GroupLayout(jPanel102);
        jPanel102.setLayout(jPanel102Layout);
        jPanel102Layout.setHorizontalGroup(
            jPanel102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel102Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane26))
        );
        jPanel102Layout.setVerticalGroup(
            jPanel102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel102Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane26, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel93.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnothergenexcelsheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnothergenexcelsheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnothergenexcelsheet.setText("Generate Employee List Sheet");
        btnothergenexcelsheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnothergenexcelsheetActionPerformed(evt);
            }
        });

        btn_non_teaching_save.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_non_teaching_save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btn_non_teaching_save.setText("Save");
        btn_non_teaching_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_non_teaching_saveActionPerformed(evt);
            }
        });

        btnotherupdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnotherupdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnotherupdate.setText("Update");
        btnotherupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnotherupdateActionPerformed(evt);
            }
        });

        btnotherdelete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnotherdelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/delete_small.png"))); // NOI18N
        btnotherdelete.setText("Delete");
        btnotherdelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnotherdeleteActionPerformed(evt);
            }
        });

        btnotherclear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnotherclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnotherclear.setText("Clear");
        btnotherclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnotherclearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel93Layout = new javax.swing.GroupLayout(jPanel93);
        jPanel93.setLayout(jPanel93Layout);
        jPanel93Layout.setHorizontalGroup(
            jPanel93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel93Layout.createSequentialGroup()
                .addGroup(jPanel93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel93Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(btn_non_teaching_save, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btnotherupdate)
                        .addGap(32, 32, 32)
                        .addComponent(btnotherdelete)
                        .addGap(18, 18, 18)
                        .addComponent(btnotherclear, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel93Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnothergenexcelsheet)))
                .addContainerGap(170, Short.MAX_VALUE))
        );
        jPanel93Layout.setVerticalGroup(
            jPanel93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel93Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel93Layout.createSequentialGroup()
                        .addGap(0, 55, Short.MAX_VALUE)
                        .addComponent(btnothergenexcelsheet)
                        .addGap(23, 23, 23))
                    .addGroup(jPanel93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnotherdelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnotherupdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_non_teaching_save, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnotherclear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Status");

        ntstatus.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        txtntsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtntsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtntsearchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel90Layout = new javax.swing.GroupLayout(jPanel90);
        jPanel90.setLayout(jPanel90Layout);
        jPanel90Layout.setHorizontalGroup(
            jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel90Layout.createSequentialGroup()
                .addComponent(jPanel101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel90Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel90Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(jLabel127)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtntsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ntstatus, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel90Layout.createSequentialGroup()
                        .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel93, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel102, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );
        jPanel90Layout.setVerticalGroup(
            jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel90Layout.createSequentialGroup()
                .addComponent(jPanel101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel90Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel127)
                                .addComponent(txtntsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(ntstatus, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(64, 64, 64))
                    .addGroup(jPanel90Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addComponent(jPanel90, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel90, javax.swing.GroupLayout.PREFERRED_SIZE, 578, Short.MAX_VALUE)
        );

        jTabbedPane4.addTab("Work and Related", jPanel17);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addComponent(jTabbedPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 1088, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 16, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addComponent(jTabbedPane4)
                .addContainerGap())
        );

        jTabbedPane3.addTab("Non-Teaching Staff", jPanel15);

        jPanel103.setBackground(new java.awt.Color(255, 153, 153));

        jLabel115.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel115.setForeground(new java.awt.Color(255, 255, 255));
        jLabel115.setText("Personal Info");

        javax.swing.GroupLayout jPanel103Layout = new javax.swing.GroupLayout(jPanel103);
        jPanel103.setLayout(jPanel103Layout);
        jPanel103Layout.setHorizontalGroup(
            jPanel103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel103Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel115)
                .addContainerGap(67, Short.MAX_VALUE))
        );
        jPanel103Layout.setVerticalGroup(
            jPanel103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel103Layout.createSequentialGroup()
                .addComponent(jLabel115)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        tr_JDesktopPane.setLayer(lbl_tr_profileimage, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout tr_JDesktopPaneLayout = new javax.swing.GroupLayout(tr_JDesktopPane);
        tr_JDesktopPane.setLayout(tr_JDesktopPaneLayout);
        tr_JDesktopPaneLayout.setHorizontalGroup(
            tr_JDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_tr_profileimage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        tr_JDesktopPaneLayout.setVerticalGroup(
            tr_JDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_tr_profileimage, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
        );

        btntr_uploadimg.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btntr_uploadimg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/attach.png"))); // NOI18N
        btntr_uploadimg.setText("Choose Image");
        btntr_uploadimg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntr_uploadimgActionPerformed(evt);
            }
        });

        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Personal Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel189.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel189.setText("Teacher Name");

        txttrname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrnameKeyReleased(evt);
            }
        });

        jLabel129.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel129.setText("ID No.");

        jLabel145.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel145.setText("D. O. B");

        jLabel146.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel146.setText("Gender");

        cbotrgender.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbotrgender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Gender", "Male", "Female" }));
        cbotrgender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotrgenderActionPerformed(evt);
            }
        });

        jLabel190.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel190.setText("Religion");

        cbotrrel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbotrrel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Religion", "Catholic", "Protestant", "Islamic", "Budhist", "Rastafari" }));

        jLabel147.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel147.setText("Nationality");

        txttrnationality.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrnationality.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrnationalityKeyReleased(evt);
            }
        });

        jLabel121.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel121.setText("Phone No.");

        txttrphone.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrphone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrphoneKeyReleased(evt);
            }
        });

        txttridno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttridno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttridnoKeyReleased(evt);
            }
        });

        txttrdob.setEditable(false);
        txttrdob.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrdob.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txttrdobMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txttridno, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addComponent(txttrnationality, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addComponent(txttrphone, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addComponent(txttrname, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addGroup(jPanel22Layout.createSequentialGroup()
                        .addComponent(txttrdob)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtrDob, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel22Layout.createSequentialGroup()
                        .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel146, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbotrgender, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbotrrel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel190, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel147, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel145, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel121, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel189, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel129, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(63, 63, 63))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel189)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel129)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttridno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel145)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtrDob, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(txttrdob))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel146)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbotrgender, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel190)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbotrrel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel147)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrnationality, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel121)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrphone, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        jPanel23.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Health Info", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel174.setForeground(new java.awt.Color(255, 0, 51));
        jLabel174.setText("Is the employee allergic to any of the following?");

        chktraspirin.setText("Aspirin");

        chktrpenicillin.setText("Penicillin");

        chktrpollen.setText("Pollen");

        chktrmeatandrelated.setText("Meat and Related");

        chktrotherallergy.setText("Other, Please specify ");
        chktrotherallergy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chktrotherallergyActionPerformed(evt);
            }
        });

        txttrotherallergy.setColumns(20);
        txttrotherallergy.setRows(5);
        jScrollPane27.setViewportView(txttrotherallergy);

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel174)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane27)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(chktraspirin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chktrpenicillin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chktrpollen)
                        .addGap(10, 10, 10)
                        .addComponent(chktrmeatandrelated))
                    .addComponent(chktrotherallergy, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel174)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chktrpenicillin)
                    .addComponent(chktrpollen)
                    .addComponent(chktraspirin)
                    .addComponent(chktrmeatandrelated))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chktrotherallergy)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(330, 330, 330))
        );

        jPanel105.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Insurance", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel183.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel183.setText("Insurance Carrier");

        txttrgroupno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrgroupno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrgroupnoKeyReleased(evt);
            }
        });

        jLabel184.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel184.setText("Policy Number");

        txttrinsurancecarrier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrinsurancecarrier.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrinsurancecarrierKeyReleased(evt);
            }
        });

        jLabel185.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel185.setText("Policy Holders Name");

        txttrpolicyno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrpolicyno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrpolicynoKeyReleased(evt);
            }
        });

        jLabel192.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel192.setText("Group / Category");

        txttrpolictholdername.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrpolictholdername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrpolictholdernameKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel105Layout = new javax.swing.GroupLayout(jPanel105);
        jPanel105.setLayout(jPanel105Layout);
        jPanel105Layout.setHorizontalGroup(
            jPanel105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel105Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttrinsurancecarrier)
                    .addComponent(txttrpolicyno)
                    .addComponent(txttrpolictholdername)
                    .addComponent(txttrgroupno)
                    .addGroup(jPanel105Layout.createSequentialGroup()
                        .addGroup(jPanel105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel183)
                            .addComponent(jLabel184)
                            .addComponent(jLabel185)
                            .addComponent(jLabel192))
                        .addGap(0, 233, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel105Layout.setVerticalGroup(
            jPanel105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel105Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel183)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrinsurancecarrier, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel184)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrpolicyno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel185)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrpolictholdername, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel192)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrgroupno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel91Layout = new javax.swing.GroupLayout(jPanel91);
        jPanel91.setLayout(jPanel91Layout);
        jPanel91Layout.setHorizontalGroup(
            jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel91Layout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tr_JDesktopPane)
                    .addComponent(btntr_uploadimg, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel91Layout.createSequentialGroup()
                .addComponent(jPanel103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel91Layout.setVerticalGroup(
            jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel91Layout.createSequentialGroup()
                .addComponent(jPanel103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel91Layout.createSequentialGroup()
                        .addComponent(tr_JDesktopPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btntr_uploadimg))
                    .addGroup(jPanel91Layout.createSequentialGroup()
                        .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(58, 58, 58))
        );

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addComponent(jPanel91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 21, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel91, javax.swing.GroupLayout.PREFERRED_SIZE, 585, Short.MAX_VALUE)
        );

        jTabbedPane5.addTab("Personal Details", jPanel21);

        jPanel106.setBackground(new java.awt.Color(255, 153, 153));

        jLabel116.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel116.setForeground(new java.awt.Color(255, 255, 255));
        jLabel116.setText("Work & Related");

        javax.swing.GroupLayout jPanel106Layout = new javax.swing.GroupLayout(jPanel106);
        jPanel106.setLayout(jPanel106Layout);
        jPanel106Layout.setHorizontalGroup(
            jPanel106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel106Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel116)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel106Layout.setVerticalGroup(
            jPanel106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel106Layout.createSequentialGroup()
                .addComponent(jLabel116)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        jLabel130.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel130.setText("Teaching ID");

        txtsearchtrid_two.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsearchtrid_two.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsearchtrid_twoKeyReleased(evt);
            }
        });

        jPanel25.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Work & Related", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel107.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel107.setText("Employed As");

        jLabel108.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel108.setText("Subject Specialised In");

        jLabel109.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel109.setText("KRA Pin");

        txttrkrapin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrkrapin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrkrapinKeyReleased(evt);
            }
        });

        jLabel110.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel110.setText("P F No.");

        txttrpayslipno.setEditable(false);
        txttrpayslipno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel111.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel111.setText("Department");

        txttrdept.setEditable(false);
        txttrdept.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrdept.setText("Teaching");

        cbotremployedas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbotremployedas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Employed As", "Teacher", "Intern" }));
        cbotremployedas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotremployedasActionPerformed(evt);
            }
        });

        jLabel117.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel117.setText("Salary");

        txttrsalary.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrsalary.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrsalaryKeyReleased(evt);
            }
        });

        txttrtscno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrtscno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrtscnoKeyReleased(evt);
            }
        });

        jLabel112.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel112.setText("TSC No.");

        txttrsubjectspeciality.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrsubjectspeciality.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrsubjectspecialityKeyReleased(evt);
            }
        });

        txttrnssfno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrnssfno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrnssfnoKeyReleased(evt);
            }
        });

        jLabel119.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel119.setText("NSSF No.");

        jLabel122.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel122.setText("Date of Employment / Join ");

        jLabel123.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel123.setText("Which Class?");

        cbotrjobgroup.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbotrjobgroup.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Job Group", "A", "B", "C", "D", "E", "F" }));

        jLabel124.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel124.setText("Job Group");

        cbotrclasstr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbotrclasstr.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Class Teacher?", "Yes", "No" }));
        cbotrclasstr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotrclasstrActionPerformed(evt);
            }
        });

        jLabel132.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel132.setText("Class Tr?");

        jLabel118.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel118.setText("Teaching ID");

        txttrworkingid.setEditable(false);
        txttrworkingid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txttrdateoe.setEditable(false);
        txttrdateoe.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        cbowhichclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbowhichclass.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Class", "Pre-primary 1", "Pre-primary 2", "Grade 1", "Grade 2", "Grade 3", "Class 4", "Class 5", "Class 6" }));

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttrtscno, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(jLabel111)
                    .addComponent(txttrdept, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(txttrsubjectspeciality, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(jLabel112)
                    .addComponent(jLabel108, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel122)
                    .addComponent(jLabel109)
                    .addComponent(txttrkrapin, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(jLabel123)
                    .addComponent(txttrdateoe)
                    .addComponent(cbowhichclass, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(37, 37, 37)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel107, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbotremployedas, 0, 184, Short.MAX_VALUE)
                            .addComponent(jLabel124, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbotrjobgroup, 0, 184, Short.MAX_VALUE)
                            .addComponent(jLabel132)
                            .addComponent(jLabel118)
                            .addComponent(txttrworkingid))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txttrpayslipno)
                            .addGroup(jPanel25Layout.createSequentialGroup()
                                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel119)
                                    .addComponent(txttrnssfno, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(jLabel110)
                                    .addComponent(cbotrclasstr, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(45, 45, 45))))
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(jLabel117))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(txttrsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel111)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttrdept, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel107)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbotremployedas, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel112)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttrtscno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel124)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbotrjobgroup, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel108)
                    .addComponent(jLabel118))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttrsubjectspeciality, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(txttrworkingid, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel122)
                    .addComponent(jLabel132))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbotrclasstr, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttrdateoe, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel119)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttrnssfno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel123)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbowhichclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel110)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttrpayslipno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel109)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttrkrapin, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel117)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrsalary, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );

        jPanel107.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Employed Teachers List", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        tbltrlist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane28.setViewportView(tbltrlist);

        javax.swing.GroupLayout jPanel107Layout = new javax.swing.GroupLayout(jPanel107);
        jPanel107.setLayout(jPanel107Layout);
        jPanel107Layout.setHorizontalGroup(
            jPanel107Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel107Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane28, javax.swing.GroupLayout.DEFAULT_SIZE, 551, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel107Layout.setVerticalGroup(
            jPanel107Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel107Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane28, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel95.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btntrgenerateexcelsheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btntrgenerateexcelsheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btntrgenerateexcelsheet.setText("Generate Employee List Sheet");
        btntrgenerateexcelsheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrgenerateexcelsheetActionPerformed(evt);
            }
        });

        btntrsave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btntrsave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btntrsave.setText("Save");
        btntrsave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrsaveActionPerformed(evt);
            }
        });

        btntrupdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btntrupdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btntrupdate.setText("Update");
        btntrupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrupdateActionPerformed(evt);
            }
        });

        btntrdelete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btntrdelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/delete_small.png"))); // NOI18N
        btntrdelete.setText("Delete");
        btntrdelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrdeleteActionPerformed(evt);
            }
        });

        btntrclear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btntrclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btntrclear.setText("Clear");
        btntrclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrclearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel95Layout = new javax.swing.GroupLayout(jPanel95);
        jPanel95.setLayout(jPanel95Layout);
        jPanel95Layout.setHorizontalGroup(
            jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel95Layout.createSequentialGroup()
                .addGroup(jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel95Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(btntrsave, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btntrupdate)
                        .addGap(32, 32, 32)
                        .addComponent(btntrdelete)
                        .addGap(18, 18, 18)
                        .addComponent(btntrclear, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel95Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btntrgenerateexcelsheet)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel95Layout.setVerticalGroup(
            jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel95Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel95Layout.createSequentialGroup()
                        .addGap(0, 55, Short.MAX_VALUE)
                        .addComponent(btntrgenerateexcelsheet)
                        .addGap(23, 23, 23))
                    .addGroup(jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btntrdelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btntrupdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btntrsave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btntrclear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Status");

        lbltrstatus.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        javax.swing.GroupLayout jPanel94Layout = new javax.swing.GroupLayout(jPanel94);
        jPanel94.setLayout(jPanel94Layout);
        jPanel94Layout.setHorizontalGroup(
            jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel94Layout.createSequentialGroup()
                .addGroup(jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel94Layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(jLabel130)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtsearchtrid_two, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbltrstatus)
                        .addGap(104, 104, 104))
                    .addGroup(jPanel94Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel95, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel94Layout.createSequentialGroup()
                                .addComponent(jPanel107, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(jPanel94Layout.createSequentialGroup()
                .addComponent(jPanel106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel94Layout.setVerticalGroup(
            jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel94Layout.createSequentialGroup()
                .addComponent(jPanel106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(lbltrstatus))
                    .addGroup(jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel130)
                        .addComponent(txtsearchtrid_two, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel94Layout.createSequentialGroup()
                        .addComponent(jPanel107, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addComponent(jPanel94, javax.swing.GroupLayout.PREFERRED_SIZE, 1067, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 16, Short.MAX_VALUE))
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addComponent(jPanel94, javax.swing.GroupLayout.PREFERRED_SIZE, 573, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane5.addTab("Work and Related", jPanel24);

        javax.swing.GroupLayout jPanel56Layout = new javax.swing.GroupLayout(jPanel56);
        jPanel56.setLayout(jPanel56Layout);
        jPanel56Layout.setHorizontalGroup(
            jPanel56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel56Layout.createSequentialGroup()
                .addComponent(jTabbedPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 1088, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 16, Short.MAX_VALUE))
        );
        jPanel56Layout.setVerticalGroup(
            jPanel56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel56Layout.createSequentialGroup()
                .addComponent(jTabbedPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane3.addTab("Teaching Staff", jPanel56);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 1113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 643, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Human Resource", jPanel5);

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jPanel30.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Discipline Records", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel37.setText("Pupil's Admission");

        txtdisciplinesearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtdisciplinesearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdisciplinesearchKeyReleased(evt);
            }
        });

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel42.setText("Date");

        txtdiscdate.setEditable(false);
        txtdiscdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel43.setText("By Whom");

        txtdiscbywhom.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtdiscbywhom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdiscbywhomKeyReleased(evt);
            }
        });

        tbldisclist.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbldisclist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbldisclist.setPreferredSize(new java.awt.Dimension(100, 64));
        jScrollPane14.setViewportView(tbldisclist);

        txtdiscpupilname.setEditable(false);
        txtdiscpupilname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel45.setText("Pupil's Name");

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel46.setText("Gender");

        txtdiscgender.setEditable(false);
        txtdiscgender.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel47.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel47.setText("Class");

        txtdiscclass.setEditable(false);
        txtdiscclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel48.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel48.setText("Age");

        txtdiscage.setEditable(false);
        txtdiscage.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel49.setText("Offence");

        txtdiscoffence.setColumns(20);
        txtdiscoffence.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtdiscoffence.setRows(5);
        jScrollPane17.setViewportView(txtdiscoffence);

        jLabel50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel50.setText("Punishment");

        txtdiscpunishment.setColumns(20);
        txtdiscpunishment.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtdiscpunishment.setRows(5);
        jScrollPane29.setViewportView(txtdiscpunishment);

        btndiscsave.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btndiscsave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btndiscsave.setText("SAVE");
        btndiscsave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndiscsaveActionPerformed(evt);
            }
        });

        btndiscclear.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btndiscclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btndiscclear.setText("CLEAR");
        btndiscclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndiscclearActionPerformed(evt);
            }
        });

        disc_jDesktopPane.setLayer(disc_pupil_img, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout disc_jDesktopPaneLayout = new javax.swing.GroupLayout(disc_jDesktopPane);
        disc_jDesktopPane.setLayout(disc_jDesktopPaneLayout);
        disc_jDesktopPaneLayout.setHorizontalGroup(
            disc_jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(disc_pupil_img, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
        );
        disc_jDesktopPaneLayout.setVerticalGroup(
            disc_jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(disc_pupil_img, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
        );

        txtdiscadmno.setEditable(false);
        txtdiscadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel51.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel51.setText("Admission No.");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Pupil's Status");

        discpstatus.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        discpstatus.setText("Status");

        javax.swing.GroupLayout jPanel30Layout = new javax.swing.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel30Layout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel30Layout.createSequentialGroup()
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel42)
                                .addComponent(jLabel45)
                                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtdiscdate, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtdiscpupilname, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)))
                            .addComponent(txtdiscadmno)
                            .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                            .addComponent(jScrollPane29, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                            .addComponent(jLabel51)
                            .addComponent(jLabel48)
                            .addComponent(jLabel50)
                            .addComponent(jLabel49)
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel46)
                                    .addComponent(txtdiscgender, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel47)
                                    .addComponent(txtdiscclass)))
                            .addComponent(txtdiscage, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel30Layout.createSequentialGroup()
                                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel43)
                                            .addComponent(txtdiscbywhom, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(discpstatus, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5))
                                        .addGap(49, 49, 49)
                                        .addComponent(disc_jDesktopPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel30Layout.createSequentialGroup()
                                        .addComponent(btndiscsave, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btndiscclear, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(94, 94, 94))
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(jPanel30Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jLabel37)
                        .addGap(10, 10, 10)
                        .addComponent(txtdisciplinesearch, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel30Layout.createSequentialGroup()
                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(disc_jDesktopPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel30Layout.createSequentialGroup()
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel37)
                                    .addComponent(txtdisciplinesearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel43))
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addComponent(jLabel42)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtdiscdate, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdiscbywhom, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addComponent(jLabel45)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdiscpupilname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(discpstatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addGap(4, 4, 4)
                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel30Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btndiscsave, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btndiscclear, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel30Layout.createSequentialGroup()
                        .addComponent(jLabel51)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addComponent(txtdiscadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel46)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdiscgender, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel30Layout.createSequentialGroup()
                                .addComponent(jLabel47)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdiscclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel48)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdiscage, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel49)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel50)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane29, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(34, 34, 34))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Discipline Records", jPanel6);

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Pupils List", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        tblpupilslist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblpupilslist);

        btngeneratepupilsexcelsheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btngeneratepupilsexcelsheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btngeneratepupilsexcelsheet.setText("Generate Pupils List Sheet");
        btngeneratepupilsexcelsheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngeneratepupilsexcelsheetActionPerformed(evt);
            }
        });

        jLabel64.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel64.setForeground(new java.awt.Color(255, 0, 0));
        jLabel64.setText("Search Pupil");

        txtlistsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlistsearch.setToolTipText("");
        txtlistsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlistsearchKeyReleased(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel65.setForeground(new java.awt.Color(255, 0, 0));
        jLabel65.setText("Class");

        txtclass.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtclass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtclassKeyReleased(evt);
            }
        });

        btnPupilListRefresh.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPupilListRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnPupilListRefresh.setText("Refresh");
        btnPupilListRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPupilListRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGap(435, 435, 435)
                        .addComponent(btngeneratepupilsexcelsheet)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtlistsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtclass, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(btnPupilListRefresh)
                .addGap(61, 61, 61))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel64)
                    .addComponent(txtlistsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel65)
                    .addComponent(txtclass, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPupilListRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btngeneratepupilsexcelsheet)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Pupils List", jPanel7);

        jPanel31.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Transfer Letter", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel74.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel74.setText("Pupil's Admission No.");

        jLabel75.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel75.setText("Pupi'ls Name");

        txttrnfname.setEditable(false);
        txttrnfname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrnfname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttrnfnameActionPerformed(evt);
            }
        });

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel76.setText("Parent's Name");

        txttrnfparentname.setEditable(false);
        txttrnfparentname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel77.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel77.setText("D. O. B");

        txttrnfdob.setEditable(false);
        txttrnfdob.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel78.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel78.setText("Admission No.");

        txttrnfadmno.setEditable(false);
        txttrnfadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel79.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel79.setText("Current Class");

        txttrnfcurrclass.setEditable(false);
        txttrnfcurrclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel81.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel81.setText("Cause of Leaving");

        jLabel82.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel82.setText("Conduct");

        btnclear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnclear.setText("CLEAR");
        btnclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclearActionPerformed(evt);
            }
        });

        btngeneratetranferletter.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btngeneratetranferletter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btngeneratetranferletter.setText("GENERATE LETTER");
        btngeneratetranferletter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngeneratetranferletterActionPerformed(evt);
            }
        });

        txttrnfhdtrremarks.setColumns(20);
        txttrnfhdtrremarks.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrnfhdtrremarks.setRows(5);
        txttrnfhdtrremarks.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrnfhdtrremarksKeyReleased(evt);
            }
        });
        jScrollPane24.setViewportView(txttrnfhdtrremarks);

        jLabel83.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel83.setText("Headteacher's Remarks");

        txttrnfsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrnfsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrnfsearchKeyReleased(evt);
            }
        });

        trnsfr_jDesktopPane.setLayer(trnf_profile_img, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout trnsfr_jDesktopPaneLayout = new javax.swing.GroupLayout(trnsfr_jDesktopPane);
        trnsfr_jDesktopPane.setLayout(trnsfr_jDesktopPaneLayout);
        trnsfr_jDesktopPaneLayout.setHorizontalGroup(
            trnsfr_jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(trnf_profile_img, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        trnsfr_jDesktopPaneLayout.setVerticalGroup(
            trnsfr_jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(trnf_profile_img, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
        );

        txttrnsfconduct.setColumns(20);
        txttrnsfconduct.setRows(5);
        jScrollPane3.setViewportView(txttrnsfconduct);

        txttrnfcauseoftrnf.setColumns(20);
        txttrnfcauseoftrnf.setRows(5);
        txttrnfcauseoftrnf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrnfcauseoftrnfKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(txttrnfcauseoftrnf);

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttrnfadmno)
                    .addComponent(txttrnfcurrclass)
                    .addComponent(txttrnfparentname)
                    .addComponent(txttrnfdob)
                    .addComponent(jLabel75)
                    .addComponent(jLabel76)
                    .addComponent(txttrnfname, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
                    .addComponent(jLabel77)
                    .addComponent(jLabel78)
                    .addComponent(jLabel79))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel83)
                    .addGroup(jPanel31Layout.createSequentialGroup()
                        .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane24, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                            .addComponent(jLabel82, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel81, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(18, 18, 18)
                        .addComponent(trnsfr_jDesktopPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(61, 61, 61))
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addGap(132, 132, 132)
                .addComponent(jLabel74)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttrnfsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 525, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(298, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnclear, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btngeneratetranferletter)
                .addGap(323, 323, 323))
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel31Layout.createSequentialGroup()
                        .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel74)
                            .addComponent(txttrnfsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel31Layout.createSequentialGroup()
                                .addComponent(jLabel75)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrnfname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel31Layout.createSequentialGroup()
                                        .addComponent(jLabel76)
                                        .addGap(38, 38, 38))
                                    .addComponent(txttrnfparentname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel77)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrnfdob, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel78)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrnfadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel79)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrnfcurrclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel31Layout.createSequentialGroup()
                                .addComponent(jLabel81)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel82)
                                .addGap(4, 4, 4)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel83)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane24, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                    .addComponent(trnsfr_jDesktopPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btngeneratetranferletter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnclear))
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 143, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Transfer", jPanel8);

        jPanel39.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Leaving Certificate", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtsearchleaving.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsearchleaving.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsearchleavingActionPerformed(evt);
            }
        });
        txtsearchleaving.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsearchleavingKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Pupil's Admission No.");

        btnleavingcert.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnleavingcert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnleavingcert.setText("GENERATE CERTIFICATE");
        btnleavingcert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnleavingcertActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel39Layout = new javax.swing.GroupLayout(jPanel39);
        jPanel39.setLayout(jPanel39Layout);
        jPanel39Layout.setHorizontalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel39Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsearchleaving, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnleavingcert)
                .addContainerGap(220, Short.MAX_VALUE))
        );
        jPanel39Layout.setVerticalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel39Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(txtsearchleaving, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnleavingcert))
                .addGap(535, 535, 535))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, 528, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(104, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Leaving Cert.", jPanel9);

        jPanel37.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Event Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jDesktopPane1.setLayer(sa_photo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sa_photo, javax.swing.GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sa_photo, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane1)
                .addContainerGap())
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel42.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Event Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("Date of Event");

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel38.setText("What Event / Activity");

        txtsa2.setEditable(false);
        txtsa2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsa2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsa2KeyReleased(evt);
            }
        });

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel39.setText("Location");

        txtsa3.setEditable(false);
        txtsa3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsa3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsa3KeyReleased(evt);
            }
        });

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel40.setText("Commemorated By ");

        txtsa4.setEditable(false);
        txtsa4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsa4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsa4KeyReleased(evt);
            }
        });

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Comments ");

        txtsa5.setEditable(false);
        txtsa5.setColumns(20);
        txtsa5.setRows(5);
        jScrollPane2.setViewportView(txtsa5);

        txtsa1.setEditable(false);
        txtsa1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel42Layout = new javax.swing.GroupLayout(jPanel42);
        jPanel42.setLayout(jPanel42Layout);
        jPanel42Layout.setHorizontalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel42Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtsa1, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                    .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel39, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                    .addComponent(txtsa4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtsa3, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtsa2, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap(48, Short.MAX_VALUE))
        );
        jPanel42Layout.setVerticalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel42Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsa1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel38)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsa2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsa3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsa4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(88, Short.MAX_VALUE))
        );

        jPanel43.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnsasave.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnsasave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btnsasave.setText("Save");
        btnsasave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsasaveActionPerformed(evt);
            }
        });

        btsauploadnewImage.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btsauploadnewImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/attach.png"))); // NOI18N
        btsauploadnewImage.setText("Upload New Image");
        btsauploadnewImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btsauploadnewImageActionPerformed(evt);
            }
        });

        btnsaview.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnsaview.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btnsaview.setText("View");
        btnsaview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsaviewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel43Layout = new javax.swing.GroupLayout(jPanel43);
        jPanel43.setLayout(jPanel43Layout);
        jPanel43Layout.setHorizontalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(btsauploadnewImage)
                .addGap(56, 56, 56)
                .addComponent(btnsasave, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(btnsaview)
                .addContainerGap(98, Short.MAX_VALUE))
        );
        jPanel43Layout.setVerticalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsasave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btsauploadnewImage, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsaview, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        btnprevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/previous.png"))); // NOI18N
        btnprevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpreviousActionPerformed(evt);
            }
        });

        btnnext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/nxt.png"))); // NOI18N
        btnnext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnprevious, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnnext, javax.swing.GroupLayout.PREFERRED_SIZE, 53, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(221, 221, 221)
                                .addComponent(btnprevious, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(214, 214, 214)
                                .addComponent(btnnext, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("School Album", jPanel10);

        jPanel38.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Visitors Record", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        cbovr1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbovr1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Type of Visitor", "Normal", "Important" }));

        jLabel52.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel52.setText("Name");

        txtvr1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtvr1KeyReleased(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel53.setText("Sign In Time");

        jLabel54.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel54.setText("ID No / Passport/ Military ID");

        txtvr2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtvr2KeyReleased(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel55.setText("Contact");

        txtvr5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtvr5KeyReleased(evt);
            }
        });

        jLabel56.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel56.setText("Gender");

        cbovr2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbovr2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Gender", "Male", "Female" }));

        jLabel58.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel58.setText("Purpose of Visit");

        txtvr3.setColumns(20);
        txtvr3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr3.setRows(5);
        jScrollPane19.setViewportView(txtvr3);

        jLabel61.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel61.setText("Address");

        txtvr4.setColumns(20);
        txtvr4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr4.setRows(5);
        jScrollPane20.setViewportView(txtvr4);

        btnVRClear.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnVRClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnVRClear.setText("CLEAR");
        btnVRClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVRClearActionPerformed(evt);
            }
        });

        btnVRSave.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnVRSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btnVRSave.setText("SAVE");
        btnVRSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVRSaveActionPerformed(evt);
            }
        });

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sign Out Visitor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel66.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel66.setText("Visitor's ID / Passport/ Military ID");

        txtvr7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtvr7KeyReleased(evt);
            }
        });

        btnvrsignout.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnvrsignout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btnvrsignout.setText("Sign Out");
        btnvrsignout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnvrsignoutActionPerformed(evt);
            }
        });

        jLabel59.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel59.setText("Sign Out Time");

        txtvr9.setEditable(false);
        txtvr9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel57.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel57.setText("Comment");

        txtvr8.setColumns(20);
        txtvr8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtvr8.setRows(5);
        jScrollPane18.setViewportView(txtvr8);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtvr7)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel66)
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel59)
                                .addComponent(txtvr9, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel57)
                                    .addComponent(jScrollPane18))))
                        .addGap(0, 14, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(btnvrsignout, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel66)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtvr7, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel57)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane18, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel59)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtvr9, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnvrsignout, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtvr6.setEditable(false);
        txtvr6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel38Layout = new javax.swing.GroupLayout(jPanel38);
        jPanel38.setLayout(jPanel38Layout);
        jPanel38Layout.setHorizontalGroup(
            jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel53)
                    .addComponent(txtvr6, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(557, Short.MAX_VALUE))
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel38Layout.createSequentialGroup()
                                .addGap(73, 73, 73)
                                .addComponent(cbovr1, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel61)
                                .addComponent(jLabel54)
                                .addComponent(jLabel52)
                                .addComponent(jLabel58)
                                .addComponent(txtvr2)
                                .addComponent(jLabel56)
                                .addComponent(cbovr2, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane19)
                                .addComponent(jScrollPane20)
                                .addComponent(txtvr1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel38Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(btnVRClear, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnVRSave, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 19, Short.MAX_VALUE)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(146, 146, 146))
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel55)
                            .addComponent(txtvr5, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel38Layout.setVerticalGroup(
            jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGap(0, 24, Short.MAX_VALUE)
                        .addComponent(cbovr1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel52)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtvr1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel54)
                        .addGap(5, 5, 5)
                        .addComponent(txtvr2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel56)
                        .addGap(5, 5, 5)
                        .addComponent(cbovr2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel58)
                        .addGap(11, 11, 11)
                        .addComponent(jScrollPane19, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel61)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane20, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jLabel55)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtvr5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jLabel53)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtvr6, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVRClear, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVRSave, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Visitors Record", jPanel11);

        jPanel34.setBackground(new java.awt.Color(153, 153, 255));
        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Send Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("Phone Number ");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Message");

        txtsm6.setColumns(20);
        txtsm6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm6.setRows(5);
        jScrollPane7.setViewportView(txtsm6);

        btnsmCancel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnsmCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/cancel.png"))); // NOI18N
        btnsmCancel.setText("Cancel");
        btnsmCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsmCancelActionPerformed(evt);
            }
        });

        txtsmSend.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsmSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/send.png"))); // NOI18N
        txtsmSend.setText("SEND");
        txtsmSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsmSendActionPerformed(evt);
            }
        });

        txtsm4.setEditable(false);
        txtsm4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        lblmsg.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg.setText("Pupils Adm No.");

        txtsm3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm3KeyReleased(evt);
            }
        });

        tblmsglist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane8.setViewportView(tblmsglist);

        rbparent.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbparent.setText("Parent");
        rbparent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbparentActionPerformed(evt);
            }
        });

        rbts.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbts.setText("Teaching Staff");
        rbts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtsActionPerformed(evt);
            }
        });

        rbnt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbnt.setText("Non-Teaching Staff");
        rbnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbntActionPerformed(evt);
            }
        });

        jLabel8.setBackground(new java.awt.Color(255, 0, 0));
        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(204, 0, 0));
        jLabel8.setText("Select Recipient");

        txtsm5.setEditable(false);
        txtsm5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Name");

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane8))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel34Layout.createSequentialGroup()
                                    .addComponent(rbparent, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(95, 95, 95)
                                    .addComponent(rbts, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(46, 46, 46)
                                    .addComponent(rbnt)
                                    .addGap(11, 11, 11))
                                .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel34Layout.createSequentialGroup()
                                    .addGap(118, 118, 118)
                                    .addComponent(btnsmCancel)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel34Layout.createSequentialGroup()
                                    .addGap(233, 233, 233)
                                    .addComponent(jLabel16))
                                .addGroup(jPanel34Layout.createSequentialGroup()
                                    .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblmsg)
                                        .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel17)
                                        .addComponent(txtsm5, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 39, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(txtsm4, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbparent)
                            .addComponent(rbts)
                            .addComponent(rbnt))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblmsg, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm5, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsm4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsmCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(227, 227, 227)
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(265, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 24, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Send Message", jPanel12);

        jPanel35.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        tbllogdetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbllogdetail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbllogdetailKeyReleased(evt);
            }
        });
        jScrollPane15.setViewportView(tbllogdetail);

        jPanel36.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnlogclear.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnlogclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnlogclear.setText("CLEAR");
        btnlogclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlogclearActionPerformed(evt);
            }
        });

        btnloggenreport.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnloggenreport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnloggenreport.setText("GENERATE LOG REPORT");
        btnloggenreport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnloggenreportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addGap(242, 242, 242)
                .addComponent(btnloggenreport, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnlogclear, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(241, Short.MAX_VALUE))
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnlogclear, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnloggenreport, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Work ID");

        txtlogworkid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlogworkid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlogworkidActionPerformed(evt);
            }
        });
        txtlogworkid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlogworkidKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel35Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(txtlogworkid, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(452, 452, 452))
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 1029, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel35Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtlogworkid, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 11, Short.MAX_VALUE))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(0, 14, Short.MAX_VALUE)
                .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Logs", jPanel33);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtnlogout)
                .addGap(32, 32, 32))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jbtnlogout, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 14, Short.MAX_VALUE))
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        lblloggedinusercode.setText("currently logged in user code");

        jLabel3.setText("Logged in as:");

        lbltime.setText("time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(lblloggedinusercode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbltime, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(1038, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1372, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblloggedinusercode)
                    .addComponent(lbltime))
                .addGap(119, 119, 119))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 764, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnlibActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlibActionPerformed
        try {
            this.dispose();
            
            Library lib = new Library();
            lib.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnlibActionPerformed

    private void btnacademicsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnacademicsActionPerformed
        try {
            // TODO add your handling code here:
            Academics ac = new Academics();
            ac.setVisible(true);
            //this.dispose();
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnacademicsActionPerformed

    private void btnfinanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfinanceActionPerformed
        // TODO add your handling code here:
        this.dispose();
        
        try {
            FandA fa = new FandA();
            fa.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnfinanceActionPerformed

    private void btnadmissionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnadmissionActionPerformed
        try {
            // TODO add your handling code here:
            Admissions ad = new Admissions();
            ad.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnadmissionActionPerformed

    private void btnstoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstoreActionPerformed
        try {
            this.dispose();
            
            Stock_keeping sk = new Stock_keeping();
            sk.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnstoreActionPerformed

    private void btn_non_teaching_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_non_teaching_saveActionPerformed
        try {
            // TODO add your handling code here:
            String name = txtotnamee.getText();
            String idno = txtotidno.getText();
            
//            Date oDate = jotDob.getDate();
//            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String ntdob = txtotdob.getText();
            
            //Gender
            String gndr;
            switch (cbootgndr.getSelectedIndex()) {
                case 1:
                    gndr = cbootgndr.getSelectedItem().toString();
                    break;
                case 2:
                    gndr = cbootgndr.getSelectedItem().toString();
                    break;
                default:
                    gndr = "";
                    break;
            }
            
            //Religion
            String religion;
            switch (cbootrel.getSelectedIndex()) {
                case 1:
                    religion = cbootrel.getSelectedItem().toString();
                    break;
                case 2:
                    religion = cbootrel.getSelectedItem().toString();
                    break;
                case 3:
                    religion = cbootrel.getSelectedItem().toString();
                    break;
                case 4:
                    religion = cbootrel.getSelectedItem().toString();
                    break;
                case 5:
                    religion = cbootrel.getSelectedItem().toString();
                    break;
                default:
                    religion = "";
                    break;
            }
            
            String nationality = txtotnationality.getText();
            String phone = txtotphone.getText();
            //String email = txtotemail.getText();
            
            String aspirin;
            if(chkotaspirin.isSelected()){
                aspirin = chkotaspirin.getText();
            }else{
                aspirin = "";
            }
            
            String penicillin;
            if(chkotpenicillin.isSelected()){
                penicillin = chkotpenicillin.getText();
            }else{
                penicillin = "";
            }
            
            String pollen;
            if(chkotpollen.isSelected()){
                pollen = chkotpollen.getText();
            }else{
                pollen = "";
            }
            
            String meatandrelated;
            if(chkotmeatandrelated.isSelected()){
                meatandrelated = chkotmeatandrelated.getText();
            }else{
                meatandrelated = "";
            }
            
            String otherhealthinfo;
            if(chkototherhealthinfo.isSelected()){
                otherhealthinfo = txtototherhealthinfo.getText();
            }else{
                otherhealthinfo = "";
            }
            
            String insurancecarrier = txtotinsurancecarrier.getText();
            String insurancepolicyno = txtotpolicyno.getText();
            String policyholdername = txtotpolicyholdername.getText();
            String policycategory = txtotgroup_category.getText();
            //byte[] otherstaff_profileimg = other_image;
            
            String dept;
            switch (cbootdept.getSelectedIndex()) {
                case 1:
                    dept = cbootdept.getSelectedItem().toString();
                    break;
                case 2:
                    dept = cbootdept.getSelectedItem().toString();
                    break;
                default:
                    dept = "";
                    break;
            }
            
            String empas;
            switch (cbootemployedas.getSelectedIndex()) {
                case 1:
                    empas = cbootemployedas.getSelectedItem().toString();
                    break;
                case 2:
                    empas = cbootemployedas.getSelectedItem().toString();
                    break;
                default:
                    empas = "";
                    break;
            }
            
            String workid = txtotworkid.getText().toUpperCase();
            String doe = txtotdoe.getText();
            String nssf = txtotnssf.getText();
            String krapin = txtotkrapin.getText();
            String payslipno = txtotpayslip.getText();
            String salary = txtotsalary.getText();
            
            int status = 1;
            
            //Validating fields
            if (name.isEmpty() || ntdob.isEmpty() || nationality.isEmpty() || phone.isEmpty() || insurancecarrier.isEmpty() ||
                insurancepolicyno.isEmpty() || policyholdername.isEmpty() || policycategory.isEmpty() || cbootemployedas.getSelectedIndex() == 0 ||
                doe.isEmpty() || nssf.isEmpty() || krapin.isEmpty() || salary.isEmpty() || idno.isEmpty() || workid.isEmpty() || 
                cbootgndr.getSelectedIndex() == 0 || cbootrel.getSelectedIndex() == 0) {
                
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            
            }else{
                
                int x = JOptionPane.showConfirmDialog(null, "Are you sure to add a new Non-Teaching staff member?", "New Staff", JOptionPane.YES_NO_OPTION);

                if (x == 0) {

                }
                String sql = "INSERT INTO `otherstaff`(`ename`, `eid_no`, `edob`, `e_gender`, `e_religion`, `e_nationality`, `e_phone`, "
                        + " `e_aspirin`, `e_penicillin`, `e_pollen`, `e_meatandrelated`, `e_other`, `icarrier`, `ipolicy_no`, `ipolicy_holder_name`,"
                        + " `igroup_no`, `e_profile_img`, `e_dept`, `e_employed_as`, `e_work_id`, `e_doe`, `e_nssf_no`, `e_kra_pin`, `e_payslip_no`,"
                        + " `e_salary`, `status`) "

                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                pst = conn.prepareStatement(sql);
                pst.setString(1, name);
                pst.setString(2, idno);
                pst.setString(3, ntdob);
                pst.setString(4, gndr);
                pst.setString(5, religion);
                pst.setString(6, nationality);
                pst.setString(7, phone);
                pst.setString(8, aspirin);
                pst.setString(9, penicillin);
                pst.setString(10, pollen);
                pst.setString(11, meatandrelated);
                pst.setString(12, otherhealthinfo);
                pst.setString(13, insurancecarrier);
                pst.setString(14, insurancepolicyno);
                pst.setString(15, policyholdername);
                pst.setString(16, policycategory);
                pst.setBytes(17, other_image);
                pst.setString(18, dept);
                pst.setString(19, empas);
                pst.setString(20, workid);
                pst.setString(21, doe);
                pst.setString(22, nssf);
                pst.setString(23, krapin);
                pst.setString(24, payslipno);
                pst.setString(25, salary);
                pst.setInt(26, status);

                pst.execute();

                JOptionPane.showMessageDialog(null, "Data successfully inserted");
                
                //Registering as user with default credentials
                int empasno = cbootemployedas.getSelectedIndex();
                if (empasno != 1 && empasno != 2 && empasno != 3 ) {
                    System.out.println("These users do not require accounts.");
                } else {
                    try {
                        String userquery = "INSERT INTO `testuser` (`designation`, `username`, `usercode`) VALUES (?,?,?)";
                        pst = conn.prepareStatement(userquery);
                        pst.setString(1, empas);
                        pst.setString(2, name);
                        pst.setString(3, workid);
                        
                        pst.execute();
                        
                        System.out.println("New user created.");
                        
                    } catch (SQLException ex) {
                        Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                
                nt_clearFields();
                
                DefaultTableModel model = (DefaultTableModel)tblothlist.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                UpdateNT_table();
          }
            
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }          
        
    }//GEN-LAST:event_btn_non_teaching_saveActionPerformed

    private boolean isValid(String email){
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
                
        Pattern pat = Pattern.compile(emailRegex); 

        if (email == null) 

            return false; 

        return pat.matcher(email).matches();
    }
    
    private void chkotaspirinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkotaspirinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkotaspirinActionPerformed

    private void chkototherhealthinfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkototherhealthinfoActionPerformed
        // TODO add your handling code here:
        if(chkototherhealthinfo.isSelected()){
                txtototherhealthinfo.setEditable(true);
        }else{
            txtototherhealthinfo.setEditable(false);
        }
    }//GEN-LAST:event_chkototherhealthinfoActionPerformed

    private void chkotpenicillinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkotpenicillinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkotpenicillinActionPerformed

    private void chkotpollenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkotpollenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkotpollenActionPerformed

    private void chkotmeatandrelatedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkotmeatandrelatedActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkotmeatandrelatedActionPerformed

    private void btntrsaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrsaveActionPerformed
        // TODO add your handling code here:
        try {                       
            String name = txttrname.getText();
            String idno = txttridno.getText();
            String trdob = txttrdob.getText();

            //Gender
            String gndr;
            switch (cbotrgender.getSelectedIndex()) {
                case 1:
                    gndr = cbotrgender.getSelectedItem().toString();
                    break;
                case 2:
                    gndr = cbotrgender.getSelectedItem().toString();
                    break;
                default:
                    gndr = "";
                    break;
                 }

            //Religion
            String religion;
            switch (cbotrrel.getSelectedIndex()) {
                case 1:
                    religion = cbotrrel.getSelectedItem().toString();
                    break;
                case 2:
                    religion = cbotrrel.getSelectedItem().toString();
                    break;
                case 3:
                    religion = cbotrrel.getSelectedItem().toString();
                    break;
                case 4:
                    religion = cbotrrel.getSelectedItem().toString();
                    break;
                case 5:
                    religion = cbotrrel.getSelectedItem().toString();
                    break;
                default:
                    religion = "";
                    break;
            }

            String nationality = txttrnationality.getText();
            String phone = txttrphone.getText();
            //String email = txttremail.getText();

            String aspirin;
            if(chktraspirin.isSelected()){
                aspirin = chktraspirin.getText();
            }else{
                aspirin = "";
            }

            String penicillin;
            if(chktrpenicillin.isSelected()){
                penicillin = chktrpenicillin.getText();
            }else{
                penicillin = "";
            }

            String pollen;
            if(chktrpollen.isSelected()){
                pollen = chktrpollen.getText();
            }else{
                pollen = "";
            }

            String meatandrelated;
            if(chktrmeatandrelated.isSelected()){
                meatandrelated = chktrmeatandrelated.getText();
            }else{
                meatandrelated = "";
            }

            String otherhealthinfo;
            if(chktrotherallergy.isSelected()){
                otherhealthinfo = txttrotherallergy.getText();
            }else{
                otherhealthinfo = "";
            }

            String insurancecarrier = txttrinsurancecarrier.getText();
            String insurancepolicyno = txttrpolicyno.getText();
            String policyholdername = txttrpolictholdername.getText();
            String policycategory = txttrgroupno.getText();
            //byte[] tr_profileimg = this.teacher_image;

            String dept = txttrdept.getText();

            String empas;
            switch (cbotremployedas.getSelectedIndex()) {
                case 1:
                    empas = cbotremployedas.getSelectedItem().toString();
                    break;
                case 2:
                    empas = cbotremployedas.getSelectedItem().toString();
                    break;
                default:
                    empas = "";
                    break;
            }

            String tscno = txttrtscno.getText();

            String jobgrp;
            switch (cbotrjobgroup.getSelectedIndex()) {
                case 1:
                    jobgrp = cbotrjobgroup.getSelectedItem().toString();
                    break;
                case 2:
                    jobgrp = cbotrjobgroup.getSelectedItem().toString();
                    break;
                default:
                    jobgrp = "";
                    break;
            }

            String speciality = txttrsubjectspeciality.getText();
            String teachingid = txttrworkingid.getText().toUpperCase();
            String doe = txttrdateoe.getText();
            
            String classtr;
            switch (cbotrclasstr.getSelectedIndex()) {
                case 1:
                    classtr = cbotrclasstr.getSelectedItem().toString();
                    break;
                case 2:
                    classtr = cbotrclasstr.getSelectedItem().toString();
                    break;
                default:
                    classtr = "";
                    break;
            }

            String whichclass;
            switch (cbotrclasstr.getSelectedIndex()) {
                case 1:
                    cbowhichclass.setEditable(true);
                    whichclass = cbowhichclass.getSelectedItem().toString();
                    break;
                case 2:
                    cbowhichclass.setEditable(false);
                    whichclass = "";
                    break;
                default:
                    whichclass = "";
                    break;
            }

            String nssf = txttrnssfno.getText();
            String krapin = txttrkrapin.getText();
            String payslipno = txttrpayslipno.getText();
            String salary = txttrsalary.getText();

            int status = 1;
            
            //Validating fields
            if (!name.isEmpty() && !trdob.isEmpty() && !nationality.isEmpty() && !phone.isEmpty() && !insurancecarrier.isEmpty() && 
                !insurancepolicyno.isEmpty() && !policyholdername.isEmpty() && !policycategory.isEmpty() && 
                !doe.isEmpty() && !nssf.isEmpty() && !krapin.isEmpty() && !salary.isEmpty() && !idno.isEmpty() && !teachingid.isEmpty() && 
                cbotrgender.getSelectedIndex() != 0 && !dept.isEmpty() && cbotrrel.getSelectedIndex() != 0 && cbotremployedas.getSelectedIndex() != 0 
                && !tscno.isEmpty() && cbotrjobgroup.getSelectedIndex() != 0 && !speciality.isEmpty() && cbotrclasstr.getSelectedIndex() != 0   ) {
                
                int x = JOptionPane.showConfirmDialog(null, "Add new teacher?", "New Teacher", JOptionPane.YES_NO_OPTION);
                
                if (x == 0) {
                    String sql = "INSERT INTO `teachers`(`t_name`, `t_idno`, `t_dob`, `t_gender`, `t_religion`, `t_nationality`, `t_phone`, "
                            + " `t_haspirin`, `t_hpenicillin`, `t_hpollen`, `t_hmeatandrelated`, `t_hother`, `t_icarrier`, `t_ipolicyno`, `t_ipolicyholder`,"
                            + " `t_igroup`, `t_profile_img`, `t_dept`, `t_emp_as`, `t_tscno`, `t_jobgrp`, `t_sbjcrspeciality`, `t_trid`, `t_dateofjoin`,"
                            + " `t_class_tr`, `t_which_class`, `t_nssf_no`, `t_kra_pin`, `t_payslip_no`, `t_salary`, `status`) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, name);
                    pst.setString(2, idno);
                    pst.setString(3, trdob);
                    pst.setString(4, gndr);
                    pst.setString(5, religion);
                    pst.setString(6, nationality);
                    pst.setString(7, phone);
                    //pst.setString(8, email);
                    pst.setString(8, aspirin);
                    pst.setString(9, penicillin);
                    pst.setString(10, pollen);
                    pst.setString(11, meatandrelated);
                    pst.setString(12, otherhealthinfo);
                    pst.setString(13, insurancecarrier);
                    pst.setString(14, insurancepolicyno);
                    pst.setString(15, policyholdername);
                    pst.setString(16, policycategory);
                    pst.setBytes(17, teacher_image);
                    pst.setString(18, dept);
                    pst.setString(19, empas);
                    pst.setString(20, tscno);
                    pst.setString(21, jobgrp);
                    pst.setString(22, speciality);
                    pst.setString(23, teachingid);
                    pst.setString(24, doe);
                    pst.setString(25, classtr);
                    pst.setString(26, whichclass);
                    pst.setString(27, nssf);
                    pst.setString(28, krapin);
                    pst.setString(29, payslipno);
                    pst.setString(30, salary);
                    pst.setInt(31, status);
                    
                    pst.execute();
                    
                    JOptionPane.showMessageDialog(null, "Success.");
                    
                    //Registering as user with default credentials
                    try {
                        String userquery = "INSERT INTO `testuser`(`designation`, `username`, `usercode`) VALUES (?,?,?)";
                        pst = conn.prepareStatement(userquery);
                        pst.setString(1, empas);
                        pst.setString(2, name);
                        pst.setString(3, teachingid);

                        pst.execute();

                        System.out.println("New user created.");

                    } catch (SQLException ex) {
                        Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    clearTrFields();
                    
                    DefaultTableModel model = (DefaultTableModel)tbltrlist.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    UpdateTr_table();
                    
                }
            }else{
                
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");

            }
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }//GEN-LAST:event_btntrsaveActionPerformed

    private void chktrotherallergyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chktrotherallergyActionPerformed
        // TODO add your handling code here:
        if(chktrotherallergy.isSelected()){
                txttrotherallergy.setEditable(true);
        }else{
            txttrotherallergy.setEditable(false);
        }
    }//GEN-LAST:event_chktrotherallergyActionPerformed

    private void btndiscsaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndiscsaveActionPerformed
        // TODO add your handling code here:
        //Saving Disciplinary Data        
         try{
             String date = txtdiscdate.getText();
             String pupilsname = txtdiscpupilname.getText();
             String pupiladmno = txtdiscadmno.getText();
             String pupilgender = txtdiscgender.getText();
             String pupilclass = txtdiscclass.getText();
             String pupilage = txtdiscage.getText();
             String pupiloffence = txtdiscoffence.getText();
             String pupilpunishment = txtdiscpunishment.getText();
             String punishmentbywhom = txtdiscbywhom.getText();
             String pupilstatus = discpstatus.getText();
             //byte[] pupil_profileimg = this.pupil_image;
             
             if (pupiloffence.isEmpty() || pupilpunishment.isEmpty() || punishmentbywhom.isEmpty()) {
                 
                 JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled");
                 
             } else {             
                int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Add a new Disciplinary record?", "Add Record", JOptionPane.YES_NO_OPTION);

                if(x==0){

                String sql ="INSERT INTO `discipline`(`d_pdate`, `d_pname`, `d_padmno`, `d_pgender`, `d_pclass`, `d_p_age`, `d_poffence`,"
                        + " `d_ppunishment`, `d_pwhom`, `status`, `d_p_profile_img`) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?) ";

                   pst = conn.prepareStatement(sql);
                   pst.setString(1, date);
                   pst.setString(2, pupilsname);
                   pst.setString(3, pupiladmno);
                   pst.setString(4, pupilgender);
                   pst.setString(5, pupilclass);
                   pst.setString(6, pupilage);
                   pst.setString(7, pupiloffence);
                   pst.setString(8, pupilpunishment);
                   pst.setString(9, punishmentbywhom);
                   pst.setString(10, pupilstatus);
                   pst.setBytes(11, pupil_image);
                   pst.execute();

                   JOptionPane.showMessageDialog(null, "Data succesfully recorded");
                   
                   DefaultTableModel model = (DefaultTableModel)tblothlist.getModel();
                   model.setColumnCount(0);
                   model.setRowCount(0);
                   Discipline_table();

                   txtdiscdate.setText("");
                   txtdiscpupilname.setText("");
                   txtdiscadmno.setText("");
                   txtdiscgender.setText("");
                   txtdiscclass.setText("");
                   txtdiscage.setText("");
                   txtdiscoffence.setText("");
                   txtdiscpunishment.setText("");
                   txtdiscbywhom.setText("");
                   discpstatus.setText("Status");
                   disc_pupil_img.setText("");
                }   
            }
	}catch(SQLException e){
            JOptionPane.showMessageDialog(null,e);
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
				
        }			
    }//GEN-LAST:event_btndiscsaveActionPerformed

    private void txtdisciplinesearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdisciplinesearchKeyReleased
        // TODO add your handling code here:
        //Searching in discipline
        try {
            String sql = "SELECT `name`,`admno`,`gender`,`n_class`,`age`,`status`,`profile_image` FROM `admissions` WHERE `admno`=? AND `status` = \"1\" ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txtdisciplinesearch.getText());
            rs = pst.executeQuery();
            
            if (rs.next()) {
                String pupilsname = rs.getString("name");
                txtdiscpupilname.setText(pupilsname);
                String padmno = rs.getString("admno");
                txtdiscadmno.setText(padmno);
                String pupilgndr = rs.getString("gender");
                txtdiscgender.setText(pupilgndr);
                String pupilclass = rs.getString("n_class");
                txtdiscclass.setText(pupilclass);
                String pupilage = rs.getString("age");
                txtdiscage.setText(pupilage);
                String pupilstatus = rs.getString("status");
                discpstatus.setText(pupilstatus);
//                byte[] disc_profileimage = rs.getBytes("profile_image");
//                ImageIcon imageIcon = new ImageIcon(new ImageIcon(disc_profileimage).getImage().getScaledInstance(disc_pupil_img.getWidth(), disc_pupil_img.getHeight(), Image.SCALE_DEFAULT));
//                disc_pupil_img.setIcon(imageIcon);
                
                byte[] disc_profileimage = rs.getBytes("profile_image");
                ImageIcon imageIcon = new ImageIcon(new ImageIcon(disc_profileimage).getImage().getScaledInstance(disc_pupil_img.getWidth(), disc_pupil_img.getHeight(), Image.SCALE_SMOOTH));
                disc_pupil_img.setIcon(imageIcon);
            }
            
        } catch (SQLException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, e);
        }
        
    }//GEN-LAST:event_txtdisciplinesearchKeyReleased

    private void btn_other_uploadimgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_other_uploadimgActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        
        filename = f.getAbsolutePath();
        ImageIcon imageIcon = new ImageIcon (new ImageIcon(filename).getImage().getScaledInstance(lbl_other_profileimage.getWidth(), lbl_other_profileimage.getHeight(), Image.SCALE_SMOOTH));
        lbl_other_profileimage.setIcon(imageIcon);
        
        try{
         File image = new File(filename);
         FileInputStream fix = new FileInputStream(image);
         ByteArrayOutputStream bos = new ByteArrayOutputStream();
         byte[] buf = new byte[1024];
         
         for(int number;(number = fix.read(buf))!= -1;){
             bos.write(buf, 0, number);
         }
         other_image = bos.toByteArray();
            
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Ops! File could not be uploaded");
        }
    }//GEN-LAST:event_btn_other_uploadimgActionPerformed

    private void btntr_uploadimgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntr_uploadimgActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        
        filename = f.getAbsolutePath();
        ImageIcon imageIcon = new ImageIcon (new ImageIcon(filename).getImage().getScaledInstance(lbl_tr_profileimage.getWidth(), lbl_tr_profileimage.getHeight(), Image.SCALE_SMOOTH));
        lbl_tr_profileimage.setIcon(imageIcon);
        
        try{
         File image = new File(filename);
         FileInputStream fix = new FileInputStream(image);
         ByteArrayOutputStream bos = new ByteArrayOutputStream();
         byte[] buf = new byte[1024];
         
         for(int number;(number = fix.read(buf))!= -1;){
             bos.write(buf, 0, number);
         }
         teacher_image = bos.toByteArray();
            
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Ops! File could not be uploaded");
        }
    }//GEN-LAST:event_btntr_uploadimgActionPerformed

    private void btntrupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrupdateActionPerformed
        // TODO add your handling code here:
        txttridno.setEditable(false);
        //txttremail.setEditable(false);
        txttrinsurancecarrier.setEditable(false);
        txttrpolicyno.setEditable(false);
        txttrpolictholdername.setEditable(false);
        txttrgroupno.setEditable(false);
        txttrtscno.setEditable(false);
        txttrdateoe.setEditable(false);
        txttrnssfno.setEditable(false);
        txttrkrapin.setEditable(false);
        txttrpayslipno.setEditable(false);
        
        //Updating Teaching staff
        int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Update Teaching Staff record?", "Update Record", JOptionPane.YES_NO_OPTION);
        if(x==0){
        try{
          String tr_workid = txtsearchtrid_two.getText();
          
            if (!tr_workid.isEmpty()) {
                String sql = "UPDATE `teachers` SET `t_name`=?,`t_idno`=?,`t_dob`=?,`t_gender`=?,`t_religion`=?,"
                        + "`t_nationality`=?,`t_phone`=?,`t_haspirin`=?,`t_hpenicillin`=?,"
                        + "`t_hpollen`=?,`t_hmeatandrelated`=?,`t_hother`=?,`t_icarrier`=?,`t_ipolicyno`=?,"
                        + "`t_ipolicyholder`=?,`t_igroup`=?,`t_profile_img`=?,`t_dept`=?,`t_emp_as`=?,"
                        + "`t_tscno`=?,`t_jobgrp`=?,`t_sbjcrspeciality`=?,`t_trid`=?,`t_dateofjoin`=?,"
                        + "`t_class_tr`=?,`t_which_class`=?,`t_nssf_no`=?,`t_kra_pin`=?,`t_payslip_no`=?,"
                        + "`t_salary`=?,`status`=? WHERE `t_trid` = '"+tr_workid+"'";
                pst = conn.prepareStatement(sql);

                pst.setString(1, txttrname.getText());
                pst.setString(2, txttridno.getText());

                Date oDate = jtrDob.getDate();
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String date = df.format(oDate);
                pst.setString(3, date);

                String gender;
                gender = cbotrgender.getSelectedItem().toString();
                pst.setString(4, gender);

                String religion;
                religion = cbotrrel.getSelectedItem().toString();
                pst.setString(5, religion);

                pst.setString(6, txttrnationality.getText());
                pst.setString(7, txttrphone.getText());
                //pst.setString(8, txttremail.getText());

                String aspirin = "";
                if (chktraspirin.isSelected()) {
                    aspirin += chktraspirin.getText();
                }
                pst.setString(8, aspirin);
                
                String penicillin = "";
                if (chktrpenicillin.isSelected()) {
                    penicillin += chktrpenicillin.getText();
                }
                pst.setString(9, penicillin);
                
                String pollen = "";
                if (chktrpollen.isSelected()) {
                    pollen += chktrpollen.getText();
                }
                pst.setString(10, pollen);
                
                String meatandrelated = "";
                if (chktrmeatandrelated.isSelected()) {
                    meatandrelated += chktrmeatandrelated.getText();
                }
                pst.setString(11, meatandrelated);
                
                String otherallergyinfo = "";
                if (chktrotherallergy.isSelected()) {
                    otherallergyinfo += txttrotherallergy.getText();
                }
                pst.setString(12, otherallergyinfo);
                
                pst.setString(13, txttrinsurancecarrier.getText());
                pst.setString(14, txttrpolicyno.getText());
                pst.setString(15, txttrpolictholdername.getText());
                pst.setString(16, txttrgroupno.getText());
                pst.setBytes(17, this.teacher_image);
                
                pst.setString(18, txttrdept.getText());
                
                String empdas;
                empdas = cbotremployedas.getSelectedItem().toString();
                pst.setString(19, empdas);
                
                pst.setString(20, txttrtscno.getText());
                
                String jobgroup;
                jobgroup = cbotrjobgroup.getSelectedItem().toString();
                pst.setString(21, jobgroup);
                
                pst.setString(22, txttrsubjectspeciality.getText());
                pst.setString(23, txttrworkingid.getText());
                pst.setString(24, txttrdateoe.getText());
                
                String classtr;
                classtr = cbotrclasstr.getSelectedItem().toString();
                pst.setString(25, classtr);

                pst.setString(26, cbowhichclass.getSelectedItem().toString());
                pst.setString(27, txttrnssfno.getText());
                pst.setString(28, txttrkrapin.getText());
                pst.setString(29, txttrpayslipno.getText());
                pst.setDouble(30, Double.parseDouble(txttrsalary.getText()));
                pst.setString(31, lbltrstatus.getText());
                
                
                pst.executeUpdate();

                JOptionPane.showMessageDialog(null, "Teaching Record Updated!");

                clearTrFields();
                
                DefaultTableModel model = (DefaultTableModel)tbltrlist.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                UpdateTr_table();
                
            }else{
                JOptionPane.showMessageDialog(null, "Error");
                
            }
            
        }catch(HeadlessException | SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
            
        }
        
        }
        
    }//GEN-LAST:event_btntrupdateActionPerformed

    private void btnotherupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnotherupdateActionPerformed
        // TODO add your handling code here:
        txtotidno.setEditable(false);
        //txtotemail.setEditable(false);
        txtotinsurancecarrier.setEditable(false);
        txtotpolicyno.setEditable(false);
        txtotpolicyholdername.setEditable(false);
        txtotgroup_category.setEditable(false);
        txtotdoe.setEditable(false);
        txtotnssf.setEditable(false);
        txtotkrapin.setEditable(false);
        
        //Updating Non-teaching staff
        int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Update record?", "Update Record", JOptionPane.YES_NO_OPTION);
        if(x==0){
        try{
          String nt_workid = txtntsearch.getText();
          
            if (nt_workid.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Error");
                
            }else{
                String sql = "UPDATE `otherstaff` SET `ename`=?,`eid_no`=?,`edob`=?,`e_gender`=?,`e_religion`=?,`e_nationality`=?,`e_phone`=?,"
                        + "`e_aspirin`=?,`e_penicillin`=?,`e_pollen`=?,`e_meatandrelated`=?,`e_other`=?,`icarrier`=?,`ipolicy_no`=?,"
                        + "`ipolicy_holder_name`=?,`igroup_no`=?,`e_profile_img`=?,`e_dept`=?,`e_employed_as`=?,`e_work_id`=?,`e_doe`=?,`e_nssf_no`=?,"
                        + "`e_kra_pin`=?,`e_payslip_no`=?,`e_salary`=? WHERE `e_work_id` = '"+nt_workid+"'";
                pst = conn.prepareStatement(sql);

                pst.setString(1, txtotnamee.getText());
                pst.setString(2, txtotidno.getText());

                Date oDate = jotDob.getDate();
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String date = df.format(oDate);                
                pst.setString(3, date);

                String gender;
                gender = cbootgndr.getSelectedItem().toString();
                pst.setString(4, gender);

                String religion;
                religion = cbootrel.getSelectedItem().toString();
                pst.setString(5, religion);

                pst.setString(6, txtotnationality.getText());
                pst.setString(7, txtotphone.getText());
                //pst.setString(8, txtotemail.getText());

                String aspirin = "";
                  if (chkotaspirin.isSelected()) {
                      aspirin += chkotaspirin.getText();
                  }
                  pst.setString(8, aspirin);

                  String penicillin = "";
                  if (chkotpenicillin.isSelected()) {
                      penicillin += chkotpenicillin.getText();
                  }
                  pst.setString(9, penicillin);

                  String pollen = "";
                  if (chkotpollen.isSelected()) {
                      pollen += chkotpollen.getText();
                  }
                  pst.setString(10, pollen);

                  String meatandrelated = "";
                  if (chkotmeatandrelated.isSelected()) {
                      meatandrelated += chkotmeatandrelated.getText();
                  }
                  pst.setString(11, meatandrelated);

                  String otherallergyinfo = "";
                  if (chkototherhealthinfo.isSelected()) {
                      otherallergyinfo += txtototherhealthinfo.getText();
                  }
                  pst.setString(12, otherallergyinfo);          

                pst.setString(13, txtotinsurancecarrier.getText());
                pst.setString(14, txtotpolicyno.getText());
                pst.setString(15, txtotpolicyholdername.getText());
                pst.setString(16, txtotgroup_category.getText());
                pst.setBytes(17, other_image);

                String dept;
                dept = cbootdept.getSelectedItem().toString();
                pst.setString(18, dept);

                String empdas;
                empdas = cbootemployedas.getSelectedItem().toString();
                pst.setString(19, empdas);

                pst.setString(20, txtotworkid.getText());
                pst.setString(21, txtotdoe.getText());
                pst.setString(22, txtotnssf.getText());
                pst.setString(23, txtotkrapin.getText());
                pst.setString(24, txtotpayslip.getText());
                pst.setDouble(25, Double.parseDouble(txtotsalary.getText()));


                pst.executeUpdate();

                JOptionPane.showMessageDialog(null, "Record Updated!");

                nt_clearFields();
                
                DefaultTableModel model = (DefaultTableModel)tblothlist.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                UpdateNT_table();
                
            }
            
        }catch(HeadlessException | SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
            
        }
        
        }
        
    }//GEN-LAST:event_btnotherupdateActionPerformed

    private void btnotherclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnotherclearActionPerformed
        // TODO add your handling code here:
            nt_clearFields();
            
            DefaultTableModel model = (DefaultTableModel)tblothlist.getModel();
            model.setColumnCount(0);
            model.setRowCount(0);
            //UpdateNT_table();
            
            tblothlist.setModel(UpdateNT_table());
    }//GEN-LAST:event_btnotherclearActionPerformed

    private void btnotherdeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnotherdeleteActionPerformed
        // TODO add your handling code here:
        int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Delete record?","Delete Record",JOptionPane.YES_NO_OPTION);
        
        if(x==0){
            try{
                String nt_workid = txtntsearch.getText();
                
                if (nt_workid.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Error");
                }else{

                    String sql = "UPDATE `otherstaff` SET `status` = 0 WHERE `e_work_id` = '"+nt_workid+"' ";
                    pst = conn.prepareStatement(sql);

                    pst.executeUpdate();
               
                    JOptionPane.showMessageDialog(null, "Record Deleted!");
                    
                    nt_clearFields();

                    DefaultTableModel model = (DefaultTableModel)tblothlist.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    UpdateNT_table();
                    
                }
                
                //Removing from users list
                try {
                    String remsql = "DELETE FROM `testuser` WHERE `usercode`=? ";
                    pst = conn.prepareStatement(remsql);
                    pst.setString(1, nt_workid);
                    pst.execute();
                } catch (SQLException e) {
                    Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
                }
                
            }catch(HeadlessException | SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }
            finally{
                try{
                    rs.close();
                    pst.close();
                }catch(SQLException e){

                }            
            }             
        } 
    }//GEN-LAST:event_btnotherdeleteActionPerformed

    private void btntrclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrclearActionPerformed
        // TODO add your handling code here:
        clearTrFields();
    }//GEN-LAST:event_btntrclearActionPerformed

    private void txtsearchtrid_twoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsearchtrid_twoKeyReleased
        // TODO add your handling code here:
        String searchtwo = txtsearchtrid_two.getText().toUpperCase();
        
        DefaultTableModel table = (DefaultTableModel) tbltrlist.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(table);
        tbltrlist.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(searchtwo.toUpperCase()));
        
        if (searchtwo.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter teacher's ID");
            DefaultTableModel model = (DefaultTableModel)tbltrlist.getModel();
            model.setColumnCount(0);
            model.setRowCount(0);
            UpdateTr_table();
        }else{
           try {
                String sql = "SELECT * FROM `teachers` WHERE `t_trid` = ? AND `status` = \"1\" ";

                pst = conn.prepareStatement(sql);
                pst.setString(1, txtsearchtrid_two.getText().toUpperCase());
                rs = pst.executeQuery();

                   if (rs.next()) {
                       String empname = rs.getString("t_name");
                       txttrname.setText(empname);
                       String empidno = rs.getString("t_idno");
                       txttridno.setText(empidno);
                       String trdob = rs.getString("t_dob");
                       txttrdob.setText(trdob);

                       String gender = rs.getString("t_gender");
                       switch(gender){
                           case "Male":
                               cbotrgender.setSelectedIndex(1);
                               break;
                           case "Female":
                               cbotrgender.setSelectedIndex(2);
                               break;
                       }

                       String religion = rs.getString("t_religion");
                       switch(religion){
                           case "Catholic":
                               cbotrrel.setSelectedIndex(1);
                               break;
                           case "Protestant":
                               cbotrrel.setSelectedIndex(2);
                               break;
                           case "Islamic":
                               cbotrrel.setSelectedIndex(3);
                               break;
                           case "Budhist":
                               cbotrrel.setSelectedIndex(4);
                               break;
                           case "Rastafari":
                               cbotrrel.setSelectedIndex(5);
                               break;
                       }

                       String nationality = rs.getString("t_nationality");
                       txttrnationality.setText(nationality);
                       String phone = rs.getString("t_phone");
                       txttrphone.setText(phone);
//                       String email = rs.getString("t_email");
//                       txttremail.setText(email);

                       String aspirin = rs.getString("t_haspirin");
                       switch(aspirin){
                           case "Aspirin":
                               chktraspirin.setSelected(true);
                       }

                       String penicillin = rs.getString("t_hpenicillin");
                       switch(penicillin){
                           case "Penicillin":
                               chktrpenicillin.setSelected(true);
                       }

                       String pollen = rs.getString("t_hpollen");
                       switch(pollen){
                           case "Pollen":
                               chktrpollen.setSelected(true);
                       }

                       String meatnrelated = rs.getString("t_hmeatandrelated");
                       switch(meatnrelated){
                           case "Meat and Related":
                               chktrmeatandrelated.setSelected(true);
                       }

                       String other = rs.getString("t_hother");
                       chktrotherallergy.setSelected(true);
                       txttrotherallergy.setText(other);

                       String insurancecarrier = rs.getString("t_icarrier");
                       txttrinsurancecarrier.setText(insurancecarrier);
                       String policyno = rs.getString("t_ipolicyno");
                       txttrpolicyno.setText(policyno);
                       String policyholdername = rs.getString("t_ipolicyholder");
                       txttrpolictholdername.setText(policyholdername);
                       String insurancecategory = rs.getString("t_igroup");
                       txttrgroupno.setText(insurancecategory);

                       byte[] tr_profileimg = rs.getBytes("t_profile_img");
                       ImageIcon trImageIcon = new ImageIcon(new ImageIcon(tr_profileimg).getImage().getScaledInstance(lbl_tr_profileimage.getWidth(), lbl_tr_profileimage.getHeight(), Image.SCALE_DEFAULT));
                       lbl_tr_profileimage.setIcon(trImageIcon);
                       this.teacher_image = tr_profileimg;

                       String trdept = rs.getString("t_dept");
                       txttrdept.setText(trdept);

                       String empldas = rs.getString("t_emp_as");
                       switch(empldas){
                           case "Teacher":
                               cbotremployedas.setSelectedIndex(1);
                               break;
                           case "Intern":
                               cbotremployedas.setSelectedIndex(2);
                               break;
                       }

                       String tscno = rs.getString("t_tscno");
                       txttrtscno.setText(tscno);

                       String trjobgrp = rs.getString("t_jobgrp");
                       switch(trjobgrp){
                           case "A":
                               cbotrjobgroup.setSelectedIndex(1);
                               break;
                           case "B":
                               cbotrjobgroup.setSelectedIndex(2);
                               break;
                           case "C":
                               cbotrjobgroup.setSelectedIndex(3);
                               break;
                           case "D":
                               cbotrjobgroup.setSelectedIndex(4);
                               break;
                           case "E":
                               cbotrjobgroup.setSelectedIndex(5);
                               break;
                           case "F":
                               cbotrjobgroup.setSelectedIndex(6);
                               break;
                       }

                       String trspeciality = rs.getString("t_sbjcrspeciality");
                       txttrsubjectspeciality.setText(trspeciality);
                       String trid = rs.getString("t_trid");
                       txttrworkingid.setText(trid);
                       String trdoj = rs.getString("t_dateofjoin");
                       txttrdateoe.setText(trdoj);

                       String classtr = rs.getString("t_class_tr");
                       switch(classtr){
                           case "Yes":
                               cbotrclasstr.setSelectedIndex(1);
                               break;
                           case "No":
                               cbotrclasstr.setSelectedIndex(2);
                               break;
                       }

                       String whichclass = rs.getString("t_which_class");
                       cbowhichclass.setSelectedItem(whichclass);
                       String tnssfno = rs.getString("t_nssf_no");
                       txttrnssfno.setText(tnssfno);
                       String tkrapin = rs.getString("t_kra_pin");
                       txttrkrapin.setText(tkrapin);
                       String payslip = rs.getString("t_payslip_no");
                       txttrpayslipno.setText(payslip);
                       String trsalary = rs.getString("t_salary");
                       txttrsalary.setText(trsalary);                 
                       String status = rs.getString("status");
                       lbltrstatus.setText(status);
                   }

              } catch (SQLException ex) {
                  Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
              } 
        }
    }//GEN-LAST:event_txtsearchtrid_twoKeyReleased

    private void btntrdeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrdeleteActionPerformed
        // TODO add your handling code here:
        int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Delete record?","Delete Record", JOptionPane.YES_NO_OPTION);
        
        if(x==0){
            try{

                String tr_workid = txtsearchtrid_two.getText();
                
                if (!tr_workid.isEmpty()) {
                    
                    String sql = "UPDATE `teachers` SET `status` = 0 WHERE `t_trid` = '"+tr_workid+"'  ";
                    pst = conn.prepareStatement(sql);

                    pst.executeUpdate();
               
                    JOptionPane.showMessageDialog(null, "Record Deleted!");

                    nt_clearFields();
                    
                    DefaultTableModel model = (DefaultTableModel)tbltrlist.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    UpdateTr_table();
                    
                }else{
                    JOptionPane.showMessageDialog(null, "Ops! An error occurred. Please enter the right Teacher Identification Number");
                }
                
                try {
                    String remsql = "DELETE FROM `testuser` WHERE `usercode`=? ";
                    pst = conn.prepareStatement(remsql);
                    pst.setString(1, tr_workid);
                    pst.execute();
                } catch (SQLException e) {
                    Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
                }
                
            }catch(HeadlessException | SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }
            finally{
                try{
                    rs.close();
                    pst.close();
                }catch(SQLException e){

                }            
            }        
      }
    }//GEN-LAST:event_btntrdeleteActionPerformed

    private void btndiscclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndiscclearActionPerformed
        // TODO add your handling code here:
        txtdiscdate.setText("");
        txtdiscpupilname.setText("");
        txtdiscadmno.setText("");
        txtdiscgender.setText("");
        txtdiscclass.setText("");
        txtdiscage.setText("");
        txtdiscoffence.setText("");
        txtdiscpunishment.setText("");
        txtdiscbywhom.setText("");
        discpstatus.setText("Status");
        ImageIcon img = new ImageIcon();
        disc_pupil_img.setIcon(img);
    }//GEN-LAST:event_btndiscclearActionPerformed

    private void txtlistsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlistsearchKeyReleased
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) tblpupilslist.getModel();
        String pupillistsearch = txtlistsearch.getText();
        TableRowSorter<DefaultTableModel> pl = new TableRowSorter<>(table);
        tblpupilslist.setRowSorter(pl);
        pl.setRowFilter(RowFilter.regexFilter(pupillistsearch));
    }//GEN-LAST:event_txtlistsearchKeyReleased

    //Getting each cell data from pupillist table
    private String getCellValue(int x, int y){
        return pupillist.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btngeneratepupilsexcelsheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngeneratepupilsexcelsheetActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{pupillist.getColumnName(0),pupillist.getColumnName(1),pupillist.getColumnName(2),
            pupillist.getColumnName(3),pupillist.getColumnName(4)});
        
        //Looping through the table rows
        for (int i = 0; i < pupillist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getCellValue(i, 0),getCellValue(i, 1),getCellValue(i, 2),getCellValue(i, 3),getCellValue(i, 4)});
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\pupils list.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
  
    }//GEN-LAST:event_btngeneratepupilsexcelsheetActionPerformed

    private void txttrnfsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrnfsearchKeyReleased
        //Checking for clearance
        checkWhetherClearedFromFinanceForTransfer();
        checkWhetherClearedFromLibraryforTransfer();
        
        try {
            String emptychecker = txttrnfsearch.getText();
            if (emptychecker.isEmpty()) {
                txttrnfname.setText("");
                txttrnfparentname.setText("");
                txttrnfdob.setText("");
                txttrnfadmno.setText("");
                txttrnfcurrclass.setText("");
                trnf_profile_img.setText(null);
            }else{            
                String sql = "SELECT `name`,`father_name`,`dob`,`admno`,`n_class`,`profile_image` FROM `admissions` WHERE `admno`=? AND `status` = \"1\" ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txttrnfsearch.getText());
                rs = pst.executeQuery();

                while (rs.next()) {
                    String pupilsname = rs.getString("name");
                    txttrnfname.setText(pupilsname);
                    String fname = rs.getString("father_name");
                    txttrnfparentname.setText(fname);
                    String pdob = rs.getString("dob");
                    txttrnfdob.setText(pdob);
                    String padmno = rs.getString("admno");
                    txttrnfadmno.setText(padmno);
                    String pclass = rs.getString("n_class");
                    txttrnfcurrclass.setText(pclass);
                    byte[] trnsf_profileimage = rs.getBytes("profile_image");
                    ImageIcon imageIcon = new ImageIcon(new ImageIcon(trnsf_profileimage).getImage().getScaledInstance(trnf_profile_img.getWidth(), trnf_profile_img.getHeight(), Image.SCALE_DEFAULT));
                    trnf_profile_img.setIcon(imageIcon);
                    this.pupil_image = trnsf_profileimage;
                }
            }
            
        } catch (SQLException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_txttrnfsearchKeyReleased

    private void txttrnfnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttrnfnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttrnfnameActionPerformed

    private void checkWhetherClearedFromFinanceForTransfer(){
        double finclr = 0.00;
        String trnfsearch = txttrnfsearch.getText();
        
        if (trnfsearch.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter pupil's admission number.");
        } else {
            try {
                String sql = "SELECT * FROM `fees_payment` WHERE `adm_no`='"+trnfsearch+"' ORDER BY `id` DESC LIMIT 1 ";
                    pst = conn.prepareStatement(sql);
                    rs = pst.executeQuery();

                    while (rs.next()) {
                        finclr = Double.parseDouble(rs.getString("balance"));
                    }

                    //JOptionPane.showMessageDialog(null, finclr);
                    
                    if (finclr <= 0) {
                        System.out.println("All good for finance transfer.");
                    } else {
                       JOptionPane.showMessageDialog(null, "The pupil has fees balance. Please ensure the pupil has settled the debt first."); 
                       clearTransfer();
                    }

            } catch (NumberFormatException | SQLException e) {
                
            }            
        }
        
    }
    
    private void checkWhetherClearedFromFinanceForLeavingCert(){
        double finclr = 0.00;
        String leavingsearch = txtsearchleaving.getText();
        
        if (leavingsearch.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter pupil's admission number.");
        } else {
            try {
                String sql = "SELECT * FROM `fees_payment` WHERE `adm_no`='"+leavingsearch+"' ORDER BY `id` DESC LIMIT 1 ";
                    pst = conn.prepareStatement(sql);
                    rs = pst.executeQuery();

                    while (rs.next()) {
                        finclr = Double.parseDouble(rs.getString("balance"));
                    }

                    //JOptionPane.showMessageDialog(null, finclr);
                    
                    if (finclr <= 0) {
                        System.out.println("All good for finance leaving cert.");
                } else {
                       JOptionPane.showMessageDialog(null, "The pupil has fees balance. Please ensure the pupil has settled the debt first."); 
                       txtsearchleaving.setText("");
                }
                    
            } catch (NumberFormatException | SQLException e) {
                
            }            
        }
        
    }
    
    private void checkWhetherClearedFromLibraryforTransfer(){
        String trnfsearch = txttrnfsearch.getText();
        int noofbooks = 0;
        
        if (trnfsearch.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter pupil's admission number.");
        } else {        
            try {
                String pbquery = "SELECT COUNT(`id`) AS id FROM `library_borrow` WHERE `custodian_id`='"+trnfsearch+"' ";
                pst = conn.prepareStatement(pbquery);
                rs = pst.executeQuery();

                while (rs.next()) {
                    noofbooks = Integer.parseInt(rs.getString("id"));
                }
                
                //JOptionPane.showMessageDialog(null, noofbooks);
                
                if (noofbooks <= 0) {
                        System.out.println("All good for library leaving cert.");
                } else {
                       JOptionPane.showMessageDialog(null, "The pupil is in possession of a library material. Please ensure the pupil has cleared with the library first."); 
                       clearTransfer();
                }
                
            } catch (NumberFormatException | SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    private void checkWhetherClearedFromLibraryforLeavingCert(){
        String leavingsearch = txtsearchleaving.getText();
        int noofbooks = 0;
        
        if (leavingsearch.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter pupil's admission number.");
        } else {        
            try {
                String pbquery = "SELECT COUNT(`id`) AS id FROM `library_borrow` WHERE `custodian_id`='"+leavingsearch+"' ";
                pst = conn.prepareStatement(pbquery);
                rs = pst.executeQuery();

                while (rs.next()) {
                    noofbooks = Integer.parseInt(rs.getString("id"));
                }
                
                if (noofbooks <= 0) {
                        System.out.println("All good for library leaving cert.");
                } else {
                       JOptionPane.showMessageDialog(null, "The pupil is in possession of a library material. Please ensure the pupil has cleared with the library first."); 
                       txtsearchleaving.setText("");
                }
                
            } catch (NumberFormatException | SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    private void btngeneratetranferletterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngeneratetranferletterActionPerformed
    //Checking for clearance from library and finance
    

    // Generating transfer letter
        int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to issue Transfer Letter?","Delete Record",JOptionPane.YES_NO_OPTION);
        
        if(x==0){
            
            //Inserting into transfer
            try {
                String tpupilname = txttrnfname.getText();
                String tparentname = txttrnfparentname.getText();
                String tdob = txttrnfdob.getText();
                String tadmno = txttrnfadmno.getText();
                String tcurr_class = txttrnfcurrclass.getText();
                String tcauseofleaving = txttrnfcauseoftrnf.getText();
                String tconduct = txttrnsfconduct.getText();
                String thdtr_remark = txttrnfhdtrremarks.getText();
                
                if (!tcauseofleaving.isEmpty() && !tconduct.isEmpty() && !thdtr_remark.isEmpty() ) {
                    
                    String sql = "INSERT INTO `transfers`(`t_pname`, `t_parentname`, `t_pdob`, `t_padmno`, `t_pcurr_class`,"
                            + " `t_pcause`, `t_pconduct`, `t_htr_rmrks`, `t_profile_img`) VALUES (?,?,?,?,?,?,?,?,?) ";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, tpupilname);
                    pst.setString(2, tparentname);
                    pst.setString(3, tdob);
                    pst.setString(4, tadmno);
                    pst.setString(5, tcurr_class);
                    pst.setString(6, tcauseofleaving);
                    pst.setString(7, tconduct);
                    pst.setString(8, thdtr_remark);
                    pst.setBytes(9, pupil_image);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Transfer table insertion.");
                    
                    //Updating admissions
                    try {
                        String tpadmno = txttrnfadmno.getText();

                        String adsql = "UPDATE `admissions` SET `status` = 0 WHERE `admno` = '"+tpadmno+"'";
                        pst = conn.prepareStatement(adsql);

                        pst.executeUpdate();

                        JOptionPane.showMessageDialog(null, "Transfer transaction completed");

                    } catch (HeadlessException | SQLException e) {
                        Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(null, "Please fill all the fields as required.");
                }
                

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }           
            
    
            //Generating letter
            String padmno = txttrnfadmno.getText();
            String ppclass = txttrnfcurrclass.getText();

            if (!padmno.isEmpty()) {

                try {
                    JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\Transfer_Letter.jrxml");
                    String query = "SELECT * FROM `transfers` WHERE `t_padmno`='"+padmno+"' AND `t_pcurr_class`='"+ppclass+"' ";

                    JRDesignQuery updateQuery = new JRDesignQuery();
                    updateQuery.setText(query);

                    jdesign.setQuery(updateQuery);

                    JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                    JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                    JasperViewer.viewReport(jprint, false);

                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, "Sorry. The could not be generated.");
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Please enter the admission number.");
            }
        }
    }//GEN-LAST:event_btngeneratetranferletterActionPerformed

    private void txtsearchleavingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsearchleavingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtsearchleavingActionPerformed

    private void txtsearchleavingKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsearchleavingKeyReleased
        // TODO add your handling code here:    
        checkWhetherClearedFromFinanceForLeavingCert();
        checkWhetherClearedFromLibraryforLeavingCert();
    }//GEN-LAST:event_txtsearchleavingKeyReleased

    private void btnleavingcertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnleavingcertActionPerformed
        // TODO add your handling code here: 
        String padmno = txtsearchleaving.getText();
        String ppclass = "Grade 6";
        String adclass ="";
        
        try {
            String query = "SELECT * FROM `admissions` WHERE `admno`='"+padmno+"' ";
            pst = conn.prepareStatement(query);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
               adclass = rs.getString("curr_class"); 
            }
            //JOptionPane.showMessageDialog(null, adclass);
        } catch (SQLException e) {
            
        }

            if (!padmno.isEmpty()) {
                
                if (adclass.equals(ppclass)) {
                    
                    int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to issue Leaving Certificate?","Issue Leaving Certificate",JOptionPane.YES_NO_OPTION);

                    if(x==0){

                        try {
                            JasperDesign jdesign = JRXmlLoader.load("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\SISLogyx\\src\\sislogyx\\LeavingCert.jrxml");
                            String query = "SELECT * FROM `admissions` WHERE `admno`='"+padmno+"' AND `curr_class`='"+ppclass+"' AND `status`=\"1\" ";

                            JRDesignQuery updateQuery = new JRDesignQuery();
                            updateQuery.setText(query);

                            jdesign.setQuery(updateQuery);

                            JasperReport jreport = JasperCompileManager.compileReport(jdesign);
                            JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);

                            JasperViewer.viewReport(jprint, false);

                        } catch (JRException ex) {
                            JOptionPane.showMessageDialog(null, "Sorry, pupil NOT qualified for a leaving certificate.");
                            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                    
            } else {
                 JOptionPane.showMessageDialog(null, "Sorry, pupil NOT qualified for a leaving certificate.");   
            }
                
            } else {
                JOptionPane.showMessageDialog(null, "Please enter the admission number.");
            }
            
            //Updating admissions
            try {
                String sql = "UPDATE `admissions` SET `status` = 0 WHERE `admno` = '"+padmno+"'";
                pst = conn.prepareStatement(sql);

                pst.executeUpdate();

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }finally{
                try{
                    rs.close();
                    pst.close();
                }catch(SQLException e){

                }            
            }
    }//GEN-LAST:event_btnleavingcertActionPerformed

    private void btsauploadnewImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btsauploadnewImageActionPerformed
        // TODO add your handling code here:
        Calendar cal = new GregorianCalendar();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        txtsa1.setText(day+"/"+month+"/"+year);
        
        txtsa2.setEditable(true);
        txtsa3.setEditable(true);
        txtsa4.setEditable(true);
        txtsa5.setEditable(true);
        
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        
        filename = f.getAbsolutePath();
        ImageIcon imageIcon = new ImageIcon (new ImageIcon(filename).getImage().getScaledInstance(sa_photo.getWidth(), sa_photo.getHeight(), Image.SCALE_DEFAULT));
        sa_photo.setIcon(imageIcon);
        
        try{
         File image = new File(filename);
         FileInputStream fix = new FileInputStream(image);
         ByteArrayOutputStream bos = new ByteArrayOutputStream();
         byte[] buf = new byte[1024];
         
         for(int number;(number = fix.read(buf))!= -1;){
             bos.write(buf, 0, number);
         }
         sch_album = bos.toByteArray();
            
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Ops! File could not be uploaded");
        }
    }//GEN-LAST:event_btsauploadnewImageActionPerformed

    private void btnsasaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsasaveActionPerformed
        // TODO add your handling code here:
        try{
             String dateofevent = txtsa1.getText();
             String whatevent = txtsa2.getText();
             String location = txtsa3.getText();
             String commemorator = txtsa4.getText();
             String comment = txtsa5.getText();
             //byte[] schoolalbum_img = this.sch_album;
             
             if (!dateofevent.isEmpty() && !whatevent.isEmpty() && !location.isEmpty() && !commemorator.isEmpty() && !comment.isEmpty()) {
                 
                int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Add new photo in the school's album?", "Add to album", JOptionPane.YES_NO_OPTION);

                if(x==0){

                   String sql ="INSERT INTO `schoolalbum`(`event_date`, `event`, `location`, `commemorator`, `comments`, `photo`) "
                        + "VALUES (?,?,?,?,?,?) ";

                   pst = conn.prepareStatement(sql);
                   pst.setString(1, dateofevent);
                   pst.setString(2, whatevent);
                   pst.setString(3, location);
                   pst.setString(4, commemorator);
                   pst.setString(5, comment);
                   pst.setBytes(6, sch_album);

                   pst.execute();
                   JOptionPane.showMessageDialog(null, "Album successfully updated.");

                   txtsa1.setText("");
                   txtsa2.setText("");
                   txtsa3.setText("");
                   txtsa4.setText("");
                   txtsa5.setText("");
                   ImageIcon sch_alb = new ImageIcon();
                   sa_photo.setIcon(sch_alb);
               }
            }else{
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            }
	}catch(SQLException e){
            JOptionPane.showMessageDialog(null,e);
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
				
        }
    }//GEN-LAST:event_btnsasaveActionPerformed

    private void btnsaviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsaviewActionPerformed
        // TODO add your handling code here:
        showAlbumItems(0);
    }//GEN-LAST:event_btnsaviewActionPerformed

    private void btnnextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnextActionPerformed
        // TODO add your handling code here:
        pos++;
        if (pos >= getAlbumItemsList().size()) {
            pos = getAlbumItemsList().size() - 1;
        }
        showAlbumItems(pos);
    }//GEN-LAST:event_btnnextActionPerformed

    private void btnpreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpreviousActionPerformed
        // TODO add your handling code here:
        pos --;
        if (pos < 0) {
            pos = 0;
        }
        showAlbumItems(pos);
    }//GEN-LAST:event_btnpreviousActionPerformed

    private void btnVRSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVRSaveActionPerformed
        // TODO add your handling code here:
        Calendar cal = new GregorianCalendar();
        int second = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        txtvr6.setText(hour+":"+minute+":"+second);
        
        int x = JOptionPane.showConfirmDialog(null, "Are you sure to Add new visitor's record?", "Add to visitors record", JOptionPane.YES_NO_OPTION);
        
        if(x == 0){
            try {
                String vtype;
                switch(cbovr1.getSelectedIndex()){
                    case 1:
                        vtype = cbovr1.getSelectedItem().toString();
                        break;                    
                    case 2:
                        vtype = cbovr1.getSelectedItem().toString();
                        break;                
                    default:
                       vtype = ""; 
                }
                String vname = txtvr1.getText();
                String vid = txtvr2.getText();

                String vgndr;
                switch(cbovr2.getSelectedIndex()){
                    case 1:
                        vgndr = cbovr2.getSelectedItem().toString();
                        break;                    
                    case 2:
                        vgndr = cbovr2.getSelectedItem().toString();
                        break;                
                    default:
                       vgndr = ""; 
                }
                String vpurporse = txtvr3.getText();
                String vaddress = txtvr4.getText();
                String vcontact = txtvr5.getText();                
                String vsignintime = txtvr6.getText();
                String vcomment = txtvr8.getText();
                String vsignouttime = txtvr9.getText();
                int vstatus = 1;
                
                if (cbovr1.getSelectedIndex() != 1 && !vname.isEmpty() && !vid.isEmpty() && cbovr2.getSelectedIndex() != 1 && !vpurporse.isEmpty()
                        && vaddress.isEmpty() && vcontact.isEmpty() && vsignintime.isEmpty()) {

                String sql = "INSERT INTO `visitors`(`v_type`, `v_name`, `v_id`, `v_gender`, `v_purpose`, `v_address`, `v_signintime`,"
                        + " `v_contact`, `v_comment`, `v_signout`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                
                pst = conn.prepareStatement(sql);
                pst.setString(1, vtype);
                pst.setString(2, vname);
                pst.setString(3, vid);
                pst.setString(4, vgndr);
                pst.setString(5, vpurporse);
                pst.setString(6, vaddress);
                pst.setString(7, vsignintime);
                pst.setString(8, vcontact);
                pst.setString(9, vcomment);
                pst.setString(10, vsignouttime);
                pst.setInt(11, vstatus);

                pst.execute();
                JOptionPane.showMessageDialog(null, "Record added successfully");
                
                cbovr1.setSelectedIndex(0);
                txtvr1.setText("");
                txtvr2.setText("");
                cbovr2.setSelectedIndex(0);
                txtvr3.setText("");
                txtvr4.setText("");
                txtvr5.setText("");
                txtvr6.setText("");
                txtvr7.setText("");
                txtvr8.setText("");
                txtvr9.setText("");
                
            }else{
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            }

            } catch (HeadlessException | SQLException e) {
                JOptionPane.showMessageDialog(null, "Ops! Operation failed!");
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }//GEN-LAST:event_btnVRSaveActionPerformed

    private void btnVRClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVRClearActionPerformed
        // TODO add your handling code here:
        clearVisitors();
    }//GEN-LAST:event_btnVRClearActionPerformed

    private void txtvr7KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtvr7KeyReleased
        // TODO add your handling code here:
        if(txtvr7.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Identification number too long!");
            txtvr7.setText("");
        }else{
            cbovr1.setSelectedIndex(0);
            txtvr1.setEditable(false);
            txtvr2.setEditable(false);
            cbovr2.setSelectedIndex(0);
            txtvr3.setEditable(false);
            txtvr4.setEditable(false);
            txtvr5.setEditable(false);
            txtvr6.setEditable(false);

            try {
                String sql = "SELECT * FROM `visitors` WHERE `v_id`=? AND `status` = \"1\"  ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtvr7.getText());
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String vtype = rs.getString("v_type");
                    switch(vtype){
                        case "Normal":
                         cbovr1.setSelectedIndex(1);
                         break;
                        case "Important":
                         cbovr1.setSelectedIndex(2);
                         break;
                    }
                    String vname = rs.getString("v_name");
                    txtvr1.setText(vname);
                    String vid = rs.getString("v_id");
                    txtvr2.setText(vid);
                    String vgender = rs.getString("v_gender");
                    switch(vgender){
                        case "Male":
                         cbovr2.setSelectedIndex(1);
                         break;
                        case "Female":
                         cbovr2.setSelectedIndex(2);
                         break;
                    }
                    String vpurpose = rs.getString("v_purpose");
                    txtvr3.setText(vpurpose);
                    String vaddress = rs.getString("v_address");
                    txtvr4.setText(vaddress);
                    String vcontact = rs.getString("v_contact");
                    txtvr5.setText(vcontact);
                    String vsignintime = rs.getString("v_signintime");
                    txtvr6.setText(vsignintime);
                }
            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
    }//GEN-LAST:event_txtvr7KeyReleased

    private void btnvrsignoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnvrsignoutActionPerformed
        // TODO add your handling code here:
        Calendar cal = new GregorianCalendar();
        int second = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        txtvr9.setText(hour+":"+minute+":"+second);
        
        int x = JOptionPane.showConfirmDialog(null, "Are you sure to sign out the visitor?", "Sign Out Visitor?", JOptionPane.YES_NO_OPTION);
        
        if (x == 0) {
            String vid = txtvr7.getText();
            try {
                if (!vid.isEmpty()) {
                    String sql = "UPDATE `visitors` SET `v_comment`=?,`v_signouttime`=?,`status`=0 WHERE `v_id`='"+vid+"' ";
                    pst = conn.prepareStatement(sql);
                    
                    pst.setString(1, txtvr8.getText());
                    pst.setString(2, txtvr9.getText());
                    
                    pst.executeUpdate();
                    
                    JOptionPane.showMessageDialog(null, "Visitor signed out.");
                    
                    clearVisitors();
                }else{
                    JOptionPane.showMessageDialog(null, "Please provide the ID of the visitor");
                }
            } catch (HeadlessException | SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }//GEN-LAST:event_btnvrsignoutActionPerformed

    private void txtsmSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsmSendActionPerformed
        // TODO add your handling code here:
//        if (rb1.isSelected()) {
//            sendSMS();
//        }else if(rb2.isSelected()){
//            try {
//                sendEmail();
//            } catch (MessagingException ex) {
//                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }else{
//            JOptionPane.showMessageDialog(null, "Please select message type");
//        }
        
        sendSMS();
        
    }//GEN-LAST:event_txtsmSendActionPerformed

    private void getSMSContact(){
        if (rbparent.isSelected()) {
            String msgadmno = txtsm3.getText();
            
            try {
                String sql = "SELECT * FROM `admissions` WHERE `admno`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, msgadmno);
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String contact = rs.getString("ecpc_phone");
                    txtsm4.setText(contact);
                    txtsm5.setText(rs.getString("name"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
            
        } else if (rbts.isSelected()) {
            try {
                String msgadmno = txtsm3.getText();
                
                String sql = "SELECT * FROM `teachers` WHERE `t_trid`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, msgadmno);
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String contact = rs.getString("t_phone");
                    txtsm4.setText(contact);
                    txtsm5.setText(rs.getString("t_name"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }else if (rbnt.isSelected()) {
            try {
                String msgadmno = txtsm3.getText();
                
                String sql = "SELECT * FROM `otherstaff` WHERE `e_work_id`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, msgadmno);
                rs = pst.executeQuery();

                while (rs.next()) {                
                    String contact = rs.getString("e_phone");
                    txtsm4.setText(contact);
                    txtsm5.setText(rs.getString("ename"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Please select recipient.");
            txtsm3.setText("");
        }
    }
    
//    private void sendEmail() throws MessagingException{
//        System.out.println("Preparing to send mail...");
//        String emailmsg = txtsm6.getText();
//        String recepient = txtsm8.getText();        
//        String sjct = txtsm9.getText();
//        
//        String msgtype = "";
//        if (rb2.isSelected()) {
//            msgtype = rb2.getText();
//        }
//
//        if (!msgtype.isEmpty() && !recepient.isEmpty() && !sjct.isEmpty()) {
//            
//            if (cbosm2.getSelectedIndex() == 2) {
//                if (isValid(recepient)) {
//                    System.out.print("Valid email");
//                } else {
//                    JOptionPane.showMessageDialog(null, "Invalid email");
//                }
//            }
//            
//        
//            Properties props = new Properties();
//            props.put("mail.smtp.auth", "true");
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", "smtp.gmail.com");
//            props.put("mail.smtp.port", "587");
//    //        
//            String myEmailAcc = System.getenv("EMAIL");
//            String myEmailAppPssd = System.getenv("APP_PSSD");
//
//            Session session = Session.getInstance(props, new Authenticator() {
//                @Override
//                protected PasswordAuthentication getPasswordAuthentication(){
//                    return new PasswordAuthentication(myEmailAcc, myEmailAppPssd);
//                }
//            });
//            Message message = prepareMessage(session, myEmailAcc,recepient, sjct, emailmsg);
//
//            Transport.send(message);
//            JOptionPane.showMessageDialog(null, "Mail sent succesfully");
//            
//            messageList();
//        
//        try {  
//            String sql = "INSERT INTO `messages` (`m_type`, `e_recipient`, `m_from`, `m_sbjct`, `m_msg`) VALUES (?,?,?,?,?)";
//            pst = conn.prepareStatement(sql);
//            pst.setString(1, msgtype);
//            pst.setString(2, recepient);
//            pst.setString(3, myEmailAcc);
//            pst.setString(4, sjct);
//            pst.setString(5, emailmsg);
//
//            pst.execute();
//
//            JOptionPane.showMessageDialog(null, "Message saved");
//
//            rb1.setSelected(false);
//                
//            } catch (SQLException e) {
//            }
//        
//        }else{
//            JOptionPane.showMessageDialog(null, "Please fill the fields as required");
//        }
//        
//    }
//    
//    private static Message prepareMessage(Session session, String myEmailAcc, String recepient, String sjct, String emailmsg){
//        try {
//            Message msg = new MimeMessage(session);
//            msg.setFrom(new InternetAddress(myEmailAcc));
//            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient.trim()));
//            msg.setSubject(sjct);
//            msg.setText(emailmsg);
//            return msg;
//        } catch (MessagingException ex) {
//            JOptionPane.showMessageDialog(null, "Sorry. We are currently unable to send the email");
//            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//
////        try {
////            Properties props = new Properties();
////            props.put("mail.smtp.host", "smtp.gmail.com");
////            props.put("mail.smtp.port", "587");
////            props.put("mail.smtp.user", "kathurimakimathi415@gmail.com");
////            props.put("mail.smtp.auth", "true");
////            props.put("mail.smtp.starttls.enable", "true");
////            props.put("mail.smtp.debug", "true");
////            props.put("mail.smtp.socketFactory.port", "587");
////            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////            props.put("mail.smtp.socketFactory.fallback", "false");
////
////            Session session = Session.getDefaultInstance(props, null);
////            session.setDebug(true);
////            MimeMessage msg = new MimeMessage(session);
////            msg.setText(emailmsg);
////            msg.setSubject(sjct);
////            msg.setFrom(myEmailAcc.trim());
////            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recepient.trim()));
////            msg.saveChanges();
////
////            try (Transport transport = session.getTransport("smtp")) {
////                transport.connect("smtp.gmail.com", myEmailAcc, myEmailAppPssd);
////                transport.sendMessage(msg, msg.getAllRecipients());
////            }
////            JOptionPane.showMessageDialog(null, "Mail sent succesfully");
////        
////        } catch (HeadlessException | MessagingException e) {
////            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
////        }
//    }
    
    private void txtsm3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm3KeyReleased
        // TODO add your handling code here:
        getSMSContact();
    }//GEN-LAST:event_txtsm3KeyReleased

    private void jbtnlogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnlogoutActionPerformed
        try {                                           
            // TODO add your handling code here:
            try {
                String asql = "INSERT INTO `auditlog`(`empid`, `date`, `time`, `status`) VALUES (?,?,?,?)";
                pst = conn.prepareStatement(asql);
                Date currentDate = GregorianCalendar.getInstance().getTime();
                DateFormat df = DateFormat.getInstance();
                String dateString = df.format(currentDate);
                
                Date d = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String timeString = sdf.format(d);
                
                String time = timeString;
                String date = dateString;
                pst.setString(1, Emp.empId);
                pst.setString(2, date);
                pst.setString(3, time);
                pst.setString(4, "Logged out");
                pst.execute();
                
            } catch (SQLException e) {
            }
            
            this.dispose();
            
            Login l = new Login();
            l.setVisible(true);
            
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jbtnlogoutActionPerformed

    private void tbllogdetailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbllogdetailKeyReleased
        // TODO add your handling code here:
//        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            try {
//                int row = tbllogdetail.getSelectedRow();
//                String tableClick = (tbllogdetail.getModel().getValueAt(row, 0).toString());
//                String sql = "SELECT * FROM `auditlog` WHERE `empid`=? ";
//                pst = conn.prepareStatement(sql);
//                pst.setString(1, tableClick);
//                rs = pst.executeQuery();
//                
//                while (rs.next()) {                    
//                    getValue();
//                }
//                
//            } catch (SQLException e) {
//            }
//        }
    }//GEN-LAST:event_tbllogdetailKeyReleased

    private void txtlogworkidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlogworkidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlogworkidActionPerformed

    private void txtlogworkidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlogworkidKeyReleased
        // TODO add your handling code here:
                
        DefaultTableModel table = (DefaultTableModel) tbllogdetail.getModel();
        String loglistsearch = txtlogworkid.getText();
        TableRowSorter<DefaultTableModel> pl = new TableRowSorter<>(table);
        tbllogdetail.setRowSorter(pl);
        pl.setRowFilter(RowFilter.regexFilter(loglistsearch));
    }//GEN-LAST:event_txtlogworkidKeyReleased

    private void btnlogclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlogclearActionPerformed
        // TODO add your handling code here:
        txtlogworkid.setText("");
    }//GEN-LAST:event_btnlogclearActionPerformed

    private String getLogCellValue(int x, int y){
        return tbllogdetail.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btnloggenreportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnloggenreportActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{tbllogdetail.getColumnName(0),tbllogdetail.getColumnName(1),tbllogdetail.getColumnName(2),
            tbllogdetail.getColumnName(3)});
        
        //Looping through the table rows
        for (int i = 0; i < tbllogdetail.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getLogCellValue(i, 0),getLogCellValue(i, 1),getLogCellValue(i, 2),getLogCellValue(i, 3)});
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Audit log.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Log generation success");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btnloggenreportActionPerformed

    private String getNTCellValue(int x, int y){
        return nt.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btnothergenexcelsheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnothergenexcelsheetActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{nt.getColumnName(0),nt.getColumnName(1),nt.getColumnName(2),nt.getColumnName(3),nt.getColumnName(4),
            nt.getColumnName(5),nt.getColumnName(6),nt.getColumnName(7)});
        
        //Looping through the table rows
        for (int i = 0; i < nt.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getNTCellValue(i, 0),getNTCellValue(i, 1),getNTCellValue(i, 2),getNTCellValue(i, 3),
                getNTCellValue(i, 4),getNTCellValue(i, 5),getNTCellValue(i, 6),getNTCellValue(i, 7)});
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("D:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Non-teaching staff list.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Non-teaching staff list generation successful");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btnothergenexcelsheetActionPerformed

    private String getTRCellValue(int x, int y){
        return tbltrlist.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btntrgenerateexcelsheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrgenerateexcelsheetActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{tbltrlist.getColumnName(0),tbltrlist.getColumnName(1),tbltrlist.getColumnName(2),tbltrlist.getColumnName(3),tbltrlist.getColumnName(4),
            tbltrlist.getColumnName(5),tbltrlist.getColumnName(6),tbltrlist.getColumnName(7),tbltrlist.getColumnName(8)});
        
        //Looping through the table rows
        for (int i = 0; i < tbltrlist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getTRCellValue(i, 0),getTRCellValue(i, 1),getTRCellValue(i, 2),getTRCellValue(i, 3),
                getTRCellValue(i, 4),getTRCellValue(i, 5),getTRCellValue(i, 6),getTRCellValue(i, 7),getTRCellValue(i, 8)});
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("D:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Teaching staff list.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Teaching staff list generation successful");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btntrgenerateexcelsheetActionPerformed

    private void cbotrclasstrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotrclasstrActionPerformed
        // TODO add your handling code here:
        int classtr = cbotrclasstr.getSelectedIndex();
        
        switch (classtr) {
            case 1:
                cbowhichclass.setEnabled(true);
                break;
            case 2:
                cbowhichclass.setEnabled(false);
                cbowhichclass.setSelectedIndex(0);
                break;
            default:
                cbowhichclass.setEnabled(false);
                cbowhichclass.setSelectedIndex(0);
                break;
        }
        
    }//GEN-LAST:event_cbotrclasstrActionPerformed

    private void txtclassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtclassKeyReleased
        // TODO add your handling code here:
        if(txtclass.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Classname too long!");
            txtclass.setText("");
        }else{
            DefaultTableModel table = (DefaultTableModel) tblpupilslist.getModel();
            String classsearch = txtclass.getText();
            TableRowSorter<DefaultTableModel> pl = new TableRowSorter<>(table);
            tblpupilslist.setRowSorter(pl);
            pl.setRowFilter(RowFilter.regexFilter(classsearch));
        }
    }//GEN-LAST:event_txtclassKeyReleased

    private void btnPupilListRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPupilListRefreshActionPerformed
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)tblpupilslist.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        Pupils_List();
    }//GEN-LAST:event_btnPupilListRefreshActionPerformed

    private void btnnewterm1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnewterm1ActionPerformed
        // TODO add your handling code here:
        WhoseProfile wp = new WhoseProfile();
        wp.setVisible(true);
    }//GEN-LAST:event_btnnewterm1ActionPerformed

    private void btnnewtermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnewtermActionPerformed
        try {
            // TODO add your handling code here:
            CurrentTerm ct = new CurrentTerm();
            ct.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnnewtermActionPerformed

    private void cbootemployedasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbootemployedasActionPerformed
        // TODO add your handling code here:        
        int num;
        String trid = "TR";
        num = 50 + (int) (Math.random()*5);
        trid += num + 5;
        txttrworkingid.setText(trid);
        
        int empas = cbootemployedas.getSelectedIndex();
        switch (empas) {
            case 1:
                {
                    int num1;
                    String ntid = "FA";
                    num1 = 59 + (int) (Math.random()*989);
                    ntid += num1 + 5;
                    txtotworkid.setText(ntid);
                    break;
                }
            case 2:
                {
                    int num1;
                    String ntid = "LIB";
                    num1 = 59 + (int) (Math.random()*989);
                    ntid += num1 + 5;
                    txtotworkid.setText(ntid);
                    break;
                }
            case 3:
                {
                    int num1;
                    String ntid = "SK";
                    num1 = 59 + (int) (Math.random()*989);
                    ntid += num1 + 5;
                    txtotworkid.setText(ntid);
                    break;
                }
            case 4:
                {
                    int num1;
                    String ntid = "GD";
                    num1 = 59 + (int) (Math.random()*989);
                    ntid += num1 + 5;
                    txtotworkid.setText(ntid);
                    break;
                }
            case 5:
                {
                    int num1;
                    String ntid = "CO";
                    num1 = 59 + (int) (Math.random()*989);
                    ntid += num1 + 5;
                    txtotworkid.setText(ntid);
                    break;
                }
            default:
                txtotworkid.setText("");
                break;
        }
    }//GEN-LAST:event_cbootemployedasActionPerformed

    private void cbotremployedasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotremployedasActionPerformed
        // TODO add your handling code here:
        int trempas = cbotremployedas.getSelectedIndex();
        switch (trempas) {
            case 1:
                {
                    int num;
                    String trid = "TR";
                    num = 50 + (int) (Math.random()*5);
                    trid += num + 5;
                    txttrworkingid.setText(trid);
                    break;
                }
            case 2:
                {
                    int num;
                    String trid = "ITR";
                    num = 50 + (int) (Math.random()*5);
                    trid += num + 5;
                    txttrworkingid.setText(trid);
                    break;
                }
            default:
                txttrworkingid.setText("");
                break;
        }
    }//GEN-LAST:event_cbotremployedasActionPerformed

    private void btnnewterm2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnewterm2ActionPerformed
        try {
            // TODO add your handling code here:
            FeesSetup fs = new FeesSetup();
            fs.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnnewterm2ActionPerformed

    private void btnPromoteNextToClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromoteNextToClassActionPerformed
        try {
            // TODO add your handling code here:
            NextClass nc = new NextClass();
            nc.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnPromoteNextToClassActionPerformed

    private void txtotidnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotidnoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtotidnoKeyPressed

    private void txtotidnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotidnoKeyReleased
        // TODO add your handling code here:
        if (!txtotidno.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numbers allowed!");
            txtotidno.setText("");
        }else{
            String idlen = txtotidno.getText();
            if (idlen.length() > 8) {
                JOptionPane.showMessageDialog(null, "Invalid ID number!");
                txtotidno.setText("");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtotidnoKeyReleased

    private void txtotnameeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotnameeKeyReleased
        // TODO add your handling code here:
        if (txtotnamee.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtotnamee.setText("");
        }else if(txtotnamee.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txtotnamee.setText("");
        }
        
    }//GEN-LAST:event_txtotnameeKeyReleased

    private void txtotnationalityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotnationalityKeyReleased
        // TODO add your handling code here:
        if (txtotnationality.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtotnationality.setText("");
        }else if(txtotnationality.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Nationality too long!");
            txtotnationality.setText("");
        }
    }//GEN-LAST:event_txtotnationalityKeyReleased

    private void txtotphoneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotphoneKeyReleased
        // TODO add your handling code here:
        if (!txtotphone.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numbers allowed!");
            txtotphone.setText("");
        }else{
            String idlen = txtotphone.getText();
            if (idlen.length() > 10) {
                JOptionPane.showMessageDialog(null, "Invalid phone number!");
                txtotphone.setText("");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txtotphoneKeyReleased

    private void txtototherhealthinfoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtototherhealthinfoKeyReleased
        // TODO add your handling code here:
        if (txtototherhealthinfo.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtototherhealthinfo.setText("");
        }
    }//GEN-LAST:event_txtototherhealthinfoKeyReleased

    private void txtotinsurancecarrierKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotinsurancecarrierKeyReleased
        // TODO add your handling code here:
        if (txtotinsurancecarrier.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtotinsurancecarrier.setText("");
        }else if(txtotinsurancecarrier.getText().length() > 20){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txtotinsurancecarrier.setText("");
        }
    }//GEN-LAST:event_txtotinsurancecarrierKeyReleased

    private void txtotpolicynoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotpolicynoKeyReleased
        // TODO add your handling code here:
        if (txtotpolicyno.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtotpolicyno.setText("");
        }else if(txtotpolicyno.getText().length() > 7){
            JOptionPane.showMessageDialog(null, "Policy number too long!");
            txtotpolicyno.setText("");
        }
    }//GEN-LAST:event_txtotpolicynoKeyReleased

    private void txtotpolicyholdernameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotpolicyholdernameKeyReleased
        // TODO add your handling code here:
        if (txtotpolicyholdername.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtotpolicyholdername.setText("");
        }else if(txtotpolicyholdername.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Policy holder name too long!");
            txtotpolicyholdername.setText("");
        }
    }//GEN-LAST:event_txtotpolicyholdernameKeyReleased

    private void txtotgroup_categoryKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotgroup_categoryKeyReleased
        // TODO add your handling code here:
        if (txtotgroup_category.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtotgroup_category.setText("");
        }else if(txtotgroup_category.getText().length() > 8){
            JOptionPane.showMessageDialog(null, "Group category too long!");
            txtotgroup_category.setText("");
        }
    }//GEN-LAST:event_txtotgroup_categoryKeyReleased

    private void txtotsalaryKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotsalaryKeyReleased
        // TODO add your handling code here:
        if (!txtotsalary.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters not allowed!");
            txtotsalary.setText("");
        }if(txtotsalary.getText().length() > 8){
            JOptionPane.showMessageDialog(null, "KRA PIN too long!");
            txtotsalary.setText("");
        }
    }//GEN-LAST:event_txtotsalaryKeyReleased

    private void txttrnfcauseoftrnfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrnfcauseoftrnfKeyReleased
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txttrnfcauseoftrnfKeyReleased

    private void txttrnfhdtrremarksKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrnfhdtrremarksKeyReleased
        // TODO add your handling code here:
        if (txttrnfhdtrremarks.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrnfhdtrremarks.setText("");
        }
    }//GEN-LAST:event_txttrnfhdtrremarksKeyReleased

    private void txtsa2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsa2KeyReleased
        // TODO add your handling code here:
        if (txtsa2.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtsa2.setText("");
        }
    }//GEN-LAST:event_txtsa2KeyReleased

    private void txtsa3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsa3KeyReleased
        // TODO add your handling code here:
        if (txtsa3.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtsa3.setText("");
        }
    }//GEN-LAST:event_txtsa3KeyReleased

    private void txtsa4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsa4KeyReleased
        // TODO add your handling code here:
        if (txtsa4.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtsa4.setText("");
        }
    }//GEN-LAST:event_txtsa4KeyReleased

    private void txtvr1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtvr1KeyReleased
        // TODO add your handling code here:
        if (txtvr1.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtvr1.setText("");
        }else if(txtvr1.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txtvr1.setText("");
        }
    }//GEN-LAST:event_txtvr1KeyReleased

    private void txtvr2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtvr2KeyReleased
        // TODO add your handling code here:
        if(txtvr2.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Identification number too long!");
            txtvr2.setText("");
        }
    }//GEN-LAST:event_txtvr2KeyReleased

    private void txtvr5KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtvr5KeyReleased
        // TODO add your handling code here:
        if (!txtvr5.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtvr5.setText("");
        }else if(txtvr5.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Phone number too long!");
            txtvr5.setText("");
        }
    }//GEN-LAST:event_txtvr5KeyReleased

    private void btnclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclearActionPerformed
        // TODO add your handling code here:
        clearTransfer();
    }//GEN-LAST:event_btnclearActionPerformed

    private void clearTransfer(){
        txttrnfcauseoftrnf.setText("");
        txttrnsfconduct.setText("");
        txttrnfhdtrremarks.setText("");
        txttrnfname.setText("");
        txttrnfparentname.setText("");
        txttrnfdob.setText("");
        txttrnfadmno.setText("");
        txttrnfcurrclass.setText("");
        txttrnfsearch.setText("");
    }
    
    private void txttrnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrnameKeyReleased
        // TODO add your handling code here:
        if (txttrname.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrname.setText("");
        }else if(txttrname.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txttrname.setText("");
        }
    }//GEN-LAST:event_txttrnameKeyReleased

    private void txttridnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttridnoKeyReleased
        // TODO add your handling code here:
        if (!txttridno.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttridno.setText("");
        }else{
            String idlen = txttridno.getText();
            if (idlen.length() > 8) {
                JOptionPane.showMessageDialog(null, "Invalid ID number!");
                txttridno.setText("");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txttridnoKeyReleased

    private void txttrnationalityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrnationalityKeyReleased
        // TODO add your handling code here:
        if (txttrnationality.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrnationality.setText("");
        }else if(txttrnationality.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txttrnationality.setText("");
        }
    }//GEN-LAST:event_txttrnationalityKeyReleased

    private void txttrphoneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrphoneKeyReleased
        // TODO add your handling code here:
        if (!txttrphone.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrphone.setText("");
        }else{
            String idlen = txttrphone.getText();
            if (idlen.length() > 10) {
                JOptionPane.showMessageDialog(null, "Invalid phone number!");
                txttrphone.setText("");
            }else{
                System.out.println("All good!");
            }
        }
    }//GEN-LAST:event_txttrphoneKeyReleased

    private void txttrinsurancecarrierKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrinsurancecarrierKeyReleased
        // TODO add your handling code here:
        if (txttrinsurancecarrier.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrinsurancecarrier.setText("");
        }else if(txttrinsurancecarrier.getText().length() > 20){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txttrinsurancecarrier.setText("");
        }
    }//GEN-LAST:event_txttrinsurancecarrierKeyReleased

    private void txttrpolicynoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrpolicynoKeyReleased
        // TODO add your handling code here:
        if (txttrpolicyno.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrpolicyno.setText("");
        }else if(txttrpolicyno.getText().length() > 7){
            JOptionPane.showMessageDialog(null, "Policy number too long!");
            txttrpolicyno.setText("");
        }
    }//GEN-LAST:event_txttrpolicynoKeyReleased

    private void txttrpolictholdernameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrpolictholdernameKeyReleased
        // TODO add your handling code here:
        if (txttrpolictholdername.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrpolictholdername.setText("");
        }else if(txttrpolictholdername.getText().length() > 7){
            JOptionPane.showMessageDialog(null, "Policy holder name too long!");
            txttrpolictholdername.setText("");
        }
    }//GEN-LAST:event_txttrpolictholdernameKeyReleased

    private void txttrgroupnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrgroupnoKeyReleased
        // TODO add your handling code here:
        if (txttrgroupno.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrgroupno.setText("");
        }else if(txttrgroupno.getText().length() > 7){
            JOptionPane.showMessageDialog(null, "Group category too long!");
            txttrgroupno.setText("");
        }
    }//GEN-LAST:event_txttrgroupnoKeyReleased

    private void txttrsubjectspecialityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrsubjectspecialityKeyReleased
        // TODO add your handling code here:
        if (txttrsubjectspeciality.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttrsubjectspeciality.setText("");
        }else if(txttrsubjectspeciality.getText().length() > 55){
            JOptionPane.showMessageDialog(null, "Subject specalization name too long!");
            txttrsubjectspeciality.setText("");
        }
    }//GEN-LAST:event_txttrsubjectspecialityKeyReleased

    private void txttrsalaryKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrsalaryKeyReleased
        // TODO add your handling code here:
        if (!txttrsalary.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txttrsalary.setText("");
            
        }else if(txttrsalary.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "KRA PIN too long!");
            txttrsalary.setText("");
        }
    }//GEN-LAST:event_txttrsalaryKeyReleased

    private void txtdiscbywhomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdiscbywhomKeyReleased
        // TODO add your handling code here:
        if (txtdiscbywhom.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtdiscbywhom.setText("");
        }else if(txtdiscbywhom.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txtdiscbywhom.setText("");
        }
    }//GEN-LAST:event_txtdiscbywhomKeyReleased

    private void rbtsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtsActionPerformed
        // TODO add your handling code here:
        if (rbts.isSelected()) {
            lblmsg.setText("Work ID");
            rbparent.setSelected(false);
            rbnt.setSelected(false);
            txtsm3.setText("");
            txtsm4.setText("");
            txtsm5.setText("");
            txtsm6.setText("");
        }
    }//GEN-LAST:event_rbtsActionPerformed

    private void rbparentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbparentActionPerformed
        // TODO add your handling code here:
        if (rbparent.isSelected()) {
            lblmsg.setText("Pupils Adm No.");
            rbts.setSelected(false);
            rbnt.setSelected(false);
            txtsm3.setText("");
            txtsm4.setText("");
            txtsm5.setText("");
            txtsm6.setText("");
        }
    }//GEN-LAST:event_rbparentActionPerformed

    private void rbntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbntActionPerformed
        // TODO add your handling code here:
        if (rbts.isSelected()) {
            lblmsg.setText("Work ID");
            rbparent.setSelected(false);
            rbts.setSelected(false);
            txtsm3.setText("");
            txtsm4.setText("");
            txtsm5.setText("");
            txtsm6.setText("");
        }
    }//GEN-LAST:event_rbntActionPerformed

    private void btnsmCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsmCancelActionPerformed
        // TODO add your handling code here:
        rbparent.setSelected(false);
        rbts.setSelected(false);
        rbnt.setSelected(false);
        txtsm3.setText("");
        txtsm4.setText("");
        txtsm5.setText("");
        txtsm6.setText("");
    }//GEN-LAST:event_btnsmCancelActionPerformed

    private void txtotidnoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotidnoKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtotidnoKeyTyped

    private void cbotrgenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotrgenderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbotrgenderActionPerformed

    private void txtntsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtntsearchKeyReleased
        // TODO add your handling code here:
        String ntsearch = txtntsearch.getText();
        
        DefaultTableModel model = (DefaultTableModel)tblothlist.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        UpdateNT_table();

        DefaultTableModel table = (DefaultTableModel) tblothlist.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(table);
        tblothlist.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(ntsearch));

        if (ntsearch.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter worker's ID");
        }else{
            try {
                String sql = "SELECT * FROM `otherstaff` WHERE `e_work_id` = ? AND `status` = \"1\" ";

                pst = conn.prepareStatement(sql);
                pst.setString(1, txtntsearch.getText());
                rs = pst.executeQuery();

                   if (rs.next()) {
                       String empname = rs.getString("ename");
                       txtotnamee.setText(empname);
                       String empidno = rs.getString("eid_no");
                       txtotidno.setText(empidno);
                       
                       
                       String otdob = rs.getString("edob");
                       txtotdob.setText(otdob);

                       String gender = rs.getString("e_gender");
                       switch(gender){
                           case "Male":
                               cbootgndr.setSelectedIndex(1);
                               break;
                           case "Female":
                               cbootgndr.setSelectedIndex(2);
                               break;
                       }

                       String religion = rs.getString("e_religion");
                       switch(religion){
                           case "Catholic":
                               cbootrel.setSelectedIndex(1);
                               break;
                           case "Protestant":
                               cbootrel.setSelectedIndex(2);
                               break;
                           case "Islamic":
                               cbootrel.setSelectedIndex(3);
                               break;
                           case "Budhist":
                               cbootrel.setSelectedIndex(4);
                               break;
                           case "Rastafari":
                               cbootrel.setSelectedIndex(5);
                               break;
                       }
                       String nationality = rs.getString("e_nationality");
                       txtotnationality.setText(nationality);
                       String phone = rs.getString("e_phone");
                       txtotphone.setText(phone);

                       String aspirin = rs.getString("e_aspirin");
                       switch(aspirin){
                           case "Aspirin":
                               chkotaspirin.setSelected(true);
                       }

                       String penicillin = rs.getString("e_penicillin");
                       switch(penicillin){
                           case "Penicillin":
                               chkotpenicillin.setSelected(true);
                       }

                       String pollen = rs.getString("e_pollen");
                       switch(pollen){
                           case "Pollen":
                               chkotpollen.setSelected(true);
                       }

                       String meatnrelated = rs.getString("e_meatandrelated");
                       switch(meatnrelated){
                           case "Meat and Related":
                               chkotmeatandrelated.setSelected(true);
                       }

                       String other = rs.getString("e_other");
                       chkototherhealthinfo.setSelected(true);
                       txtototherhealthinfo.setText(other);

                       byte[] other_profileimage = rs.getBytes("e_profile_img");
                       ImageIcon imageIcon = new ImageIcon(new ImageIcon(other_profileimage).getImage().getScaledInstance(lbl_other_profileimage.getWidth(), lbl_other_profileimage.getHeight(), Image.SCALE_SMOOTH));
                       lbl_other_profileimage.setIcon(imageIcon);
                       other_image = other_profileimage;

                       String insurancecarrier = rs.getString("icarrier");
                       txtotinsurancecarrier.setText(insurancecarrier);
                       String policyno = rs.getString("ipolicy_no");
                       txtotpolicyno.setText(policyno);
                       String policyholdername = rs.getString("ipolicy_holder_name");
                       txtotpolicyholdername.setText(policyholdername);
                       String insurancecategory = rs.getString("igroup_no");
                       txtotgroup_category.setText(insurancecategory);

                       String dept = rs.getString("e_dept");
                       switch(dept){
                           case "Finance and Accounts":
                               cbootdept.setSelectedIndex(1);
                               break;
                           case "Stock-keeping":
                               cbootdept.setSelectedIndex(2);
                               break;
                           case "Library":
                               cbootdept.setSelectedIndex(3);
                               break;
                       }

                       String empldas = rs.getString("e_employed_as");
                       switch(empldas){
                           case "Secretary":
                               cbootemployedas.setSelectedIndex(1);
                               break;
                       }

                       String workid = rs.getString("e_work_id");
                       txtotworkid.setText(workid);
                       String doe = rs.getString("e_doe");
                       txtotdoe.setText(doe);
                       String nssfno = rs.getString("e_nssf_no");
                       txtotnssf.setText(nssfno);
                       String krapin = rs.getString("e_kra_pin");
                       txtotkrapin.setText(krapin);
                       String payslipno = rs.getString("e_payslip_no");
                       txtotpayslip.setText(payslipno);
                       String salary = rs.getString("e_salary");
                       txtotsalary.setText(salary);

                       String status = rs.getString("status");
                       ntstatus.setText(status);

                   }

              } catch (SQLException ex) {
                  Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
              }
        }
    }//GEN-LAST:event_txtntsearchKeyReleased

    private void jotDobMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jotDobMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jotDobMouseClicked

    private void jotDobKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jotDobKeyPressed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jotDobKeyPressed

    private void jotDobKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jotDobKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jotDobKeyReleased

    private void jotDobMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jotDobMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jotDobMousePressed

    private void jotDobMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jotDobMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jotDobMouseReleased

    private void txtotnameeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotnameeKeyPressed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtotnameeKeyPressed

    private void txtotnameeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotnameeKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtotnameeKeyTyped

    private void txtotdobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtotdobActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtotdobActionPerformed

    private void txtotdobMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtotdobMouseClicked
        // TODO add your handling code here:
        if (jotDob.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please select birth date");
            
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            //Code to ensure that dob is not less than 18 years ago
            Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.DATE, -6570); //-6570 = (-365 * 18)
            String x = sdf.format(cal1.getTime());
            String dob = sdf.format(jotDob.getDate());

            double rslt = x.compareTo(dob);

            //Code to limit date from exceeding today's date
            Calendar cal2 = Calendar.getInstance();
            cal2.add(Calendar.DATE, 0);
            String tdate = sdf.format(cal2.getTime());

            double rslt2 = dob.compareTo(tdate);

            if (rslt2 > 0) {
                JOptionPane.showMessageDialog(null, "Date cannot exceed todays date!");
                txtotdob.setText(null);
                jotDob.setDate(null);

            }else if (rslt < 0) {
                JOptionPane.showMessageDialog(null, "Employee too young!");
                txtotdob.setText(null);
                jotDob.setDate(null);

            }else{
                txtotdob.setText(dob);
            }
        }
    }//GEN-LAST:event_txtotdobMouseClicked

    private void txttrdobMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txttrdobMouseClicked
        // TODO add your handling code here:
        if (jtrDob.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please select birth date");
            
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            //Code to ensure that dob is not less than 18 years ago
            Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.DATE, -6570); //-6570 = (-365 * 18)
            String x = sdf.format(cal1.getTime());
            String dob = sdf.format(jtrDob.getDate());

            double rslt = x.compareTo(dob);

            //Code to limit date from exceeding today's date
            Calendar cal2 = Calendar.getInstance();
            cal2.add(Calendar.DATE, 0);
            String tdate = sdf.format(cal2.getTime());

            double rslt2 = dob.compareTo(tdate);

            if (rslt2 > 0) {
                JOptionPane.showMessageDialog(null, "Date cannot exceed todays date!");
                txttrdob.setText(null);
                jtrDob.setDate(null);

            }else if (rslt < 0) {
                JOptionPane.showMessageDialog(null, "Employee too young!");
                txttrdob.setText(null);
                jtrDob.setDate(null);

            }else{
                txttrdob.setText(dob);
            }
        }
    }//GEN-LAST:event_txttrdobMouseClicked

    private void txtotnssfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotnssfKeyReleased
        // TODO add your handling code here:
        if(txtotnssf.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Number too long!");
            txtotnssf.setText("");
        }
    }//GEN-LAST:event_txtotnssfKeyReleased

    private void txtotkrapinKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotkrapinKeyReleased
        // TODO add your handling code here:
        if(txtotkrapin.getText().length() > 8){
            JOptionPane.showMessageDialog(null, "KRA PIN too long!");
            txtotkrapin.setText("");
        }
    }//GEN-LAST:event_txtotkrapinKeyReleased

    private void txttrtscnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrtscnoKeyReleased
        // TODO add your handling code here:
        if(txttrtscno.getText().length() > 7){
            JOptionPane.showMessageDialog(null, "TSC Number too long!");
            txttrtscno.setText("");
        }
    }//GEN-LAST:event_txttrtscnoKeyReleased

    private void txttrnssfnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrnssfnoKeyReleased
        // TODO add your handling code here:
        if(txttrnssfno.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "NSSF Number too long!");
            txttrnssfno.setText("");
        }
    }//GEN-LAST:event_txttrnssfnoKeyReleased

    private void txttrkrapinKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrkrapinKeyReleased
        // TODO add your handling code here:
        if(txttrkrapin.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "KRA PIN too long!");
            txttrkrapin.setText("");
        }
    }//GEN-LAST:event_txttrkrapinKeyReleased
    
    private void sendSMS(){
        String bulksmsusername = System.getenv("BULKSMSUSRNAME");
        String bulksmspssd = System.getenv("BULKSMSPSSD");
        String msg = txtsm6.getText();
        String phoneno = txtsm4.getText();
//        if (rb1.isSelected()) {
//            txtsm3.setEditable(true);
//            
//            String msgtype = "";
//            if (rb1.isSelected()) {
//                msgtype = rb1.getText();
//            }
            String padmno = txtsm3.getText();
            String mrecepient = txtsm4.getText();
            String mssg = txtsm6.getText();

            if (!padmno.isEmpty() && !mrecepient.isEmpty() && !mssg.isEmpty()) {
            
                getSMSContact();
                SMS send = new SMS();
                send.SendSMS(bulksmsusername, bulksmspssd, msg, phoneno, "https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0");

                JOptionPane.showMessageDialog(null, "Message sent");
                
                
                try {
                    String mfrom = "Administrator";
                
                    String sql = "INSERT INTO `messages`(`padmno`, `m_from`, `m_to`, `m_msg`) VALUES (?,?,?)";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, padmno);
                    pst.setString(2, mfrom);
                    pst.setString(3, mrecepient);
                    pst.setString(4, mssg);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Message saved.");

                    //rb1.setSelected(false);
                    
                    messageList();
                
                } catch (SQLException e) {
                }
            
            }else{
                    JOptionPane.showMessageDialog(null, "Please ensure all the fields are filled.");
            }           
            
            
//        } else if(rb2.isSelected() && cbosm2.getSelectedIndex() == 3) {
//                txtsm3.setEditable(false);
//                txtsm4.setEditable(false);
//                txtsm8.setEditable(true);
//            
//            
//        }else{
//            JOptionPane.showMessageDialog(null, "Please select the type of message to send.");
//        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Admin().setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    private void clearVisitors(){
        cbovr1.setSelectedIndex(0);
        txtvr1.setText("");
        txtvr2.setText("");
        cbovr2.setSelectedIndex(0);
        txtvr3.setText("");
        txtvr4.setText("");
        txtvr5.setText("");
        txtvr6.setText("");
        txtvr7.setText("");
        txtvr8.setText("");
        txtvr9.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPromoteNextToClass;
    private javax.swing.JButton btnPupilListRefresh;
    private javax.swing.JButton btnVRClear;
    private javax.swing.JButton btnVRSave;
    private javax.swing.JButton btn_non_teaching_save;
    private javax.swing.JButton btn_other_uploadimg;
    private javax.swing.JButton btnacademics;
    private javax.swing.JButton btnadmission;
    private javax.swing.JButton btnclear;
    private javax.swing.JButton btndiscclear;
    private javax.swing.JButton btndiscsave;
    private javax.swing.JButton btnfinance;
    private javax.swing.JButton btngeneratepupilsexcelsheet;
    private javax.swing.JButton btngeneratetranferletter;
    private javax.swing.JButton btnleavingcert;
    private javax.swing.JButton btnlib;
    private javax.swing.JButton btnlogclear;
    private javax.swing.JButton btnloggenreport;
    private javax.swing.JButton btnnewterm;
    private javax.swing.JButton btnnewterm1;
    private javax.swing.JButton btnnewterm2;
    private javax.swing.JButton btnnext;
    private javax.swing.JButton btnotherclear;
    private javax.swing.JButton btnotherdelete;
    private javax.swing.JButton btnothergenexcelsheet;
    private javax.swing.JButton btnotherupdate;
    private javax.swing.JButton btnprevious;
    private javax.swing.JButton btnsasave;
    private javax.swing.JButton btnsaview;
    private javax.swing.JButton btnsmCancel;
    private javax.swing.JButton btnstore;
    private javax.swing.JButton btntr_uploadimg;
    private javax.swing.JButton btntrclear;
    private javax.swing.JButton btntrdelete;
    private javax.swing.JButton btntrgenerateexcelsheet;
    private javax.swing.JButton btntrsave;
    private javax.swing.JButton btntrupdate;
    private javax.swing.JButton btnvrsignout;
    private javax.swing.JButton btsauploadnewImage;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbootdept;
    private javax.swing.JComboBox<String> cbootemployedas;
    private javax.swing.JComboBox<String> cbootgndr;
    private javax.swing.JComboBox<String> cbootrel;
    private javax.swing.JComboBox<String> cbotrclasstr;
    private javax.swing.JComboBox<String> cbotremployedas;
    private javax.swing.JComboBox<String> cbotrgender;
    private javax.swing.JComboBox<String> cbotrjobgroup;
    private javax.swing.JComboBox<String> cbotrrel;
    private javax.swing.JComboBox<String> cbovr1;
    private javax.swing.JComboBox<String> cbovr2;
    private javax.swing.JComboBox<String> cbowhichclass;
    private javax.swing.JCheckBox chkotaspirin;
    private javax.swing.JCheckBox chkotmeatandrelated;
    private javax.swing.JCheckBox chkototherhealthinfo;
    private javax.swing.JCheckBox chkotpenicillin;
    private javax.swing.JCheckBox chkotpollen;
    private javax.swing.JCheckBox chktraspirin;
    private javax.swing.JCheckBox chktrmeatandrelated;
    private javax.swing.JCheckBox chktrotherallergy;
    private javax.swing.JCheckBox chktrpenicillin;
    private javax.swing.JCheckBox chktrpollen;
    private javax.swing.JDesktopPane disc_jDesktopPane;
    private javax.swing.JLabel disc_pupil_img;
    private javax.swing.JLabel discpstatus;
    private javax.swing.JDesktopPane jDesktopPane;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel118;
    private javax.swing.JLabel jLabel119;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel126;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel129;
    private javax.swing.JLabel jLabel130;
    private javax.swing.JLabel jLabel131;
    private javax.swing.JLabel jLabel132;
    private javax.swing.JLabel jLabel133;
    private javax.swing.JLabel jLabel141;
    private javax.swing.JLabel jLabel142;
    private javax.swing.JLabel jLabel143;
    private javax.swing.JLabel jLabel145;
    private javax.swing.JLabel jLabel146;
    private javax.swing.JLabel jLabel147;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel164;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel174;
    private javax.swing.JLabel jLabel179;
    private javax.swing.JLabel jLabel180;
    private javax.swing.JLabel jLabel181;
    private javax.swing.JLabel jLabel182;
    private javax.swing.JLabel jLabel183;
    private javax.swing.JLabel jLabel184;
    private javax.swing.JLabel jLabel185;
    private javax.swing.JLabel jLabel186;
    private javax.swing.JLabel jLabel187;
    private javax.swing.JLabel jLabel189;
    private javax.swing.JLabel jLabel190;
    private javax.swing.JLabel jLabel192;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel101;
    private javax.swing.JPanel jPanel102;
    private javax.swing.JPanel jPanel103;
    private javax.swing.JPanel jPanel104;
    private javax.swing.JPanel jPanel105;
    private javax.swing.JPanel jPanel106;
    private javax.swing.JPanel jPanel107;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel56;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel89;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanel90;
    private javax.swing.JPanel jPanel91;
    private javax.swing.JPanel jPanel93;
    private javax.swing.JPanel jPanel94;
    private javax.swing.JPanel jPanel95;
    private javax.swing.JPanel jPanel99;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane19;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane20;
    private javax.swing.JScrollPane jScrollPane24;
    private javax.swing.JScrollPane jScrollPane25;
    private javax.swing.JScrollPane jScrollPane26;
    private javax.swing.JScrollPane jScrollPane27;
    private javax.swing.JScrollPane jScrollPane28;
    private javax.swing.JScrollPane jScrollPane29;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JTabbedPane jTabbedPane5;
    private javax.swing.JButton jbtnlogout;
    private com.toedter.calendar.JDateChooser jotDob;
    private com.toedter.calendar.JDateChooser jtrDob;
    private javax.swing.JLabel lbl_other_profileimage;
    private javax.swing.JLabel lbl_tr_profileimage;
    private javax.swing.JLabel lblloggedinusercode;
    private javax.swing.JLabel lblmsg;
    private javax.swing.JLabel lbltime;
    private javax.swing.JLabel lbltrstatus;
    private javax.swing.JLabel ntstatus;
    private javax.swing.JRadioButton rbnt;
    private javax.swing.JRadioButton rbparent;
    private javax.swing.JRadioButton rbts;
    private javax.swing.JLabel sa_photo;
    private javax.swing.JTable tbldisclist;
    private javax.swing.JTable tbllogdetail;
    private javax.swing.JTable tblmsglist;
    private javax.swing.JTable tblothlist;
    private javax.swing.JTable tblpupilslist;
    private javax.swing.JTable tbltrlist;
    private javax.swing.JDesktopPane tr_JDesktopPane;
    private javax.swing.JLabel trnf_profile_img;
    private javax.swing.JDesktopPane trnsfr_jDesktopPane;
    private javax.swing.JTextField txtclass;
    private javax.swing.JTextField txtdiscadmno;
    private javax.swing.JTextField txtdiscage;
    private javax.swing.JTextField txtdiscbywhom;
    private javax.swing.JTextField txtdiscclass;
    private javax.swing.JTextField txtdiscdate;
    private javax.swing.JTextField txtdiscgender;
    private javax.swing.JTextField txtdisciplinesearch;
    private javax.swing.JTextArea txtdiscoffence;
    private javax.swing.JTextArea txtdiscpunishment;
    private javax.swing.JTextField txtdiscpupilname;
    private javax.swing.JTextField txtlistsearch;
    private javax.swing.JTextField txtlogworkid;
    private javax.swing.JTextField txtntsearch;
    private javax.swing.JTextField txtotdob;
    private javax.swing.JTextField txtotdoe;
    private javax.swing.JTextField txtotgroup_category;
    private javax.swing.JTextField txtotidno;
    private javax.swing.JTextField txtotinsurancecarrier;
    private javax.swing.JTextField txtotkrapin;
    private javax.swing.JTextField txtotnamee;
    private javax.swing.JTextField txtotnationality;
    private javax.swing.JTextField txtotnssf;
    private javax.swing.JTextArea txtototherhealthinfo;
    private javax.swing.JTextField txtotpayslip;
    private javax.swing.JTextField txtotphone;
    private javax.swing.JTextField txtotpolicyholdername;
    private javax.swing.JTextField txtotpolicyno;
    private javax.swing.JTextField txtotsalary;
    private javax.swing.JTextField txtotworkid;
    private javax.swing.JTextField txtsa1;
    private javax.swing.JTextField txtsa2;
    private javax.swing.JTextField txtsa3;
    private javax.swing.JTextField txtsa4;
    private javax.swing.JTextArea txtsa5;
    private javax.swing.JTextField txtsearchleaving;
    private javax.swing.JTextField txtsearchtrid_two;
    private javax.swing.JTextField txtsm3;
    private javax.swing.JTextField txtsm4;
    private javax.swing.JTextField txtsm5;
    private javax.swing.JTextArea txtsm6;
    private javax.swing.JButton txtsmSend;
    private javax.swing.JTextField txttrdateoe;
    private javax.swing.JTextField txttrdept;
    private javax.swing.JTextField txttrdob;
    private javax.swing.JTextField txttrgroupno;
    private javax.swing.JTextField txttridno;
    private javax.swing.JTextField txttrinsurancecarrier;
    private javax.swing.JTextField txttrkrapin;
    private javax.swing.JTextField txttrname;
    private javax.swing.JTextField txttrnationality;
    private javax.swing.JTextField txttrnfadmno;
    private javax.swing.JTextArea txttrnfcauseoftrnf;
    private javax.swing.JTextField txttrnfcurrclass;
    private javax.swing.JTextField txttrnfdob;
    private javax.swing.JTextArea txttrnfhdtrremarks;
    private javax.swing.JTextField txttrnfname;
    private javax.swing.JTextField txttrnfparentname;
    private javax.swing.JTextField txttrnfsearch;
    private javax.swing.JTextArea txttrnsfconduct;
    private javax.swing.JTextField txttrnssfno;
    private javax.swing.JTextArea txttrotherallergy;
    private javax.swing.JTextField txttrpayslipno;
    private javax.swing.JTextField txttrphone;
    private javax.swing.JTextField txttrpolictholdername;
    private javax.swing.JTextField txttrpolicyno;
    private javax.swing.JTextField txttrsalary;
    private javax.swing.JTextField txttrsubjectspeciality;
    private javax.swing.JTextField txttrtscno;
    private javax.swing.JTextField txttrworkingid;
    private javax.swing.JTextField txtvr1;
    private javax.swing.JTextField txtvr2;
    private javax.swing.JTextArea txtvr3;
    private javax.swing.JTextArea txtvr4;
    private javax.swing.JTextField txtvr5;
    private javax.swing.JTextField txtvr6;
    private javax.swing.JTextField txtvr7;
    private javax.swing.JTextArea txtvr8;
    private javax.swing.JTextField txtvr9;
    // End of variables declaration//GEN-END:variables

    
    
}
