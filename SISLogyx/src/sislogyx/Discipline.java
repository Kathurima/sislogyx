/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import java.awt.Color;
import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author OREON
 */
public class Discipline extends javax.swing.JFrame {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    
    byte[] pupil_image = null;
    
    DefaultTableModel dm = new DefaultTableModel();

    /**
     * Creates new form Discipline
     * @throws java.sql.SQLException
     */
    public Discipline() throws SQLException {
        super("Discipline Panel");
        
        initComponents();
        
        conn = (Connection) DBConnect.connect();
        
        Current_Date();
        
        tbltrdisclist.setModel(Discipline_table());
        
    }
    
    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);

                    txttrdiscdate.setText(day+"/"+month+"/"+year);

                    int second = cal.get(Calendar.SECOND);
                    int minute = cal.get(Calendar.MINUTE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);
                    
                    //Emp.SIStime = hour+":"+minute+":"+second;

//                    lbltime.setText(hour+":"+minute+":"+second);
//                    lbltime.setForeground(Color.red);
                }
            }
        };
        clock.start();
   }

    private DefaultTableModel Discipline_table(){
        dm.addColumn("Date");
        dm.addColumn("Pupil's Name");
        dm.addColumn("Adm. No");
        dm.addColumn("Gender");
        dm.addColumn("Class");
        dm.addColumn("Age");
        dm.addColumn("Offence");
        dm.addColumn("Punishment");
        dm.addColumn("Punished By");
        dm.addColumn("Status");
        
        try{
            String sql = "SELECT `d_pdate`,`d_pname`,`d_padmno`,`d_pgender`,`d_pclass`,`d_p_age`,`d_poffence`,`d_ppunishment`,`d_pwhom`,`status` FROM `discipline` ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();

            while (rs.next()) {
                String date = rs.getString("d_pdate");
                String pupilsname = rs.getString("d_pname");
                String padmno = rs.getString("d_padmno");
                String gender = rs.getString("d_pgender");
                String pclass = rs.getString("d_pclass");
                String age = rs.getString("d_p_age");
                String offense = rs.getString("d_poffence");
                String punishment = rs.getString("d_ppunishment");
                String punisher = rs.getString("d_pwhom");
                String status = rs.getString("status");
                
                String[] rowdata = {date, pupilsname, padmno, gender, pclass, age, offense, punishment, punisher, status};
                dm.addRow(rowdata);
            }
            
            return dm;
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        return null;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        txttrdisciplinesearch = new javax.swing.JTextField();
        jLabel74 = new javax.swing.JLabel();
        txttrdiscdate = new javax.swing.JTextField();
        jLabel75 = new javax.swing.JLabel();
        txttrdiscbywhom = new javax.swing.JTextField();
        jScrollPane22 = new javax.swing.JScrollPane();
        tbltrdisclist = new javax.swing.JTable();
        txttrdiscpupilname = new javax.swing.JTextField();
        jLabel76 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        txttrdiscgender = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        txttrdiscclass = new javax.swing.JTextField();
        jLabel79 = new javax.swing.JLabel();
        txttrdiscage = new javax.swing.JTextField();
        jLabel80 = new javax.swing.JLabel();
        jScrollPane23 = new javax.swing.JScrollPane();
        txttrdiscoffence = new javax.swing.JTextArea();
        jLabel81 = new javax.swing.JLabel();
        jScrollPane33 = new javax.swing.JScrollPane();
        txttrdiscpunishment = new javax.swing.JTextArea();
        btndiscsave4 = new javax.swing.JButton();
        btndiscclear4 = new javax.swing.JButton();
        disc_jDesktopPane4 = new javax.swing.JDesktopPane();
        trdisc_pupil_img = new javax.swing.JLabel();
        txttrdiscadmno = new javax.swing.JTextField();
        jLabel82 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        disctrpstatus = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Discipline Records", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Pupil's Admission");

        txttrdisciplinesearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrdisciplinesearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrdisciplinesearchKeyReleased(evt);
            }
        });

        jLabel74.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel74.setText("Date");

        txttrdiscdate.setEditable(false);
        txttrdiscdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel75.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel75.setText("By Whom");

        txttrdiscbywhom.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrdiscbywhom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttrdiscbywhomKeyReleased(evt);
            }
        });

        tbltrdisclist.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbltrdisclist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbltrdisclist.setPreferredSize(new java.awt.Dimension(100, 64));
        jScrollPane22.setViewportView(tbltrdisclist);

        txttrdiscpupilname.setEditable(false);
        txttrdiscpupilname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel76.setText("Pupil's Name");

        jLabel77.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel77.setText("Gender");

        txttrdiscgender.setEditable(false);
        txttrdiscgender.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel78.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel78.setText("Class");

        txttrdiscclass.setEditable(false);
        txttrdiscclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel79.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel79.setText("Age");

        txttrdiscage.setEditable(false);
        txttrdiscage.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel80.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel80.setText("Offence");

        txttrdiscoffence.setColumns(20);
        txttrdiscoffence.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrdiscoffence.setRows(5);
        jScrollPane23.setViewportView(txttrdiscoffence);

        jLabel81.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel81.setText("Punishment");

        txttrdiscpunishment.setColumns(20);
        txttrdiscpunishment.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttrdiscpunishment.setRows(5);
        jScrollPane33.setViewportView(txttrdiscpunishment);

        btndiscsave4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btndiscsave4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
        btndiscsave4.setText("SAVE");
        btndiscsave4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndiscsave4ActionPerformed(evt);
            }
        });

        btndiscclear4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btndiscclear4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btndiscclear4.setText("CLEAR");
        btndiscclear4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndiscclear4ActionPerformed(evt);
            }
        });

        disc_jDesktopPane4.setLayer(trdisc_pupil_img, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout disc_jDesktopPane4Layout = new javax.swing.GroupLayout(disc_jDesktopPane4);
        disc_jDesktopPane4.setLayout(disc_jDesktopPane4Layout);
        disc_jDesktopPane4Layout.setHorizontalGroup(
            disc_jDesktopPane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(trdisc_pupil_img, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
        );
        disc_jDesktopPane4Layout.setVerticalGroup(
            disc_jDesktopPane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(trdisc_pupil_img, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
        );

        txttrdiscadmno.setEditable(false);
        txttrdiscadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel82.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel82.setText("Admission No.");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Pupil's Status");

        disctrpstatus.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        disctrpstatus.setText("Status");

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                .addContainerGap(146, Short.MAX_VALUE)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel82)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel74)
                                .addComponent(jLabel76)
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txttrdiscdate, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txttrdiscpupilname, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)))
                            .addComponent(txttrdiscadmno)
                            .addComponent(jScrollPane23, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                            .addComponent(jLabel79)
                            .addComponent(txttrdiscage, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane33, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                            .addComponent(jLabel81)
                            .addComponent(jLabel80)
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel77)
                                    .addComponent(txttrdiscgender, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel78)
                                    .addComponent(txttrdiscclass, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel75)
                                    .addComponent(txttrdiscbywhom, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(disctrpstatus, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGap(49, 49, 49)
                                .addComponent(disc_jDesktopPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(94, 94, 94))
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(jScrollPane22, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jLabel41)
                        .addGap(10, 10, 10)
                        .addComponent(txttrdisciplinesearch, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btndiscclear4, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btndiscsave4, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(76, 76, 76))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(disc_jDesktopPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel41)
                                    .addComponent(txttrdisciplinesearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel75))
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addComponent(jLabel74)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txttrdiscdate, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttrdiscbywhom, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addComponent(jLabel76)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrdiscpupilname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(disctrpstatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addGap(4, 4, 4)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane22, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(jLabel82)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addComponent(txttrdiscadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel77)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrdiscgender, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addComponent(jLabel78)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttrdiscclass, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel79)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttrdiscage, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel80)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane23, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel81)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane33, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btndiscsave4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btndiscclear4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(34, 34, 34))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 16, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 51, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txttrdisciplinesearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrdisciplinesearchKeyReleased
        // TODO add your handling code here:
        //Searching in discipline
        try {
            String sql = "SELECT `name`,`admno`,`gender`,`n_class`,`age`,`status`,`profile_image` FROM `admissions` WHERE `admno`=? AND `status` = \"1\" ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txttrdisciplinesearch.getText());
            rs = pst.executeQuery();

            if (rs.next()) {
                String pupilsname = rs.getString("name");
                txttrdiscpupilname.setText(pupilsname);
                String padmno = rs.getString("admno");
                txttrdiscadmno.setText(padmno);
                String pupilgndr = rs.getString("gender");
                txttrdiscgender.setText(pupilgndr);
                String pupilclass = rs.getString("n_class");
                txttrdiscclass.setText(pupilclass);
                String pupilage = rs.getString("age");
                txttrdiscage.setText(pupilage);
                String pupilstatus = rs.getString("status");
                int ps = Integer.parseInt(pupilstatus);
                if (ps ==1 ) {
                    disctrpstatus.setText("Active");
                } else {
                    disctrpstatus.setText("Dormant");
                }
                

                byte[] trdisc_profileimage = rs.getBytes("profile_image");
                ImageIcon imageIcon = new ImageIcon(new ImageIcon(trdisc_profileimage).getImage().getScaledInstance(trdisc_pupil_img.getWidth(), trdisc_pupil_img.getHeight(), Image.SCALE_SMOOTH));
                trdisc_pupil_img.setIcon(imageIcon);
            }

        } catch (SQLException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_txttrdisciplinesearchKeyReleased

    private void btndiscsave4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndiscsave4ActionPerformed
        // TODO add your handling code here:
        //Saving Disciplinary Data
        try{
            String date = txttrdiscdate.getText();
            String pupilsname = txttrdiscpupilname.getText();
            String pupiladmno = txttrdiscadmno.getText();
            String pupilgender = txttrdiscgender.getText();
            String pupilclass = txttrdiscclass.getText();
            String pupilage = txttrdiscage.getText();
            String pupiloffence = txttrdiscoffence.getText();
            String pupilpunishment = txttrdiscpunishment.getText();
            String punishmentbywhom = txttrdiscbywhom.getText();
            String pupilstatus = disctrpstatus.getText();
            //byte[] pupil_profileimg = this.pupil_image;

            if (pupiloffence.isEmpty() || pupilpunishment.isEmpty() || punishmentbywhom.isEmpty()) {

                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled");

            } else {
                int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Add a new Disciplinary record?", "Add Record", JOptionPane.YES_NO_OPTION);

                if(x==0){

                    String sql ="INSERT INTO `discipline`(`d_pdate`, `d_pname`, `d_padmno`, `d_pgender`, `d_pclass`, `d_p_age`, `d_poffence`,"
                    + " `d_ppunishment`, `d_pwhom`, `status`, `d_p_profile_img`) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?) ";

                    pst = conn.prepareStatement(sql);
                    pst.setString(1, date);
                    pst.setString(2, pupilsname);
                    pst.setString(3, pupiladmno);
                    pst.setString(4, pupilgender);
                    pst.setString(5, pupilclass);
                    pst.setString(6, pupilage);
                    pst.setString(7, pupiloffence);
                    pst.setString(8, pupilpunishment);
                    pst.setString(9, punishmentbywhom);
                    pst.setString(10, pupilstatus);
                    pst.setBytes(11, pupil_image);
                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Data succesfully recorded");

                    DefaultTableModel model = (DefaultTableModel)tbltrdisclist.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    Discipline_table();

                    txttrdiscpupilname.setText("");
                    txttrdiscadmno.setText("");
                    txttrdiscgender.setText("");
                    txttrdiscclass.setText("");
                    txttrdiscage.setText("");
                    txttrdiscoffence.setText("");
                    txttrdiscpunishment.setText("");
                    txttrdiscbywhom.setText("");
                    disctrpstatus.setText("Status");
                    trdisc_pupil_img.setText("");
                }
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,e);
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);

        }
    }//GEN-LAST:event_btndiscsave4ActionPerformed

    private void btndiscclear4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndiscclear4ActionPerformed
        // TODO add your handling code here:
        //txtdiscdate.setText("");
        txttrdiscpupilname.setText("");
        txttrdiscadmno.setText("");
        txttrdiscgender.setText("");
        txttrdiscclass.setText("");
        txttrdiscage.setText("");
        txttrdiscoffence.setText("");
        txttrdiscpunishment.setText("");
        txttrdiscbywhom.setText("");
        disctrpstatus.setText("Status");
        trdisc_pupil_img.setText("");
    }//GEN-LAST:event_btndiscclear4ActionPerformed

    private void txttrdiscbywhomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttrdiscbywhomKeyReleased
        // TODO add your handling code here:
        if(txttrdiscbywhom.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txttrdiscbywhom.setText("");
        }
    }//GEN-LAST:event_txttrdiscbywhomKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Discipline.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Discipline.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Discipline.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Discipline.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Discipline().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Discipline.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btndiscclear4;
    private javax.swing.JButton btndiscsave4;
    private javax.swing.JDesktopPane disc_jDesktopPane4;
    private javax.swing.JLabel disctrpstatus;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JScrollPane jScrollPane22;
    private javax.swing.JScrollPane jScrollPane23;
    private javax.swing.JScrollPane jScrollPane33;
    private javax.swing.JTable tbltrdisclist;
    private javax.swing.JLabel trdisc_pupil_img;
    private javax.swing.JTextField txttrdiscadmno;
    private javax.swing.JTextField txttrdiscage;
    private javax.swing.JTextField txttrdiscbywhom;
    private javax.swing.JTextField txttrdiscclass;
    private javax.swing.JTextField txttrdiscdate;
    private javax.swing.JTextField txttrdiscgender;
    private javax.swing.JTextField txttrdisciplinesearch;
    private javax.swing.JTextArea txttrdiscoffence;
    private javax.swing.JTextArea txttrdiscpunishment;
    private javax.swing.JTextField txttrdiscpupilname;
    // End of variables declaration//GEN-END:variables
}
