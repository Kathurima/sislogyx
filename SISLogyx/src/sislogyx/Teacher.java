/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public class Teacher extends javax.swing.JFrame {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    
    String classtr, trclass;
    
    DefaultTableModel classpupillist = new DefaultTableModel();
    DefaultTableModel trlibowed = new DefaultTableModel();

    /**
     * Creates new form Teacher_Menu
     * @throws java.sql.SQLException
     */
    public Teacher() throws SQLException {
        super("Teacher's Dashboard");
        
        initComponents();
        
        conn = (Connection) DBConnect.connect();
        
        Current_Date();
        
        lbltrloggedin.setText(String.valueOf(Emp.empId));
        
        getIfClassTr();
        
        if (classtr.equals("Yes")) {
            System.out.println(classtr);
            System.out.println(trclass);
            trlblclassassigned.setText(trclass);
        } else {
            System.out.println("Not class tr.");
            trlblclassassigned.setText("Not class teacher.");
        }
        
        tblclasspupilslist.setModel(ClassPupils_List());
        tbltrlibowed.setModel(TrLibOwed_List());
        
        getTrDetails();
        
    }
    
    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);

                    //txtlibborrowdate.setText(day+"/"+month+"/"+year);
                    

                    int second = cal.get(Calendar.SECOND);
                    int minute = cal.get(Calendar.MINUTE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);

                    lbltrtime.setText(hour+":"+minute+":"+second);
                    lbltrtime.setForeground(Color.red);
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException ie) {
//                        Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ie);
//                    }
                }
            }
        };
        clock.start();
   }
    
    private void getIfClassTr(){
        try {
//            String sql = "SELECT `t_class_tr`, `t_which_class` FROM `teachers` WHERE `t_trid`=\"ND002\" AND `status`=\"1\" ";
            String sql = "SELECT `t_class_tr`, `t_which_class` FROM `teachers` WHERE `t_trid`='"+lbltrloggedin.getText()+"' AND `status`=\"1\" ";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                classtr = rs.getString("t_class_tr");
                trclass = rs.getString("t_which_class");
            }
            
        } catch (SQLException e) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }
    
    private DefaultTableModel ClassPupils_List(){
        classpupillist.addColumn("Adm. No.");
        classpupillist.addColumn("Name");
        classpupillist.addColumn("Surname");
        classpupillist.addColumn("Gender");
        classpupillist.addColumn("Class");
        
        try {
            String sql = "SELECT * FROM `admissions` WHERE `curr_class`='"+trlblclassassigned.getText()+"' ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                String pladmno = rs.getString("admno");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String gender = rs.getString("gender");
                String pclass = rs.getString("curr_class");
                
                String[] rowdata = {pladmno, name, surname, gender, pclass};
                classpupillist.addRow(rowdata);
            }
            
            return classpupillist;
            
        } catch (SQLException e) {
        }
        
        return null;        
    }
    
    private DefaultTableModel TrLibOwed_List(){
        trlibowed.addColumn("Custodian ID");
        trlibowed.addColumn("Custodian Name");
        trlibowed.addColumn("Custodian Category");
        trlibowed.addColumn("Material ID");
        trlibowed.addColumn("Title");
        trlibowed.addColumn("Author");
        trlibowed.addColumn("Category");
        trlibowed.addColumn("Subject");
        trlibowed.addColumn("Copies");
        //trlibowed.addColumn("Edition");
        //trlibowed.addColumn("YOP");        
        trlibowed.addColumn("Borrow Date");
        trlibowed.addColumn("Return Date");
        
        try{
            String sql = "SELECT * FROM `library_borrow` WHERE `custodian_id`='"+lbltrloggedin.getText()+"' ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();
            
            while (rs.next()) {
                String ctype = rs.getString("custodian_type");
                String mid = rs.getString("material_id");
                String title = rs.getString("title");
                String author = rs.getString("author");
                String cat = rs.getString("category");
                String sbjct = rs.getString("subject");
                String copies = rs.getString("lendedcopies");
//                String edtn = rs.getString("edition");
//                String yop = rs.getString("yop");
                String cid = rs.getString("custodian_id");
                //String cdesig = rs.getString("e_dept");
                String cname = rs.getString("custodian_name");
                String bdate = rs.getString("borrow_date");
                String rdate = rs.getString("return_date");
                
                String[] rowdata = {cid,cname,ctype,mid,title,author,cat,sbjct,copies,bdate,rdate};
                trlibowed.addRow(rowdata);
            }
            
            return trlibowed;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;     
    }
    
    private void getTrDetails(){
//        try {
//            String sql = "SELECT * FROM `teachers` WHERE `t_trid`=? ";
//            pst = conn.prepareStatement(sql);
//            pst.setString(1, lbltrloggedin.getText());
//            rs = pst.executeQuery();
//            
//            while (rs.next()) {                
//                trlblworkid.setText(rs.getString("t_trid"));
//                lbltrclassassign.setText(rs.getString("t_which_class"));
//                lbltrtscno.setText(rs.getString("t_tscno"));
//                trlblname.setText(rs.getString("t_name"));
//                lbltrgndr.setText(rs.getString("t_gender"));
//                lbltrnat.setText(rs.getString("t_nationality"));
//                lbltrbdate.setText(rs.getString("t_dob"));
//                lbltrdateemp.setText(rs.getString("t_dateofjoin"));
//                lbltrcontact.setText(rs.getString("t_phone"));
//                byte[] tr_profileimage = rs.getBytes("t_profile_img");
//                ImageIcon imageIcon = new ImageIcon(new ImageIcon(tr_profileimage).getImage().getScaledInstance(trprofileimg.getWidth(), trprofileimg.getHeight(), Image.SCALE_SMOOTH));
//                trprofileimg.setIcon(imageIcon);
//            }
//            
//        } catch (SQLException e) {
//        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnTrLogout = new javax.swing.JButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        txtadmnosearch = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtpname = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtpadmno = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtpclass = new javax.swing.JTextField();
        jPanel16 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        txtmatno = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txtmattitle = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtmatpub = new javax.swing.JTextField();
        btnIssue = new javax.swing.JButton();
        btnCollect = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblclasspupilslist = new javax.swing.JTable();
        btnPupilListRefresh = new javax.swing.JButton();
        btngeneratepupilsexcelsheet = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbltrlibowed = new javax.swing.JTable();
        btnTrLibOwed = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        btnTRDiscipline = new javax.swing.JButton();
        btnEnterExamMarks = new javax.swing.JButton();
        btnEditExamMarks = new javax.swing.JButton();
        btnfViewPupilsInfo = new javax.swing.JButton();
        btnTrChangePssd = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        trlblclassassigned = new javax.swing.JLabel();
        lbltrloggedin = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lbltrtime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1366, 766));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        jPanel3.setBackground(new java.awt.Color(102, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("TEACHERS' DASHBOARD");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1)
                .addContainerGap(52, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnTrLogout.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnTrLogout.setText("Logout");
        btnTrLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrLogoutActionPerformed(evt);
            }
        });

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 952, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 540, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Home", jPanel6);

        jPanel12.setBackground(new java.awt.Color(0, 153, 153));
        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Issue Class Textbook", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Pupil's Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setText("Admission No.");

        txtadmnosearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtadmnosearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtadmnosearchKeyReleased(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setText("Name");

        txtpname.setEditable(false);
        txtpname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setText("Admission No.");

        txtpadmno.setEditable(false);
        txtpadmno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setText("Class");

        txtpclass.setEditable(false);
        txtpclass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtadmnosearch, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(txtpname, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(txtpadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addComponent(txtpclass, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtpclass, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(txtadmnosearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtpname, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtpadmno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED), "Material Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel22.setText("Material No. ");

        txtmatno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtmatno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmatnoActionPerformed(evt);
            }
        });
        txtmatno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmatnoKeyReleased(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel23.setText("Title ");

        txtmattitle.setEditable(false);
        txtmattitle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel24.setText("Publisher ");

        txtmatpub.setEditable(false);
        txtmatpub.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtmatpub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmatpubActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22)
                    .addComponent(txtmatno, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(txtmattitle, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24)
                    .addComponent(txtmatpub, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmatpub, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmattitle, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmatno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnIssue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnIssue.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/issue.png"))); // NOI18N
        btnIssue.setText("ISSUE");
        btnIssue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIssueActionPerformed(evt);
            }
        });

        btnCollect.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCollect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/return.png"))); // NOI18N
        btnCollect.setText("COLLECT");
        btnCollect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCollectActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addComponent(btnIssue, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCollect, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(76, 76, 76))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnIssue)
                    .addComponent(btnCollect))
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(122, Short.MAX_VALUE)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(145, 145, 145))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Issue Class Textbook", jPanel5);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Class Pupils List", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        tblclasspupilslist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblclasspupilslist);

        btnPupilListRefresh.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPupilListRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnPupilListRefresh.setText("Refresh");
        btnPupilListRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPupilListRefreshActionPerformed(evt);
            }
        });

        btngeneratepupilsexcelsheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btngeneratepupilsexcelsheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btngeneratepupilsexcelsheet.setText("Generate Class List");
        btngeneratepupilsexcelsheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngeneratepupilsexcelsheetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btngeneratepupilsexcelsheet)
                        .addGap(18, 18, 18)
                        .addComponent(btnPupilListRefresh)))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPupilListRefresh)
                    .addComponent(btngeneratepupilsexcelsheet))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Class Pupils List", jPanel4);

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Library Materials", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        tbltrlibowed.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbltrlibowed);

        btnTrLibOwed.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnTrLibOwed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnTrLibOwed.setText("Refresh");
        btnTrLibOwed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrLibOwedActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(825, Short.MAX_VALUE)
                .addComponent(btnTrLibOwed)
                .addContainerGap())
            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel14Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(btnTrLibOwed)
                .addGap(0, 469, Short.MAX_VALUE))
            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel14Layout.createSequentialGroup()
                    .addGap(44, 44, 44)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Library Materials", jPanel8);

        jPanel7.setBackground(new java.awt.Color(102, 255, 204));

        btnTRDiscipline.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnTRDiscipline.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/thumbdown.png"))); // NOI18N
        btnTRDiscipline.setText("DISCIPLINE");
        btnTRDiscipline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTRDisciplineActionPerformed(evt);
            }
        });

        btnEnterExamMarks.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEnterExamMarks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/letter.png"))); // NOI18N
        btnEnterExamMarks.setText("ENTER EXAM MARKS");
        btnEnterExamMarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnterExamMarksActionPerformed(evt);
            }
        });

        btnEditExamMarks.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEditExamMarks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/calculate.png"))); // NOI18N
        btnEditExamMarks.setText("EDIT CLASS MARKS");
        btnEditExamMarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditExamMarksActionPerformed(evt);
            }
        });

        btnfViewPupilsInfo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnfViewPupilsInfo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/details.png"))); // NOI18N
        btnfViewPupilsInfo.setText("VIEW PUPIL DETAILS");
        btnfViewPupilsInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfViewPupilsInfoActionPerformed(evt);
            }
        });

        btnTrChangePssd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnTrChangePssd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/changepssd.png"))); // NOI18N
        btnTrChangePssd.setText("CHANGE PASSWORD");
        btnTrChangePssd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrChangePssdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTRDiscipline, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEnterExamMarks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditExamMarks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnfViewPupilsInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                    .addComponent(btnTrChangePssd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(btnTRDiscipline, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(btnEnterExamMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(btnEditExamMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(btnfViewPupilsInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(btnTrChangePssd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(96, Short.MAX_VALUE))
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Class Assigned: ");

        trlblclassassigned.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        trlblclassassigned.setText("Grade");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(trlblclassassigned, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(145, 145, 145)
                        .addComponent(btnTrLogout))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 987, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 73, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTrLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(trlblclassassigned, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 588, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        lbltrloggedin.setText("currently logged in usercode");

        jLabel3.setText("Logged in as:");

        lbltrtime.setText("time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(lbltrloggedin)
                        .addGap(18, 18, 18)
                        .addComponent(lbltrtime, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lbltrloggedin)
                    .addComponent(lbltrtime))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 8, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTRDisciplineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTRDisciplineActionPerformed
        try {
            // TODO add your handling code here:
            Discipline d = new Discipline();
            d.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTRDisciplineActionPerformed

    private void btnEnterExamMarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnterExamMarksActionPerformed
        try {
            // TODO add your handling code here:
            Academics ac = new Academics();
            ac.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnEnterExamMarksActionPerformed

    private void btnTrLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrLogoutActionPerformed
        // TODO add your handling code here:
        try {
            try {
                String asql = "INSERT INTO `auditlog`(`empid`, `date`, `time`, `status`) VALUES (?,?,?,?)";
                pst = conn.prepareStatement(asql);
                Date currentDate = GregorianCalendar.getInstance().getTime();
                DateFormat df = DateFormat.getInstance();
                String dateString = df.format(currentDate);
                
                Date d = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String timeString = sdf.format(d);
                
                String time = timeString;
                String date = dateString;
                pst.setString(1, Emp.empId);
                pst.setString(2, date);
                pst.setString(3, time);
                pst.setString(4, "Logged out");
                pst.execute();
                
            } catch (SQLException e) {
            }
            
            this.dispose();
            Login l = new Login();
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTrLogoutActionPerformed

    private void btnEditExamMarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditExamMarksActionPerformed
        // TODO add your handling code here:
        if (classtr.equals("Yes")) {
            
            switch (trclass) {
                case "Pre-primary 1":
                case "Pre-primary 2":
                {
                try {
                    Pre_Primary_Marks_Edit ppme = new Pre_Primary_Marks_Edit();
                    ppme.setVisible(true);
                    break;
                } catch (SQLException ex) {
                    Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
                }
                }
                case "Grade 1":
                case "Grade 2":
                case "Grade 3":
                    {
                try {
                    Lower_Primary_Edit_Marks lpme = new Lower_Primary_Edit_Marks();
                    lpme.setVisible(true);
                    break;
                } catch (SQLException ex) {
                    Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
                }
                    }
                case "Grade 4":
                case "Grade 5":
                case "Grade 6":
                    {
                try {
                    Lower_Primary_Edit_Marks lpme = new Lower_Primary_Edit_Marks();
                    lpme.setVisible(true);
                    break;
                } catch (SQLException ex) {
                    Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
                }
                    }
                default:
                    break;
            }
            
        } else{
            JOptionPane.showMessageDialog(null, "Not a class teacher.");
        }
    }//GEN-LAST:event_btnEditExamMarksActionPerformed

    private void btnPupilListRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPupilListRefreshActionPerformed
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)tblclasspupilslist.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        ClassPupils_List();
    }//GEN-LAST:event_btnPupilListRefreshActionPerformed

    //Getting each cell data from pupillist table
    private String getCellValue(int x, int y){
        return classpupillist.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btngeneratepupilsexcelsheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngeneratepupilsexcelsheetActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();

        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();

        //Add column headers
        data.put("-1", new Object[]{classpupillist.getColumnName(0),classpupillist.getColumnName(1),classpupillist.getColumnName(2),
            classpupillist.getColumnName(3),classpupillist.getColumnName(4)});

    //Looping through the table rows
    for (int i = 0; i < classpupillist.getRowCount(); i++) {
        data.put(Integer.toString(i), new Object[]{getCellValue(i, 0),getCellValue(i, 1),getCellValue(i, 2),getCellValue(i, 3),getCellValue(i, 4)});
        }

        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;

        for (String key : ids) {
            row = ws.createRow(rowID++);

            //Get Data as per Key
            Object[] values = data.get(key);

            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }

        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\"+trlblclassassigned.getText()+" Class list.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Class list successfully generated.");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }

    }//GEN-LAST:event_btngeneratepupilsexcelsheetActionPerformed

    private void btnTrLibOwedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrLibOwedActionPerformed
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)tbltrlibowed.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        TrLibOwed_List();
    }//GEN-LAST:event_btnTrLibOwedActionPerformed

    private void btnfViewPupilsInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfViewPupilsInfoActionPerformed
        try {
            // TODO add your handling code here:
            PupilProfile pd = new PupilProfile();
            pd.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnfViewPupilsInfoActionPerformed

    private void txtmatnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmatnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmatnoActionPerformed

    private void txtmatpubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmatpubActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmatpubActionPerformed

    private void btnIssueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIssueActionPerformed
        // TODO add your handling code here:
        if (!txtpname.getText().isEmpty() && !txtpadmno.getText().isEmpty() && !txtpclass.getText().isEmpty() && !txtmatno.getText().isEmpty() &&
                !txtmattitle.getText().isEmpty() && !txtmatpub.getText().isEmpty()) {
            
            String pname = txtpname.getText();
            String padmno = txtpadmno.getText();
            String pclass = txtpclass.getText();
            String matno = txtmatno.getText();
            String matname = txtmattitle.getText();
            String matpub = txtmatpub.getText();
            int status = 1;
            
            try {
                String sql = "INSERT INTO `classissuedbooks`(`pname`, `padmno`, `pclass`, `mat_no`, `title`, `publisher`, `status`)"
                        + " VALUES (?,?,?,?,?,?,?)";
                pst = conn.prepareStatement(sql);
                
                pst.setString(1, pname);
                pst.setString(2, padmno);
                pst.setString(3, pclass);
                pst.setString(4, matno);
                pst.setString(5, matname);
                pst.setString(6, matpub);
                pst.setInt(7, status);
                
                pst.execute();
                
                JOptionPane.showMessageDialog(null, "Book Issued.");
                
                clearTrFields();
                
            } catch (SQLException e) {
                Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, e);
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "Please fill all the fields as required.");
        }
    }//GEN-LAST:event_btnIssueActionPerformed

    private void txtadmnosearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtadmnosearchKeyReleased
        // TODO add your handling code here:
        if (txtadmnosearch.getText().isEmpty()) {
            clearTrFields();
        } else {
            try {
                String sql = "SELECT * FROM `admissions` WHERE `admno`=? AND `curr_class`='"+trlblclassassigned.getText()+"' ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtadmnosearch.getText());
                rs = pst.executeQuery();

                while (rs.next()) {                
                    txtpname.setText(rs.getString("name"));
                    txtpadmno.setText(rs.getString("admno"));
                    txtpclass.setText(rs.getString("curr_class"));
                }

            } catch (SQLException e) {
                Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
    }//GEN-LAST:event_txtadmnosearchKeyReleased

    private void txtmatnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatnoKeyReleased
        // TODO add your handling code here:
        try {
            String sql = "SELECT * FROM `library_borrow` WHERE `material_id`=? AND `custodian_id`='"+lbltrloggedin.getText()+"' ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txtmatno.getText());
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                txtmattitle.setText(rs.getString("title"));
                txtmatpub.setText(rs.getString("publisher"));
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Ops! Book not found. ");
        }
    }//GEN-LAST:event_txtmatnoKeyReleased

    private void btnCollectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCollectActionPerformed
        // TODO add your handling code here:
        if (!txtpadmno.getText().isEmpty() && txtmatno.getText().isEmpty()) {
            try {
                String sql = "UPDATE `classissuedbooks` SET `status`=\"0\" WHERE `padmno`=? AND `mat_no`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtpadmno.getText());
                pst.setString(2, txtmatno.getText());

                pst.executeUpdate();
                
                JOptionPane.showMessageDialog(null, "Success.");
                
                clearTrFields();

            } catch (SQLException e) {
                Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please fill in all the fields as required.");
        }
        
    }//GEN-LAST:event_btnCollectActionPerformed

    private void btnTrChangePssdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrChangePssdActionPerformed
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            Change_Password cp = new Change_Password();
            cp.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTrChangePssdActionPerformed

    private void clearTrFields(){
        txtpname.setText("");
        txtpadmno.setText("");
        txtpclass.setText("");
        txtmattitle.setText("");
        txtmatpub.setText("");
        txtmatno.setText("");
        txtadmnosearch.setText("");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Teacher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Teacher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Teacher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Teacher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Teacher().setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCollect;
    private javax.swing.JButton btnEditExamMarks;
    private javax.swing.JButton btnEnterExamMarks;
    private javax.swing.JButton btnIssue;
    private javax.swing.JButton btnPupilListRefresh;
    private javax.swing.JButton btnTRDiscipline;
    private javax.swing.JButton btnTrChangePssd;
    private javax.swing.JButton btnTrLibOwed;
    private javax.swing.JButton btnTrLogout;
    private javax.swing.JButton btnfViewPupilsInfo;
    private javax.swing.JButton btngeneratepupilsexcelsheet;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JLabel lbltrloggedin;
    private javax.swing.JLabel lbltrtime;
    private javax.swing.JTable tblclasspupilslist;
    private javax.swing.JTable tbltrlibowed;
    private javax.swing.JLabel trlblclassassigned;
    private javax.swing.JTextField txtadmnosearch;
    private javax.swing.JTextField txtmatno;
    private javax.swing.JTextField txtmatpub;
    private javax.swing.JTextField txtmattitle;
    private javax.swing.JTextField txtpadmno;
    private javax.swing.JTextField txtpclass;
    private javax.swing.JTextField txtpname;
    // End of variables declaration//GEN-END:variables
}
