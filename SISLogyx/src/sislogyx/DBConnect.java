/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public class DBConnect {
    
    public static Connection connect() throws SQLException{
        
        Connection conn = null;
        
        String sislpssd = System.getenv("SISLOGYX");
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sislogyx","root",sislpssd);
            System.out.println("Yaay!! Connection Successful!");
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Failed");
        }
        return conn;        
    }
    
}
