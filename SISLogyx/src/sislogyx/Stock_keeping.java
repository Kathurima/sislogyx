/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import com.mysql.jdbc.Connection;
import com.teknikindustries.bulksms.SMS;
import java.awt.Color;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public class Stock_keeping extends javax.swing.JFrame {
    private Connection conn = null;
    private  PreparedStatement pst = null;
    private  ResultSet rs = null;
    
    int yr, qt, issqt, cissqt, issuedqt, currstckqty;
    String dbitemid, dbcustid;
    
    DefaultTableModel updatelist = new DefaultTableModel();
    
    DefaultTableModel stocklist = new DefaultTableModel();
    
    DefaultTableModel issuedstocklist = new DefaultTableModel();
    
    DefaultTableModel returnedstock = new DefaultTableModel();
    
    DefaultTableModel msglist = new DefaultTableModel();
    
    DefaultTableModel issuestocklistitems = new DefaultTableModel();

    /**
     * Creates new form Stock
     * @throws java.sql.SQLException
     */
    public Stock_keeping() throws SQLException {
        super("Stock-keeping Department");
        initComponents();
        
        conn = (Connection) DBConnect.connect();
        
        Current_Date();
        
        skloggedinusrcode.setText(String.valueOf(Emp.empId));
        
        tblupdatetable.setModel(updateTable());
        tblstocklist.setModel(stockList());
        tblissuestock.setModel(issueStockListItems());
        tblreturnitems.setModel(returnIssuedStock());
        tblmsglist.setModel(messageList());
        
        
        //Admin's
        String admin = skloggedinusrcode.getText();
        
        if (admin.contains("ADM")) {
            btnBack.setVisible(true);
        } else {
            btnBack.setVisible(false);
        }
        
        
    }
    
    private DefaultTableModel messageList(){
        //msglist.addColumn("Message Type");
        msglist.addColumn("Adm no.");
        //msglist.addColumn("Email Recipient");
        msglist.addColumn("From");
        msglist.addColumn("To");
        msglist.addColumn("Message");
        
        try {
            String sql = "SELECT * FROM `messages` WHERE `m_from`=\"Stock-keeping\" ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                //String msgtype = rs.getString("m_type");
                String padmno = rs.getString("padmno");
                String mfrom = rs.getString("m_from");
                String mto = rs.getString("m_to");
                //String msbjct = rs.getString("m_sbjct");
                String m_msg = rs.getString("m_msg");
                
                String[] rowdata = {padmno, mfrom, mto, m_msg};
                msglist.addRow(rowdata);
            }
            
            return msglist;
            
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);
                    
                    yr = year;

                    txtisdate.setText(year+"-"+(month+1)+"-"+day);
                    txtrdr.setText(year+"-"+(month+1)+"-"+day);
                    txtrplnshdate.setText(year+"-"+(month+1)+"-"+day);
                    

                    int second = cal.get(Calendar.SECOND);
                    int minute = cal.get(Calendar.MINUTE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);

                    skloggedintime.setText(hour+":"+minute+":"+second);
                    skloggedintime.setForeground(Color.red);
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException ie) {
//                        Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ie);
//                    }
                }
            }
        };
        clock.start();
   }
    
    private void clearStockFields(){
        cbostocktype.setSelectedIndex(0);
        stockid.setText("");
        stockname.setText("");
        stockvendor.setText("");
        stockreceiptno.setText("");
        stockquantity.setText("");
        stockdateofpur.setDate(null);
        stockbrand.setText("");
        stockwv.setText("");
        stockdesk.setText("");
        stockcostperitem.setText("");
    }
    
    private DefaultTableModel updateTable(){
        updatelist.addColumn("Stock Type");
        updatelist.addColumn("Stock ID");
        updatelist.addColumn("Stock Name");
        updatelist.addColumn("Brand");
        updatelist.addColumn("Vendor");
        updatelist.addColumn("Rcpt No.");
        updatelist.addColumn("Cost P.I");
        updatelist.addColumn("Qty");
        updatelist.addColumn("Replenish Date");
        updatelist.addColumn("Weight/Volume");
        updatelist.addColumn("Description");
        
        try{
            String sql = "SELECT `stock_type`, `item_id`, `item_name`, `brand`, `vendor`, `vendor_receipt_no`, `costperitem`,"
                         + " `quantity`, `date_of_purchase`, `weight_or_volume`, `description` FROM `stock_data` ";
            pst = conn.prepareStatement(sql);
            
            rs = pst.executeQuery();
            //tblothlist.setModel(DbUtils.resultSetToTableModel(rs));
            while (rs.next()) {
                String stype = rs.getString("stock_type");
                String itemid = rs.getString("item_id");
                String itemname = rs.getString("item_name");
                String brand = rs.getString("brand");
                String vendor = rs.getString("vendor");
                String vrcptno = rs.getString("vendor_receipt_no");
                String cpi = rs.getString("costperitem");
                String qty = rs.getString("quantity");
                String dop = rs.getString("date_of_purchase");
                String wv = rs.getString("weight_or_volume");
                String desc = rs.getString("description");
                
                String[] rowdata = {stype, itemid, itemname, brand, vendor, vrcptno, cpi, qty, dop, wv, desc};
                updatelist.addRow(rowdata);
            }
            
            return updatelist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;
    }
    
    private DefaultTableModel stockList(){
        stocklist.addColumn("Stock Type");
        stocklist.addColumn("Stock ID");
        stocklist.addColumn("Stock Name");
        stocklist.addColumn("Brand");
        stocklist.addColumn("Vendor");
        stocklist.addColumn("Rcpt No.");
        stocklist.addColumn("Cost P.I");
        stocklist.addColumn("Qty");
        stocklist.addColumn("Last Updated");
        stocklist.addColumn("Weight/Volume");        
        stocklist.addColumn("Description");
        
        try{
            String sql = "SELECT * FROM `stock_data` ";
            pst = conn.prepareStatement(sql);
            
            rs = pst.executeQuery();
            //tblothlist.setModel(DbUtils.resultSetToTableModel(rs));
            while (rs.next()) {
                String stype = rs.getString("stock_type");
                String itemid = rs.getString("item_id");
                String itemname = rs.getString("item_name");
                String brand = rs.getString("brand");
                String vendor = rs.getString("vendor");
                String vrcptno = rs.getString("vendor_receipt_no");
                String cpi = rs.getString("costperitem");
                String qty = rs.getString("quantity");
                String dop = rs.getString("date_of_purchase");
                String wv = rs.getString("weight_or_volume");
                String desc = rs.getString("description");
                
                String[] rowdata = {stype, itemid, itemname, brand, vendor, vrcptno, cpi, qty, dop, wv, desc};
                stocklist.addRow(rowdata);
            }
            
            return stocklist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;
    }
    
    private DefaultTableModel issueStockListItems(){
        issuestocklistitems.addColumn("Stock Type");
        issuestocklistitems.addColumn("Stock ID");
        issuestocklistitems.addColumn("Stock Name");
        issuestocklistitems.addColumn("Brand");
        issuestocklistitems.addColumn("Qty");        
        issuestocklistitems.addColumn("Description");
        
        try{
            String sql = "SELECT * FROM `stock_data` ";
            pst = conn.prepareStatement(sql);
            
            rs = pst.executeQuery();
            while (rs.next()) {
                String stype = rs.getString("stock_type");
                String itemid = rs.getString("item_id");
                String itemname = rs.getString("item_name");
                String brand = rs.getString("brand");
                String qty = rs.getString("quantity");
                String desc = rs.getString("description");
                
                String[] rowdata = {stype, itemid, itemname, brand, qty, desc};
                issuestocklistitems.addRow(rowdata);
            }
            
            return issuestocklistitems;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;
    }
    
    private DefaultTableModel issuedStockList(){
        issuedstocklist.addColumn("Stock Type");
        issuedstocklist.addColumn("Stock ID");
        issuedstocklist.addColumn("Stock Name");
        issuedstocklist.addColumn("Qty");
        issuedstocklist.addColumn("Date Acquired");
        issuedstocklist.addColumn("Return Date");
        issuedstocklist.addColumn("Custodian ID");
        issuedstocklist.addColumn("Custodian Name");
        issuedstocklist.addColumn("Department");
        issuedstocklist.addColumn("Employed As");
        
        try{
            String sql = "SELECT * FROM `issue_stock` ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();
            //tblothlist.setModel(DbUtils.resultSetToTableModel(rs));
            while (rs.next()) {
                String stype = rs.getString("stock_type");
                String itemid = rs.getString("item_id");
                String itemname = rs.getString("item_name");
                String qty = rs.getString("qty");
                String doa = rs.getString("doa");
                String dor = rs.getString("dor");
                String cid = rs.getString("custid");
                String cname = rs.getString("custname");
                String cdept = rs.getString("custdept");
                String cempas = rs.getString("custempas");
                
                String[] rowdata = {stype, itemid, itemname, qty, doa, dor, cid, cname, cdept, cempas};
                issuedstocklist.addRow(rowdata);
            }
            
            return issuedstocklist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;
    }
    
    private DefaultTableModel returnIssuedStock(){
        returnedstock.addColumn("Custodian ID");
        returnedstock.addColumn("Custodian Name");
        returnedstock.addColumn("Stock ID");
        returnedstock.addColumn("Stock Name");
        returnedstock.addColumn("Qty");
        returnedstock.addColumn("Date Acquired");
        returnedstock.addColumn("Return Date");
        
        try{
            String sql = "SELECT * FROM `issue_stock` WHERE `qty` > 0 AND `status`=\"1\" ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();
            //tblothlist.setModel(DbUtils.resultSetToTableModel(rs));
            while (rs.next()) {
                String cid = rs.getString("custid");
                String cname = rs.getString("custname");
                String itemid = rs.getString("item_id");
                String itemname = rs.getString("item_name");
                String qty = rs.getString("qty");
                String doa = rs.getString("doa");
                String dor = rs.getString("dor");
                
                String cdept = rs.getString("custdept");
                String cempas = rs.getString("custempas");
                
                String[] rowdata = {cid,cname,itemid,itemname,qty,doa,dor,cdept,cempas};
                returnedstock.addRow(rowdata);
            }
            
            return returnedstock;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnLogOut = new javax.swing.JButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        stockid = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        stockdateofpur = new com.toedter.calendar.JDateChooser();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        stockdesk = new javax.swing.JTextArea();
        cbostocktype = new javax.swing.JComboBox<>();
        jLabel22 = new javax.swing.JLabel();
        stockname = new javax.swing.JTextField();
        stockreceiptno = new javax.swing.JTextField();
        stockvendor = new javax.swing.JTextField();
        stockwv = new javax.swing.JTextField();
        stockquantity = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        stockcostperitem = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        stockbrand = new javax.swing.JTextField();
        btnUpdateClear1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        txtsearchupdatestock = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtsname = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txtitemid = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txtwv = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txtdsc = new javax.swing.JTextArea();
        txtrplnshdate = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblupdatetable = new javax.swing.JTable();
        jLabel39 = new javax.swing.JLabel();
        txtqnty = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtbrand = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtvrno = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txtcost = new javax.swing.JTextField();
        txtitemcat = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        btnUpdateClear = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        txtisitidno = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtissqty = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtisitname = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtisdate = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jdatereturn = new com.toedter.calendar.JDateChooser();
        jLabel29 = new javax.swing.JLabel();
        txtisstype = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtiscid = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtisdept = new javax.swing.JTextField();
        txtisempas = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtiscname = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtiscwid = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblissuestock = new javax.swing.JTable();
        btnIssueClear = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtrcid = new javax.swing.JTextField();
        txtritemname = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        txtrcname = new javax.swing.JTextField();
        txtrqnoi = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        txtrdiss = new javax.swing.JTextField();
        txtrdr = new javax.swing.JTextField();
        txtritemid = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        txtrnoir = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblreturnitems = new javax.swing.JTable();
        txtrsearchcid = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        btnReturnClear = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblstocklist = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtsearchstocklist = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        txtsm6 = new javax.swing.JTextArea();
        btnsmCancel = new javax.swing.JButton();
        txtsmSend = new javax.swing.JButton();
        txtsm4 = new javax.swing.JTextField();
        lblmsg = new javax.swing.JLabel();
        txtsm3 = new javax.swing.JTextField();
        jScrollPane11 = new javax.swing.JScrollPane();
        tblmsglist = new javax.swing.JTable();
        lblmsg1 = new javax.swing.JLabel();
        txtsm5search = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        btnAddNewStock = new javax.swing.JButton();
        btnUpdateBtn = new javax.swing.JButton();
        btnItemReturn = new javax.swing.JButton();
        btnXLSSheet = new javax.swing.JButton();
        btnIssueStock = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        skloggedinusrcode = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        skloggedintime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        jPanel3.setBackground(new java.awt.Color(102, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("STOCK-KEEPING DASHBOARD");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(46, 46, 46))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        btnLogOut.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLogOut.setText("LOGOUT");
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "New Stock", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Item ID");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Item Name");

        stockid.setEditable(false);
        stockid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Vendor");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Vendor Receipt No.");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Number of pieces or items");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("Date of Purchase");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Description");

        stockdesk.setColumns(20);
        stockdesk.setRows(5);
        jScrollPane2.setViewportView(stockdesk);

        cbostocktype.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cbostocktype.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Stock Type", "Consumable", "Asset" }));
        cbostocktype.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbostocktypeActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Weight/ Volume (of each consumable item)");

        stockname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stocknameKeyReleased(evt);
            }
        });

        stockreceiptno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockreceiptno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockreceiptnoKeyReleased(evt);
            }
        });

        stockvendor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockvendor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockvendorKeyReleased(evt);
            }
        });

        stockwv.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockwv.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockwvKeyReleased(evt);
            }
        });

        stockquantity.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockquantity.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        stockquantity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockquantityKeyReleased(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Cost Per Item");

        stockcostperitem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockcostperitem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockcostperitemKeyReleased(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("Brand ");

        stockbrand.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockbrand.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockbrandKeyReleased(evt);
            }
        });

        btnUpdateClear1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUpdateClear1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnUpdateClear1.setText("CLEAR");
        btnUpdateClear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateClear1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(263, 263, 263)
                .addComponent(cbostocktype, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 379, Short.MAX_VALUE))
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel13)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12)
                    .addComponent(jLabel11)
                    .addComponent(stockid)
                    .addComponent(stockname)
                    .addComponent(stockvendor)
                    .addComponent(stockreceiptno)
                    .addComponent(jLabel18)
                    .addComponent(stockcostperitem, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(stockbrand, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(stockquantity, javax.swing.GroupLayout.PREFERRED_SIZE, 399, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(69, 69, 69))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnUpdateClear1)
                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(stockdateofpur, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel15)
                                .addComponent(jLabel22)
                                .addComponent(stockwv, javax.swing.GroupLayout.PREFERRED_SIZE, 399, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel16)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addComponent(cbostocktype, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(37, 37, 37))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(stockid, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stockquantity, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(13, 13, 13)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockname, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockdateofpur, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockwv, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockbrand, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockvendor, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockreceiptno, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(stockcostperitem, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdateClear1))))
                .addGap(111, 111, 111))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 44, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 73, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("New Stock", jPanel6);

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Update Stock", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 0, 0));
        jLabel30.setText("Search ");

        txtsearchupdatestock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsearchupdatestock.setToolTipText("Type item's name");
        txtsearchupdatestock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsearchupdatestockKeyReleased(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setText("Item Name");

        txtsname.setEditable(false);
        txtsname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsnameKeyReleased(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel32.setText("Stock type");

        txtitemid.setEditable(false);
        txtitemid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setText("Weight/ Volume of each item");

        txtwv.setEditable(false);
        txtwv.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("Replenish Date");

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setText("Description");

        txtdsc.setEditable(false);
        txtdsc.setColumns(20);
        txtdsc.setRows(5);
        jScrollPane8.setViewportView(txtdsc);

        txtrplnshdate.setEditable(false);
        txtrplnshdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tblupdatetable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblupdatetable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblupdatetableMouseClicked(evt);
            }
        });
        tblupdatetable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblupdatetableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblupdatetable);

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel39.setText("Quantity");

        txtqnty.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtqnty.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtqnty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtqntyKeyReleased(evt);
            }
        });

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel40.setText("Brand");

        txtbrand.setEditable(false);
        txtbrand.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel36.setText("Vendor Receipt No.");

        txtvrno.setEditable(false);
        txtvrno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel37.setText("Cost per Item");

        txtcost.setEditable(false);
        txtcost.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtcost.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcostKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcostKeyReleased(evt);
            }
        });

        txtitemcat.setEditable(false);
        txtitemcat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel38.setText("Item ID");

        btnUpdateClear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUpdateClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnUpdateClear.setText("CLEAR");
        btnUpdateClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtitemcat, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtitemid, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel39, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel40, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtbrand, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtsname, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtcost, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtvrno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtqnty, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel38)
                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtrplnshdate)
                                    .addComponent(jLabel34)
                                    .addComponent(jLabel33)
                                    .addComponent(txtwv, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                                    .addGroup(jPanel16Layout.createSequentialGroup()
                                        .addComponent(jLabel35)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addGap(66, 66, 66))
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 677, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                                        .addComponent(btnUpdateClear)
                                        .addGap(12, 12, 12)))
                                .addContainerGap())))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsearchupdatestock, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel32)
                        .addGap(10, 10, 10)
                        .addComponent(txtitemcat, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel38)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtitemid, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel31))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel33)
                            .addComponent(jLabel35))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(txtwv, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel34)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtrplnshdate, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)))
                .addGap(2, 2, 2)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtsearchupdatestock, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnUpdateClear))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(txtsname, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel40)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbrand, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel39)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtqnty, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtvrno, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcost, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 26, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Update Stock", jPanel5);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Issue Stock", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        txtisitidno.setEditable(false);
        txtisitidno.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Item ID ");

        txtissqty.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtissqty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtissqtyKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Qty/ No. of Items");

        txtisitname.setEditable(false);
        txtisitname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtisitname.setToolTipText("Type item's name");
        txtisitname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtisitnameKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Date of Acquisition");

        txtisdate.setEditable(false);
        txtisdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Item Name");

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel28.setText("Date of return (for assets only)");

        jdatereturn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jdatereturnMouseClicked(evt);
            }
        });
        jdatereturn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jdatereturnKeyPressed(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel29.setText("Stock Type");

        txtisstype.setEditable(false);
        txtisstype.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(txtisdate, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                        .addComponent(jLabel28)
                                        .addGap(19, 19, 19))
                                    .addComponent(jdatereturn, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(172, 172, 172))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(txtisitidno, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                                        .addGap(33, 33, 33)))
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(txtissqty, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtisitname)))
                .addContainerGap())
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtisstype, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtisitname, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtisstype, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtisitidno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtissqty, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtisdate)
                    .addComponent(jdatereturn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Item Acquirer", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Custodian ID ");

        txtiscid.setEditable(false);
        txtiscid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("Department");

        txtisdept.setEditable(false);
        txtisdept.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtisempas.setEditable(false);
        txtisempas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setText("Employed As");

        txtiscname.setEditable(false);
        txtiscname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel26.setText("Custodian Name");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setText("Custodian Work ID");

        txtiscwid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtiscwid.setToolTipText("Type custodian's work ID");
        txtiscwid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtiscwidKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtiscid)
                    .addComponent(txtisdept, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtisempas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtiscname, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                            .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(135, 135, 135))))
                .addContainerGap())
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtiscwid, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtiscwid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtisempas, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtiscid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtiscname, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtisdept, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        tblissuestock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblissuestock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblissuestockMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblissuestock);

        btnIssueClear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnIssueClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnIssueClear.setText("CLEAR");
        btnIssueClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIssueClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnIssueClear)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnIssueClear)
                .addGap(22, 22, 22))
        );

        jTabbedPane2.addTab("Issue Stock", jPanel4);

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Return Stock Items", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Items Issued", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Custodian Work ID");

        txtrcid.setEditable(false);
        txtrcid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtritemname.setEditable(false);
        txtritemname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Item Name");

        txtrcname.setEditable(false);
        txtrcname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtrqnoi.setEditable(false);
        txtrqnoi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtrqnoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtrqnoiActionPerformed(evt);
            }
        });

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel42.setText("Name");

        jLabel43.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel43.setText("Quantity/ No. of Items");

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel45.setText("Return Date");

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel46.setText("Date Issued");

        txtrdiss.setEditable(false);
        txtrdiss.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtrdr.setEditable(false);
        txtrdr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtritemid.setEditable(false);
        txtritemid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel48.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel48.setText("Item ID");

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel49.setText("No. of Items Returned");

        txtrnoir.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtrnoir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtrnoirKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrnoirKeyReleased(evt);
            }
        });

        tblreturnitems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblreturnitems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblreturnitemsMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tblreturnitems);

        txtrsearchcid.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtrsearchcid.setToolTipText("Type custodian's work ID");
        txtrsearchcid.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtrsearchcid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrsearchcidKeyReleased(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(255, 0, 0));
        jLabel47.setText("Search ");

        btnReturnClear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnReturnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clear.png"))); // NOI18N
        btnReturnClear.setText("CLEAR");
        btnReturnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtrcname, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtrcid, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel42)
                        .addComponent(jLabel48)
                        .addComponent(jLabel41)
                        .addComponent(jLabel43)
                        .addComponent(txtritemid, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                        .addComponent(txtritemname)
                        .addComponent(txtrqnoi))
                    .addComponent(jLabel46)
                    .addComponent(jLabel45)
                    .addComponent(txtrdr, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtrdiss, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel49)
                                    .addComponent(txtrnoir, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addGap(78, 78, 78)
                                .addComponent(jLabel47)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtrsearchcid, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 154, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnReturnClear))
                            .addComponent(jScrollPane5))))
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrcid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel49)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrnoir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel42)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel47)
                        .addComponent(txtrsearchcid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtrcname, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel48)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtritemid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtritemname, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel43)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrqnoi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel46)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrdiss, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrdr, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addComponent(btnReturnClear)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Return Items", jPanel17);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock List", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        tblstocklist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(tblstocklist);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setText("Search Stock Name");

        txtsearchstocklist.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtsearchstocklist.setToolTipText("Type the type of stock");
        txtsearchstocklist.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsearchstocklistKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 942, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(132, 132, 132)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtsearchstocklist, javax.swing.GroupLayout.PREFERRED_SIZE, 481, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtsearchstocklist, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44))
        );

        jTabbedPane2.addTab("Stock List", jPanel15);

        jPanel34.setBackground(new java.awt.Color(153, 153, 255));
        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Send Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setText("To");

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Message");

        txtsm6.setColumns(20);
        txtsm6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm6.setRows(5);
        jScrollPane10.setViewportView(txtsm6);

        btnsmCancel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnsmCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/cancel.png"))); // NOI18N
        btnsmCancel.setText("Cancel");
        btnsmCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsmCancelActionPerformed(evt);
            }
        });

        txtsmSend.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsmSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/send.png"))); // NOI18N
        txtsmSend.setText("SEND");
        txtsmSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsmSendActionPerformed(evt);
            }
        });

        txtsm4.setEditable(false);
        txtsm4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm4KeyReleased(evt);
            }
        });

        lblmsg.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg.setText("Name");

        txtsm3.setEditable(false);
        txtsm3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm3KeyReleased(evt);
            }
        });

        tblmsglist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane11.setViewportView(tblmsglist);

        lblmsg1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg1.setText("Search Custodian");

        txtsm5search.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm5search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm5searchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnsmCancel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(105, 105, 105))
                            .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel34Layout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblmsg)
                                        .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtsm4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel19)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel34Layout.createSequentialGroup()
                                        .addGap(237, 237, 237)
                                        .addComponent(jLabel20)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(lblmsg1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtsm5search, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsm5search, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblmsg1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(lblmsg, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsmCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(238, Short.MAX_VALUE)
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(147, 147, 147))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 32, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Send Message", jPanel12);

        jPanel7.setBackground(new java.awt.Color(102, 255, 204));

        btnAddNewStock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAddNewStock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/new_stock.png"))); // NOI18N
        btnAddNewStock.setText("ADD NEW");
        btnAddNewStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewStockActionPerformed(evt);
            }
        });

        btnUpdateBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUpdateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnUpdateBtn.setText("UPDATE");
        btnUpdateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateBtnActionPerformed(evt);
            }
        });

        btnItemReturn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnItemReturn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/return.png"))); // NOI18N
        btnItemReturn.setText("RETURN");
        btnItemReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnItemReturnActionPerformed(evt);
            }
        });

        btnXLSSheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnXLSSheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnXLSSheet.setText("Print Inventory List ");
        btnXLSSheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXLSSheetActionPerformed(evt);
            }
        });

        btnIssueStock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnIssueStock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/issue.png"))); // NOI18N
        btnIssueStock.setText("ISSUE STOCK");
        btnIssueStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIssueStockActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnItemReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnUpdateBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAddNewStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnIssueStock, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(19, 19, 19))
                    .addComponent(btnXLSSheet, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(btnAddNewStock, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(btnUpdateBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addComponent(btnIssueStock, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(btnItemReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(btnXLSSheet, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(129, 129, 129))
        );

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBack.setText("BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnLogOut))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1013, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnLogOut, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        skloggedinusrcode.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        skloggedinusrcode.setText("currently logged in usercode");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Logged in as:");

        skloggedintime.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        skloggedintime.setText("time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(skloggedinusrcode)
                        .addGap(18, 18, 18)
                        .addComponent(skloggedintime, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(skloggedinusrcode)
                    .addComponent(skloggedintime))
                .addGap(40, 40, 40))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setSize(new java.awt.Dimension(1349, 756));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddNewStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewStockActionPerformed
        // TODO add your handling code here:
        try {
            String uuid = UUID.randomUUID().toString();
            String stype = (String) cbostocktype.getSelectedItem();
            String sid = stockid.getText();
            String sname = stockname.getText();
            String brand = stockbrand.getText();
            String svendor = stockvendor.getText();
            String srcptno = stockreceiptno.getText();
            String scost = stockcostperitem.getText();
            String sqty = stockquantity.getText();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String sdop = sdf.format(stockdateofpur.getDate());
            String swv = stockwv.getText();
            String sdesc = stockdesk.getText();
            
            if (cbostocktype.getSelectedIndex() != 0 && !sid.isEmpty() && !sname.isEmpty() && !svendor.isEmpty() && !srcptno.isEmpty() 
                    && !sqty.isEmpty() && !sdop.isEmpty() && !sdesc.isEmpty() && !scost.isEmpty() && !brand.isEmpty() ) {
                 String sql = "INSERT INTO `stock_data`(`uuid`, `stock_type`, `item_id`, `item_name`, `brand`, `vendor`, `vendor_receipt_no`, `costperitem`,"
                         + " `quantity`, `date_of_purchase`, `weight_or_volume`, `description`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";
                 pst = conn.prepareStatement(sql);
                 pst.setString(1, uuid);
                 pst.setString(2, stype);
                 pst.setString(3, sid);
                 pst.setString(4, sname.toUpperCase());
                 pst.setString(5, brand);
                 pst.setString(6, svendor);
                 pst.setString(7, srcptno);
                 pst.setString(8, scost);
                 pst.setString(9, sqty);
                 pst.setString(10, sdop);
                 pst.setString(11, swv);
                 pst.setString(12, sdesc);
                 
                 pst.execute();
                 
                 JOptionPane.showMessageDialog(null, "Stock item added.");
                 
                DefaultTableModel model = (DefaultTableModel)tblstocklist.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                stockList();
                 
                 clearStockFields();
                 
                DefaultTableModel updtemodel = (DefaultTableModel)tblupdatetable.getModel();
                updtemodel.setColumnCount(0);
                updtemodel.setRowCount(0);
                updateTable();
                
                DefaultTableModel ismodel = (DefaultTableModel)tblissuestock.getModel();
                ismodel.setColumnCount(0);
                ismodel.setRowCount(0);
                tblissuestock.setModel(issueStockListItems());
                 
            } else {
                JOptionPane.showMessageDialog(null, "Please fill all the fields as required.");
            }
            
        } catch (HeadlessException | SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }

//        int count = 0;
//        int batchSize = Integer.parseInt(stockquantity.getText());
//        int noi = Integer.parseInt(stockquantity.getText());
//        
//            try {
//                
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                    
//                String sql = "INSERT INTO `stock_data`(`uuid`, `stock_type`, `item_id`, `item_name`, `brand`, `vendor`, `vendor_receipt_no`, `costperitem`,"
//                         + " `date_of_purchase`, `weight_or_volume`, `description`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ";
//                 conn.setAutoCommit(false);
//                 pst = conn.prepareStatement(sql);
//
//                 for (int i = 0; i <= noi; i++) {
//                    stockTypeID();
//                    
//                    pst.setString(1, UUID.randomUUID().toString());
//                    pst.setString(2, (String) cbostocktype.getSelectedItem());
//                    pst.setString(3, stockid.getText());
//                    pst.setString(4, stockname.getText().toUpperCase());
//                    pst.setString(5, stockbrand.getText());
//                    pst.setString(6, stockvendor.getText());
//                    pst.setString(7, stockreceiptno.getText());
//                    pst.setString(8, stockcostperitem.getText());
//                    //pst.setString(9, stockquantity.getText());
//                    pst.setString(9, sdf.format(stockdateofpur.getDate()));
//                    pst.setString(10, stockwv.getText());
//                    pst.setString(11, stockdesk.getText());
//
//                    pst.addBatch();
//
//                    count ++;
//
//                     if (count % batchSize == 0) {
//                         System.out.println("Commit the batch.");
//                         int[] rows = pst.executeBatch();
//                         System.out.println("No. of rows inserted: "+rows.length);
//                         conn.commit();
//                     }
//                }
//
//                    //JOptionPane.showMessageDialog(null, "Stock item added, Round "+i);
//
//                    DefaultTableModel model = (DefaultTableModel)tblstocklist.getModel();
//                    model.setColumnCount(0);
//                    model.setRowCount(0);
//                    stockList();
//
//                    clearStockFields();
//
//                    DefaultTableModel updtemodel = (DefaultTableModel)tblupdatetable.getModel();
//                    updtemodel.setColumnCount(0);
//                    updtemodel.setRowCount(0);
//                    updateTable();
//                    
//            } catch (HeadlessException | SQLException e) {
//                try {
//                    Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
//                    conn.rollback();
//                    System.out.println("Rollbacking changes...");
//                } catch (SQLException ex) {
//                    Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
    }//GEN-LAST:event_btnAddNewStockActionPerformed

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        // TODO add your handling code here:
        try {                                           
            // TODO add your handling code here:
            try {
                String asql = "INSERT INTO `auditlog`(`empid`, `date`, `time`, `status`) VALUES (?,?,?,?)";
                pst = conn.prepareStatement(asql);
                Date currentDate = GregorianCalendar.getInstance().getTime();
                DateFormat df = DateFormat.getInstance();
                String dateString = df.format(currentDate);
                
                Date d = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String timeString = sdf.format(d);
                
                String time = timeString;
                String date = dateString;
                pst.setString(1, Emp.empId);
                pst.setString(2, date);
                pst.setString(3, time);
                pst.setString(4, "Logged out");
                pst.execute();
                
            } catch (SQLException e) {
            }
            
            this.dispose();
            
            Login l = new Login();
            l.setVisible(true);
            
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnLogOutActionPerformed
    
    private void getQuantity(){
        try {
            //Getting the total materials in the collection
            String cpquery = "SELECT `quantity` FROM `stock_data` WHERE `item_id`=? ";
            pst = conn.prepareStatement(cpquery);
            pst.setString(1, txtitemid.getText());
            rs = pst.executeQuery();

            while (rs.next()) {                        
                String copies = rs.getString("quantity");
                qt = Integer.parseInt(copies);
            }
        } catch (NumberFormatException | SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void btnUpdateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateBtnActionPerformed
        // TODO add your handling code here:
//        getQuantity();
//        int nitems = Integer.parseInt(txtqnty.getText());
//        int newqty = qt + nitems;

        int nitems = Integer.parseInt(txtqnty.getText());
        
        try {
            //Updating stock remainder after new update
            String updatecoll = "UPDATE `stock_data` SET `item_name`=?, `quantity`=?, `vendor_receipt_no`=?,`costperitem`=?, "
                    + " `weight_or_volume`=?, `date_of_purchase`=?, `description`=? WHERE `item_id`='"+txtitemid.getText()+"' ";
            pst = conn.prepareStatement(updatecoll);
            
            pst.setString(1, txtsname.getText());
            pst.setInt(2, nitems);            
            pst.setString(3, txtvrno.getText());
            pst.setString(4, txtcost.getText());
            pst.setString(5, txtwv.getText());
            pst.setString(6, txtrplnshdate.getText());
            pst.setString(7, txtdsc.getText());            

            pst.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Stock updated. ");
            //System.out.println("Stock updated.");
            
            issueUpdateTables();
            
            clearUpdateStock();
            
        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }//GEN-LAST:event_btnUpdateBtnActionPerformed

    private String getCellValue(int x, int y){
        return tblstocklist.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btnXLSSheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXLSSheetActionPerformed
        // TODO add your handling code here:
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{tblstocklist.getColumnName(0),tblstocklist.getColumnName(1),tblstocklist.getColumnName(2), tblstocklist.getColumnName(3),
            tblstocklist.getColumnName(4), tblstocklist.getColumnName(5),tblstocklist.getColumnName(6),tblstocklist.getColumnName(7), tblstocklist.getColumnName(8),
            tblstocklist.getColumnName(9),tblstocklist.getColumnName(10) });
        
        //Looping through the table rows
        for (int i = 0; i < tblstocklist.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getCellValue(i, 0),getCellValue(i, 1),getCellValue(i, 2),getCellValue(i, 3),getCellValue(i, 4),
            getCellValue(i, 5),getCellValue(i, 6),getCellValue(i, 7),getCellValue(i, 8),getCellValue(i, 9),getCellValue(i, 10) });
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Stock list.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btnXLSSheetActionPerformed

    private void txtsearchstocklistKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsearchstocklistKeyReleased
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) tblstocklist.getModel();
        String stocklistsearch = txtsearchstocklist.getText().toUpperCase();
        TableRowSorter<DefaultTableModel> pl = new TableRowSorter<>(table);
        tblstocklist.setRowSorter(pl);
        pl.setRowFilter(RowFilter.regexFilter(stocklistsearch));
    }//GEN-LAST:event_txtsearchstocklistKeyReleased

    private void txtsm3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm3KeyReleased
        // TODO add your handling code here:
        //getSMSContact();
    }//GEN-LAST:event_txtsm3KeyReleased

    private void txtsmSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsmSendActionPerformed
        // TODO add your handling code here:
        //        if (rb1.isSelected()) {
            //            sendSMS();
            //        }else if(rb2.isSelected()){
            //            try {
                //                sendEmail();
                //            } catch (MessagingException ex) {
                //                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
                //            }
            //        }else{
            //            JOptionPane.showMessageDialog(null, "Please select message type");
            //        }
            
            sendSMS();
    }//GEN-LAST:event_txtsmSendActionPerformed

    private void sendSMS(){
        String bulksmsusername = System.getenv("BULKSMSUSRNAME");
        String bulksmspssd = System.getenv("BULKSMSPSSD");
        String msg = txtsm6.getText();
        String phoneno = txtsm4.getText();
//        if (rb1.isSelected()) {
//            txtsm3.setEditable(true);
//            
//            String msgtype = "";
//            if (rb1.isSelected()) {
//                msgtype = rb1.getText();
//            }
            String padmno = txtsm3.getText();
            String mrecepient = txtsm4.getText();
            String mssg = txtsm6.getText();

            if (!padmno.isEmpty() && !mrecepient.isEmpty() && !mssg.isEmpty()) {
            
                getSMSContact();
                SMS send = new SMS();
                send.SendSMS(bulksmsusername, bulksmspssd, msg, phoneno, "https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0");

                JOptionPane.showMessageDialog(null, "Message sent");
                
                
                try {
                    String mfrom = "Stock-keeping";
                
                    String sql = "INSERT INTO `messages`(`padmno`, `m_from`, `m_to`, `m_msg`) VALUES (?,?,?)";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, padmno);
                    pst.setString(2, mfrom);
                    pst.setString(3, mrecepient);
                    pst.setString(4, mssg);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Message saved.");

                    //rb1.setSelected(false);
                    
                    messageList();
                
                } catch (SQLException e) {
                    Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
                }
            
            }else{
                    JOptionPane.showMessageDialog(null, "Please ensure all the fields are filled.");
            }           
            
    }
     
     private void getSMSContact(){
        String msgadmno = txtsm3.getText();
            
            try {
            String sql = "SELECT `ecpc_phone` FROM `admissions` WHERE `admno`=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, msgadmno);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                String contact = rs.getString("ecpc_phone");
                txtsm4.setText(contact);
            }
            
            } catch (SQLException e) {
                Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
            }
    }
    
    private void tblupdatetableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblupdatetableKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tblupdatetableKeyReleased

    private void tblupdatetableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblupdatetableMouseClicked
        // TODO add your handling code here:
        JTable table = (JTable) evt.getSource();
        int selectedRow = table.getSelectedRow();
        int selectedColumn = table.getSelectedColumn();
        String selectedCellValue = (String) table.getValueAt(selectedRow, selectedColumn);

        try {
            String sql = "SELECT * FROM `stock_data` WHERE `item_id` = ? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, selectedCellValue);
            rs = pst.executeQuery();

            while (rs.next()) {
                txtitemcat.setText(rs.getString("stock_type"));
                txtitemid.setText(rs.getString("item_id"));
                txtsname.setText(rs.getString("item_name"));
                txtbrand.setText(rs.getString("brand"));
                txtqnty.setText(String.valueOf(rs.getInt("quantity")));
                txtvrno.setText(rs.getString("vendor_receipt_no"));
                txtcost.setText(rs.getString("costperitem"));
                txtwv.setText(rs.getString("weight_or_volume"));
                txtrplnshdate.setText(rs.getString("date_of_purchase"));
                txtdsc.setText(rs.getString("description"));
            }

        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }

    }//GEN-LAST:event_tblupdatetableMouseClicked

    private void txtsearchupdatestockKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsearchupdatestockKeyReleased
        // TODO add your handling code here:
        if (txtsearchupdatestock.getText().isEmpty()) {
            clearUpdateStock();
        } else {
            DefaultTableModel table = (DefaultTableModel) tblupdatetable.getModel();
            String stockupdatesearch = txtsearchupdatestock.getText().toUpperCase();
            TableRowSorter<DefaultTableModel> pl = new TableRowSorter<>(table);
            tblupdatetable.setRowSorter(pl);
            pl.setRowFilter(RowFilter.regexFilter(stockupdatesearch));
        }

    }//GEN-LAST:event_txtsearchupdatestockKeyReleased

    private void cbostocktypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbostocktypeActionPerformed
        // TODO add your handling code here:
        int type = cbostocktype.getSelectedIndex();

        switch (type) {
            case 1:
            {
                int num1;
                SecureRandom sr = new SecureRandom();
                int sr1 = sr.nextInt(1000);
                String consid = "C/"+yr+"/ND/";
                num1 = 59 * sr1 + (int) (Math.random()*78989);
                consid += num1 + 5;
                stockid.setText(consid);
                break;
            }
            case 2:
            {
                int num1;
                SecureRandom sr = new SecureRandom();
                int sr1 = sr.nextInt(1000);
                String nconsid = "AS/"+yr+"/ND/";
                num1 = 99 * sr1 + (int) (Math.random()*98989);
                nconsid += num1 + 5;
                stockid.setText(nconsid);
                break;
            }
            default:
            stockid.setText("");
            break;
        }
    }//GEN-LAST:event_cbostocktypeActionPerformed
    
    private void txtisitnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtisitnameKeyReleased
        // TODO add your handling code here:
        try {
            if (txtisitname.getText().isEmpty()) {
                clearIssueStock();
            } else {
                String sql = "SELECT `stock_type`, `item_id` FROM `stock_data` WHERE `item_name`=? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtisitname.getText());
                rs = pst.executeQuery();

                while (rs.next()) {                
                    txtisstype.setText(rs.getString("stock_type"));
                    txtisitidno.setText(rs.getString("item_id"));
                }
            }                        
        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_txtisitnameKeyReleased

    private void txtissqtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtissqtyKeyReleased
        // TODO add your handling code here:
        try {
            int qty = Integer.parseInt(txtissqty.getText());
        
            if (!txtissqty.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                txtissqty.setText("");
            }else if(qty < 0){
                JOptionPane.showMessageDialog(null, "Invalid stock quantity value!");
                txtissqty.setText("");
            }else{            
                getTotalQuantityFromStockCollection();
                int issqty = Integer.parseInt(txtissqty.getText());

                if (issqty > issqt) {
                    JOptionPane.showMessageDialog(null, "Sorry. You've exceeded the number of available items. There are only "+issqt+" items in stock.");
                    clearIssueStock();
                } else if(issqt <= 0){
                    JOptionPane.showMessageDialog(null, "Sorry. Items out of stock.");
                    clearIssueStock();
                }else{
                    System.out.println("All good.");
                }
            } 
            
        } catch (HeadlessException | NumberFormatException e) {
             JOptionPane.showMessageDialog(null, "Invalid stock value.");
             txtissqty.setText("");
        }
          
        
    }//GEN-LAST:event_txtissqtyKeyReleased

    private void getTotalQuantityFromStockCollection(){
        try {
            //Getting the total materials in the collection
            String cpquery = "SELECT `quantity` FROM `stock_data` WHERE `item_id`=? ";
            pst = conn.prepareStatement(cpquery);
            pst.setString(1, txtisitidno.getText());
            rs = pst.executeQuery();

            while (rs.next()) {                        
                String qnty = rs.getString("quantity");
                issqt = Integer.parseInt(qnty);
            }
            //JOptionPane.showMessageDialog(null, issqt);
        } catch (NumberFormatException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            //Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void updateStockAfterIssuing(){
        try {
            //Updating material remainder after borrowing
            String updatecoll = "UPDATE `stock_data` SET `quantity`=? WHERE `item_id`='"+txtisitidno.getText()+"' ";
            pst = conn.prepareStatement(updatecoll);
            
            int issued = Integer.parseInt(txtissqty.getText());

            int rmngmat = issqt - issued;

            pst.setInt(1, rmngmat);

            pst.executeUpdate();
            
            //JOptionPane.showMessageDialog(null, "Stock data table updated. ");
            //System.out.println("Collection updated.");
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            //Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
   
    private void issueUpdateTables(){
        DefaultTableModel ismodel = (DefaultTableModel)tblissuestock.getModel();
        ismodel.setColumnCount(0);
        ismodel.setRowCount(0);
        tblissuestock.setModel(issueStockListItems());

        DefaultTableModel model = (DefaultTableModel)tblupdatetable.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        tblupdatetable.setModel(updateTable());

        DefaultTableModel stockmodel = (DefaultTableModel)tblstocklist.getModel();
        stockmodel.setColumnCount(0);
        stockmodel.setRowCount(0);
        tblupdatetable.setModel(stockList());
        
        DefaultTableModel rmodel = (DefaultTableModel)tblreturnitems.getModel();
        rmodel.setColumnCount(0);
        rmodel.setRowCount(0);
        tblreturnitems.setModel(returnIssuedStock());
    }
    
    private void btnIssueStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIssueStockActionPerformed
        // TODO add your handling code here:
        issueStockIDChecker();
        String curritemid = txtisitidno.getText();
        String currcustid = txtiscid.getText();
        String stype = txtisstype.getText();
        
        if (curritemid.equals(dbitemid) && currcustid.equals(dbcustid)) {
            //JOptionPane.showMessageDialog(null, "Equal");
            
            //Updating Issued stock
            getQuantityFromIssueStock();
            updateIssuedStock();
            getTotalQuantityFromStockCollection();
            updateStockAfterIssuing();
            
            //Update all the tables
            issueUpdateTables();
            
            clearIssueStock();
            
        } else {
            if (stype.equals("Asset")) {
                String uuid = UUID.randomUUID().toString();
                    if (!txtissqty.getText().isEmpty() && jdatereturn.getDate() != null && !txtiscid.getText().isEmpty()) {

                        try {

                                String sql = "INSERT INTO `issue_stock`(`uuid`, `stock_type`, `item_id`, `item_name`, `qty`, `doa`, `dor`, `custid`, "
                                        + "`custname`, `custdept`, `custempas`, `status`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

                                pst = conn.prepareStatement(sql);
                                pst.setString(1, uuid);
                                pst.setString(2, txtisstype.getText());
                                pst.setString(3, txtisitidno.getText());
                                pst.setString(4, txtisitname.getText().toUpperCase());
                                pst.setString(5, txtissqty.getText());
                                pst.setString(6, txtisdate.getText());

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                String isdop = sdf.format(jdatereturn.getDate());
                                pst.setString(7, isdop);

                                pst.setString(8, txtiscid.getText());
                                pst.setString(9, txtiscname.getText());
                                pst.setString(10, txtisdept.getText());
                                pst.setString(11, txtisempas.getText());
                                int status = 1;
                                pst.setInt(12, status);

                                pst.execute();

                                JOptionPane.showMessageDialog(null, "Stock Issued.");

                                getTotalQuantityFromStockCollection();
                                updateStockAfterIssuing();

                                //Update all the tables
                                issueUpdateTables();

                                clearIssueStock();

                      } catch (SQLException e) {
                        Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
                      }

                } else {
                        JOptionPane.showMessageDialog(null, "Please ensure all the fields have been filled as required. ");
                }
                    
            } else {
                String uuid = UUID.randomUUID().toString();
                    if (!txtissqty.getText().isEmpty() && !txtiscid.getText().isEmpty()) {

                        try {

                                String sql = "INSERT INTO `issue_stock`(`uuid`, `stock_type`, `item_id`, `item_name`, `qty`, `doa`, `custid`, "
                                        + "`custname`, `custdept`, `custempas`, `status` ) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

                                pst = conn.prepareStatement(sql);
                                pst.setString(1, uuid);
                                pst.setString(2, txtisstype.getText());
                                pst.setString(3, txtisitidno.getText());
                                pst.setString(4, txtisitname.getText().toUpperCase());
                                pst.setString(5, txtissqty.getText());
                                pst.setString(6, txtisdate.getText());
                                pst.setString(7, txtiscid.getText());
                                pst.setString(8, txtiscname.getText());
                                pst.setString(9, txtisdept.getText());
                                pst.setString(10, txtisempas.getText());
                                int status = 1;
                                pst.setInt(11, status);

                                pst.execute();

                                JOptionPane.showMessageDialog(null, "Stock Issued.");

                                getTotalQuantityFromStockCollection();
                                updateStockAfterIssuing();

                                //Update all the tables
                                issueUpdateTables();

                                clearIssueStock();

                      } catch (SQLException e) {
                        Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
                      }

                } else {
                        JOptionPane.showMessageDialog(null, "Please ensure all the fields have been filled as required. ");
                }
            }
            
            
        }       
        
    }//GEN-LAST:event_btnIssueStockActionPerformed

    private void clearIssueStock(){
        txtisstype.setText("");
        txtisitidno.setText("");
        txtisitname.setText("");
        txtissqty.setText("");
        txtisdate.setText("");
        jdatereturn.setDate(null);
        txtiscid.setText("");
        txtiscname.setText("");
        txtisdept.setText("");
        txtisempas.setText("");
        txtiscwid.setText("");
    }
    
    private void getQuantityFromIssueStock(){
        try {
            //Getting the total materials in the collection
            String cpquery = "SELECT `qty` FROM `issue_stock` WHERE `item_id`=? AND `custid`=? AND `status`=\"1\" ";
            pst = conn.prepareStatement(cpquery);
            pst.setString(1, txtisitidno.getText());
            pst.setString(2, txtiscid.getText());
            rs = pst.executeQuery();

            while (rs.next()) {                        
                String copies = rs.getString("qty");
                cissqt = Integer.parseInt(copies);
            }
        } catch (NumberFormatException | SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void updateIssuedStock(){
        try {
            //Updating material remainder after borrowing
            String updatecoll = "UPDATE `issue_stock` SET `qty`=? WHERE `item_id`='"+txtisitidno.getText()+"' AND `custid`='"+txtiscid.getText()+"'"
                    + " AND `status`=\"1\" ";
            pst = conn.prepareStatement(updatecoll);
            
            int additionalissued = Integer.parseInt(txtissqty.getText());

            int newcustidqty = cissqt + additionalissued;

            pst.setInt(1, newcustidqty);

            pst.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Stock Issued. ");
            //System.out.println("Collection updated.");
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            //Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void issueStockIDChecker(){
        try {
            String sql = "SELECT `item_id`, `custid` FROM `issue_stock` WHERE `status`=\"1\" ";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                dbitemid = rs.getString("item_id");
                dbcustid  = rs.getString("custid");
            }
        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void txtiscwidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtiscwidKeyReleased
        // TODO add your handling code here:
        if (txtiscwid.getText().isEmpty()) {
            txtiscid.setText("");
            txtiscname.setText("");
            txtisdept.setText("");
            txtisempas.setText("");
            
        } else {
            try {
                String sql = "SELECT * FROM `otherstaff` WHERE `e_work_id`=? AND `status`=\"1\" ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtiscwid.getText().toUpperCase());
                rs = pst.executeQuery();

                while (rs.next()) {
                    txtiscid.setText(rs.getString("e_work_id"));
                    txtiscname.setText(rs.getString("ename"));
                    txtisdept.setText(rs.getString("e_dept"));
                    txtisempas.setText(rs.getString("e_employed_as"));
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Ops! Person not found");
                Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }//GEN-LAST:event_txtiscwidKeyReleased

    private void txtrqnoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtrqnoiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtrqnoiActionPerformed

    private void getQuantityCountFromIssuedStock(){
        try {
            //Getting the total materials in the collection
            String cpquery = "SELECT `qty` FROM `issue_stock` WHERE `item_id`=? AND `custid`=? AND `status`=\"1\" ";
            pst = conn.prepareStatement(cpquery);
            pst.setString(1, txtritemid.getText());
            pst.setString(2, txtrcid.getText());
            rs = pst.executeQuery();

            while (rs.next()) {                        
                String qnty = rs.getString("qty");
                issuedqt = Integer.parseInt(qnty);
            }
            //JOptionPane.showMessageDialog(null, "Quantity from issued stock "+issuedqt);
        } catch (NumberFormatException | SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void updateIssuedStockAfterReturning(){
        try {
            //Updating material remainder after borrowing
            String updatecoll = "UPDATE `issue_stock` SET `qty`=? WHERE `item_id`='"+txtritemid.getText()+"' AND `custid`='"+txtrcid.getText()+"' ";
            pst = conn.prepareStatement(updatecoll);
            
            int noireturned = Integer.parseInt(txtrnoir.getText());

            int issuedremainingqty = issuedqt - noireturned;

            pst.setInt(1, issuedremainingqty);

            pst.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Item returned.");
            //JOptionPane.showMessageDialog(null, "Issued Stock data table updated. (Subtraction) ");
            //System.out.println("Collection updated.");
            
            if (issuedremainingqty == 0) {
                
               String updatestat = "UPDATE `issue_stock` SET `status`=\"-1\" WHERE `item_id`='"+txtritemid.getText()+"' AND `custid`='"+txtrcid.getText()+"' ";
               pst = conn.prepareStatement(updatestat);
               pst.executeUpdate();
               JOptionPane.showMessageDialog(null, "Issue stock status updated.");
                
            } else {
                System.out.println("All good.");
            }
            
        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void updateStockCollectionAfterReturning(){
        try {
            //Updating material remainder after borrowing
            String updatecoll = "UPDATE `stock_data` SET `quantity`=? WHERE `item_id`='"+txtritemid.getText()+"' ";
            pst = conn.prepareStatement(updatecoll);
            
            int newstockqty = Integer.parseInt(txtrnoir.getText());

            int stockcollectionnewqty = currstckqty + newstockqty;

            pst.setInt(1, stockcollectionnewqty);

            pst.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Item returned. ");
            //System.out.println("Collection updated.");
            
        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void getCurrentStockQuantity(){
        try {
            //Getting the total materials in the collection
            String cpquery = "SELECT `quantity` FROM `stock_data` WHERE `item_id`=? ";
            pst = conn.prepareStatement(cpquery);
            pst.setString(1, txtritemid.getText());
            rs = pst.executeQuery();

            while (rs.next()) {                        
                String qnty = rs.getString("quantity");
                currstckqty = Integer.parseInt(qnty);
            }
            JOptionPane.showMessageDialog(null, currstckqty);
        } catch (NumberFormatException | SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    
    private void checkStockType(){
        int selectedRow = tblreturnitems.getSelectedRow();
        int selectedColumn = tblreturnitems.getSelectedColumn();
        String selectedCellValue = (String) tblreturnitems.getValueAt(selectedRow, selectedColumn);
        String stype = "";
        
            try {
                String sql = "SELECT * FROM `issue_stock` WHERE `item_id` = ? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, selectedCellValue);
                rs = pst.executeQuery();

                while (rs.next()) {
                    stype = rs.getString("stock_type");               
                }
                
                if (stype.equalsIgnoreCase("Consumable")) {
                    JOptionPane.showMessageDialog(null, "Consumable item cannot be returned.");
                } else {
                    System.out.println("All good.");
                }

            } catch (SQLException e) {
                Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
            }
    }
    
    private void btnItemReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnItemReturnActionPerformed
        // TODO add your handling code here:      
        
        if (!txtrnoir.getText().isEmpty()) {
            try {
                String sql = "INSERT INTO `return_items`(`custid`, `custname`, `item_id`, `item_name`, `qty`, `date_issued`,"
                        + " `return_date`, `noireturned`) VALUES (?,?,?,?,?,?,?,?)";
                pst = conn.prepareStatement(sql);
                pst.setString(1, txtrcid.getText());
                pst.setString(2, txtrcname.getText());
                pst.setString(3, txtritemid.getText());
                pst.setString(4, txtritemname.getText().toUpperCase());
                pst.setString(5, txtrqnoi.getText());
                pst.setString(6, txtrdiss.getText());
                pst.setString(7, txtrdr.getText());
                pst.setString(8, txtrnoir.getText());

                pst.execute();

                //JOptionPane.showMessageDialog(null, "Saved.");
                
                getQuantityCountFromIssuedStock();
                updateIssuedStockAfterReturning();
                
                issueUpdateTables();
                
                getCurrentStockQuantity();
                updateStockCollectionAfterReturning();
                
                issueUpdateTables();
                
                clearReturnedStock();

            } catch (SQLException e) {
                Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please fill the required field. ");
        }
    }//GEN-LAST:event_btnItemReturnActionPerformed

    private void txtrsearchcidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrsearchcidKeyReleased
        // TODO add your handling code here:
            DefaultTableModel table = (DefaultTableModel) tblreturnitems.getModel();
            String stockitemsearch = txtrsearchcid.getText().toUpperCase();
            TableRowSorter<DefaultTableModel> pl = new TableRowSorter<>(table);
            tblreturnitems.setRowSorter(pl);
            pl.setRowFilter(RowFilter.regexFilter(stockitemsearch));
    }//GEN-LAST:event_txtrsearchcidKeyReleased

    private void tblreturnitemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblreturnitemsMouseClicked
        // TODO add your handling code here:
        //checkStockType();
        
        JTable table = (JTable) evt.getSource();
        int selectedRow = table.getSelectedRow();
        int selectedColumn = table.getSelectedColumn();
        String selectedCellValue = (String) table.getValueAt(selectedRow, selectedColumn);
        
        String stype = "";
        
            try {
                String sql = "SELECT * FROM `issue_stock` WHERE `item_id` = ? ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, selectedCellValue);
                rs = pst.executeQuery();

                while (rs.next()) {
                    stype = rs.getString("stock_type");               
                }

            } catch (SQLException e) {
                Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
            }
            
        String cid = txtrsearchcid.getText();
        
        if (stype.equalsIgnoreCase("Consumable")) {
            JOptionPane.showMessageDialog(null, "Consumable item cannot be returned.");
            txtrsearchcid.setText("");
        } else {
            if (!cid.isEmpty()) {
                
                try {
                    String sql = "SELECT * FROM `issue_stock` WHERE `item_id` = ? AND `custid`='"+cid+"' AND `stock_type`=\"Asset\" ";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, selectedCellValue);
                    rs = pst.executeQuery();

                    while (rs.next()) {
                        txtrcid.setText(rs.getString("custid"));
                        txtrcname.setText(rs.getString("custname"));
                        txtrdiss.setText(rs.getString("doa"));
                        txtritemid.setText(rs.getString("item_id"));
                        txtritemname.setText(rs.getString("item_name"));
                        txtrqnoi.setText(rs.getString("qty"));                
                    }

                } catch (SQLException e) {
                    Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
                }
                
            } else {
                JOptionPane.showMessageDialog(null, "Please provide the Custodian's work ID.");
            }
        }
        
    }//GEN-LAST:event_tblreturnitemsMouseClicked

    private void txtrnoirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrnoirKeyPressed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtrnoirKeyPressed

    private void txtrnoirKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrnoirKeyReleased
        // TODO add your handling code here:
        try{
            int qty = Integer.parseInt(txtrnoir.getText());
            if (!txtrnoir.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                txtrnoir.setText("");
            }else if(qty <= 0){
                JOptionPane.showMessageDialog(null, "Invalid stock quantity value!");
                txtrnoir.setText("");
            }else{

                getQuantityCountFromIssuedStock();
                int rtndqty = Integer.parseInt(txtrnoir.getText());

                if (rtndqty > issuedqt) {
                    JOptionPane.showMessageDialog(null, "Sorry. You've do not posses the item. There are only "+issuedqt+" items assigned to you.");
                    txtrnoir.setText("");
                    clearIssueStock();
                } else if(issuedqt <= 0){
                    JOptionPane.showMessageDialog(null, "Sorry. Items out of stock.");
                    clearIssueStock();
                }else{
                    System.out.println("All good.");
                }

            }
        } catch (HeadlessException | NumberFormatException e) {
             JOptionPane.showMessageDialog(null, "Invalid value.");
             txtrnoir.setText("");
        }
        
    }//GEN-LAST:event_txtrnoirKeyReleased

    private void stocknameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stocknameKeyReleased
        // TODO add your handling code here:
        if (stockname.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            stockname.setText("");
        }else if(stockname.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            stockname.setText("");
        }
    }//GEN-LAST:event_stocknameKeyReleased

    private void stockbrandKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockbrandKeyReleased
        // TODO add your handling code here:
        if (stockbrand.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            stockbrand.setText("");
        }else if(stockbrand.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Brand name too long!");
            stockbrand.setText("");
        }
    }//GEN-LAST:event_stockbrandKeyReleased

    private void stockvendorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockvendorKeyReleased
        // TODO add your handling code here:
        if (stockvendor.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            stockvendor.setText("");
        }else if(stockvendor.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Vendor name too long!");
            stockvendor.setText("");
        }
    }//GEN-LAST:event_stockvendorKeyReleased

    private void stockcostperitemKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockcostperitemKeyReleased
        // TODO add your handling code here:
        if (!stockcostperitem.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            stockcostperitem.setText("");
        }else if(stockcostperitem.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Cost of item is too large!");
            stockcostperitem.setText("");
        }
    }//GEN-LAST:event_stockcostperitemKeyReleased

    private void stockquantityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockquantityKeyReleased
        // TODO add your handling code here:
        try {
            int cps = Integer.parseInt(stockquantity.getText());
        
            if (!stockquantity.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                stockquantity.setText("");
            }else if(cps < 0 ){
                JOptionPane.showMessageDialog(null, "Invalid number of books!");
                stockquantity.setText("");
            }
        } catch (HeadlessException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            stockquantity.setText("");
        }
    }//GEN-LAST:event_stockquantityKeyReleased

    private void txtsnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsnameKeyReleased
        // TODO add your handling code here:
        if (txtsname.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtsname.setText("");
        }
    }//GEN-LAST:event_txtsnameKeyReleased

    private void txtqntyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtqntyKeyReleased
        // TODO add your handling code here:
        try {
            int cps = Integer.parseInt(txtqnty.getText());
        
            if (!txtqnty.getText().matches("[0-9]+(.){0,1}[0-9]*") ) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                txtqnty.setText("");
            }else if(cps < 0 ){
                JOptionPane.showMessageDialog(null, "Invalid number of books!");
                txtqnty.setText("");
            }
        } catch (HeadlessException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Characters are not allowed!");
            txtqnty.setText("");
        }
    }//GEN-LAST:event_txtqntyKeyReleased

    private void txtcostKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcostKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcostKeyPressed

    private void txtcostKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcostKeyReleased
        // TODO add your handling code here:
        if (!txtcost.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtcost.setText("");
        }
    }//GEN-LAST:event_txtcostKeyReleased

    private void txtsm4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm4KeyReleased
        // TODO add your handling code here:
        if (!txtsm4.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtsm4.setText("");
        }
    }//GEN-LAST:event_txtsm4KeyReleased

    private void txtsm5searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm5searchKeyReleased
        // TODO add your handling code here:
        try {
            String msgadmno = txtsm5search.getText();

            String sql = "SELECT * FROM `otherstaff` WHERE `e_work_id`=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, msgadmno);
            rs = pst.executeQuery();

            while (rs.next()) {                
                String contact = rs.getString("e_phone");
                txtsm4.setText(contact);
                txtsm3.setText(rs.getString("ename"));
            }

        } catch (SQLException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_txtsm5searchKeyReleased

    private void btnsmCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsmCancelActionPerformed
        // TODO add your handling code here:
        txtsm3.setText("");
        txtsm4.setText("");
        txtsm6.setText("");
    }//GEN-LAST:event_btnsmCancelActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        String admin = skloggedinusrcode.getText();
        
        if (admin.contains("ADM")) {
            this.dispose();
            
            try {
                Admin ad = new Admin();
                ad.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Stock_keeping sk = new Stock_keeping();
                sk.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnBackActionPerformed

    private void tblissuestockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblissuestockMouseClicked
        // TODO add your handling code here:
        JTable table = (JTable) evt.getSource();
        int selectedRow = table.getSelectedRow();
        int selectedColumn = table.getSelectedColumn();
        String selectedCellValue = (String) table.getValueAt(selectedRow, selectedColumn);
        
        try {
            String sql = "SELECT * FROM `stock_data` WHERE `item_id` = ? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, selectedCellValue);
            rs = pst.executeQuery();

            while (rs.next()) {
                txtisstype.setText(rs.getString("stock_type"));
                txtisitidno.setText(rs.getString("item_id"));
                txtisitname.setText(rs.getString("item_name"));
            }
            
            String stype = txtisstype.getText();
        
            if (!stype.equalsIgnoreCase("Asset")) {
                //JOptionPane.showMessageDialog(null, "Only assets can be returned!");
                jdatereturn.setEnabled(false);
            } else {
                jdatereturn.setEnabled(true);
            }

        } catch (SQLException e) {
            Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }//GEN-LAST:event_tblissuestockMouseClicked

    private void jdatereturnKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jdatereturnKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jdatereturnKeyPressed

    private void jdatereturnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jdatereturnMouseClicked
        // TODO add your handling code here:        
    }//GEN-LAST:event_jdatereturnMouseClicked

    private void btnUpdateClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateClearActionPerformed
        // TODO add your handling code here:
        clearUpdateStock();
    }//GEN-LAST:event_btnUpdateClearActionPerformed

    private void btnUpdateClear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateClear1ActionPerformed
        // TODO add your handling code here:
        clearStockFields();
    }//GEN-LAST:event_btnUpdateClear1ActionPerformed

    private void btnIssueClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIssueClearActionPerformed
        // TODO add your handling code here:
        clearIssueStock();
    }//GEN-LAST:event_btnIssueClearActionPerformed

    private void btnReturnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnClearActionPerformed
        // TODO add your handling code here:
        clearReturnedStock();
    }//GEN-LAST:event_btnReturnClearActionPerformed

    private void stockreceiptnoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockreceiptnoKeyReleased
        // TODO add your handling code here:
        if(stockreceiptno.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Receipt number too long!");
            stockreceiptno.setText("");
        }
    }//GEN-LAST:event_stockreceiptnoKeyReleased

    private void stockwvKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockwvKeyReleased
        // TODO add your handling code here:
        if(stockwv.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Weight/volume too long!");
            stockwv.setText("");
        }
    }//GEN-LAST:event_stockwvKeyReleased

    private void clearUpdateStock(){
        txtitemcat.setText("");
        txtitemid.setText("");
        txtsname.setText("");
        txtbrand.setText("");
        txtqnty.setText("");
        txtvrno.setText("");
        txtcost.setText("");
        txtwv.setText("");
        txtrplnshdate.setText("");
        txtdsc.setText("");
        txtisstype.setText("");
        txtisitidno.setText("");
        txtissqty.setText("");
    }
    
    private void clearReturnedStock(){
        txtrcid.setText("");
        txtrcname.setText("");
        txtritemid.setText("");
        txtritemname.setText("");
        txtrqnoi.setText("");
        txtrdiss.setText("");
        txtrdr.setText("");
        txtrnoir.setText("");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Stock_keeping.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Stock_keeping.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Stock_keeping.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Stock_keeping.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Stock_keeping().setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Stock_keeping.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNewStock;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnIssueClear;
    private javax.swing.JButton btnIssueStock;
    private javax.swing.JButton btnItemReturn;
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnReturnClear;
    private javax.swing.JButton btnUpdateBtn;
    private javax.swing.JButton btnUpdateClear;
    private javax.swing.JButton btnUpdateClear1;
    private javax.swing.JButton btnXLSSheet;
    private javax.swing.JButton btnsmCancel;
    private javax.swing.JComboBox<String> cbostocktype;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane2;
    private com.toedter.calendar.JDateChooser jdatereturn;
    private javax.swing.JLabel lblmsg;
    private javax.swing.JLabel lblmsg1;
    private javax.swing.JLabel skloggedintime;
    private javax.swing.JLabel skloggedinusrcode;
    private javax.swing.JTextField stockbrand;
    private javax.swing.JTextField stockcostperitem;
    private com.toedter.calendar.JDateChooser stockdateofpur;
    private javax.swing.JTextArea stockdesk;
    private javax.swing.JTextField stockid;
    private javax.swing.JTextField stockname;
    private javax.swing.JTextField stockquantity;
    private javax.swing.JTextField stockreceiptno;
    private javax.swing.JTextField stockvendor;
    private javax.swing.JTextField stockwv;
    private javax.swing.JTable tblissuestock;
    private javax.swing.JTable tblmsglist;
    private javax.swing.JTable tblreturnitems;
    private javax.swing.JTable tblstocklist;
    private javax.swing.JTable tblupdatetable;
    private javax.swing.JTextField txtbrand;
    private javax.swing.JTextField txtcost;
    private javax.swing.JTextArea txtdsc;
    private javax.swing.JTextField txtiscid;
    private javax.swing.JTextField txtiscname;
    private javax.swing.JTextField txtiscwid;
    private javax.swing.JTextField txtisdate;
    private javax.swing.JTextField txtisdept;
    private javax.swing.JTextField txtisempas;
    private javax.swing.JTextField txtisitidno;
    private javax.swing.JTextField txtisitname;
    private javax.swing.JTextField txtissqty;
    private javax.swing.JTextField txtisstype;
    private javax.swing.JTextField txtitemcat;
    private javax.swing.JTextField txtitemid;
    private javax.swing.JTextField txtqnty;
    private javax.swing.JTextField txtrcid;
    private javax.swing.JTextField txtrcname;
    private javax.swing.JTextField txtrdiss;
    private javax.swing.JTextField txtrdr;
    private javax.swing.JTextField txtritemid;
    private javax.swing.JTextField txtritemname;
    private javax.swing.JTextField txtrnoir;
    private javax.swing.JTextField txtrplnshdate;
    private javax.swing.JTextField txtrqnoi;
    private javax.swing.JTextField txtrsearchcid;
    private javax.swing.JTextField txtsearchstocklist;
    private javax.swing.JTextField txtsearchupdatestock;
    private javax.swing.JTextField txtsm3;
    private javax.swing.JTextField txtsm4;
    private javax.swing.JTextField txtsm5search;
    private javax.swing.JTextArea txtsm6;
    private javax.swing.JButton txtsmSend;
    private javax.swing.JTextField txtsname;
    private javax.swing.JTextField txtvrno;
    private javax.swing.JTextField txtwv;
    // End of variables declaration//GEN-END:variables
}
