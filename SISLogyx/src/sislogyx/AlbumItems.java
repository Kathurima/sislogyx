/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

/**
 *
 * @author OREON
 */
public class AlbumItems {
    private final String dateofevent;
    private final String whatevent;
    private final String location;
    private final String commemorator;
    private final String comment;
    private final byte[] album_photo;
    
    public AlbumItems(String DOE, String EVENT, String LOCATION, String COMMEMORATOR, String COMMENT, byte[] PHOTO){
        this.dateofevent = DOE;
        this.whatevent = EVENT;
        this.location = LOCATION;
        this.commemorator = COMMEMORATOR;
        this.comment = COMMENT;
        this.album_photo = PHOTO;
    }
    
    public String getDateofEvent(){
        return dateofevent;
    }
    
    public String getEvent(){
        return whatevent;
    }
    
     public String getLocation(){
        return location;
    }
    
    public String getCommemorator(){
        return commemorator;
    }
    
    public String getComment(){
        return comment;
    }
    
    public byte[] getAlbumPhoto(){
        return album_photo;
    }
}
