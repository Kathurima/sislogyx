/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sislogyx;

import com.mysql.jdbc.Connection;
import com.teknikindustries.bulksms.SMS;
import java.awt.Color;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ENG.KATHURIMAKIMATHI
 */
public class Library extends javax.swing.JFrame {
    private Connection conn = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;
    
    int cp;
    int yr;
    
    String classtr, trclass;
    
    DefaultTableModel borrowlist = new DefaultTableModel();
    
    DefaultTableModel returnlist = new DefaultTableModel();
    
    DefaultTableModel catalogue = new DefaultTableModel();
    
    DefaultTableModel msglist = new DefaultTableModel();

    /**
     * Creates new form Library
     * @throws java.sql.SQLException
     */
    public Library() throws SQLException {
        super("Librarian Panel");
        
        initComponents();
        
        conn = (Connection) DBConnect.connect();
        
        Current_Date();
        
        libloggedinusercode.setText(String.valueOf(Emp.empId));
        
        libtblborrow.setModel(borrowBook());
        tblreturn.setModel(returnBook());
        tblcatalogue.setModel(Catalogue());
        tblmsglist.setModel(messageList());
        
        //Admin's
        String admin = libloggedinusercode.getText();
        
        if (admin.contains("ADM")) {
            btnLibBack.setVisible(true);
        } else {
            btnLibBack.setVisible(false);
        }
        
    }
    
    private void Current_Date(){
                
        Thread clock = new Thread(){
            @Override
            public void run(){
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);

                    txtlibborrowdate.setText(day+"/"+(month+1)+"/"+year);
                    txtdateadded.setText(day+"/"+(month+1)+"/"+year);
                    yr = year;
                    
                    cal.add(Calendar.DAY_OF_MONTH,30);
                    if (rb1.isSelected()) {
                        txtlibreturndate.setText((day+4)+"/"+month+"/"+year);
                    } else if(rb2.isSelected()){
                        txtlibreturndate.setText(day+"/"+(month+3)+"/"+year);
                    }else if(rb3.isSelected()){
                        txtlibreturndate.setText((day+4)+"/"+month+"/"+year);
                    }else{
                        txtlibreturndate.setText("");
                    }
                    

                    int second = cal.get(Calendar.SECOND);
                    int minute = cal.get(Calendar.MINUTE);
                    int hour = cal.get(Calendar.HOUR_OF_DAY);

                    libloggedintime.setText(hour+":"+minute+":"+second);
                    libloggedintime.setForeground(Color.red);
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException ie) {
//                        Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ie);
//                    }
                }
            }
        };
        clock.start();
   }
    
    private DefaultTableModel messageList(){
        //msglist.addColumn("Message Type");
        msglist.addColumn("Adm no.");
        //msglist.addColumn("Email Recipient");
        msglist.addColumn("From");
        msglist.addColumn("To");
        msglist.addColumn("Message");
        
        try {
            String sql = "SELECT * FROM `messages` WHERE `m_from`=\"Library\" ";
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                String padmno = rs.getString("padmno");
                String mfrom = rs.getString("m_from");
                String mto = rs.getString("m_to");
                String m_msg = rs.getString("m_msg");
                
                String[] rowdata = {padmno, mfrom, mto, m_msg};
                msglist.addRow(rowdata);
            }
            
            return msglist;
            
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    private DefaultTableModel returnBook(){
        returnlist.addColumn("Custodian ID");
        returnlist.addColumn("Custodian Name");
        returnlist.addColumn("Material ID");
        returnlist.addColumn("Title");
        returnlist.addColumn("Borrow Date");
        returnlist.addColumn("Return Date");
        
        try{
            String sql = "SELECT * FROM `library_return` ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();
            
            while (rs.next()) {
                
                String cid = rs.getString("custodian_id");
                String cname = rs.getString("custodian_name");
                String mid = rs.getString("material_id");
                String title = rs.getString("title");
                String bdate = rs.getString("borrow_date");
                String rdate = rs.getString("return_date");
                
                String[] rowdata = {cid,cname,mid,title,mid,bdate,rdate};
                returnlist.addRow(rowdata);
            }
            
            return returnlist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;
    }
    
    private DefaultTableModel borrowBook(){
        borrowlist.addColumn("Custodian ID");
        borrowlist.addColumn("Custodian Name");
        borrowlist.addColumn("Custodian Category");
        borrowlist.addColumn("Material ID");
        borrowlist.addColumn("Title");
        borrowlist.addColumn("Author");
        //borrowlist.addColumn("Category");
        //borrowlist.addColumn("Subject");
        //borrowlist.addColumn("Publisher");
        //borrowlist.addColumn("Edition");
        //borrowlist.addColumn("YOP");        
        borrowlist.addColumn("Borrow Date");
        borrowlist.addColumn("Return Date");
        
        try{
            String sql = "SELECT * FROM `library_borrow` ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();
            
            while (rs.next()) {
                String ctype = rs.getString("custodian_type");
                String mid = rs.getString("material_id");
                String title = rs.getString("title");
                String author = rs.getString("author");
                //String cat = rs.getString("category");
                //String sbjct = rs.getString("subject");
                //String pub = rs.getString("publisher");
                //String edtn = rs.getString("edition");
                //String yop = rs.getString("yop");
                String cid = rs.getString("custodian_id");
                //String cdesig = rs.getString("e_dept");
                String cname = rs.getString("custodian_name");
                String bdate = rs.getString("borrow_date");
                String rdate = rs.getString("return_date");
                
                String[] rowdata = {cid,cname,ctype,mid,title,author,bdate,rdate};
                borrowlist.addRow(rowdata);
            }
            
            return borrowlist;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;        
    }
    
    private DefaultTableModel Catalogue(){
        catalogue.addColumn("Genre");
        catalogue.addColumn("ISBN/Document No.");
        catalogue.addColumn("Title");
        catalogue.addColumn("Author");
        catalogue.addColumn("Y.O.P");
        catalogue.addColumn("Edition");
        catalogue.addColumn("Publisher");
        catalogue.addColumn("Subject");
        catalogue.addColumn("Condition");
        
        
        
        try{
            String sql = "SELECT * FROM `lib_collection` WHERE `status`=\"1\" ";
            pst = conn.prepareStatement(sql);            
            rs = pst.executeQuery();
            
            while (rs.next()) {
                String genre = rs.getString("genre");
                String isbn = rs.getString("isbn_doc_no");
                String title = rs.getString("title");
                String author = rs.getString("author");
                String dop = rs.getString("date_published");
                String edtn = rs.getString("edition");
                String pub = rs.getString("publisher");
                String sbjct = rs.getString("subject");
                String mc = rs.getString("material_condition");
                
                String[] rowdata = {genre,isbn,title,author,dop,edtn,pub,sbjct,mc};
                catalogue.addRow(rowdata);
            }
            
            return catalogue;
            
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
            try{
                rs.close();
                pst.close();
            }catch(SQLException e){
                
            }
        }
        
        return null;        
    }
    
    private void clearBorrowFields(){
        txtlibmaterialid.setText("");
        txtlibtitle.setText("");
        txtlibauthor.setText("");
        txtlibcategory.setText("");
        txtlibsbjct.setText("");
        txtlibpublisher.setText("");
        txtlibedition.setText("");
        txtlibyop.setText("");
        txtlibcustodianid.setText("");
        txtlibcustodiandesig.setText("");
        txtlibcustodianname.setText("");
        txtlibborrowdate.setText("");
        txtlibreturndate.setText("");
        rb1.setSelected(false);
        rb2.setSelected(false);
        rb3.setSelected(false);
    }
    
    private void clearAddNewFields(){
        cbogenre.setSelectedIndex(0);
        txtisbn.setText("");
        txttitle.setText("");
        txtauthor.setText("");
        txtdp.setText("");
        txtedtn.setText("");
        txtdateadded.setText("");
        txtpub.setText("");
        txtcprt.setText("");
        //txtcopies.setText("");
        txtsbjct.setText("");
        txtcndtn.setText("");
    }
    
    private void clearUpdateFields(){
        txtupsearch.setText("");
        cboupgenre.setSelectedIndex(0);
        txtupisbn.setText("");
        txtuptitle.setText("");
        txtupauthor.setText("");
        txtupyp.setText("");
        txtupedtn.setText("");
        txtupdr.setText("");
        txtuppub.setText("");
        txtupcprt.setText("");
        //txtupcopies.setText("");
        txtupsbjct.setText("");
        txtupcndtn.setText("");
    }

    private void clearReturnFields(){
        txtcstdnid.setText("");
        txtcstdnname.setText("");
        txtbdate.setText("");
        txtrdate.setText("");
        txtmid.setText("");
        txtrtitle.setText("");
        txtcat.setText("");
        txtrpub.setText("");
        txtredtn.setText("");
        //lblstatus.setText("Status");
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jbtnlogout = new javax.swing.JButton();
        jTabbedPane7 = new javax.swing.JTabbedPane();
        jpnl_personal_info = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel97 = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        libtblborrow = new javax.swing.JTable();
        libsearch = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        jLabel102 = new javax.swing.JLabel();
        jLabel105 = new javax.swing.JLabel();
        txtlibcustodiandesig = new javax.swing.JTextField();
        jLabel104 = new javax.swing.JLabel();
        txtlibreturndate = new javax.swing.JTextField();
        jLabel103 = new javax.swing.JLabel();
        jLabel99 = new javax.swing.JLabel();
        txtlibborrowdate = new javax.swing.JTextField();
        txtlibcustodianname = new javax.swing.JTextField();
        txtlibcustodianid = new javax.swing.JTextField();
        rb1 = new javax.swing.JRadioButton();
        rb2 = new javax.swing.JRadioButton();
        rb3 = new javax.swing.JRadioButton();
        jPanel17 = new javax.swing.JPanel();
        txtlibsbjct = new javax.swing.JTextField();
        txtlibmaterialid = new javax.swing.JTextField();
        jLabel92 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        jLabel96 = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        txtlibtitle = new javax.swing.JTextField();
        txtlibedition = new javax.swing.JTextField();
        txtlibpublisher = new javax.swing.JTextField();
        jLabel100 = new javax.swing.JLabel();
        txtlibyop = new javax.swing.JTextField();
        txtlibcategory = new javax.swing.JTextField();
        jLabel90 = new javax.swing.JLabel();
        txtlibauthor = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tblreturn = new javax.swing.JTable();
        txtreturnsearch = new javax.swing.JTextField();
        jPanel18 = new javax.swing.JPanel();
        jLabel78 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        txtbdate = new javax.swing.JTextField();
        txtcstdnid = new javax.swing.JTextField();
        txtrdate = new javax.swing.JTextField();
        txtcstdnname = new javax.swing.JTextField();
        jLabel75 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel81 = new javax.swing.JLabel();
        txtredtn = new javax.swing.JTextField();
        jLabel83 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        jLabel79 = new javax.swing.JLabel();
        txtrpub = new javax.swing.JTextField();
        txtrtitle = new javax.swing.JTextField();
        txtcat = new javax.swing.JTextField();
        txtmid = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtsearchmatid = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        cbogenre = new javax.swing.JComboBox<>();
        jLabel62 = new javax.swing.JLabel();
        txtisbn = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        txttitle = new javax.swing.JTextField();
        jLabel64 = new javax.swing.JLabel();
        txtauthor = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        txtedtn = new javax.swing.JTextField();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        txtcprt = new javax.swing.JTextField();
        jLabel71 = new javax.swing.JLabel();
        txtsbjct = new javax.swing.JTextField();
        jLabel72 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtcndtn = new javax.swing.JTextArea();
        txtpub = new javax.swing.JTextField();
        txtdateadded = new javax.swing.JTextField();
        txtdp = new javax.swing.JTextField();
        jPanel20 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel89 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtupdr = new javax.swing.JTextField();
        cboupgenre = new javax.swing.JComboBox<>();
        jScrollPane9 = new javax.swing.JScrollPane();
        txtupcndtn = new javax.swing.JTextArea();
        txtupsbjct = new javax.swing.JTextField();
        jLabel85 = new javax.swing.JLabel();
        txtupauthor = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtuppub = new javax.swing.JTextField();
        txtupisbn = new javax.swing.JTextField();
        txtupedtn = new javax.swing.JTextField();
        jLabel84 = new javax.swing.JLabel();
        jLabel94 = new javax.swing.JLabel();
        jLabel106 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        txtuptitle = new javax.swing.JTextField();
        txtupcprt = new javax.swing.JTextField();
        txtupyp = new javax.swing.JTextField();
        jLabel87 = new javax.swing.JLabel();
        jLabel86 = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        txtupsearch = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        txtcsearch = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblcatalogue = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        txtcataloguetype = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        txtsm6 = new javax.swing.JTextArea();
        btnsmCancel = new javax.swing.JButton();
        txtsmSend = new javax.swing.JButton();
        txtsm4 = new javax.swing.JTextField();
        lblmsg = new javax.swing.JLabel();
        txtsm3 = new javax.swing.JTextField();
        jScrollPane13 = new javax.swing.JScrollPane();
        tblmsglist = new javax.swing.JTable();
        txtsm5search = new javax.swing.JTextField();
        lblmsg1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btnLibChangePssd = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        btnReturn = new javax.swing.JButton();
        btnLend = new javax.swing.JButton();
        btnAddNew = new javax.swing.JButton();
        btnCatalogueSheet = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnLibBack = new javax.swing.JButton();
        libloggedinusercode = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        libloggedintime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));

        jPanel3.setBackground(new java.awt.Color(102, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("LIBRARY DASHBOARD");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(86, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        jbtnlogout.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jbtnlogout.setText("LOGOUT");
        jbtnlogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnlogoutActionPerformed(evt);
            }
        });

        jTabbedPane7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jpnl_personal_info.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jpnl_personal_infoMouseClicked(evt);
            }
        });

        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/lib.jpg"))); // NOI18N

        javax.swing.GroupLayout jpnl_personal_infoLayout = new javax.swing.GroupLayout(jpnl_personal_info);
        jpnl_personal_info.setLayout(jpnl_personal_infoLayout);
        jpnl_personal_infoLayout.setHorizontalGroup(
            jpnl_personal_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnl_personal_infoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, 1014, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpnl_personal_infoLayout.setVerticalGroup(
            jpnl_personal_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 640, Short.MAX_VALUE)
        );

        jTabbedPane7.addTab("Home", jpnl_personal_info);

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Borrow Book", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel97.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel97.setForeground(new java.awt.Color(255, 0, 0));
        jLabel97.setText("Material ID");

        libtblborrow.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane12.setViewportView(libtblborrow);

        libsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        libsearch.setToolTipText("Type Material's ID");
        libsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                libsearchKeyReleased(evt);
            }
        });

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Custodian Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel102.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel102.setText("Designation");

        jLabel105.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel105.setText("Custodian ID");

        txtlibcustodiandesig.setEditable(false);
        txtlibcustodiandesig.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel104.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel104.setText("Borrow Date");

        txtlibreturndate.setEditable(false);
        txtlibreturndate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel103.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel103.setText("Return Date");

        jLabel99.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel99.setText("Custodian Name");

        txtlibborrowdate.setEditable(false);
        txtlibborrowdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlibcustodianname.setEditable(false);
        txtlibcustodianname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlibcustodianid.setEditable(false);
        txtlibcustodianid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtlibcustodianid.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtlibcustodianidMouseClicked(evt);
            }
        });
        txtlibcustodianid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlibcustodianidActionPerformed(evt);
            }
        });
        txtlibcustodianid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtlibcustodianidKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlibcustodianidKeyReleased(evt);
            }
        });

        rb1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rb1.setText("Pupil");
        rb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rb1ActionPerformed(evt);
            }
        });

        rb2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rb2.setText("Teacher");
        rb2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rb2ActionPerformed(evt);
            }
        });

        rb3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rb3.setText("Other staff");
        rb3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rb3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel99)
                            .addComponent(jLabel102)
                            .addComponent(jLabel103)
                            .addComponent(jLabel104)
                            .addComponent(jLabel105))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtlibreturndate)
                            .addComponent(txtlibcustodianname)
                            .addComponent(txtlibborrowdate)
                            .addComponent(txtlibcustodiandesig)
                            .addComponent(txtlibcustodianid, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(101, 101, 101)
                        .addComponent(rb1)
                        .addGap(55, 55, 55)
                        .addComponent(rb2)
                        .addGap(42, 42, 42)
                        .addComponent(rb3)))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rb1)
                    .addComponent(rb2)
                    .addComponent(rb3))
                .addGap(11, 11, 11)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel105)
                    .addComponent(txtlibcustodianid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel102)
                    .addComponent(txtlibcustodiandesig, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel99)
                    .addComponent(txtlibcustodianname, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibborrowdate, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel104))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibreturndate, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel103))
                .addGap(14, 14, 14))
        );

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Material Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtlibsbjct.setEditable(false);
        txtlibsbjct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlibmaterialid.setEditable(false);
        txtlibmaterialid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel92.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel92.setText("Material ID");

        jLabel95.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel95.setText("Publisher");

        jLabel98.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel98.setText("Author");

        jLabel93.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel93.setText("Subject");

        jLabel96.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel96.setText("Category");

        jLabel101.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel101.setText("Edition");

        txtlibtitle.setEditable(false);
        txtlibtitle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlibedition.setEditable(false);
        txtlibedition.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlibpublisher.setEditable(false);
        txtlibpublisher.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel100.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel100.setText("Year Published");

        txtlibyop.setEditable(false);
        txtlibyop.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtlibcategory.setEditable(false);
        txtlibcategory.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel90.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel90.setText("Title");

        txtlibauthor.setEditable(false);
        txtlibauthor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel90)
                            .addComponent(jLabel92))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtlibmaterialid, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtlibtitle, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                            .addComponent(jLabel96)
                            .addGap(327, 327, 327))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                            .addComponent(jLabel98)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtlibauthor, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtlibcategory, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel93)
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel17Layout.createSequentialGroup()
                            .addComponent(jLabel100, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(2, 2, 2)
                            .addComponent(txtlibyop))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel17Layout.createSequentialGroup()
                            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel95)
                                .addComponent(jLabel101))
                            .addGap(29, 29, 29)
                            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtlibedition, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtlibpublisher, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtlibsbjct, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel92)
                    .addComponent(txtlibmaterialid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel90)
                    .addComponent(txtlibtitle, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel98)
                    .addComponent(txtlibauthor, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibcategory, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel96))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibsbjct, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel93))
                .addGap(26, 26, 26)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibpublisher, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel95))
                .addGap(26, 26, 26)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibedition, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel101))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlibyop, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel100))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13))
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(jLabel97)
                .addGap(18, 18, 18)
                .addComponent(libsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel97)
                    .addComponent(libsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 26, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane7.addTab("Lend", jPanel5);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Return Book", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 0, 0));
        jLabel19.setText("Search Custodian ID");

        tblreturn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane10.setViewportView(tblreturn);

        txtreturnsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtreturnsearch.setToolTipText("Type Custodian's ID");
        txtreturnsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtreturnsearchKeyReleased(evt);
            }
        });

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Custodian Detail ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(253, 153, 153))); // NOI18N

        jLabel78.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel78.setText("Return Date");

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel76.setText("Custodian Name ");

        txtbdate.setEditable(false);
        txtbdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtcstdnid.setEditable(false);
        txtcstdnid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtrdate.setEditable(false);
        txtrdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtcstdnname.setEditable(false);
        txtcstdnname.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel75.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel75.setText("Custodian ID");

        jLabel77.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel77.setText("Borrow Date");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel76)
                            .addComponent(jLabel77)
                            .addComponent(jLabel78))
                        .addGap(18, 36, Short.MAX_VALUE)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtcstdnname, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                            .addComponent(txtbdate)
                            .addComponent(txtrdate)))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel75)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                    .addContainerGap(152, Short.MAX_VALUE)
                    .addComponent(txtcstdnid, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel75)
                .addGap(18, 18, 18)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcstdnname, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel76))
                .addGap(18, 18, 18)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel77)
                    .addComponent(txtbdate, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtrdate, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel78))
                .addContainerGap(50, Short.MAX_VALUE))
            .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel18Layout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(txtcstdnid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(191, Short.MAX_VALUE)))
        );

        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Material Detail", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel81.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel81.setText("Category");

        txtredtn.setEditable(false);
        txtredtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel83.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel83.setText("Edition");

        jLabel80.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel80.setText("Title");

        jLabel82.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel82.setText("Publisher");

        jLabel79.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel79.setText("Material ID");

        txtrpub.setEditable(false);
        txtrpub.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtrtitle.setEditable(false);
        txtrtitle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtcat.setEditable(false);
        txtcat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtmid.setEditable(false);
        txtmid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel80)
                    .addComponent(jLabel79)
                    .addComponent(jLabel82)
                    .addComponent(jLabel81)
                    .addComponent(jLabel83))
                .addGap(18, 18, 18)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtrpub, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addComponent(txtcat, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtrtitle, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtmid, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtredtn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel79)
                    .addComponent(txtmid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel80)
                    .addComponent(txtrtitle, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcat, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel81))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtrpub, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel82))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtredtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel83))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 0, 0));
        jLabel4.setText("Returned materials");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 0, 0));
        jLabel21.setText("Search Material ID");

        txtsearchmatid.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsearchmatid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsearchmatidKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel15Layout.createSequentialGroup()
                            .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel15Layout.createSequentialGroup()
                                    .addComponent(jLabel19)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtreturnsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel15Layout.createSequentialGroup()
                                    .addGap(48, 48, 48)
                                    .addComponent(jLabel21)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtsearchmatid, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel15Layout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addComponent(jScrollPane10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1014, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel4))
                .addContainerGap(63, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtreturnsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtsearchmatid, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane7.addTab("Return", jPanel6);

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Add New", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        cbogenre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cbogenre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Genre", "Book", "Magazine", "Papers" }));

        jLabel62.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel62.setText("ISBN/Doc No.");

        txtisbn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtisbn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtisbnKeyReleased(evt);
            }
        });

        jLabel63.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel63.setText("Title");

        txttitle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txttitle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttitleKeyReleased(evt);
            }
        });

        jLabel64.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel64.setText("Author");

        txtauthor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtauthor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtauthorKeyReleased(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel65.setText("Year of Publication");

        jLabel66.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel66.setText("Edition");

        txtedtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtedtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtedtnKeyReleased(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel67.setText("Date Recorded");

        jLabel68.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel68.setText("Publisher");

        jLabel69.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel69.setText("Copyrighted Year");

        txtcprt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtcprt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcprtKeyReleased(evt);
            }
        });

        jLabel71.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel71.setText("Subject");

        txtsbjct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsbjct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsbjctKeyReleased(evt);
            }
        });

        jLabel72.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel72.setText("Condition");

        txtcndtn.setColumns(20);
        txtcndtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtcndtn.setRows(5);
        txtcndtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcndtnKeyReleased(evt);
            }
        });
        jScrollPane6.setViewportView(txtcndtn);

        txtpub.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtpub.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpubKeyReleased(evt);
            }
        });

        txtdateadded.setEditable(false);
        txtdateadded.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtdp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtdp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdpKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdpKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(300, 300, 300)
                        .addComponent(cbogenre, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtauthor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                                .addComponent(txttitle, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel64, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel62, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtisbn, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel63, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel65, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel66, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel67, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(txtdateadded, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtedtn, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtdp, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(46, 46, 46)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel68)
                            .addComponent(jLabel69)
                            .addComponent(jLabel71)
                            .addComponent(txtsbjct)
                            .addComponent(jLabel72)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtpub)
                            .addComponent(txtcprt, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbogenre, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel62)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtisbn, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel63)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttitle, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel64)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtauthor, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel65)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtdp, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel66)
                        .addGap(9, 9, 9)
                        .addComponent(txtedtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel68)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtpub, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(jLabel69)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcprt, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel14Layout.createSequentialGroup()
                                .addComponent(jLabel71)
                                .addGap(50, 50, 50))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                                .addComponent(txtsbjct, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addComponent(jLabel72)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel67)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtdateadded, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 105, Short.MAX_VALUE))
        );

        jTabbedPane7.addTab("Add New", jPanel7);

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Update Material Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jPanel12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel89.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel89.setText("Copyright Year");

        jLabel74.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel74.setText("Title");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Genre");

        txtupdr.setEditable(false);
        txtupdr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        cboupgenre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cboupgenre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Genre", "Book", "Magazine", "Papers" }));
        cboupgenre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboupgenreActionPerformed(evt);
            }
        });

        txtupcndtn.setColumns(20);
        txtupcndtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupcndtn.setRows(5);
        txtupcndtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupcndtnKeyReleased(evt);
            }
        });
        jScrollPane9.setViewportView(txtupcndtn);

        txtupsbjct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupsbjct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupsbjctKeyReleased(evt);
            }
        });

        jLabel85.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel85.setText("Year of Publication");

        txtupauthor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupauthor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupauthorKeyReleased(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 0, 0));
        jLabel20.setText("Material ID");

        txtuppub.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtuppub.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtuppubKeyReleased(evt);
            }
        });

        txtupisbn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtupedtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupedtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupedtnKeyReleased(evt);
            }
        });

        jLabel84.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel84.setText("Author");

        jLabel94.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel94.setText("Subject");

        jLabel106.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel106.setText("Condition");

        jLabel88.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel88.setText("Publisher");

        txtuptitle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtuptitle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtuptitleKeyReleased(evt);
            }
        });

        txtupcprt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupcprt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupcprtKeyReleased(evt);
            }
        });

        txtupyp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupyp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupypKeyReleased(evt);
            }
        });

        jLabel87.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel87.setText("Date Recorded");

        jLabel86.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel86.setText("Edition");

        jLabel73.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel73.setText("ISBN/Doc No.");

        txtupsearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtupsearch.setToolTipText("Type Material's ID");
        txtupsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtupsearchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel84)
                    .addComponent(jLabel73)
                    .addComponent(jLabel74)
                    .addComponent(jLabel85)
                    .addComponent(jLabel86)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtupedtn, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtupyp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cboupgenre, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtuptitle)
                    .addComponent(txtupauthor)
                    .addComponent(txtupisbn, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel88)
                    .addComponent(jLabel89)
                    .addComponent(jLabel94)
                    .addComponent(jLabel106)
                    .addComponent(txtupcprt, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel87)
                    .addComponent(txtupsbjct)
                    .addComponent(txtuppub)
                    .addComponent(txtupdr)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE))
                .addGap(94, 94, 94))
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(152, 152, 152)
                .addComponent(jLabel20)
                .addGap(28, 28, 28)
                .addComponent(txtupsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(117, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtupsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addComponent(jLabel94)
                        .addGap(6, 6, 6)
                        .addComponent(txtupsbjct, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel106)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(jLabel88)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtuppub, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(jLabel89)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtupcprt, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel87)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtupdr, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboupgenre, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                                .addComponent(txtupyp, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(jLabel73)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupisbn, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel74)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtuptitle, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel84)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtupauthor, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel85)
                                .addGap(50, 50, 50)))
                        .addComponent(jLabel86)
                        .addGap(9, 9, 9)
                        .addComponent(txtupedtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(78, Short.MAX_VALUE))
        );

        jTabbedPane7.addTab("Update Details", jPanel20);

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Catalogue", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        txtcsearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtcsearch.setToolTipText("Type Material's ID");
        txtcsearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcsearchKeyReleased(evt);
            }
        });

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel50.setText("Search");

        tblcatalogue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane8.setViewportView(tblcatalogue);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("Type");

        txtcataloguetype.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtcataloguetype.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcataloguetypeKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtcataloguetype, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(jLabel50)
                .addGap(18, 18, 18)
                .addComponent(txtcsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 529, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(107, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel50)
                    .addComponent(jLabel5)
                    .addComponent(txtcataloguetype, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                .addGap(86, 86, 86))
        );

        jTabbedPane7.addTab("Catalogue", jPanel13);

        jPanel34.setBackground(new java.awt.Color(153, 153, 255));
        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Send Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 153, 153))); // NOI18N

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("To");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Message");

        txtsm6.setColumns(20);
        txtsm6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm6.setRows(5);
        jScrollPane11.setViewportView(txtsm6);

        btnsmCancel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnsmCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/cancel.png"))); // NOI18N
        btnsmCancel.setText("Cancel");
        btnsmCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsmCancelActionPerformed(evt);
            }
        });

        txtsmSend.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsmSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/send.png"))); // NOI18N
        txtsmSend.setText("SEND");
        txtsmSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsmSendActionPerformed(evt);
            }
        });

        txtsm4.setEditable(false);
        txtsm4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm4KeyReleased(evt);
            }
        });

        lblmsg.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg.setText("Teacher's Name");

        txtsm3.setEditable(false);
        txtsm3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm3KeyReleased(evt);
            }
        });

        tblmsglist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane13.setViewportView(tblmsglist);

        txtsm5search.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtsm5search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtsm5searchKeyReleased(evt);
            }
        });

        lblmsg1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblmsg1.setText("Search Teacher");

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnsmCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(156, 156, 156))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane13)
                .addContainerGap())
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(270, 270, 270)
                        .addComponent(jLabel16))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel34Layout.createSequentialGroup()
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblmsg, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtsm4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15)))
                            .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(lblmsg1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(txtsm5search, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsm5search, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblmsg1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(lblmsg, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsm4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsmSend, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsmCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(227, 227, 227)
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(182, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane7.addTab("Send Message", jPanel10);

        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnLibChangePssd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLibChangePssd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/changepssd.png"))); // NOI18N
        btnLibChangePssd.setText("CHANGE PASSWORD");
        btnLibChangePssd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLibChangePssdActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(204, 0, 0));
        jLabel8.setText("Click the button below to change your account credentials");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(btnLibChangePssd, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addGap(84, 84, 84))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLibChangePssd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(351, Short.MAX_VALUE)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(228, 228, 228))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(494, Short.MAX_VALUE))
        );

        jTabbedPane7.addTab("Account Management", jPanel4);

        jPanel8.setBackground(new java.awt.Color(102, 255, 204));

        btnReturn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnReturn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/return.png"))); // NOI18N
        btnReturn.setText("RETURN");
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });

        btnLend.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/borrow.png"))); // NOI18N
        btnLend.setText("LEND");
        btnLend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLendActionPerformed(evt);
            }
        });

        btnAddNew.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAddNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/book.png"))); // NOI18N
        btnAddNew.setText("ADD NEW");
        btnAddNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewActionPerformed(evt);
            }
        });

        btnCatalogueSheet.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCatalogueSheet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/scripts.png"))); // NOI18N
        btnCatalogueSheet.setText("Generate Catalogue List");
        btnCatalogueSheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCatalogueSheetActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/updte.png"))); // NOI18N
        btnUpdate.setText("UPDATE");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnCatalogueSheet))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddNew, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(btnLend, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                .addComponent(btnReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1))
                            .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(btnLend, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(btnReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(btnAddNew, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67)
                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(86, 86, 86)
                .addComponent(btnCatalogueSheet)
                .addContainerGap(88, Short.MAX_VALUE))
        );

        btnLibBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLibBack.setText("BACK");
        btnLibBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLibBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 21, Short.MAX_VALUE)
                        .addComponent(jTabbedPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 1039, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnLibBack, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jbtnlogout))
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 653, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jbtnlogout, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(btnLibBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        libloggedinusercode.setText("currently logged in usercode");

        jLabel2.setText("Logged in as:");

        libloggedintime.setText("time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(libloggedinusercode)
                .addGap(48, 48, 48)
                .addComponent(libloggedintime, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(992, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(libloggedinusercode)
                    .addComponent(libloggedintime))
                .addGap(134, 134, 134))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 749, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnlogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnlogoutActionPerformed
        // TODO add your handling code here:
        try {                                           
            // TODO add your handling code here:
            try {
                String asql = "INSERT INTO `auditlog`(`empid`, `date`, `time`, `status`) VALUES (?,?,?,?)";
                pst = conn.prepareStatement(asql);
                Date currentDate = GregorianCalendar.getInstance().getTime();
                DateFormat df = DateFormat.getInstance();
                String dateString = df.format(currentDate);
                
                Date d = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String timeString = sdf.format(d);
                
                String time = timeString;
                String date = dateString;
                pst.setString(1, Emp.empId);
                pst.setString(2, date);
                pst.setString(3, time);
                pst.setString(4, "Logged out");
                pst.execute();
                
            } catch (SQLException e) {
            }
            
            this.dispose();
            Login l = new Login();
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jbtnlogoutActionPerformed

    private void jpnl_personal_infoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jpnl_personal_infoMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jpnl_personal_infoMouseClicked

    private void getNoOfCopies(){
//        try {
//            //Getting the total materials in the collection
//            String cpquery = "SELECT * FROM `lib_collection` WHERE `isbn_doc_no`=? ";
//            pst = conn.prepareStatement(cpquery);
//            pst.setString(1, libsearch.getText());
//            rs = pst.executeQuery();
//
//            while (rs.next()) {                        
//                int copies = rs.getInt("no_of_copies");
//                cp = copies;
//            }
//            //JOptionPane.showMessageDialog(null, cp);
//        } catch (NumberFormatException | SQLException e) {
//            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
//        }
    }
    
    private void updateCollectionAfterBorrowing(){
        try {
            //Updating material remainder after borrowing
            //String updatecoll = "UPDATE `lib_collection` SET `status`=? WHERE `isbn_doc_no`='"+libsearch.getText()+"' ";
            String updatecoll = "UPDATE `lib_collection` SET `status`=\"0\" WHERE `isbn_doc_no`='"+libsearch.getText()+"' ";
            pst = conn.prepareStatement(updatecoll);
            
//            int lendedcopies = Integer.parseInt(txtlendcopies.getText());
//
//            int rmngmat = cp - lendedcopies;
//
//            pst.setInt(1, rmngmat);

            pst.executeUpdate();
            
            //JOptionPane.showMessageDialog(null, "Hello. Collection updated. ");
            System.out.println("Collection updated.");
            
        } catch (SQLException e) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void getIfClassTr(){
        try {
//            String sql = "SELECT `t_class_tr`, `t_which_class` FROM `teachers` WHERE `t_trid`=\"ND002\" AND `status`=\"1\" ";
            String sql = "SELECT `t_class_tr`, `t_which_class` FROM `teachers` WHERE `t_trid`='"+txtlibcustodianid.getText()+"' AND `status`=\"1\" ";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                classtr = rs.getString("t_class_tr");
                trclass = rs.getString("t_which_class");
            }
            
        } catch (SQLException e) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }
    
    private void btnLendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLendActionPerformed
        // TODO add your handling code here:
               
        try {
            String ctype;
            String teachclass="";
            if (rb1.isSelected()) {
                ctype = rb1.getText();
            } else if(rb2.isSelected()){
                ctype = rb2.getText();
                getIfClassTr();

                if (classtr.equals("Yes")) {
                    teachclass = trclass;
                } else {
                    teachclass = null;
                }
            }else if(rb3.isSelected()){
                ctype = rb3.getText();
            }else{
                ctype = "";
            }
            String matid = txtlibmaterialid.getText();
            String mtitle = txtlibtitle.getText();
            String mauthor = txtlibauthor.getText();
            String mcat = txtlibcategory.getText();
            String msbjct = txtlibsbjct.getText();
            String mpub = txtlibpublisher.getText();
            String medtn = txtlibedition.getText();
            String myop = txtlibyop.getText();
            //int lendedcopies = Integer.parseInt(txtlendcopies.getText());
            String mcstdnid = txtlibcustodianid.getText();
            String mcstdndsgntn = txtlibcustodiandesig.getText();
            String mcstdbname = txtlibcustodianname.getText();
            String borrowdate = txtlibborrowdate.getText();
            String returndate = txtlibreturndate.getText();
            
            
            if (!matid.isEmpty() && !mtitle.isEmpty() && !mauthor.isEmpty() && !mcat.isEmpty() && !msbjct.isEmpty() 
                    && !mpub.isEmpty() && !medtn.isEmpty() && !myop.isEmpty() && !mcstdnid.isEmpty() && !mcstdbname.isEmpty() ) {
//                getNoOfCopies();
//                if (cp == 0 ) {
//                        JOptionPane.showMessageDialog(null, "Sorry. Material out of stock");
//                        clearBorrowFields();
//                } else {
                int x = JOptionPane.showConfirmDialog(null, "Lend material?", "Lend Material?", JOptionPane.YES_NO_OPTION);
                
                if (x == 0) {            
                    String sql = "INSERT INTO `library_borrow`(`custodian_type`, `material_id`, `title`, `author`, `category`, `subject`, `publisher`, "
                            + "`edition`, `yop`, `custodian_id`, `custodian_desig`, `custodian_name`, `borrow_date`, `return_date`,`teachingclass`) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                    
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, ctype);
                    pst.setString(2, matid);
                    pst.setString(3, mtitle);
                    pst.setString(4, mauthor);
                    pst.setString(5, mcat);
                    pst.setString(6, msbjct);
                    pst.setString(7, mpub);
                    pst.setString(8, medtn);
                    pst.setString(9, myop);
                    //pst.setInt(10, lendedcopies);
                    pst.setString(10, mcstdnid);
                    pst.setString(11, mcstdndsgntn);
                    pst.setString(12, mcstdbname.toUpperCase());
                    //pst.setInt(13, noofcopies);
                    pst.setString(13, borrowdate);
                    pst.setString(14, returndate);                    
                    pst.setString(15, teachclass);
                    
                    pst.execute();
                    
                    JOptionPane.showMessageDialog(null, "Material Issued.");
                    
                    //getNoOfCopies();
                    updateCollectionAfterBorrowing();
                    clearBorrowFields();
                    
                    DefaultTableModel model = (DefaultTableModel)libtblborrow.getModel();
                    model.setColumnCount(0);
                    model.setRowCount(0);
                    borrowBook();
                    
                  }
                //}
                
            }else{                
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            }
            
            DefaultTableModel collmodel = (DefaultTableModel)tblcatalogue.getModel();
            collmodel.setColumnCount(0);
            collmodel.setRowCount(0);
            Catalogue();
            
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Ops! Operation failed. It may be possible the custodian has the same material. ");
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }      
    }//GEN-LAST:event_btnLendActionPerformed

    int lendnoofbooks = 0;
    private void checkForAvailabilityOfBook(){
        
        try {
                String pbquery = "SELECT COUNT(`id`) AS id FROM `lib_collection` WHERE `isbn_doc_no`='"+libsearch.getText()+"' ";
                pst = conn.prepareStatement(pbquery);
                rs = pst.executeQuery();

                while (rs.next()) {
                    lendnoofbooks = Integer.parseInt(rs.getString("id"));
                }
                
                //JOptionPane.showMessageDialog(null, lendnoofbooks);
                
            } catch (NumberFormatException | SQLException e) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
            }
    }
    
    private void libsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_libsearchKeyReleased
        // TODO add your handling code here:
        checkForAvailabilityOfBook();
        
        if (libsearch.getText().isEmpty()) {
            clearBorrowFields();
            
        }else if(lendnoofbooks <= 0){
            
            JOptionPane.showMessageDialog(null, "Sorry. Material out of stock."); 
            
        }else {
            try {            
                String sql = "SELECT * FROM `lib_collection` WHERE `isbn_doc_no` = ? AND `status`=\"1\" ";
                pst = conn.prepareStatement(sql);
                pst.setString(1, libsearch.getText());
                rs = pst.executeQuery();

                if (rs.next()) {                
                    String materialid = rs.getString("isbn_doc_no");
                    txtlibmaterialid.setText(materialid);
                    String materialtitle = rs.getString("title");
                    txtlibtitle.setText(materialtitle);
                    String materialauthor = rs.getString("author");
                    txtlibauthor.setText(materialauthor);
                    String materialcat = rs.getString("genre");
                    txtlibcategory.setText(materialcat);
                    String materialsjct = rs.getString("subject");
                    txtlibsbjct.setText(materialsjct);
                    String materialpub = rs.getString("publisher");
                    txtlibpublisher.setText(materialpub);
                    String materialedtn = rs.getString("edition");
                    txtlibedition.setText(materialedtn);
                    String materialyop = rs.getString("date_published");
                    txtlibyop.setText(materialyop);
                    //txtavailendcopies.setText(rs.getString("no_of_copies"));
                }else{
                    JOptionPane.showMessageDialog(null, "Ops! Material not found");
                }

            } catch (SQLException e) {
                
                Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
    }//GEN-LAST:event_libsearchKeyReleased

    private void txtlibcustodianidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlibcustodianidKeyReleased
        // TODO add your handling code here:
        String search = txtlibcustodianid.getText();
        
        DefaultTableModel table = (DefaultTableModel) libtblborrow.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(table);
        libtblborrow.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(search));
            
            if (rb1.isSelected()) {
                try {
                    String sql = "SELECT `name` FROM `admissions` WHERE `admno` = ? AND `status` = \"1\" ";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, txtlibcustodianid.getText());
                    rs = pst.executeQuery();

                    while (rs.next()) {                
                        String pupilname = rs.getString("name");
                        txtlibcustodianname.setText(pupilname);
                        txtlibcustodiandesig.setText("Pupil");
                    }


                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, "Ops! Pupil not found");
                    Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
                }
                    
            } else if(rb2.isSelected()){
                try {
                        String sql = "SELECT `t_name`, `t_emp_as` FROM `teachers` WHERE `t_trid` = ? AND `status` = \"1\" ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, txtlibcustodianid.getText());
                        rs = pst.executeQuery();

                        while (rs.next()) {
                            String empas = rs.getString("t_emp_as");
                            txtlibcustodiandesig.setText(empas);
                            String trname = rs.getString("t_name");
                            txtlibcustodianname.setText(trname);
                        }


                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, "Ops! Teacher not found");
                    Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
                }
            }else if(rb3.isSelected()){
                try {
                        String sql = "SELECT `ename`, `e_employed_as` FROM `otherstaff` WHERE `e_work_id` = ? AND `status` = \"1\" ";
                        pst = conn.prepareStatement(sql);
                        pst.setString(1, txtlibcustodianid.getText());
                        rs = pst.executeQuery();

                        while (rs.next()) {  
                            String empas = rs.getString("e_employed_as");
                            txtlibcustodiandesig.setText(empas);
                            String empname = rs.getString("ename");
                            txtlibcustodianname.setText(empname);
                        }

                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, "Ops! Staff not found");
                    Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
                }
            }else{
                JOptionPane.showMessageDialog(null, "Please select the respective custodian");
            }
    }//GEN-LAST:event_txtlibcustodianidKeyReleased

    private void rb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rb1ActionPerformed
        // TODO add your handling code here:
        if (rb1.isSelected()) {
            rb2.setSelected(false);
            rb3.setSelected(false);
            txtlibcustodianid.setEditable(true);
            txtlibcustodianid.setText("");
            txtlibcustodiandesig.setText("");
            txtlibcustodianname.setText("");
        }
    }//GEN-LAST:event_rb1ActionPerformed

    private void rb2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rb2ActionPerformed
        // TODO add your handling code here:
        if (rb2.isSelected()) {
            rb1.setSelected(false);
            rb3.setSelected(false);
            txtlibcustodianid.setEditable(true);
            txtlibcustodianid.setText("");
            txtlibcustodiandesig.setText("");
            txtlibcustodianname.setText("");
        }
    }//GEN-LAST:event_rb2ActionPerformed

    private void rb3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rb3ActionPerformed
        // TODO add your handling code here:
        if (rb3.isSelected()) {
            rb2.setSelected(false);
            rb1.setSelected(false);
            txtlibcustodianid.setEditable(true);
            txtlibcustodianid.setText("");
            txtlibcustodiandesig.setText("");
            txtlibcustodianname.setText("");
        }
    }//GEN-LAST:event_rb3ActionPerformed

    private void txtlibcustodianidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlibcustodianidActionPerformed
        // TODO add your handling code here:        
    }//GEN-LAST:event_txtlibcustodianidActionPerformed

    private void txtlibcustodianidKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlibcustodianidKeyPressed
        // TODO add your handling code here:        
    }//GEN-LAST:event_txtlibcustodianidKeyPressed

    private void txtlibcustodianidMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtlibcustodianidMouseClicked
        // TODO add your handling code here:
        if (rb1.isSelected() == false && rb2.isSelected() == false && rb3.isSelected() == false ) {
            JOptionPane.showMessageDialog(null, "Please select custodian category");
        }
    }//GEN-LAST:event_txtlibcustodianidMouseClicked

    private void btnAddNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewActionPerformed
        // TODO add your handling code here:
        try {
            String genre = (String) cbogenre.getSelectedItem();
            String misbn = txtisbn.getText();
            String mtitle = txttitle.getText();
            String mauthor = txtauthor.getText();
            String mdop = txtdp.getText();
            String medtn = txtedtn.getText();
            String mdaterec = txtdateadded.getText();
            String mpub = txtpub.getText();
            String mcprt = txtcprt.getText();
            String msbjct = txtsbjct.getText();
            String mcndtn = txtcndtn.getText();
            String status = "1";
                        
            if (!misbn.isEmpty() && !mtitle.isEmpty() && !mauthor.isEmpty() && !mpub.isEmpty() && !medtn.isEmpty() && !mdaterec.isEmpty() &&
                !mcprt.isEmpty() && !msbjct.isEmpty() && !mdop.isEmpty() && !mcndtn.isEmpty() && cbogenre.getSelectedIndex() != 0 ) {
                
                
                    String sql = "INSERT INTO `lib_collection`(`genre`, `isbn_doc_no`, `title`, `author`, `date_published`, `edition`,"
                        + " `date_recorded`, `publisher`, `copyright_year`, `subject`, `material_condition`, `status` ) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";
                
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, genre);
                    pst.setString(2, misbn);
                    pst.setString(3, mtitle);
                    pst.setString(4, mauthor);
                    pst.setString(5, mdop);
                    pst.setString(6, medtn);
                    pst.setString(7, mdaterec);
                    pst.setString(8, mpub);
                    pst.setString(9, mcprt);
                    pst.setString(10, msbjct);
                    pst.setString(11, mcndtn);
                    pst.setString(12, status);

                    pst.execute();              
                
                JOptionPane.showMessageDialog(null, "Success! Your material has been added.");

                clearAddNewFields();
                
                DefaultTableModel model = (DefaultTableModel)tblcatalogue.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                Catalogue();
            
            }else{                
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            }
            
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Ops! An error occured while trying to add the document. Please check the details provided. ");
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }//GEN-LAST:event_btnAddNewActionPerformed

    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        // TODO add your handling code here:
        try {
            String mid = txtmid.getText();
            String mtitle = txtrtitle.getText();
            String mcat = txtcat.getText();
            String mpub = txtrpub.getText();
            String medtn = txtlibedition.getText();
            String mcstdnid = txtcstdnid.getText();
            String mcstdn = txtcstdnname.getText();
            String borrowdate = txtbdate.getText();
            String returndate = txtrdate.getText();
            //int returnedcopies = Integer.parseInt(txtreturncopies.getText());
            
            
            if (!txtreturnsearch.getText().isEmpty() && !mid.isEmpty() && !mcstdn.isEmpty() && !mcstdnid.isEmpty()) {
                
                String sql = "INSERT INTO `library_return`(`custodian_id`, `custodian_name`, `borrow_date`, `return_date`, "
                        + "`material_id`, `title`, `category`, `publisher`, `edition` )"
                        + " VALUES (?,?,?,?,?,?,?,?,?)";
                pst = conn.prepareStatement(sql);
                pst.setString(1, mcstdnid);
                pst.setString(2, mcstdn.toUpperCase());
                pst.setString(3, borrowdate);
                pst.setString(4, returndate);
                pst.setString(5, mid);
                pst.setString(6, mtitle);
                pst.setString(7, mcat);
                pst.setString(8, mpub);
                pst.setString(9, medtn);
                //pst.setInt(10, returnedcopies);
                
                pst.execute();
                
                System.out.println("Success");
                
                deleteFromBorrowAfterReturn();
                
                DefaultTableModel model = (DefaultTableModel)tblreturn.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                returnBook();
                
            }else{                
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            }
            
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Ops! An error occured.");
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }
        
        
        
    }//GEN-LAST:event_btnReturnActionPerformed

    private void deleteFromBorrowAfterReturn(){
        try {
            String delquery = "DELETE FROM `library_borrow` WHERE `material_id`=? ";
            pst = conn.prepareStatement(delquery);
            pst.setString(1, txtmid.getText());
            pst.execute();
            
            JOptionPane.showMessageDialog(null, "Material Returned");
            
            //getNoOfCopies();
            updateCollectionAfterReturn();
            
            clearReturnFields();
            
            DefaultTableModel model = (DefaultTableModel)tblcatalogue.getModel();
            model.setColumnCount(0);
            model.setRowCount(0);
            Catalogue();
            
        } catch (SQLException e) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void updateCollectionAfterReturn(){
        try {
            //Updating material remainder after borrowing
            //String updatecoll = "UPDATE `lib_collection` SET `no_of_copies`=? WHERE `isbn_doc_no`='"+txtmid.getText()+"' ";
            String updatecoll = "UPDATE `lib_collection` SET `status`=\"1\" WHERE `isbn_doc_no`='"+txtmid.getText()+"' ";
            pst = conn.prepareStatement(updatecoll);
            
//            int returnedcopies = Integer.parseInt(txtreturncopies.getText());
//
//            int rmngmat = cp + returnedcopies;
//
//            pst.setInt(1, rmngmat);

            pst.executeUpdate();
            
            //JOptionPane.showMessageDialog(null, "Hello. Collection updated. ");
            System.out.println("Collection updated.");
            
        } catch (SQLException e) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void txtcsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcsearchKeyReleased
        // TODO add your handling code here:
        String searchcatalogue = txtcsearch.getText();
        
        DefaultTableModel table = (DefaultTableModel) tblcatalogue.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(table);
        tblcatalogue.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(searchcatalogue));
    }//GEN-LAST:event_txtcsearchKeyReleased

    private void txtsmSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsmSendActionPerformed
        // TODO add your handling code here:
//        if (rb1.isSelected()) {
//            sendSMS();
//        }else if(rb2.isSelected()){
//            try {
//                sendEmail();
//            } catch (MessagingException ex) {
//                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }else{
//            JOptionPane.showMessageDialog(null, "Please select message type");
//        }

        sendSMS();

    }//GEN-LAST:event_txtsmSendActionPerformed

     private void sendSMS(){
        String bulksmsusername = System.getenv("BULKSMSUSRNAME");
        String bulksmspssd = System.getenv("BULKSMSPSSD");
        String msg = txtsm6.getText();
        String phoneno = txtsm4.getText();
//        if (rb1.isSelected()) {
//            txtsm3.setEditable(true);
//            
//            String msgtype = "";
//            if (rb1.isSelected()) {
//                msgtype = rb1.getText();
//            }
            String padmno = txtsm3.getText();
            String mrecepient = txtsm4.getText();
            String mssg = txtsm6.getText();

            if (!padmno.isEmpty() && !mrecepient.isEmpty() && !mssg.isEmpty()) {
            
                getSMSContact();
                SMS send = new SMS();
                send.SendSMS(bulksmsusername, bulksmspssd, msg, phoneno, "https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0");

                JOptionPane.showMessageDialog(null, "Message sent");
                
                
                try {
                    String mfrom = "Library";
                
                    String sql = "INSERT INTO `messages`(`padmno`, `m_from`, `m_to`, `m_msg`) VALUES (?,?,?)";
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, padmno);
                    pst.setString(2, mfrom);
                    pst.setString(3, mrecepient);
                    pst.setString(4, mssg);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Message saved.");

                    //rb1.setSelected(false);
                    
                    messageList();
                
                } catch (SQLException e) {
                }
            
            }else{
                    JOptionPane.showMessageDialog(null, "Please ensure all the fields are filled.");
            }           
            
            
//        } else if(rb2.isSelected() && cbosm2.getSelectedIndex() == 3) {
//                txtsm3.setEditable(false);
//                txtsm4.setEditable(false);
//                txtsm8.setEditable(true);
//            
//            
//        }else{
//            JOptionPane.showMessageDialog(null, "Please select the type of message to send.");
//        }
    }
     
     private void getSMSContact(){
        String msgadmno = txtsm3.getText();
            
            try {
            String sql = "SELECT `ecpc_phone` FROM `admissions` WHERE `admno`=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, msgadmno);
            rs = pst.executeQuery();
            
            while (rs.next()) {                
                String contact = rs.getString("ecpc_phone");
                txtsm4.setText(contact);
            }
            
            } catch (SQLException e) {
                Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
            }
    }
    
    private void txtsm3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm3KeyReleased
        // TODO add your handling code here:
        if (txtsm3.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtsm3.setText("");
        }else{
           //getSMSContact(); 
        }
        
    }//GEN-LAST:event_txtsm3KeyReleased

    private String getCellValue(int x, int y){
        return catalogue.getValueAt(x, y).toString(); //x rep the row index, y rep the column index
    }
    
    private void btnCatalogueSheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCatalogueSheetActionPerformed
        // TODO add your handling code here:
        
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet ws = wb.createSheet();
        
        //Load data to tree map
        TreeMap<String, Object[]> data = new TreeMap<>();
        
        //Add column headers
        data.put("-1", new Object[]{catalogue.getColumnName(0),catalogue.getColumnName(1),catalogue.getColumnName(2), catalogue.getColumnName(3),
            catalogue.getColumnName(4),catalogue.getColumnName(5),catalogue.getColumnName(6), catalogue.getColumnName(7), catalogue.getColumnName(8) });
        
        //Looping through the table rows
        for (int i = 0; i < catalogue.getRowCount(); i++) {
            data.put(Integer.toString(i), new Object[]{getCellValue(i, 0),getCellValue(i, 1),getCellValue(i, 2),getCellValue(i, 3),getCellValue(i, 4),
            getCellValue(i, 5),getCellValue(i, 6),getCellValue(i, 7),getCellValue(i, 8) });
        }
        
        //Writing to Excel
        Set<String> ids = data.keySet();
        XSSFRow row;
        int rowID = 0;
        
        for (String key : ids) {
            row = ws.createRow(rowID++);
            
            //Get Data as per Key
            Object[] values = data.get(key);
            
            int cellID = 0;
            for (Object o : values) {
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(o.toString());
            }
        }
        
        //Writing the excel sheet to file system
        try {
            try (FileOutputStream fos = new FileOutputStream(new File("G:\\PURE ACADEMICS\\SOFTWARE ENGINEERING\\SCIT\\YEAR FOUR\\SEM 01\\RESEARCH PROJECT PROPOSAL\\PROJECT\\Library catalogue.xlsx"))) {
                wb.write(fos);
                JOptionPane.showMessageDialog(null, "Success");
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }catch (IOException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }//GEN-LAST:event_btnCatalogueSheetActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        try {
            String genre = (String) cboupgenre.getSelectedItem();
            String misbn = txtupisbn.getText();
            String mtitle = txtuptitle.getText();
            String mauthor = txtupauthor.getText();
            String myp = txtupyp.getText();
            String medtn = txtupedtn.getText();
            String mdaterec = txtupdr.getText();
            String mpub = txtuppub.getText();
            String mcprt = txtupcprt.getText();
            //Integer mnocp = Integer.parseInt(txtupcopies.getText());
            String msbjct = txtupsbjct.getText();
            String mcndtn = txtupcndtn.getText();
                        
            if (!misbn.isEmpty() && !mtitle.isEmpty() && !mauthor.isEmpty() && !mpub.isEmpty() && !medtn.isEmpty() && !mdaterec.isEmpty() &&
                !mcprt.isEmpty() && !msbjct.isEmpty() && !myp.isEmpty() && !mcndtn.isEmpty() && cboupgenre.getSelectedIndex() != 0 ) {
                
                
                    String sql = "UPDATE `lib_collection` SET `genre`=?,`isbn_doc_no`=?,`title`=?,`author`=?,`date_published`=?,`edition`=?,"
                            + "`date_recorded`=?,`publisher`=?,`copyright_year`=?,`subject`=?,`material_condition`=? WHERE `isbn_doc_no`='"+txtupisbn.getText().toUpperCase()+"' ";
                
                    pst = conn.prepareStatement(sql);
                    pst.setString(1, genre);
                    pst.setString(2, misbn);
                    pst.setString(3, mtitle);
                    pst.setString(4, mauthor);
                    pst.setString(5, myp);
                    pst.setString(6, medtn);
                    pst.setString(7, mdaterec);
                    pst.setString(8, mpub);
                    pst.setString(9, mcprt);
                    //pst.setInt(10, mnocp);
                    pst.setString(10, msbjct);
                    pst.setString(11, mcndtn);

                    pst.execute();

                //}               
                
                JOptionPane.showMessageDialog(null, "Material Details Updated");

                clearUpdateFields();
                
                DefaultTableModel model = (DefaultTableModel)tblcatalogue.getModel();
                model.setColumnCount(0);
                model.setRowCount(0);
                Catalogue();
            
            }else{                
                JOptionPane.showMessageDialog(null, "Please ensure all the relevant fields are filled as required.");
            }
            
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Ops! An error occured while trying to add the document. Please check the details provided. ");
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void txtreturnsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtreturnsearchKeyReleased
        // TODO add your handling code here:
        if (txtreturnsearch.getText().toUpperCase().isEmpty()) {
            clearReturnFields();
            //JOptionPane.showMessageDialog(null, "Please enter the ID to search. ");
        } else {
            try {
                String sql = "SELECT * FROM `library_borrow` WHERE `custodian_id`=? ";

                pst = conn.prepareStatement(sql);
                pst.setString(1, txtreturnsearch.getText());
                rs = pst.executeQuery();

                while (rs.next()) {  
                    txtcstdnid.setText(rs.getString("custodian_id"));
                    txtcstdnname.setText(rs.getString("custodian_name"));
                    txtbdate.setText(rs.getString("borrow_date"));
                    txtrdate.setText(rs.getString("return_date"));
                    
                }

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Sorry. Person not found");
                Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }//GEN-LAST:event_txtreturnsearchKeyReleased

    private void txtcataloguetypeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcataloguetypeKeyReleased
        // TODO add your handling code here:
        String searchcataloguetype = txtcataloguetype.getText();
        
        DefaultTableModel table = (DefaultTableModel) tblcatalogue.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(table);
        tblcatalogue.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(searchcataloguetype));
    }//GEN-LAST:event_txtcataloguetypeKeyReleased

    private void btnLibChangePssdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLibChangePssdActionPerformed
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            Change_Password cp = new Change_Password();
            cp.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Teacher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnLibChangePssdActionPerformed

    private void txttitleKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttitleKeyReleased
        // TODO add your handling code here:
        if (txttitle.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txttitle.setText("");
        }else if(txttitle.getText().length() > 30){
            JOptionPane.showMessageDialog(null, "Material title too long!");
            txttitle.setText("");
        }
    }//GEN-LAST:event_txttitleKeyReleased

    private void txtauthorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtauthorKeyReleased
        // TODO add your handling code here:
        if (txtauthor.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtauthor.setText("");
        }else if(txtauthor.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Author name too long!");
            txtauthor.setText("");
        }
    }//GEN-LAST:event_txtauthorKeyReleased

    private void txtdpKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdpKeyReleased
        // TODO add your handling code here:
          if (!txtdp.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                txtdp.setText("");
            }else{
              int lenyr = Integer.parseInt(txtdp.getText());
              if (lenyr > yr || lenyr <= 0) {
                  JOptionPane.showMessageDialog(null, "Wrong year!");
                  txtdp.setText("");
              } else {
                  System.out.println("All good");
              }
          }
        
    }//GEN-LAST:event_txtdpKeyReleased

    private void txtpubKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpubKeyReleased
        // TODO add your handling code here:
        if (txtpub.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
            txtpub.setText("");
        }else if(txtpub.getText().length() > 30){
            JOptionPane.showMessageDialog(null, "Author name too long!");
            txtpub.setText("");
        }
    }//GEN-LAST:event_txtpubKeyReleased

    private void txtcprtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcprtKeyReleased
        // TODO add your handling code here:
        int lenyr = Integer.parseInt(txtcprt.getText());
        try {
            if (!txtcprt.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                txtcprt.setText("");
            }else{
                if (lenyr > yr ) {
                  JOptionPane.showMessageDialog(null, "Wrong year!");
                  txtcprt.setText("");
              } else {
                  System.out.println("All good");
              }
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtcprt.setText("");
        }
        
    }//GEN-LAST:event_txtcprtKeyReleased

    private void txtsbjctKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsbjctKeyReleased
        // TODO add your handling code here:
        if (txtsbjct.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtsbjct.setText("");
        }else if(txtsbjct.getText().length() > 30){
            JOptionPane.showMessageDialog(null, "Subject name too long!");
            txtsbjct.setText("");
        }
    }//GEN-LAST:event_txtsbjctKeyReleased

    private void txtcndtnKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcndtnKeyReleased
        // TODO add your handling code here:
        if (txtcndtn.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtcndtn.setText("");
        }
    }//GEN-LAST:event_txtcndtnKeyReleased

    private void txtsm4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm4KeyReleased
        // TODO add your handling code here:
        if (!txtsm4.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtsm4.setText("");
        }
    }//GEN-LAST:event_txtsm4KeyReleased

    private void txtdpKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdpKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdpKeyTyped

    private void txtsm5searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsm5searchKeyReleased
        // TODO add your handling code here:
        try {
            String msgadmno = txtsm5search.getText();

            String sql = "SELECT * FROM `teachers` WHERE `t_trid`=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, msgadmno);
            rs = pst.executeQuery();

            while (rs.next()) {                
                String contact = rs.getString("t_phone");
                txtsm4.setText(contact);
                txtsm3.setText(rs.getString("t_name"));
            }

        } catch (SQLException e) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, e);
        }
    }//GEN-LAST:event_txtsm5searchKeyReleased

    private void btnsmCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsmCancelActionPerformed
        // TODO add your handling code here:
        txtsm3.setText("");
        txtsm4.setText("");
        txtsm6.setText("");
    }//GEN-LAST:event_btnsmCancelActionPerformed

    private void btnLibBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLibBackActionPerformed
        // TODO add your handling code here:
        String admin = libloggedinusercode.getText();
        
        if (admin.contains("ADM")) {
            this.dispose();
            try {
                Admin ad = new Admin();
                ad.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Library lib = new Library();
                lib.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnLibBackActionPerformed

    private void txtedtnKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtedtnKeyReleased
        // TODO add your handling code here:
//        if (txtauthor.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
//            JOptionPane.showMessageDialog(null, "Only alphabets are allowed!");
//            txtauthor.setText("");
//        }
//        int idlen = Integer.parseInt(txtpplang.getText());
//            if (idlen > 100) {
//                JOptionPane.showMessageDialog(null, "Value cannot be greater than 100!");
//                txtpplang.setText("0");
//            }else{
//                System.out.println("All good!");
//            }
        if(txtedtn.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Author name too long!");
            txtedtn.setText("");
        }
    }//GEN-LAST:event_txtedtnKeyReleased

    private void cboupgenreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboupgenreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboupgenreActionPerformed

    private void txtupcndtnKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupcndtnKeyReleased
        // TODO add your handling code here:
        if (txtupcndtn.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtupcndtn.setText("");
        }
    }//GEN-LAST:event_txtupcndtnKeyReleased

    private void txtupsbjctKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupsbjctKeyReleased
        // TODO add your handling code here:
        if (txtupsbjct.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtupsbjct.setText("");
        }else if(txtupsbjct.getText().length() > 30){
            JOptionPane.showMessageDialog(null, "Subject too long!");
            txtupsbjct.setText("");
        }
    }//GEN-LAST:event_txtupsbjctKeyReleased

    private void txtupauthorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupauthorKeyReleased
        // TODO add your handling code here:
        if (txtupauthor.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtupauthor.setText("");
        }else if(txtupauthor.getText().length() > 25){
            JOptionPane.showMessageDialog(null, "Name too long!");
            txtupauthor.setText("");
        }
    }//GEN-LAST:event_txtupauthorKeyReleased

    private void txtuppubKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtuppubKeyReleased
        // TODO add your handling code here:
        if (!txtuppub.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtuppub.setText("");
        }else if(txtuppub.getText().length() > 30){
            JOptionPane.showMessageDialog(null, "Publisher name too long!");
            txtuppub.setText("");
        }
    }//GEN-LAST:event_txtuppubKeyReleased

    private void txtuptitleKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtuptitleKeyReleased
        // TODO add your handling code here:
        if (txtuptitle.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
            JOptionPane.showMessageDialog(null, "Only characters are allowed!");
            txtuptitle.setText("");
        }else if(txtuptitle.getText().length() > 30){
            JOptionPane.showMessageDialog(null, "Title too long!");
            txtuptitle.setText("");
        }
    }//GEN-LAST:event_txtuptitleKeyReleased

    private void txtupcprtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupcprtKeyReleased
        // TODO add your handling code here:
        int lenyr = Integer.parseInt(txtdp.getText());
        try {
            if (!txtupcprt.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
                JOptionPane.showMessageDialog(null, "Only characters are allowed!");
                txtupcprt.setText("");
            }else{
                if (lenyr > yr) {
                    JOptionPane.showMessageDialog(null, "Wrong year!");
                    txtupcprt.setText("");
                } else {
                    System.out.println("All good");
                }
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupcprt.setText("");
        }
    }//GEN-LAST:event_txtupcprtKeyReleased

    private void txtupypKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupypKeyReleased
        // TODO add your handling code here:
        int lenyr = Integer.parseInt(txtdp.getText());
        try {
            if (!txtupyp.getText().matches("[0-9]+(.){0,1}[0-9]*")) {
                JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
                txtupyp.setText("");
            }else{
                if (lenyr > yr || lenyr < 1000) {
                    JOptionPane.showMessageDialog(null, "Wrong year!");
                    txtupyp.setText("");
                } else {
                    System.out.println("All good");
                }
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Only numerics are allowed!");
            txtupyp.setText("");
        }
    }//GEN-LAST:event_txtupypKeyReleased

    private void txtupsearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupsearchKeyReleased
        // TODO add your handling code here:
        if (txtupsearch.getText().toUpperCase().isEmpty()) {
            clearReturnFields();
            //JOptionPane.showMessageDialog(null, "Please enter the ID to search. ");
        } else {
            try {
                String sql = "SELECT `genre`, `isbn_doc_no`, `title`, `author`, `date_published`, `edition`, `date_recorded`, `publisher`,"
                + " `copyright_year`, `no_of_copies`, `subject`, `material_condition` FROM `lib_collection` WHERE `isbn_doc_no`=? ";

                pst = conn.prepareStatement(sql);
                pst.setString(1, txtupsearch.getText().toUpperCase());
                rs = pst.executeQuery();

                while (rs.next()) {
                    String gender = rs.getString("genre");
                    switch(gender){
                        case "Book":
                        cboupgenre.setSelectedIndex(1);
                        break;
                        case "Magazine":
                        cboupgenre.setSelectedIndex(2);
                        break;
                        case "Papers":
                        cboupgenre.setSelectedIndex(2);
                        break;
                    }
                    String misbn = rs.getString("isbn_doc_no");
                    txtupisbn.setText(misbn);
                    String mtitle = rs.getString("title");
                    txtuptitle.setText(mtitle);
                    String mauthor = rs.getString("author");
                    txtupauthor.setText(mauthor);
                    String myp = rs.getString("date_published");
                    txtupyp.setText(myp);
                    String medtn = rs.getString("edition");
                    txtupedtn.setText(medtn);
                    String mdr = rs.getString("date_recorded");
                    txtupdr.setText(mdr);
                    String mpub = rs.getString("publisher");
                    txtuppub.setText(mpub);
                    String mcprt = rs.getString("copyright_year");
                    txtupcprt.setText(mcprt);
                    //                    String mcps = rs.getString("no_of_copies");
                    //                    txtupcopies.setText(mcps);
                    String msbjct = rs.getString("subject");
                    txtupsbjct.setText(msbjct);
                    String mcdtn = rs.getString("material_condition");
                    txtupcndtn.setText(mcdtn);

                }

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Sorry. Requested materail not found. ");
                Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }//GEN-LAST:event_txtupsearchKeyReleased

    private void txtsearchmatidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtsearchmatidKeyReleased
        // TODO add your handling code here:
        if (txtsearchmatid.getText().toUpperCase().isEmpty()) {
            clearReturnFields();
            //JOptionPane.showMessageDialog(null, "Please enter the ID to search. ");
        } else {
            try {
                String sql = "SELECT * FROM `library_borrow` WHERE `material_id`=? AND `custodian_id`=? ";

                pst = conn.prepareStatement(sql);
                pst.setString(1, txtsearchmatid.getText());
                pst.setString(2, txtcstdnid.getText());
                rs = pst.executeQuery();

                while (rs.next()) {  
                    txtmid.setText(rs.getString("material_id"));
                    txtrtitle.setText(rs.getString("title"));
                    txtcat.setText(rs.getString("category"));
                    txtrpub.setText(rs.getString("publisher"));
                    txtredtn.setText(rs.getString("edition"));                    
                }

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Sorry. Person not found.");
                Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
    }//GEN-LAST:event_txtsearchmatidKeyReleased

    private void txtisbnKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtisbnKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtisbnKeyReleased

    private void txtupedtnKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupedtnKeyReleased
        // TODO add your handling code here:
        if(txtupedtn.getText().length() > 10){
            JOptionPane.showMessageDialog(null, "Edition too long!");
            txtupedtn.setText("");
        }
    }//GEN-LAST:event_txtupedtnKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Library().setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNew;
    private javax.swing.JButton btnCatalogueSheet;
    private javax.swing.JButton btnLend;
    private javax.swing.JButton btnLibBack;
    private javax.swing.JButton btnLibChangePssd;
    private javax.swing.JButton btnReturn;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnsmCancel;
    private javax.swing.JComboBox<String> cbogenre;
    private javax.swing.JComboBox<String> cboupgenre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane7;
    private javax.swing.JButton jbtnlogout;
    private javax.swing.JPanel jpnl_personal_info;
    private javax.swing.JLabel lblmsg;
    private javax.swing.JLabel lblmsg1;
    private javax.swing.JLabel libloggedintime;
    private javax.swing.JLabel libloggedinusercode;
    private javax.swing.JTextField libsearch;
    private javax.swing.JTable libtblborrow;
    private javax.swing.JRadioButton rb1;
    private javax.swing.JRadioButton rb2;
    private javax.swing.JRadioButton rb3;
    private javax.swing.JTable tblcatalogue;
    private javax.swing.JTable tblmsglist;
    private javax.swing.JTable tblreturn;
    private javax.swing.JTextField txtauthor;
    private javax.swing.JTextField txtbdate;
    private javax.swing.JTextField txtcat;
    private javax.swing.JTextField txtcataloguetype;
    private javax.swing.JTextArea txtcndtn;
    private javax.swing.JTextField txtcprt;
    private javax.swing.JTextField txtcsearch;
    private javax.swing.JTextField txtcstdnid;
    private javax.swing.JTextField txtcstdnname;
    private javax.swing.JTextField txtdateadded;
    private javax.swing.JTextField txtdp;
    private javax.swing.JTextField txtedtn;
    private javax.swing.JTextField txtisbn;
    private javax.swing.JTextField txtlibauthor;
    private javax.swing.JTextField txtlibborrowdate;
    private javax.swing.JTextField txtlibcategory;
    private javax.swing.JTextField txtlibcustodiandesig;
    private javax.swing.JTextField txtlibcustodianid;
    private javax.swing.JTextField txtlibcustodianname;
    private javax.swing.JTextField txtlibedition;
    private javax.swing.JTextField txtlibmaterialid;
    private javax.swing.JTextField txtlibpublisher;
    private javax.swing.JTextField txtlibreturndate;
    private javax.swing.JTextField txtlibsbjct;
    private javax.swing.JTextField txtlibtitle;
    private javax.swing.JTextField txtlibyop;
    private javax.swing.JTextField txtmid;
    private javax.swing.JTextField txtpub;
    private javax.swing.JTextField txtrdate;
    private javax.swing.JTextField txtredtn;
    private javax.swing.JTextField txtreturnsearch;
    private javax.swing.JTextField txtrpub;
    private javax.swing.JTextField txtrtitle;
    private javax.swing.JTextField txtsbjct;
    private javax.swing.JTextField txtsearchmatid;
    private javax.swing.JTextField txtsm3;
    private javax.swing.JTextField txtsm4;
    private javax.swing.JTextField txtsm5search;
    private javax.swing.JTextArea txtsm6;
    private javax.swing.JButton txtsmSend;
    private javax.swing.JTextField txttitle;
    private javax.swing.JTextField txtupauthor;
    private javax.swing.JTextArea txtupcndtn;
    private javax.swing.JTextField txtupcprt;
    private javax.swing.JTextField txtupdr;
    private javax.swing.JTextField txtupedtn;
    private javax.swing.JTextField txtupisbn;
    private javax.swing.JTextField txtuppub;
    private javax.swing.JTextField txtupsbjct;
    private javax.swing.JTextField txtupsearch;
    private javax.swing.JTextField txtuptitle;
    private javax.swing.JTextField txtupyp;
    // End of variables declaration//GEN-END:variables
}
